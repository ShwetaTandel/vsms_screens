<?php

session_start();
include ('../config/phpConfig.php');
$siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
$emparray = array();
$sql = "select * from " . $mDbName . ".outstandingactions where actionSiteId in (" . $siteIds . ") and actionStatus in ('Assigned', 'Re-assigned', 'Re-opened', 'Re-scheduled');";

$mainResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainResult)) {
    $curr = $row;
    $actionId = $row['actionId'];
    $rescheduledCnt = 0;
    $rescheduledCntQuery = "select count(*) as rescheduledCnt from " . $mDbName . ".actions, " . $mDbName . ".action_history where actions.id =action_history.action_number and action_history.status = 'RE-SCHEDULED' and action_number=" . $actionId;
    $rescheduledCntData = mysqli_query($connection, $rescheduledCntQuery);
    while ($mInnerRow = mysqli_fetch_assoc($rescheduledCntData)) {
        $rescheduledCnt = $mInnerRow['rescheduledCnt'];
    }
    $curr['rescheduledCnt'] = $rescheduledCnt;
    if ($rescheduledCnt === '0') {
        $curr['rescheduled'] = 'No';
    } else {
        $curr['rescheduled'] = 'Yes';
    }
    $date_now = new DateTime();
    $date2 = new DateTime($curr['estimated_completion_date']);
    if ($date_now > $date2) {
        $curr['isOverdue'] = 'Yes';
    } else {
        $curr['isOverdue'] = 'No';
    }
    array_walk_recursive($curr, function (&$item) {
        $item = mb_convert_encoding($item, "UTF-8");
    });
    $emparray[] = $curr;
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
?>