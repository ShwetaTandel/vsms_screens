<?php

session_start();
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';


$filter = $_GET['filter'];
if ($filter === 'OUTSTANDINGACTIONS') {
    $ownerId = $_SESSION['vsmsUserData']['id'];
    $sql = "SELECT * FROM " . $mDbName . ".outstandingactions where action_owner =  " . $ownerId . " and actionStatus in ('ASSIGNED' , 'RE-ASSIGNED','RE-OPENED','RE-SCHEDULED') ORDER BY estimated_completion_date asc;";
} else if ($filter === 'ACTIONDETAILS') {
    $rowID = $_GET['rowID'];
    $sql = "select  * from actions where id = " . $rowID;
} else if ($filter === 'ACTIONDATA') {
    $rowID = $_GET['rowID'];
    $sql = "select actions.* , actionsite.code as actionsite,reportsite.code as reportsite , incident.incident_number as reportId,  incident_date as reportDate, incident_status.description as reportStatus, location, location_other ,deadline, assigned_by, action_status.description as actionStatus, estimated_completion_date, concat(first_name, ' ', last_name) as actionOwner, manager_name, assigned_at from " . $mDbName . ".actions, " . $mDbName . ".incident, " . $mDbName . ".users, " . $mDbName . ".site actionsite, " . $mDbName . ".site reportsite, " . $mDbName . ".incident_status, " . $mDbName . ".action_status where incident.incident_number = actions.report_id and action_source = 'Incident' and actions.action_owner = users.id and incident.site_id = reportsite.id and actions.action_site = actionsite.id and incident.status = incident_status.status and actions.status= action_status.status   and  actions.id = " . $rowID;
}else if ($filter === 'ACTIONDATAHAZARD') {
    $rowID = $_GET['rowID'];
    $sql = "select actions.* , actionsite.code as actionsite,reportsite.code as reportsite , hazard.hazard_number as reportId,  hazard_date as reportDate, hazard_status.description as reportStatus, location, location_other ,deadline, assigned_by, action_status.description as actionStatus, estimated_completion_date, concat(first_name, ' ', last_name) as actionOwner, manager_name, assigned_at from " . $mDbName . ".actions, " . $mDbName . ".hazard, " . $mDbName . ".users, " . $mDbName . ".site actionsite, " . $mDbName . ".site reportsite, " . $mDbName . ".hazard_status, " . $mDbName . ".action_status where hazard.hazard_number = actions.report_id and action_source = 'Hazard' and actions.action_owner = users.id and hazard.site_id = reportsite.id and actions.action_site = actionsite.id and hazard.status = hazard_status.status and actions.status= action_status.status   and  actions.id = " . $rowID;
} else if ($filter === 'ACTIONDATAOTHER') {
    $rowID = $_GET['rowID'];
    $sql = "select actions.* , code as actionsite,  deadline, assigned_by, action_status.description as actionStatus, estimated_completion_date, concat(first_name, ' ', last_name) as actionOwner, manager_name, assigned_at from " . $mDbName . ".actions, " . $mDbName . ".users, " . $mDbName . ".site ," . $mDbName . ".action_status where  actions.action_owner = users.id and  actions.action_site = site.id and  actions.status= action_status.status   and  actions.id = " . $rowID;
}else if ($filter === 'ACTIONDATAGRA') {
    $rowID = $_GET['rowID'];
    $sql = "select actions.* , actionsite.code as actionsite,reportsite.code as reportsite , risk_assessment.gra_id as reportId,  gra_date as reportDate, gra_status.description as reportStatus, deadline, assigned_by, action_status.description as actionStatus, estimated_completion_date, concat(first_name, ' ', last_name) as actionOwner, manager_name, assigned_at from " . $mDbName . ".actions, " . $mDbName . ".risk_assessment, " . $mDbName . ".users, " . $mDbName . ".site actionsite, " . $mDbName . ".site reportsite, " . $mDbName . ".gra_status, " . $mDbName . ".action_status where risk_assessment.gra_id = actions.report_id and action_source = 'Risk Assessment' and actions.action_owner = users.id and risk_assessment.site_id = reportsite.id and actions.action_site = actionsite.id and risk_assessment.status = gra_status.status and actions.status= action_status.status   and  actions.id = " . $rowID;
} else if ($filter === 'SITEINCIDENTS') {
    $siteId = $_GET['siteId'];
    $sql = "select incident.incident_number as reportId from " . $mDbName . ".incident, " . $mDbName . ".site, " . $mDbName . ".incident_status where incident.site_id = site.id and  incident.status = incident_status.status and incident.status in ('L4_APPROVED', '_CLOSED') and incident.site_id = " . $siteId;
}else if ($filter === 'SITEINCIDENTSFORFR') {
    $siteId = $_GET['siteId'];
    $sql = "select * from " . $mDbName . ".incident, " . $mDbName . ".site, " . $mDbName . ".incident_status where incident.site_id = site.id and  incident.status = incident_status.status and incident.status not in ('L1_SAVED', 'L1_CREATED', '_DELETED') and incident.site_id = " . $siteId;
} else if ($filter === 'SITEHAZARDS') {
    $siteId = $_GET['siteId'];

    $sql = "select hazard.hazard_number as reportId from " . $mDbName . ".hazard where status in ('L2_APPROVED','_CLOSED') and site_id = " . $siteId;
} else if ($filter === 'INCIDENTDATA') {
    $incidentId = $_GET['iid'];
    $sql = "select * from " . $mDbName . ".incident, " . $mDbName . ".site, " . $mDbName . ".incident_status where incident.site_id = site.id and  incident.status = incident_status.status  and incident.incident_number = " . $incidentId;
}else if ($filter === 'HAZARDDATA') {
    $hazardId = $_GET['iid'];
    $sql = "select * from " . $mDbName . ".hazard, " . $mDbName . ".site, " . $mDbName . ".hazard_status where hazard.site_id = site.id and  hazard.status = hazard_status.status  and hazard.hazard_number = " . $hazardId;
} else if ($filter === "ACTIONHISTORY") {
    $actionId = $_GET['actionId'];
    $sql = "Select id,status,updated_by_user_id, updated_by, comments, updated_at from action_history where action_number = " . $actionId . " and status not in ('CREATED', 'SAVED', 'ASSIGNED') UNION Select id,status,updated_by_user_id, updated_by, comments, updated_at from action_history where action_number = " . $actionId . " and status in ('ASSIGNED') group by status order by id desc";
    
} else if ($filter === "SITEACTIONS") {
    $sitedIds = join(",", $_SESSION['vsmsUserData']['sites']);

    $sql = "SELECT *, substring(actionDesc, 1, 100) as actionDesc FROM " . $mDbName . ".allsiteactions where siteId in (" . $sitedIds . ")  and actionStatus not like 'CREATED' ORDER BY actionId desc;";
}else if ($filter === 'NEWACTIONS') {
    $rowID = $_GET['iid'];
    $sql = "select  * from actions where report_id  = " . $rowID . " and action_source = 'Incident' and status = 'ASSIGNED' and notified = false;";
}else if ($filter === 'NEWACTIONSHAZARD') {
    $rowID = $_GET['hid'];
    $sql = "select  * from actions where report_id  = " . $rowID . " and action_source = 'HAZARD' and status = 'ASSIGNED' and notified = false;";
}else if ($filter === 'NEWACTIONSGRA') {
    $rowID = $_GET['graid'];
    $sql = "select  * from actions where report_id  = " . $rowID . " and action_source = 'Risk Assessment' and status = 'ASSIGNED' and notified = false;";
}else if ($filter === "ACTIONCOMMENTS") {
    $actionId = $_GET['actionId'];
    $sql = "SELECT comments as COMMENTS, updated_by as UPDATE_BY, DATE_FORMAT(updated_at, '%d/%m/%Y') as UPDATE_AT FROM ". $mDbName . ".action_history where comments like 'Action Comments:%' and action_number = " . $actionId ;
    
}
 ChromePhp::log($sql);
///Incase of action data get files as well
if ($filter === 'ACTIONDATA' || $filter === 'ACTIONDATAOTHER' || $filter === 'ACTIONDATAHAZARD' || $filter === 'ACTIONDATAGRA') {
     $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
   
    while ($row = mysqli_fetch_assoc($result)) {
        //$emparray[] = array_map('utf8_encode', $row);
        $curr = $row;
        $files = array();
        $mDetailsQuery1 = "SELECT * FROM " . $mDbName . ".actions_files where action_id =".$rowID;
        $mDetailData1 = mysqli_query($connection, $mDetailsQuery1);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData1)) {
               $files[] = $mInnerRow;
        }
        $photos = array();
        $mDetailsQuery2 = "SELECT * FROM " . $mDbName . ".actions_photos where action_id =".$rowID;
        $mDetailData2 = mysqli_query($connection, $mDetailsQuery2);
        while ($mInnerRow1 = mysqli_fetch_assoc($mDetailData2)) {
               $photos[] = $mInnerRow1;
        }
        $curr['files'] = $files;
        $curr['photos'] = $photos;
        array_walk_recursive( $curr, function (&$item) { $item = mb_convert_encoding( $item, 'UTF-8' ); } );
        $emparray[] = $curr;
        
    }
} else {
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
}
echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>