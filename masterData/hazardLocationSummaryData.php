<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",",$_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from ". $mDbName . ".site where id in (".$_GET['siteIds'].");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
 while ($row = mysqli_fetch_assoc($siteResult)) {
     $siteNames[$row['id']] = $row['code'];
     
 }
foreach ($siteIds as &$siteId) {
    
    $currSql = "select 'Current Period' as type,sum(decant) as decant, sum(despatch) as despatch, sum(lpgBulkStorage) as lpgBulkStorage, sum(marshalling) as marshalling, sum(mezzanine) as mezzanine, sum(nes) as nes, sum(officeFirstFloor) as officeFirstFloor, sum(officeGroundFloor) as officeGroundFloor, sum(openStores) as openStores, sum(picking) as picking, sum(polySort) as polySort, sum(putaway) as putaway, sum(racking) as racking, sum(receiving) as receiving, sum(smallStores) as smallStores, sum(stillageSort) as stillageSort, sum(other) as other, hazardSite from ". $mDbName . ".hazard_location_summary_report where site_id =" . $siteId . " and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by hazardSite ";
    $prevSql = '';
     $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type,sum(decant) as decant, sum(despatch) as despatch, sum(lpgBulkStorage) as lpgBulkStorage, sum(marshalling) as marshalling, sum(mezzanine) as mezzanine, sum(nes) as nes, sum(officeFirstFloor) as officeFirstFloor, sum(officeGroundFloor) as officeGroundFloor, sum(openStores) as openStores, sum(picking) as picking, sum(polySort) as polySort, sum(putaway) as putaway, sum(racking) as racking, sum(receiving) as receiving, sum(smallStores) as smallStores, sum(stillageSort) as stillageSort, sum(other) as other, hazardSite from ". $mDbName . ".hazard_location_summary_report where site_id =" . $siteId . " and hazard_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by hazardSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }
    
    
    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();
    
    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if($row['type'] === 'Current Period'){
            $currRow = $row;
            
            $currRow['total'] = $currRow['decant'] +   $currRow['despatch'] +   $currRow['lpgBulkStorage'] +   $currRow['marshalling'] +   $currRow['mezzanine'] +   $currRow['nes'] +   $currRow['officeFirstFloor'] +   $currRow['officeGroundFloor'] +   $currRow['openStores'] +   $currRow['picking'] +   $currRow['polySort'] +   $currRow['putaway'] +   $currRow['racking'] +   $currRow['receiving'] +  $currRow['smallStores'] + $currRow['stillageSort'] +   $currRow['other'];
            if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
            
            $emparray[] = array_map('utf8_encode', $currRow);
            

        }else if($row['type'] === 'Previous Period'){
            $prevRow = $row;
            $prevRow['total'] = $prevRow['decant'] +   $prevRow['despatch'] +   $prevRow['lpgBulkStorage'] +   $prevRow['marshalling'] +   $prevRow['mezzanine'] +   $prevRow['nes'] +   $prevRow['officeFirstFloor'] +   $prevRow['officeGroundFloor'] +   $prevRow['openStores'] +   $prevRow['picking'] +   $prevRow['polySort'] +   $prevRow['putaway'] +   $prevRow['racking'] +   $prevRow['receiving'] +  $prevRow['smallStores'] + $prevRow['stillageSort'] +   $prevRow['other'];
             if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);

        }
    }
    if(count($prevRow) ===0 && $prevFromDate !== 'None'){
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
    }
    if(count($currRow ) === 0){
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
         if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {
        
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
    
}
$totalCurrRow['hazardSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['hazardSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow){
   $varRow = array();
   $varRow['type']= 'Variance';
   $varRow['hazardSite']= $currRow['hazardSite'];
    $varRow['decant'] = $currRow['decant'] - $prevRow['decant'];
   $varRow['despatch'] = $currRow['despatch'] - $prevRow['despatch'];
   $varRow['lpgBulkStorage'] = $currRow['lpgBulkStorage'] - $prevRow['lpgBulkStorage'];
   $varRow['marshalling'] = $currRow['marshalling'] - $prevRow['marshalling'];
   $varRow['mezzanine'] = $currRow['mezzanine'] - $prevRow['mezzanine'];
   $varRow['nes'] = $currRow['nes'] - $prevRow['nes'];
   $varRow['officeFirstFloor'] = $currRow['officeFirstFloor'] - $prevRow['officeFirstFloor'];
   $varRow['officeGroundFloor'] = $currRow['officeGroundFloor'] - $prevRow['officeGroundFloor'];
   $varRow['openStores'] = $currRow['openStores'] - $prevRow['openStores'];
   $varRow['picking'] = $currRow['picking'] - $prevRow['picking'];
   $varRow['polySort'] = $currRow['polySort'] - $prevRow['polySort'];
   $varRow['putaway'] = $currRow['putaway'] - $prevRow['putaway'];
   $varRow['racking'] = $currRow['racking'] - $prevRow['racking'];
   $varRow['receiving'] = $currRow['receiving'] - $prevRow['receiving'];
   $varRow['smallStores'] = $currRow['smallStores'] - $prevRow['smallStores'];
   $varRow['stillageSort'] = $currRow['stillageSort'] - $prevRow['stillageSort'];
   $varRow['other'] = $currRow['other'] - $prevRow['other'];
   $varRow['total'] = $currRow['total'] - $prevRow['total'];
   return $varRow;
   
}
function addInTotal($row, $totalPrevRow){
   $totalPrevRow['decant'] += $row['decant'];
   $totalPrevRow['despatch'] += $row['despatch'];
   $totalPrevRow['lpgBulkStorage'] += $row['lpgBulkStorage'];
   $totalPrevRow['marshalling'] += $row['marshalling'];
   $totalPrevRow['mezzanine'] += $row['mezzanine'];
   $totalPrevRow['nes'] += $row['nes'] ;
   $totalPrevRow['officeFirstFloor'] += $row['officeFirstFloor'];
   $totalPrevRow['officeGroundFloor'] += $row['officeGroundFloor'];
   $totalPrevRow['openStores'] += $row['openStores'];
   $totalPrevRow['picking'] += $row['picking'];
   $totalPrevRow['polySort'] += $row['polySort'];
   $totalPrevRow['putaway'] += $row['putaway'];
   $totalPrevRow['racking'] += $row['racking'];
   $totalPrevRow['receiving'] += $row['receiving'];
   $totalPrevRow['smallStores'] += $row['smallStores'];
   $totalPrevRow['stillageSort'] += $row['stillageSort'] ;
   $totalPrevRow['other'] += $row['other'] ;
   $totalPrevRow['total'] += $row['total'];
   return $totalPrevRow;
}
function createEmptyRow($type, $site){
   $varRow = array();
   $varRow['type']= $type;
   $varRow['hazardSite'] = $site;
   $varRow['decant'] = 0;
   $varRow['despatch'] = 0;
   $varRow['lpgBulkStorage'] = 0;
   $varRow['marshalling'] = 0;
   $varRow['mezzanine'] = 0;
   $varRow['nes'] = 0;
   $varRow['officeFirstFloor'] = 0;
   $varRow['officeGroundFloor'] = 0;
   $varRow['openStores'] = 0;
   $varRow['picking'] = 0;
   $varRow['polySort'] = 0;
   $varRow['putaway'] = 0;
   $varRow['racking'] = 0;
   $varRow['receiving'] = 0;
   $varRow['smallStores'] = 0;
   $varRow['stillageSort'] = 0;
   $varRow['other'] = 0;
   $varRow['total'] = 0;
   return $varRow;

}
?>