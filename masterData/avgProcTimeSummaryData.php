<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}


foreach ($siteIds as &$siteId) {
    $prevRow = array();
    $currRow = array();
    $varRow = array();
    ///Query for INCIDENT REPORTED
    $currIReportedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentReported))), '%H:%i:%s') as incidentReported, incidentSite from " . $mDbName . ".proc_time_incident_reported where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevIReportedSql = '';
    $mainIReportedSql = $currIReportedSql;
    $currRow['incidentClosed'] = 0;
    $currRow['fiApproved'] = 0;
    $currRow['fiSubmitted'] = 0;
    $currRow['incidentReported'] = 0;
    $currRow['initialReporApproved'] = 0;
    $currRow['initialReportSubmitted'] = 0;
    $currRow['type'] = 'Current Period';
    $currRow['incidentSite'] = $siteNames[$siteId];

    $prevRow['incidentClosed'] = 0;
    $prevRow['fiApproved'] = 0;
    $prevRow['fiSubmitted'] = 0;
    $prevRow['incidentReported'] = 0;
    $prevRow['initialReporApproved'] = 0;
    $prevRow['initialReportSubmitted'] = 0;
    $prevRow['type'] = 'Previous Period';
    $prevRow['incidentSite'] = $siteNames[$siteId];


    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevIReportedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentReported))), '%H:%i:%s') as incidentReported, incidentSite from " . $mDbName . ".proc_time_incident_reported where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainIReportedSql = $currIReportedSql . ' UNION ' . $prevIReportedSql;
    }
    
    $mainResult = mysqli_query($connection, $mainIReportedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {

            $currRow['incidentReported'] = $row['incidentReported'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['incidentReported'] = $row['incidentReported'];
        }
    }
    /////QUERY FOR INITIAL REPORT SUBMITTED
    $currIRSubmittedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReportSubmitted))), '%H:%i:%s') as initialReportSubmitted, incidentSite from " . $mDbName . ".proc_time_ir_submitted where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevIRSubmittedSql = '';
    $mainIRSubmittedSql = $currIRSubmittedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevIRSubmittedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReportSubmitted))), '%H:%i:%s') as initialReportSubmitted, incidentSite from " . $mDbName . ".proc_time_ir_submitted where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainIRSubmittedSql = $currIRSubmittedSql . ' UNION ' . $prevIRSubmittedSql;
    }

    $mainResult1 = mysqli_query($connection, $mainIRSubmittedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult1)) {
        if ($row['type'] === 'Current Period') {

            $currRow['initialReportSubmitted'] = $row['initialReportSubmitted'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['initialReportSubmitted'] = $row['initialReportSubmitted'];
        }
    }
    /////QUERY FOR INITIAL REPORT APPROVED
    $currIRApprovedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReporApproved))), '%H:%i:%s') as initialReporApproved, incidentSite from " . $mDbName . ".proc_time_ir_approved where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevIRApprovedSql = '';
    $mainIRApprovedSql = $currIRApprovedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevIRApprovedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReporApproved))), '%H:%i:%s') as initialReporApproved, incidentSite from " . $mDbName . ".proc_time_ir_approved where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainIRApprovedSql = $currIRApprovedSql . ' UNION ' . $prevIRApprovedSql;
    }

    $mainResult2 = mysqli_query($connection, $mainIRApprovedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult2)) {
        if ($row['type'] === 'Current Period') {
            $currRow['initialReporApproved'] = $row['initialReporApproved'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['initialReporApproved'] = $row['initialReporApproved'];
        }
    }

    /////QUERY FOR FI REPORT SUBMITTED
    $currFISubmittedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiSubmitted))), '%H:%i:%s') as fiSubmitted, incidentSite from " . $mDbName . ".proc_time_fi_submitted where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevFISubmittedSql = '';
    $mainFISubmittedSql = $currFISubmittedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevFISubmittedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiSubmitted))), '%H:%i:%s') as fiSubmitted, incidentSite from " . $mDbName . ".proc_time_fi_submitted where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainFISubmittedSql = $currFISubmittedSql . ' UNION ' . $prevFISubmittedSql;
    }

    $mainResult3 = mysqli_query($connection, $mainFISubmittedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult3)) {
        if ($row['type'] === 'Current Period') {
            $currRow['fiSubmitted'] = $row['fiSubmitted'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['fiSubmitted'] = $row['fiSubmitted'];
        }
    }

    /////QUERY FOR FI REPORT APPROVED
    $currFIApprovedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiApproved))), '%H:%i:%s') as fiApproved, incidentSite from " . $mDbName . ".proc_time_fi_approved where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevFIApprovedSql = '';
    $mainFIApprovedSql = $currFIApprovedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevFIApprovedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiApproved))), '%H:%i:%s') as fiApproved, incidentSite from " . $mDbName . ".proc_time_fi_approved where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainFIApprovedSql = $currFIApprovedSql . ' UNION ' . $prevFIApprovedSql;
    }

    $mainResult4 = mysqli_query($connection, $mainFIApprovedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult4)) {
        if ($row['type'] === 'Current Period') {
            $currRow['fiApproved'] = $row['fiApproved'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['fiApproved'] = $row['fiApproved'];
        }
    }


    /////QUERY FOR Incident Closed
    $currClosedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentClosed))), '%H:%i:%s') as incidentClosed, incidentSite from " . $mDbName . ".proc_time_incident_closed where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevClosedSql = '';
    $mainClosedSql = $currClosedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevClosedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentClosed))), '%H:%i:%s') as incidentClosed, incidentSite from " . $mDbName . ".proc_time_incident_closed where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainClosedSql = $currClosedSql . ' UNION ' . $prevClosedSql;
    }

    $mainResult5 = mysqli_query($connection, $mainClosedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult5)) {
        if ($row['type'] === 'Current Period') {
            $currRow['incidentClosed'] = $row['incidentClosed'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['incidentClosed'] = $row['incidentClosed'];
        }
    }
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $emparray[] = array_map('utf8_encode', $prevRow);
    }

    $emparray[] = array_map('utf8_encode', $currRow);
}

//Get the Acg row


$prevAvgRow = array();
$currAvgRow = array();

///Query for INCIDENT REPORTED
$currAvgIReportedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentReported))), '%H:%i:%s') as incidentReported, incidentSite from " . $mDbName . ".proc_time_incident_reported where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgIReportedSql = '';
$mainAvgIReportedSql = $currAvgIReportedSql;
$currAvgRow['incidentClosed'] = 0;
$currAvgRow['fiApproved'] = 0;
$currAvgRow['fiSubmitted'] = 0;
$currAvgRow['incidentReported'] = 0;
$currAvgRow['initialReporApproved'] = 0;
$currAvgRow['initialReportSubmitted'] = 0;
$currAvgRow['type'] = 'Current Period';
$currAvgRow['incidentSite'] = 'Average';

$prevAvgRow['incidentClosed'] = 0;
$prevAvgRow['fiApproved'] = 0;
$prevAvgRow['fiSubmitted'] = 0;
$prevAvgRow['incidentReported'] = 0;
$prevAvgRow['initialReporApproved'] = 0;
$prevAvgRow['initialReportSubmitted'] = 0;
$prevAvgRow['type'] = 'Previous Period';
$prevAvgRow['incidentSite'] = 'Average';


if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgIReportedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentReported))), '%H:%i:%s') as incidentReported, incidentSite from " . $mDbName . ".proc_time_incident_reported where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgIReportedSql = $currAvgIReportedSql . ' UNION ' . $prevAvgIReportedSql;
}
$mainAvgResult = mysqli_query($connection, $mainAvgIReportedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult)) {
    if ($row['type'] === 'Current Period') {

        $currAvgRow['incidentReported'] = $row['incidentReported'];
    } else if ($row['type'] === 'Previous Period') {

        $prevAvgRow['incidentReported'] = $row['incidentReported'];
    }
}
/////QUERY FOR INITIAL REPORT SUBMITTED
$currAvgIRSubmittedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReportSubmitted))), '%H:%i:%s') as initialReportSubmitted, incidentSite from " . $mDbName . ".proc_time_ir_submitted where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgIRSubmittedSql = '';
$mainAvgIRSubmittedSql = $currAvgIRSubmittedSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgIRSubmittedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReportSubmitted))), '%H:%i:%s') as initialReportSubmitted, incidentSite from " . $mDbName . ".proc_time_ir_submitted where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgIRSubmittedSql = $currAvgIRSubmittedSql . ' UNION ' . $prevAvgIRSubmittedSql;
}

$mainAvgResult1 = mysqli_query($connection, $mainAvgIRSubmittedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult1)) {
    if ($row['type'] === 'Current Period') {

        $currAvgRow['initialReportSubmitted'] = $row['initialReportSubmitted'];
    } else if ($row['type'] === 'Previous Period') {
        $prevAvgRow['initialReportSubmitted'] = $row['initialReportSubmitted'];
    }
}
/////QUERY FOR INITIAL REPORT APPROVED
$currAvgIRApprovedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReporApproved))), '%H:%i:%s') as initialReporApproved, incidentSite from " . $mDbName . ".proc_time_ir_approved where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgIRApprovedSql = '';
$mainAvgIRApprovedSql = $currAvgIRApprovedSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgIRApprovedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(initialReporApproved))), '%H:%i:%s') as initialReporApproved, incidentSite from " . $mDbName . ".proc_time_ir_approved where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgIRApprovedSql = $currAvgIRApprovedSql . ' UNION ' . $prevAvgIRApprovedSql;
}

$mainAvgResult2 = mysqli_query($connection, $mainAvgIRApprovedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult2)) {
    if ($row['type'] === 'Current Period') {
        $currAvgRow['initialReporApproved'] = $row['initialReporApproved'];
    } else if ($row['type'] === 'Previous Period') {
        $prevAvgRow['initialReporApproved'] = $row['initialReporApproved'];
    }
}

/////QUERY FOR FI REPORT SUBMITTED
$currAvgFISubmittedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiSubmitted))), '%H:%i:%s') as fiSubmitted, incidentSite from " . $mDbName . ".proc_time_fi_submitted where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgFISubmittedSql = '';
$mainAvgFISubmittedSql = $currAvgFISubmittedSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgFISubmittedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiSubmitted))), '%H:%i:%s') as fiSubmitted, incidentSite from " . $mDbName . ".proc_time_fi_submitted where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgFISubmittedSql = $currAvgFISubmittedSql . ' UNION ' . $prevAvgFISubmittedSql;
}

$mainAvgResult3 = mysqli_query($connection, $mainAvgFISubmittedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult3)) {
    if ($row['type'] === 'Current Period') {
        $currAvgRow['fiSubmitted'] = $row['fiSubmitted'];
    } else if ($row['type'] === 'Previous Period') {
        $prevAvgRow['fiSubmitted'] = $row['fiSubmitted'];
    }
}

/////QUERY FOR FI REPORT APPROVED
$currAvgFIApprovedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiApproved))), '%H:%i:%s') as fiApproved, incidentSite from " . $mDbName . ".proc_time_fi_approved where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgFIApprovedSql = '';
$mainAvgFIApprovedSql = $currAvgFIApprovedSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgFIApprovedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(fiApproved))), '%H:%i:%s') as fiApproved, incidentSite from " . $mDbName . ".proc_time_fi_approved where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgFIApprovedSql = $currAvgFIApprovedSql . ' UNION ' . $prevAvgFIApprovedSql;
}

$mainAvgResult4 = mysqli_query($connection, $mainAvgFIApprovedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult4)) {
    if ($row['type'] === 'Current Period') {
        $currAvgRow['fiApproved'] = $row['fiApproved'];
    } else if ($row['type'] === 'Previous Period') {
        $prevAvgRow['fiApproved'] = $row['fiApproved'];
    }
}


/////QUERY FOR Incident Closed
$currAvgClosedSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentClosed))), '%H:%i:%s') as incidentClosed, incidentSite from " . $mDbName . ".proc_time_incident_closed where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ";
$prevAvgClosedSql = '';
$mainAvgClosedSql = $currAvgClosedSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevAvgClosedSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(time_to_sec(incidentClosed))), '%H:%i:%s') as incidentClosed, incidentSite from " . $mDbName . ".proc_time_incident_closed where site_id in (" . $_GET['siteIds'] . ")and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' ";
    $mainAvgClosedSql = $currAvgClosedSql . ' UNION ' . $prevAvgClosedSql;
}

$mainAvgResult5 = mysqli_query($connection, $mainAvgClosedSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainAvgResult5)) {
    if ($row['type'] === 'Current Period') {
        $currAvgRow['incidentClosed'] = $row['incidentClosed'];
    } else if ($row['type'] === 'Previous Period') {
        $prevAvgRow['incidentClosed'] = $row['incidentClosed'];
    }
}
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $emparray[] = array_map('utf8_encode', $prevAvgRow);
}
$emparray[] = array_map('utf8_encode', $currAvgRow);




echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function createEmptyRow($type, $site) {
    $varRow = array();
    $varRow['type'] = $type;
    $varRow['incidentSite'] = $site;
    $varRow['incidentClosed'] = 0;
    $varRow['fiApproved'] = 0;
    $varRow['fiSubmitted'] = 0;
    $varRow['incidentReported'] = 0;
    $varRow['initialReporApproved'] = 0;
    $varRow['initialReportSubmitted'] = 0;
    return $varRow;
}

?>