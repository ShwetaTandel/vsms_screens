<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",",$_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from ". $mDbName . ".site where id in (".$_GET['siteIds'].");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
 while ($row = mysqli_fetch_assoc($siteResult)) {
     $siteNames[$row['id']] = $row['code'];
     
 }
 
 
foreach ($siteIds as &$siteId) {
    
       $currSql = "select  'Current Period' as type, sum(counterbalanceB1) as counterbalanceB1, sum(counterbalanceB2) as counterbalanceB2, sum(reachTruck) as reachTruck, sum(palletTruck) as palletTruck, sum(towTractor) as towTractor, sum(hlop) as hlop, sum(llop) as llop, sum(straddleCarrier) as straddleCarrier, sum(sideLoader) as sideLoader, sum(car) as car, sum(lgv) as lgv, sum(bendiTruck) as bendiTruck, sum(scissorLift) as scissorLift, sum(cherryPicker) as cherryPicker, sum(combiTruck) as combiTruck, incidentSite from " . $mDbName . ".vehicle_types_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type, sum(counterbalanceB1) as counterbalanceB1, sum(counterbalanceB2) as counterbalanceB2, sum(reachTruck) as reachTruck, sum(palletTruck) as palletTruck, sum(towTractor) as towTractor, sum(hlop) as hlop, sum(llop) as llop, sum(straddleCarrier) as straddleCarrier, sum(sideLoader) as sideLoader, sum(car) as car, sum(lgv) as lgv, sum(bendiTruck) as bendiTruck, sum(scissorLift) as scissorLift, sum(cherryPicker) as cherryPicker, sum(combiTruck) as combiTruck, incidentSite from " . $mDbName . ".vehicle_types_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }

    
    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();
    
    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if($row['type'] === 'Current Period'){
            $currRow = $row;
            
            $currRow['total'] = $currRow['counterbalanceB1'] +   $currRow['counterbalanceB2'] +   $currRow['reachTruck'] +   $currRow['palletTruck'] +   $currRow['towTractor'] +   $currRow['hlop'] +   $currRow['llop'] +   $currRow['straddleCarrier'] +   $currRow['sideLoader'] +   $currRow['car'] +   $currRow['lgv'] +   $currRow['bendiTruck'] +   $currRow['scissorLift'] +   $currRow['cherryPicker'] +  $currRow['combiTruck'] ;
            if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
            
            $emparray[] = array_map('utf8_encode', $currRow);
            

        }else if($row['type'] === 'Previous Period'){
            $prevRow = $row;
            $prevRow['total'] = $prevRow['counterbalanceB1'] +   $prevRow['counterbalanceB2'] +   $prevRow['reachTruck'] +   $prevRow['palletTruck'] +   $prevRow['towTractor'] +   $prevRow['hlop'] +   $prevRow['llop'] +   $prevRow['straddleCarrier'] +   $prevRow['sideLoader'] +   $prevRow['car'] +   $prevRow['lgv'] +   $prevRow['bendiTruck'] +   $prevRow['scissorLift'] +   $prevRow['cherryPicker'] +  $prevRow['combiTruck'] ;
             if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);

        }
    }
    if(count($prevRow) ===0 && $prevFromDate !== 'None'){
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
    }
    if(count($currRow ) === 0){
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
         if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {
        
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
    
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow){
   $varRow = array();
   $varRow['type']= 'Variance';
   $varRow['incidentSite']= $currRow['incidentSite'];
    $varRow['counterbalanceB1'] = $currRow['counterbalanceB1'] - $prevRow['counterbalanceB1'];
   $varRow['counterbalanceB2'] = $currRow['counterbalanceB2'] - $prevRow['counterbalanceB2'];
   $varRow['reachTruck'] = $currRow['reachTruck'] - $prevRow['reachTruck'];
   $varRow['palletTruck'] = $currRow['palletTruck'] - $prevRow['palletTruck'];
   $varRow['towTractor'] = $currRow['towTractor'] - $prevRow['towTractor'];
   $varRow['hlop'] = $currRow['hlop'] - $prevRow['hlop'];
   $varRow['llop'] = $currRow['llop'] - $prevRow['llop'];
   $varRow['straddleCarrier'] = $currRow['straddleCarrier'] - $prevRow['straddleCarrier'];
   $varRow['sideLoader'] = $currRow['sideLoader'] - $prevRow['sideLoader'];
   $varRow['car'] = $currRow['car'] - $prevRow['car'];
   $varRow['lgv'] = $currRow['lgv'] - $prevRow['lgv'];
   $varRow['bendiTruck'] = $currRow['bendiTruck'] - $prevRow['bendiTruck'];
   $varRow['scissorLift'] = $currRow['scissorLift'] - $prevRow['scissorLift'];
   $varRow['cherryPicker'] = $currRow['cherryPicker'] - $prevRow['cherryPicker'];
   $varRow['combiTruck'] = $currRow['combiTruck'] - $prevRow['combiTruck'];
   $varRow['total'] = $currRow['total'] - $prevRow['total'];
   return $varRow;
   
}
function addInTotal($row, $totalPrevRow){
   $totalPrevRow['counterbalanceB1'] += $row['counterbalanceB1'];
   $totalPrevRow['counterbalanceB2'] += $row['counterbalanceB2'];
   $totalPrevRow['reachTruck'] += $row['reachTruck'];
   $totalPrevRow['palletTruck'] += $row['palletTruck'];
   $totalPrevRow['towTractor'] += $row['towTractor'];
   $totalPrevRow['hlop'] += $row['hlop'] ;
   $totalPrevRow['llop'] += $row['llop'];
   $totalPrevRow['straddleCarrier'] += $row['straddleCarrier'];
   $totalPrevRow['sideLoader'] += $row['sideLoader'];
   $totalPrevRow['car'] += $row['car'];
   $totalPrevRow['lgv'] += $row['lgv'];
   $totalPrevRow['bendiTruck'] += $row['bendiTruck'];
   $totalPrevRow['scissorLift'] += $row['scissorLift'];
   $totalPrevRow['cherryPicker'] += $row['cherryPicker'];
   $totalPrevRow['combiTruck'] += $row['combiTruck'];
   $totalPrevRow['total'] += $row['total'];
   return $totalPrevRow;
}
function createEmptyRow($type, $site){
   $varRow = array();
   $varRow['type']= $type;
   $varRow['incidentSite'] = $site;
   $varRow['counterbalanceB1'] = 0;
   $varRow['counterbalanceB2'] = 0;
   $varRow['reachTruck'] = 0;
   $varRow['palletTruck'] = 0;
   $varRow['towTractor'] = 0;
   $varRow['hlop'] = 0;
   $varRow['llop'] = 0;
   $varRow['straddleCarrier'] = 0;
   $varRow['sideLoader'] = 0;
   $varRow['car'] = 0;
   $varRow['lgv'] = 0;
   $varRow['bendiTruck'] = 0;
   $varRow['scissorLift'] = 0;
   $varRow['cherryPicker'] = 0;
   $varRow['combiTruck'] = 0;
   $varRow['total'] = 0;
   return $varRow;

}
?>