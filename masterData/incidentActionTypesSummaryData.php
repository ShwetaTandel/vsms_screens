<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",",$_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from ". $mDbName . ".site where id in (".$_GET['siteIds'].");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
 while ($row = mysqli_fetch_assoc($siteResult)) {
     $siteNames[$row['id']] = $row['code'];
     
 }
foreach ($siteIds as &$siteId) {
    
    $currSql = "select 'Current Period' as type,sum(building) as building, sum(corner) as corner, sum(destacking) as destacking, sum(hydraulics) as hydraulics, sum(loading) as loading, sum(manoeuvre) as manoeuvre, sum(manualHandling) as manualHandling, sum(offloading) as offloading, sum(parking) as parking, sum(picking) as picking, sum(pullaway) as pullaway, sum(putaway) as putaway, sum(reversing) as reversing , sum(stacking) as stacking, sum(transporting) as transporting, sum(turning) as turning, sum(other) as other, incidentSite from ". $mDbName . ".action_types_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevSql = '';
     $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type,sum(building) as building, sum(corner) as corner, sum(destacking) as destacking, sum(hydraulics) as hydraulics, sum(loading) as loading, sum(manoeuvre) as manoeuvre, sum(manualHandling) as manualHandling, sum(offloading) as offloading, sum(parking) as parking, sum(picking) as picking, sum(pullaway) as pullaway, sum(putaway) as putaway, sum(reversing) as reversing , sum(stacking) as stacking, sum(transporting) as transporting, sum(turning) as turning, sum(other) as other, incidentSite from ". $mDbName . ".action_types_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }
    
    
    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();
    
    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if($row['type'] === 'Current Period'){
            $currRow = $row;
            
            $currRow['total'] = $currRow['building'] +   $currRow['corner'] +   $currRow['destacking'] +   $currRow['hydraulics'] +   $currRow['loading'] +   $currRow['manoeuvre'] +   $currRow['manualHandling'] +   $currRow['offloading'] +   $currRow['parking'] +   $currRow['picking'] +   $currRow['pullaway'] +   $currRow['putaway'] +   $currRow['reversing'] +   $currRow['stacking'] +  $currRow['transporting'] + $currRow['turning'] +   $currRow['other'];
            if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
            
            $emparray[] = array_map('utf8_encode', $currRow);
            

        }else if($row['type'] === 'Previous Period'){
            $prevRow = $row;
            $prevRow['total'] = $prevRow['building'] +   $prevRow['corner'] +   $prevRow['destacking'] +   $prevRow['hydraulics'] +   $prevRow['loading'] +   $prevRow['manoeuvre'] +   $prevRow['manualHandling'] +   $prevRow['offloading'] +   $prevRow['parking'] +   $prevRow['picking'] +   $prevRow['pullaway'] +   $prevRow['putaway'] +   $prevRow['reversing'] +   $prevRow['stacking'] +  $prevRow['transporting'] + $prevRow['turning'] +   $prevRow['other'];
             if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);

        }
    }
    if(count($prevRow) ===0 && $prevFromDate !== 'None'){
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
    }
    if(count($currRow ) === 0){
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
         if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {
        
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
    
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow){
   $varRow = array();
   $varRow['type']= 'Variance';
   $varRow['incidentSite']= $currRow['incidentSite'];
    $varRow['building'] = $currRow['building'] - $prevRow['building'];
   $varRow['corner'] = $currRow['corner'] - $prevRow['corner'];
   $varRow['destacking'] = $currRow['destacking'] - $prevRow['destacking'];
   $varRow['hydraulics'] = $currRow['hydraulics'] - $prevRow['hydraulics'];
   $varRow['loading'] = $currRow['loading'] - $prevRow['loading'];
   $varRow['manoeuvre'] = $currRow['manoeuvre'] - $prevRow['manoeuvre'];
   $varRow['manualHandling'] = $currRow['manualHandling'] - $prevRow['manualHandling'];
   $varRow['offloading'] = $currRow['offloading'] - $prevRow['offloading'];
   $varRow['parking'] = $currRow['parking'] - $prevRow['parking'];
   $varRow['picking'] = $currRow['picking'] - $prevRow['picking'];
   $varRow['pullaway'] = $currRow['pullaway'] - $prevRow['pullaway'];
   $varRow['putaway'] = $currRow['putaway'] - $prevRow['putaway'];
   $varRow['reversing'] = $currRow['reversing'] - $prevRow['reversing'];
   $varRow['stacking'] = $currRow['stacking'] - $prevRow['stacking'];
   $varRow['transporting'] = $currRow['transporting'] - $prevRow['transporting'];
   $varRow['turning'] = $currRow['turning'] - $prevRow['turning'];
   $varRow['other'] = $currRow['other'] - $prevRow['other'];
   $varRow['total'] = $currRow['total'] - $prevRow['total'];
   return $varRow;
   
}
function addInTotal($row, $totalPrevRow){
   $totalPrevRow['building'] += $row['building'];
   $totalPrevRow['corner'] += $row['corner'];
   $totalPrevRow['destacking'] += $row['destacking'];
   $totalPrevRow['hydraulics'] += $row['hydraulics'];
   $totalPrevRow['loading'] += $row['loading'];
   $totalPrevRow['manoeuvre'] += $row['manoeuvre'] ;
   $totalPrevRow['manualHandling'] += $row['manualHandling'];
   $totalPrevRow['offloading'] += $row['offloading'];
   $totalPrevRow['parking'] += $row['parking'];
   $totalPrevRow['picking'] += $row['picking'];
   $totalPrevRow['pullaway'] += $row['pullaway'];
   $totalPrevRow['putaway'] += $row['putaway'];
   $totalPrevRow['reversing'] += $row['reversing'];
   $totalPrevRow['stacking'] += $row['stacking'];
   $totalPrevRow['transporting'] += $row['transporting'];
   $totalPrevRow['turning'] += $row['turning'] ;
   $totalPrevRow['other'] += $row['other'] ;
   $totalPrevRow['total'] += $row['total'];
   return $totalPrevRow;
}
function createEmptyRow($type, $site){
   $varRow = array();
   $varRow['type']= $type;
   $varRow['incidentSite'] = $site;
   $varRow['building'] = 0;
   $varRow['corner'] = 0;
   $varRow['destacking'] = 0;
   $varRow['hydraulics'] = 0;
   $varRow['loading'] = 0;
   $varRow['manoeuvre'] = 0;
   $varRow['manualHandling'] = 0;
   $varRow['offloading'] = 0;
   $varRow['parking'] = 0;
   $varRow['picking'] = 0;
   $varRow['pullaway'] = 0;
   $varRow['putaway'] = 0;
   $varRow['reversing'] = 0;
   $varRow['stacking'] = 0;
   $varRow['transporting'] = 0;
   $varRow['turning'] = 0;
   $varRow['other'] = 0;
   $varRow['total'] = 0;
   return $varRow;

}
?>