<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}
foreach ($siteIds as &$siteId) {

    $currSql = "select 'Current Period' as type,sum(behaviour) as behaviour, sum(condition1) as condition1, sum(equipment) as equipment, sum(training) as training, sum(procedure1) as procedure1, sum(other) as other,  actionSite from " . $mDbName . ".action_action_types_summary where site_id =" . $siteId . " and assigned_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by actionSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type,sum(behaviour) as behaviour, sum(condition1) as condition1, sum(equipment) as equipment, sum(training) as training, sum(procedure1) as procedure1, sum(other) as other,  actionSite from " . $mDbName . ".action_action_types_summary where site_id =" . $siteId . " and assigned_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by actionSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }


    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();

    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {
            $currRow = $row;

            $currRow['total'] = $currRow['behaviour'] + $currRow['condition1'] + $currRow['equipment'] + $currRow['training'] + $currRow['procedure1'] + $currRow['other'];
            if (count($totalCurrRow) === 0) {
                $totalCurrRow = $currRow;
            } else {
                $totalCurrRow = addInTotal($currRow, $totalCurrRow);
            }

            $emparray[] = array_map('utf8_encode', $currRow);
        } else if ($row['type'] === 'Previous Period') {
            $prevRow = $row;
            $prevRow['total'] = $prevRow['behaviour'] + $prevRow['condition1'] + $prevRow['equipment'] + $prevRow['training'] + $prevRow['procedure1'] + $prevRow['other'];
            if (count($totalPrevRow) === 0) {
                $totalPrevRow = $prevRow;
            } else {
                $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);
        }
    }
    if (count($prevRow) === 0 && $prevFromDate !== 'None') {
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if (count($totalPrevRow) === 0) {
            $totalPrevRow = $prevRow;
        } else {
            $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        }
    }
    if (count($currRow) === 0) {
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
        if (count($totalCurrRow) === 0) {
            $totalCurrRow = $currRow;
        } else {
            $totalCurrRow = addInTotal($currRow, $totalCurrRow);
        }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {

        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}
$totalCurrRow['actionSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['actionSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['actionSite'] = $currRow['actionSite'];
    $varRow['behaviour'] = $currRow['behaviour'] - $prevRow['behaviour'];
    $varRow['condition1'] = $currRow['condition1'] - $prevRow['condition1'];
    $varRow['equipment'] = $currRow['equipment'] - $prevRow['equipment'];
    $varRow['training'] = $currRow['training'] - $prevRow['training'];
    $varRow['procedure1'] = $currRow['procedure1'] - $prevRow['procedure1'];
    $varRow['other'] = $currRow['other'] - $prevRow['other'];
    $varRow['total'] = $currRow['total'] - $prevRow['total'];
    return $varRow;
}

function addInTotal($row, $totalPrevRow) {
    $totalPrevRow['behaviour'] += $row['behaviour'];
    $totalPrevRow['condition1'] += $row['condition1'];
    $totalPrevRow['equipment'] += $row['equipment'];
    $totalPrevRow['training'] += $row['training'];
    $totalPrevRow['procedure1'] += $row['procedure1'];

    $totalPrevRow['other'] += $row['other'];
    $totalPrevRow['total'] += $row['total'];
    return $totalPrevRow;
}

function createEmptyRow($type, $site) {
    $varRow = array();
    $varRow['type'] = $type;
    $varRow['actionSite'] = $site;
    $varRow['behaviour'] = 0;
    $varRow['condition1'] = 0;
    $varRow['equipment'] = 0;
    $varRow['training'] = 0;
    $varRow['procedure1'] = 0;
    $varRow['other'] = 0;
    $varRow['total'] = 0;
    return $varRow;
}

?>