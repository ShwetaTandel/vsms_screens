
<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();

$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}


$totalCurrRow['actionRaisedByUser'] = 0;
$totalCurrRow['actionAssignedTo'] = 0;
$totalCurrRow['actionReassignedByUser'] = 0;
$totalCurrRow['actionReassignedTo'] = 0;
$totalCurrRow['actionsClosedByUser'] = 0;
$totalCurrRow['actionsCancelledByUser'] = 0;
$totalCurrRow['actionsRescheduledByUser'] = 0;
$totalCurrRow['actionsOverdue'] = 0;
$totalCurrRow['avgClosureTime'] = 0;
$totalCurrRow['type'] = 'Current Period';
$totalCurrRow['actionSite'] = 'Total';

$totalPrevRow['actionRaisedByUser'] = 0;
$totalPrevRow['actionAssignedTo'] = 0;
$totalPrevRow['actionReassignedByUser'] = 0;
$totalPrevRow['actionReassignedTo'] = 0;
$totalPrevRow['actionsClosedByUser'] = 0;
$totalPrevRow['actionsCancelledByUser'] = 0;
$totalPrevRow['actionsRescheduledByUser'] = 0;
$totalPrevRow['actionsOverdue'] = 0;
$totalPrevRow['avgClosureTime'] = 0;
$totalPrevRow['type'] = 'Previous Period';
$totalPrevRow['actionSite'] = 'Total';

$totalVarRow['actionRaisedByUser'] = 0;
$totalVarRow['actionAssignedTo'] = 0;
$totalVarRow['actionReassignedByUser'] = 0;
$totalVarRow['actionReassignedTo'] = 0;
$totalVarRow['actionsClosedByUser'] = 0;
$totalVarRow['actionsCancelledByUser'] = 0;
$totalVarRow['actionsRescheduledByUser'] = 0;
$totalVarRow['actionsOverdue'] = 0;
$totalVarRow['avgClosureTime'] = 0;
$totalVarRow['type'] = 'Variance';
$totalVarRow['actionSite'] = 'Total';

foreach ($siteIds as &$siteId) {
    $prevRow = array();
    $currRow = array();
    $varRow = array();

    $currRow['actionRaisedByUser'] = 0;
    $currRow['actionAssignedTo'] = 0;
    $currRow['actionReassignedByUser'] = 0;
    $currRow['actionReassignedTo'] = 0;
    $currRow['actionsClosedByUser'] = 0;
    $currRow['actionsCancelledByUser'] = 0;
    $currRow['actionsRescheduledByUser'] = 0;
    $currRow['actionsOverdue'] = 0;
    $currRow['avgClosureTime'] = 0;
    $currRow['type'] = 'Current Period';
    $currRow['actionSite'] = $siteNames[$siteId];

    $prevRow['actionRaisedByUser'] = 0;
    $prevRow['actionAssignedTo'] = 0;
    $prevRow['actionReassignedByUser'] = 0;
    $prevRow['actionReassignedTo'] = 0;
    $prevRow['actionsClosedByUser'] = 0;
    $prevRow['actionsCancelledByUser'] = 0;
    $prevRow['actionsRescheduledByUser'] = 0;
    $prevRow['actionsOverdue'] = 0;
    $prevRow['avgClosureTime'] = 0;
    $prevRow['type'] = 'Previous Period';
    $prevRow['actionSite'] = $siteNames[$siteId];




    ///Query for getting actions assigned by users
    $currActionRaisedSql = "select count(*) as actionRaisedByUser, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'ASSIGNED' and  action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionRaisedSql = '';
    $mainActionRaisedSql = $currActionRaisedSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionRaisedSql = "select count(*) as actionRaisedByUser, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'ASSIGNED' and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainActionRaisedSql = $currActionRaisedSql . ' UNION ' . $prevActionRaisedSql;
    }
    $mainResult = mysqli_query($connection, $mainActionRaisedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {

            $currRow['actionRaisedByUser'] = $row['actionRaisedByUser'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['actionRaisedByUser'] = $row['actionRaisedByUser'];
        }
    }
    /////QUERY FOR getting actions assgined to
    $currActionAssignedSql = "select count(*) as actionAssignedTo, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'ASSIGNED' and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionAssignedSql = '';
    $mainActionAssignedSql = $currActionAssignedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionAssignedSql = "select count(*) as actionAssignedTo, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'ASSIGNED' and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevFromDate . " 23:59:59' group by action_number";
        $mainActionAssignedSql = $currActionAssignedSql . ' UNION ' . $prevActionAssignedSql;
    }

    $mainResult1 = mysqli_query($connection, $mainActionAssignedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult1)) {
        if ($row['type'] === 'Current Period') {

            $currRow['actionAssignedTo'] = $row['actionAssignedTo'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['actionAssignedTo'] = $row['actionAssignedTo'];
        }
    }

    ///Query for getting actions re-assigned by users
    $currActionReRaisedSql = "select count(*) as actionReassignedByUser, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-ASSIGNED' and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionReRaisedSql = '';
    $mainActionReRaisedSql = $currActionReRaisedSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionReRaisedSql = "select count(*) as actionReassignedByUser, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-ASSIGNED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainActionReRaisedSql = $currActionReRaisedSql . ' UNION ' . $prevActionReRaisedSql;
    }
    $mainResult2 = mysqli_query($connection, $mainActionReRaisedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult2)) {
        if ($row['type'] === 'Current Period') {

            $currRow['actionReassignedByUser'] = $row['actionReassignedByUser'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['actionReassignedByUser'] = $row['actionReassignedByUser'];
        }
    }

    ///Query for getting actions re-assigned To users
    $currActionReRaisedToSql = "select count(*) as actionReassignedByTo, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-ASSIGNED' and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionReRaisedToSql = '';
    $mainActionReRaisedToSql = $currActionReRaisedToSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionReRaisedToSql = "select count(*) as actionReassignedByTo, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-ASSIGNED' and  action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainActionReRaisedToSql = $currActionReRaisedToSql . ' UNION ' . $prevActionReRaisedToSql;
    }
    $mainResult3 = mysqli_query($connection, $mainActionReRaisedToSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult3)) {
        if ($row['type'] === 'Current Period') {

            $currRow['actionReassignedByTo'] = $row['actionReassignedByTo'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['actionReassignedByTo'] = $row['actionReassignedByTo'];
        }
    }

    /////closed by
    $currActionClosedSql = "select count(*) as actionsClosedByUser, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CLOSED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionClosedSql = '';
    $mainActionClosedSql = $currActionClosedSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionClosedSql = "select count(*) as actionsClosedByUser, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CLOSED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainActionClosedSql = $currActionClosedSql . ' UNION ' . $prevActionClosedSql;
    }

    $mainResult4 = mysqli_query($connection, $mainActionClosedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult4)) {
        if ($row['type'] === 'Current Period') {
            $currRow['actionsClosedByUser'] = $row['actionsClosedByUser'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['actionsClosedByUser'] = $row['actionsClosedByUser'];
        }
    }


    /////QUERY FOR Incident Cancelled
    $currCancelledSql = "select count(*) as actionsCancelledByUser, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CANCELLED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevCancelledSql = '';
    $mainCancelledSql = $currCancelledSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevCancelledSql = "select count(*) as actionsCancelledByUser, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CANCELLED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainCancelledSql = $currCancelledSql . ' UNION ' . $prevCancelledSql;
    }

    $mainResult5 = mysqli_query($connection, $mainCancelledSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult5)) {
        if ($row['type'] === 'Current Period') {
            $currRow['actionsCancelledByUser'] = $row['actionsCancelledByUser'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['actionsCancelledByUser'] = $row['actionsCancelledByUser'];
        }
    }


    /////QUERY FOR Incident Re-scheduled
    $currRescheduledSql = "select count(*) as actionsRescheduledByUser, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-SCHEDULED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevRescheduledSql = '';
    $mainRescheduledSql = $currRescheduledSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevRescheduledSql = "select count(*) as actionsRescheduledByUser, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'RE-SCHEDULED'  and action_site = " .$siteId. " and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainRescheduledSql = $currRescheduledSql . ' UNION ' . $prevRescheduledSql;
    }

    $mainResult6 = mysqli_query($connection, $mainRescheduledSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult6)) {
        if ($row['type'] === 'Current Period') {
            $currRow['actionsRescheduledByUser'] = $row['actionsRescheduledByUser'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['actionsRescheduledByUser'] = $row['actionsRescheduledByUser'];
        }
    }

    /////overdue by
    $currActionOverdueSql = "select count(*) as actionsOverdue, 'Current Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CLOSED'  and action_site = " .$siteId. " and estimated_completion_date < action_history.updated_at and action_history.updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevActionOverdueSql = '';
    $mainActionOverdueSql = $currActionOverdueSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevActionOverdueSql = "select count(*) as actionsOverdue, 'Previous Period' as type from " . $mDbName . ".action_history," . $mDbName . ".actions where actions.id = action_history.action_number  and action_history.status = 'CLOSED'  and action_site = " .$siteId. " and estimated_completion_date < action_history.updated_at and action_history.updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainActionOverdueSql = $currActionOverdueSql . ' UNION ' . $prevActionOverdueSql;
    }

    $mainResult7 = mysqli_query($connection, $mainActionOverdueSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult7)) {
        if ($row['type'] === 'Current Period') {
            $currRow['actionsOverdue'] = $row['actionsOverdue'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['actionsOverdue'] = $row['actionsOverdue'];
        }
    }


    /////closure times by
    $currAvgClosureSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".action_avg_closure_time where  action_site = " .$siteId. " and updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
    $prevAvgClosureSql = '';
    $mainAvgClosureSql = $currAvgClosureSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevAvgClosureSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".action_avg_closure_time where  action_site = " .$siteId. " and updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
        $mainAvgClosureSql = $currAvgClosureSql . ' UNION ' . $prevAvgClosureSql;
    }

    $mainResult8 = mysqli_query($connection, $mainAvgClosureSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult8)) {
        if ($row['type'] === 'Current Period') {
            $currRow['avgClosureTime'] = $row['avgClosureTime'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['avgClosureTime'] = $row['avgClosureTime'];
        }
    }


    //$totalCurrRow = addInTotal($currRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $currRow);
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
      //  $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        $emparray[] = array_map('utf8_encode', $prevRow);
  
        $varRow = calcVariance($prevRow, $currRow);
      //  $totalVarRow = addInTotal($varRow, $totalVarRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}

$currTotalAvgClosureSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".action_avg_closure_time where action_site in (" . $_GET['siteIds'] . ") and updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by action_number";
$prevTotalAvgClosureSql = '';
$mainTotalAvgClosureSql = $currTotalAvgClosureSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevTotalAvgClosureSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".action_avg_closure_time where action_site in (" . $_GET['siteIds'] . ") and updated_at between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by action_number";
    $mainTotalAvgClosureSql = $currTotalAvgClosureSql . ' UNION ' . $prevTotalAvgClosureSql;
}

$mainResult8 = mysqli_query($connection, $mainTotalAvgClosureSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainResult8)) {
    if ($row['type'] === 'Current Period') {
        $totalCurrRow['avgClosureTime'] = $row['avgClosureTime'];
    } else if ($row['type'] === 'Previous Period') {
        $totalPrevRow['avgClosureTime'] = $row['avgClosureTime'];
    }
}


$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $totalVarRow = calcVariance($totalPrevRow,$totalCurrRow);
     
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['actionSite'] = $currRow['actionSite'];
    $varRow['actionRaisedByUser'] = $currRow['actionRaisedByUser'] - $prevRow['actionRaisedByUser'];
    $varRow['actionAssignedTo'] = $currRow['actionAssignedTo'] - $prevRow['actionAssignedTo'];
    $varRow['actionReassignedByUser'] = $currRow['actionReassignedByUser'] - $prevRow['actionReassignedByUser'];
    $varRow['actionReassignedTo'] = $currRow['actionReassignedTo'] - $prevRow['actionReassignedTo'];
    $varRow['actionsClosedByUser'] = $currRow['actionsClosedByUser'] - $prevRow['actionsClosedByUser'];
    $varRow['actionsCancelledByUser'] = $currRow['actionsCancelledByUser'] - $prevRow['actionsCancelledByUser'];
    $varRow['actionsRescheduledByUser'] = $currRow['actionsRescheduledByUser'] - $prevRow['actionsRescheduledByUser'];
    $varRow['actionsOverdue'] = $currRow['actionsOverdue'] - $prevRow['actionsOverdue'];
    $varRow['avgClosureTime'] = secToHR(timeToSeconds($currRow['avgClosureTime']) - timeToSeconds($prevRow['avgClosureTime']));;
    return $varRow;
}

function addInTotal($row, $totalPrevRow) {
    $totalPrevRow['actionRaisedByUser'] += $row['actionRaisedByUser'];
    $totalPrevRow['actionAssignedTo'] += $row['actionAssignedTo'];
    $totalPrevRow['actionReassignedByUser'] += $row['actionReassignedByUser'];
    $totalPrevRow['actionReassignedTo'] += $row['actionReassignedTo'];
    $totalPrevRow['actionsClosedByUser'] += $row['actionsClosedByUser'];
    $totalPrevRow['actionsCancelledByUser'] += $row['actionsCancelledByUser'];
    $totalPrevRow['actionsRescheduledByUser'] += $row['actionsRescheduledByUser'];
    $totalPrevRow['actionsOverdue'] += $row['actionsOverdue'];
    return $totalPrevRow;
}
function timeToSeconds(string $time)
{
    $arr = explode(':', $time);
    if (count($arr) == 3) {
        return $arr[0] * 3600 + $arr[1] * 60 + $arr[2];
    } else {
        return $arr[0] * 60 + $arr[1];
    }
}
function secToHR($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return "$hours:$minutes:$seconds";
}

?>