<?php

session_start();
include ('../config/phpConfig.php');
$filter = '';
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}
if (isset($_GET['siteIds'])) {
    $siteIds = $_GET['siteIds'];
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$emparray = array();

$sql = "select * from " . $mDbName . ".outstandingactions where actionSiteId in (" . $siteIds . ") and actionStatus in ('".$filter."') and updated_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ;";
if($filter === 'All'){
    $sql = "select * from " . $mDbName . ".outstandingactions where actionSiteId in (" . $siteIds . ") and assigned_at between '" . $fromDate . "' and '" . $toDate . " 23:59:59' ;";
}

$mainResult = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainResult)) {
    $curr = $row;
    $actionId = $row['actionId'];
    $rescheduledCnt = 0;
    $rescheduledCntQuery = "select count(*) as rescheduledCnt from " . $mDbName . ".actions, " . $mDbName . ".action_history where actions.id =action_history.action_number and action_history.status = 'RE-SCHEDULED' and action_number=" . $actionId;
    $rescheduledCntData = mysqli_query($connection, $rescheduledCntQuery);
    while ($mInnerRow = mysqli_fetch_assoc($rescheduledCntData)) {
        $rescheduledCnt = $mInnerRow['rescheduledCnt'];
    }
    $curr['rescheduledCnt'] = $rescheduledCnt;

    $date_now = new DateTime($curr['updated_at']);
    $date2 = new DateTime($curr['estimated_completion_date']);
    if ($date_now > $date2) {
        $curr['isLate'] = 'Yes';
    } else {
        $curr['isLate'] = 'No';
    }

    array_walk_recursive($curr, function (&$item) {
        $item = mb_convert_encoding($item, "UTF-8");
    });
    $emparray[] = $curr;
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
?>