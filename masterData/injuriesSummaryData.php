<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}
foreach ($siteIds as &$siteId) {

    $currSql = "select  'Current Period' as type, sum(rightWrist) as rightWrist,  sum(rightHand) as rightHand ,sum(rightFingers ) as rightFingers,  sum(rightForearm) as rightForearm,  sum(rightUpperArm) as rightUpperArm,  sum(rightShoulder) as rightShoulder,sum(leftWrist ) as leftWrist ,sum(leftHand ) as leftHand , sum(leftFingers) as leftFingers,sum(leftForearm ) as leftForearm, sum(leftUpperArm) as leftUpperArm, sum(leftShoulder) as leftShoulder, sum(rightFoot) as rightFoot, sum(rightToes) as rightToes , sum(rightAnkle) as rightAnkle , sum(rightLowerLeg) as rightLowerLeg , sum(rightKnee) as rightKnee,sum(rightThigh ) as rightThigh,  sum(leftFoot) as leftFoot, sum(leftToes) as leftToes, sum(leftAnkle) as leftAnkle , sum(leftLowerLeg) as leftLowerLeg,  sum(leftKnee) as leftKnee, sum(leftThigh)  as leftThigh, sum(waist)  as waist, sum(middleToLowerBack) as  middleToLowerBack, sum(upperBack) as upperBack,sum(chest) as chest,sum(stomach ) as stomach, sum(groin ) as groin, sum(rightEar) as rightEar, sum(rightEye) as rightEye,sum(head ) as head, sum(face) as face, sum(backOfHead) as backOfHead,sum(leftEye) as leftEye,sum(leftEar ) as leftEar,sum(neck) as neck , incidentSite from " . $mDbName . ".injuries_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type, sum(rightWrist) as rightWrist,  sum(rightHand) as rightHand ,sum(rightFingers ) as rightFingers,  sum(rightForearm) as rightForearm,  sum(rightUpperArm) as rightUpperArm,  sum(rightShoulder) as rightShoulder,sum(leftWrist ) as leftWrist ,sum(leftHand ) as leftHand , sum(leftFingers) as leftFingers,sum(leftForearm ) as leftForearm, sum(leftUpperArm) as leftUpperArm, sum(leftShoulder) as leftShoulder, sum(rightFoot) as rightFoot, sum(rightToes) as rightToes , sum(rightAnkle) as rightAnkle , sum(rightLowerLeg) as rightLowerLeg , sum(rightKnee) as rightKnee,sum(rightThigh ) as rightThigh,  sum(leftFoot) as leftFoot, sum(leftToes) as leftToes, sum(leftAnkle) as leftAnkle , sum(leftLowerLeg) as leftLowerLeg,  sum(leftKnee) as leftKnee, sum(leftThigh)  as leftThigh, sum(waist)  as waist, sum(middleToLowerBack) as  middleToLowerBack, sum(upperBack) as upperBack,sum(chest) as chest,sum(stomach ) as stomach, sum(groin ) as groin, sum(rightEar) as rightEar, sum(rightEye) as rightEye,sum(head ) as head, sum(face) as face, sum(backOfHead) as backOfHead,sum(leftEye) as leftEye,sum(leftEar ) as leftEar,sum(neck) as neck , incidentSite from " . $mDbName . ".injuries_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }


    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();

    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {
            $currRow = $row;
            $currRow['upperLimbs'] = $currRow['rightWrist'] + $currRow['rightHand'] + $currRow['rightFingers'] + $currRow['rightForearm'] + $currRow['rightUpperArm'] + $currRow['rightShoulder'] + $currRow['leftWrist'] + $currRow['leftHand'] + $currRow['leftFingers'] + $currRow['leftForearm'] + $currRow['leftUpperArm'] + $currRow['leftShoulder'];
            $currRow['lowerLimbs'] = $currRow['rightFoot'] + $currRow['rightToes'] + $currRow['rightAnkle'] + $currRow['rightLowerLeg'] + $currRow['rightKnee'] + $currRow['rightThigh'] + $currRow['leftFoot'] + $currRow['leftToes'] + $currRow['leftAnkle'] + $currRow['leftLowerLeg'] + $currRow['leftKnee'] + $currRow['leftThigh'];
            $currRow['torso'] = $currRow['waist'] + $currRow['middleToLowerBack'] + $currRow['upperBack'] + $currRow['chest'] + $currRow['stomach'] + $currRow['groin'];
            $currRow['headAndFace'] = $currRow['rightEar'] + $currRow['rightEye'] + $currRow['head'] + $currRow['face'] + $currRow['backOfHead'] + $currRow['leftEye'] + $currRow['leftEar'] + $currRow['neck'];
            $currRow['total'] = $currRow['upperLimbs'] + $currRow['lowerLimbs'] + $currRow['torso'] + $currRow['headAndFace'];
            if (count($totalCurrRow) === 0) {
                $totalCurrRow = $currRow;
            } else {
                $totalCurrRow = addInTotal($currRow, $totalCurrRow);
            }

            $emparray[] = array_map('utf8_encode', $currRow);
        } else if ($row['type'] === 'Previous Period') {
            $prevRow = $row;
            $prevRow['upperLimbs'] = $prevRow['rightWrist'] + $prevRow['rightHand'] + $prevRow['rightFingers'] + $prevRow['rightForearm'] + $prevRow['rightUpperArm'] + $prevRow['rightShoulder'] + $prevRow['leftWrist'] + $prevRow['leftHand'] + $prevRow['leftFingers'] + $prevRow['leftForearm'] + $prevRow['leftUpperArm'] + $prevRow['leftShoulder'];
            $prevRow['lowerLimbs'] = $prevRow['rightFoot'] + $prevRow['rightToes'] + $prevRow['rightAnkle'] + $prevRow['rightLowerLeg'] + $prevRow['rightKnee'] + $prevRow['rightThigh'] + $prevRow['leftFoot'] + $prevRow['leftToes'] + $prevRow['leftAnkle'] + $prevRow['leftLowerLeg'] + $prevRow['leftKnee'] + $prevRow['leftThigh'];
            $prevRow['torso'] = $prevRow['waist'] + $prevRow['middleToLowerBack'] + $prevRow['upperBack'] + $prevRow['chest'] + $prevRow['stomach'] + $prevRow['groin'];
            $prevRow['headAndFace'] = $prevRow['rightEar'] + $prevRow['rightEye'] + $prevRow['head'] + $prevRow['face'] + $prevRow['backOfHead'] + $prevRow['leftEye'] + $prevRow['leftEar'] + $prevRow['neck'];
            $prevRow['total'] = $prevRow['upperLimbs'] + $prevRow['lowerLimbs'] + $prevRow['torso'] + $prevRow['headAndFace'];
            if (count($totalPrevRow) === 0) {
                $totalPrevRow = $prevRow;
            } else {
                $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);
        }
    }
    if (count($prevRow) === 0 && $prevFromDate !== 'None') {
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if (count($totalPrevRow) === 0) {
            $totalPrevRow = $prevRow;
        } else {
            $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        }
    }
    if (count($currRow) === 0) {
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
        if (count($totalCurrRow) === 0) {
            $totalCurrRow = $currRow;
        } else {
            $totalCurrRow = addInTotal($currRow, $totalCurrRow);
        }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {

        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['incidentSite'] = $currRow['incidentSite'];
    $varRow['rightWrist'] = $currRow['rightWrist'] - $prevRow['rightWrist'];
    $varRow['rightHand'] = $currRow['rightHand'] - $prevRow['rightHand'];
    $varRow['rightFingers'] = $currRow['rightFingers'] - $prevRow['rightFingers'];
    $varRow['rightForearm'] = $currRow['rightForearm'] - $prevRow['rightForearm'];
    $varRow['rightUpperArm'] = $currRow['rightUpperArm'] - $prevRow['rightUpperArm'];
    $varRow['rightShoulder'] = $currRow['rightShoulder'] - $prevRow['rightShoulder'];
    $varRow['leftWrist'] = $currRow['leftWrist'] - $prevRow['leftWrist'];
    $varRow['leftHand'] = $currRow['leftHand'] - $prevRow['leftHand'];
    $varRow['leftFingers'] = $currRow['leftFingers'] - $prevRow['leftFingers'];
    $varRow['leftForearm'] = $currRow['leftForearm'] - $prevRow['leftForearm'];
    $varRow['leftUpperArm'] = $currRow['leftUpperArm'] - $prevRow['leftUpperArm'];
    $varRow['leftShoulder'] = $currRow['leftShoulder'] - $prevRow['leftShoulder'];
    $varRow['rightFoot'] = $currRow['rightFoot'] - $prevRow['rightFoot'];
    $varRow['rightToes'] = $currRow['rightToes'] - $prevRow['rightToes'];
    $varRow['rightAnkle'] = $currRow['rightAnkle'] - $prevRow['rightAnkle'];
    $varRow['rightLowerLeg'] = $currRow['rightLowerLeg'] - $prevRow['rightLowerLeg'];
    $varRow['rightKnee'] = $currRow['rightKnee'] - $prevRow['rightKnee'];
    $varRow['rightThigh'] = $currRow['rightThigh'] - $prevRow['rightThigh'];
    $varRow['leftFoot'] = $currRow['leftFoot'] - $prevRow['leftFoot'];
    $varRow['leftToes'] = $currRow['leftToes'] - $prevRow['leftToes'];
    $varRow['leftAnkle'] = $currRow['leftAnkle'] - $prevRow['leftAnkle'];
    $varRow['leftLowerLeg'] = $currRow['leftLowerLeg'] - $prevRow['leftLowerLeg'];
    $varRow['leftKnee'] = $currRow['leftKnee'] - $prevRow['leftKnee'];
    $varRow['leftThigh'] = $currRow['leftThigh'] - $prevRow['leftThigh'];
    $varRow['waist'] = $currRow['waist'] - $prevRow['waist'];
    $varRow['middleToLowerBack'] = $currRow['middleToLowerBack'] - $prevRow['middleToLowerBack'];
    $varRow['upperBack'] = $currRow['upperBack'] - $prevRow['upperBack'];
    $varRow['chest'] = $currRow['chest'] - $prevRow['chest'];
    $varRow['stomach'] = $currRow['stomach'] - $prevRow['stomach'];
    $varRow['groin'] = $currRow['groin'] - $prevRow['groin'];
    $varRow['rightEar'] = $currRow['rightEar'] - $prevRow['rightEar'];
    $varRow['rightEye'] = $currRow['rightEye'] - $prevRow['rightEye'];
    $varRow['head'] = $currRow['head'] - $prevRow['head'];
    $varRow['face'] = $currRow['face'] - $prevRow['face'];
    $varRow['backOfHead'] = $currRow['backOfHead'] - $prevRow['backOfHead'];
    $varRow['leftEye'] = $currRow['leftEye'] - $prevRow['leftEye'];
    $varRow['leftEar'] = $currRow['leftEar'] - $prevRow['leftEar'];
    $varRow['neck'] = $currRow['neck'] - $prevRow['neck'];
    $varRow['upperLimbs'] = $currRow['upperLimbs'] - $prevRow['upperLimbs'];
    $varRow['lowerLimbs'] = $currRow['lowerLimbs'] - $prevRow['lowerLimbs'];
    $varRow['torso'] = $currRow['torso'] - $prevRow['torso'];
    $varRow['headAndFace'] = $currRow['headAndFace'] - $prevRow['headAndFace'];
    $varRow['total'] = $currRow['total'] - $prevRow['total'];
    return $varRow;
}

function addInTotal($row, $totalPrevRow) {
    $totalPrevRow['rightWrist'] += $row['rightWrist'];
    $totalPrevRow['rightHand'] += $row['rightHand'];
    $totalPrevRow['rightFingers'] += $row['rightFingers'];
    $totalPrevRow['rightForearm'] += $row['rightForearm'];
    $totalPrevRow['rightUpperArm'] += $row['rightUpperArm'];
    $totalPrevRow['rightShoulder'] += $row['rightShoulder'];
    $totalPrevRow['leftWrist'] += $row['leftWrist'];
    $totalPrevRow['leftHand'] += $row['leftHand'];
    $totalPrevRow['leftFingers'] += $row['leftFingers'];
    $totalPrevRow['leftForearm'] += $row['leftForearm'];
    $totalPrevRow['leftUpperArm'] += $row['leftUpperArm'];
    $totalPrevRow['leftShoulder'] += $row['leftShoulder'];
    $totalPrevRow['rightFoot'] += $row['rightFoot'];
    $totalPrevRow['rightToes'] += $row['rightToes'];
    $totalPrevRow['rightAnkle'] += $row['rightAnkle'];
    $totalPrevRow['rightLowerLeg'] += $row['rightLowerLeg'];
    $totalPrevRow['rightKnee'] += $row['rightKnee'];
    $totalPrevRow['rightThigh'] += $row['rightThigh'];
    $totalPrevRow['leftFoot'] += $row['leftFoot'];
    $totalPrevRow['leftToes'] += $row['leftToes'];
    $totalPrevRow['leftAnkle'] += $row['leftAnkle'];
    $totalPrevRow['leftLowerLeg'] += $row['leftLowerLeg'];
    $totalPrevRow['leftKnee'] += $row['leftKnee'];
    $totalPrevRow['leftThigh'] += $row['leftThigh'];
    $totalPrevRow['waist'] += $row['waist'];
    $totalPrevRow['middleToLowerBack'] += $row['middleToLowerBack'];
    $totalPrevRow['upperBack'] += $row['upperBack'];
    $totalPrevRow['chest'] += $row['chest'];
    $totalPrevRow['stomach'] += $row['stomach'];
    $totalPrevRow['groin'] += $row['groin'];
    $totalPrevRow['rightEar'] += $row['rightEar'];
    $totalPrevRow['rightEye'] += $row['rightEye'];
    $totalPrevRow['head'] += $row['head'];
    $totalPrevRow['face'] += $row['face'];
    $totalPrevRow['backOfHead'] += $row['backOfHead'];
    $totalPrevRow['leftEye'] += $row['leftEye'];
    $totalPrevRow['leftEar'] += $row['leftEar'];
    $totalPrevRow['neck'] += $row['neck'];
    $totalPrevRow['upperLimbs'] += $row['upperLimbs'];
    $totalPrevRow['lowerLimbs'] += $row['lowerLimbs'];
    $totalPrevRow['torso'] += $row['torso'];
    $totalPrevRow['headAndFace'] += $row['headAndFace'];
    $totalPrevRow['total'] += $row['total'];
    return $totalPrevRow;
}

function createEmptyRow($type, $site) {
    $varRow = array();
    $varRow['type'] = $type;
    $varRow['incidentSite'] = $site;
    $varRow['rightWrist'] = 0;
    $varRow['rightHand'] = 0;
    $varRow['rightFingers'] = 0;
    $varRow['rightForearm'] = 0;
    $varRow['rightUpperArm'] = 0;
    $varRow['rightShoulder'] = 0;
    $varRow['leftWrist'] = 0;
    $varRow['leftHand'] = 0;
    $varRow['leftFingers'] = 0;
    $varRow['leftForearm'] = 0;
    $varRow['leftUpperArm'] = 0;
    $varRow['leftShoulder'] = 0;
    $varRow['rightFoot'] = 0;
    $varRow['rightToes'] = 0;
    $varRow['rightAnkle'] = 0;
    $varRow['rightLowerLeg'] = 0;
    $varRow['rightKnee'] = 0;
    $varRow['rightThigh'] = 0;
    $varRow['leftFoot'] = 0;
    $varRow['leftToes'] = 0;
    $varRow['leftAnkle'] = 0;
    $varRow['leftLowerLeg'] = 0;
    $varRow['leftKnee'] = 0;
    $varRow['leftThigh'] = 0;
    $varRow['waist'] = 0;
    $varRow['middleToLowerBack'] = 0;
    $varRow['upperBack'] = 0;
    $varRow['chest'] = 0;
    $varRow['stomach'] = 0;
    $varRow['groin'] = 0;
    $varRow['rightEar'] = 0;
    $varRow['rightEye'] = 0;
    $varRow['head'] = 0;
    $varRow['face'] = 0;
    $varRow['backOfHead'] = 0;
    $varRow['leftEye'] = 0;
    $varRow['leftEar'] = 0;
    $varRow['neck'] = 0;
    $varRow['upperLimbs'] = 0;
    $varRow['lowerLimbs'] = 0;
    $varRow['torso'] = 0;
    $varRow['headAndFace'] = 0;
    $varRow['total'] = 0;
    return $varRow;
}

?>