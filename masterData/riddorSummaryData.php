<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}
$totalCurrRow['riddorCnt'] = 0;
$totalCurrRow['hseCnt'] = 0;
$totalCurrRow['daysLost'] = 0;
$totalPrevRow['riddorCnt'] = 0;
$totalPrevRow['hseCnt'] = 0;
$totalPrevRow['daysLost'] = 0;
$totalVarRow['riddorCnt'] = 0;
$totalVarRow['hseCnt'] = 0;
$totalVarRow['daysLost'] = 0;

foreach ($siteIds as &$siteId) {
    $prevRow = array();
    $currRow = array();
    $varRow = array();
    $currRow['riddorCnt'] = 0;
    $currRow['hseCnt'] = 0;
    $currRow['daysLost'] = 0;
    $currRow['type'] = 'Current Period';
    $currRow['incidentSite'] = $siteNames[$siteId];

    $prevRow['riddorCnt'] = 0;
    $prevRow['hseCnt'] = 0;
    $prevRow['daysLost'] = 0;
    $prevRow['type'] = 'Previous Period';
    $prevRow['incidentSite'] = $siteNames[$siteId];


    ///Query for RIDDOR CNT
    $currRiddorCntSql = "Select 'Current Period' as type, sum(riddorCnt) as riddorCnt , incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevRiddorCntSql = '';
    $mainRiddorCntSql = $currRiddorCntSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevRiddorCntSql = "Select 'Previous Period' as type,  sum(riddorCnt) as riddorCnt, incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainRiddorCntSql = $currRiddorCntSql . ' UNION ' . $prevRiddorCntSql;
    }
    $mainResult = mysqli_query($connection, $mainRiddorCntSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {

            $currRow['riddorCnt'] = $row['riddorCnt'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['riddorCnt'] = $row['riddorCnt'];
        }
    }


    ///Query for HSE CNT
    $currHSECntSql = "Select 'Current Period' as type, count(incident_number) as hseCnt , incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevHSECntSql = '';
    $mainHSECntSql = $currHSECntSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevHSECntSql = "Select 'Previous Period' as type,  count(incident_number) as hseCnt, incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainHSECntSql = $currHSECntSql . ' UNION ' . $prevHSECntSql;
    }
    $mainResult1 = mysqli_query($connection, $mainHSECntSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult1)) {
        if ($row['type'] === 'Current Period') {

            $currRow['hseCnt'] = $row['hseCnt'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['hseCnt'] = $row['hseCnt'];
        }
    }

    ///Query for Days lost
    $currDLCnt = "Select 'Current Period' as type, sum(days_lost) as daysLost , incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevDLCnt = '';
    $mainDLCnt = $currDLCnt;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevDLCnt = "Select 'Previous Period' as type,  sum(days_lost) as daysLost, incidentSite from " . $mDbName . ".riddor_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainDLCnt = $currDLCnt . ' UNION ' . $prevDLCnt;
    }
    $mainResult2 = mysqli_query($connection, $mainDLCnt) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult2)) {
        if ($row['type'] === 'Current Period') {

            $currRow['daysLost'] = $row['daysLost'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['daysLost'] = $row['daysLost'];
        }
    }
    $emparray[] = array_map('utf8_encode', $currRow);
    $totalCurrRow = addInTotal($currRow, $totalCurrRow);
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $emparray[] = array_map('utf8_encode', $prevRow);
        $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    
    //$emparray[] = array_map('utf8_encode', $varRow);
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}
echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function addInTotal($row, $totalRow) {
    $totalRow['hseCnt'] += $row['hseCnt'];
    $totalRow['riddorCnt'] += $row['riddorCnt'];
    $totalRow['daysLost'] += $row['daysLost'];
    return $totalRow;
}

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['incidentSite'] = $currRow['incidentSite'];
    $varRow['hseCnt'] = $currRow['hseCnt'] - $prevRow['hseCnt'];
    $varRow['riddorCnt'] = $currRow['riddorCnt'] - $prevRow['riddorCnt'];
    $varRow['daysLost'] = $currRow['daysLost'] - $prevRow['daysLost'];
    return $varRow;
}

?>