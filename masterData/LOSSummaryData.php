<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",",$_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from ". $mDbName . ".site where id in (".$_GET['siteIds'].");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
 while ($row = mysqli_fetch_assoc($siteResult)) {
     $siteNames[$row['id']] = $row['code'];
     
 }
 
 
foreach ($siteIds as &$siteId) {
    
       $currSql = "select  'Current Period' as type, sum(lessThan1Week) as lessThan1Week, sum(lessThan1Month) as lessThan1Month, sum(lessThan3Month) as lessThan3Month, sum(lessThan6Month) as lessThan6Month, sum(lessThan1Year) as lessThan1Year, sum(between1And3) as between1And3, sum(moreThan3Years) as moreThan3Years, sum(moreThan5Years) as moreThan5Years, incidentSite from " . $mDbName . ".los_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type, sum(lessThan1Week) as lessThan1Week, sum(lessThan1Month) as lessThan1Month, sum(lessThan3Month) as lessThan3Month, sum(lessThan6Month) as lessThan6Month, sum(lessThan1Year) as lessThan1Year, sum(between1And3) as between1And3, sum(moreThan3Years) as moreThan3Years, sum(moreThan5Years) as moreThan5Years, incidentSite from " . $mDbName . ".los_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }

    
    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();
    
    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if($row['type'] === 'Current Period'){
            $currRow = $row;
            
            $currRow['total'] = $currRow['lessThan1Week'] +   $currRow['lessThan1Month'] +   $currRow['lessThan3Month'] +   $currRow['lessThan6Month'] +   $currRow['lessThan1Year'] +   $currRow['between1And3'] +   $currRow['moreThan3Years'] +   $currRow['moreThan5Years'] ;
            if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
            
            $emparray[] = array_map('utf8_encode', $currRow);
            

        }else if($row['type'] === 'Previous Period'){
            $prevRow = $row;
            $prevRow['total'] = $prevRow['lessThan1Week'] +   $prevRow['lessThan1Month'] +   $prevRow['lessThan3Month'] +   $prevRow['lessThan6Month'] +   $prevRow['lessThan1Year'] +   $prevRow['between1And3'] +   $prevRow['moreThan3Years'] +   $prevRow['moreThan5Years'] ;
             if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);

        }
    }
    if(count($prevRow) ===0 && $prevFromDate !== 'None'){
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
    }
    if(count($currRow ) === 0){
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
         if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {
        
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
    
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow){
   $varRow = array();
   $varRow['type']= 'Variance';
   $varRow['incidentSite']= $currRow['incidentSite'];
    $varRow['lessThan1Week'] = $currRow['lessThan1Week'] - $prevRow['lessThan1Week'];
   $varRow['lessThan1Month'] = $currRow['lessThan1Month'] - $prevRow['lessThan1Month'];
   $varRow['lessThan3Month'] = $currRow['lessThan3Month'] - $prevRow['lessThan3Month'];
   $varRow['lessThan6Month'] = $currRow['lessThan6Month'] - $prevRow['lessThan6Month'];
   $varRow['lessThan1Year'] = $currRow['lessThan1Year'] - $prevRow['lessThan1Year'];
   $varRow['between1And3'] = $currRow['between1And3'] - $prevRow['between1And3'];
   $varRow['moreThan3Years'] = $currRow['moreThan3Years'] - $prevRow['moreThan3Years'];
   $varRow['moreThan5Years'] = $currRow['moreThan5Years'] - $prevRow['moreThan5Years'];
   $varRow['total'] = $currRow['total'] - $prevRow['total'];
   return $varRow;
   
}
function addInTotal($row, $totalPrevRow){
   $totalPrevRow['lessThan1Week'] += $row['lessThan1Week'];
   $totalPrevRow['lessThan1Month'] += $row['lessThan1Month'];
   $totalPrevRow['lessThan3Month'] += $row['lessThan3Month'];
   $totalPrevRow['lessThan6Month'] += $row['lessThan6Month'];
   $totalPrevRow['lessThan1Year'] += $row['lessThan1Year'];
   $totalPrevRow['between1And3'] += $row['between1And3'] ;
   $totalPrevRow['moreThan3Years'] += $row['moreThan3Years'];
   $totalPrevRow['moreThan5Years'] += $row['moreThan5Years'];
   $totalPrevRow['total'] += $row['total'];
   return $totalPrevRow;
}
function createEmptyRow($type, $site){
   $varRow = array();
   $varRow['type']= $type;
   $varRow['incidentSite'] = $site;
   $varRow['lessThan1Week'] = 0;
   $varRow['lessThan1Month'] = 0;
   $varRow['lessThan3Month'] = 0;
   $varRow['lessThan6Month'] = 0;
   $varRow['lessThan1Year'] = 0;
   $varRow['between1And3'] = 0;
   $varRow['moreThan3Years'] = 0;
   $varRow['moreThan5Years'] = 0;
   $varRow['total'] = 0;
   return $varRow;

}
?>