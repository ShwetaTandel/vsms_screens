<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}
foreach ($siteIds as &$siteId) {

    $currSql = "select  'Current Period' as type, sum(burn) as burn ,sum(cuts ) as cuts ,sum(dropped ) as dropped ,sum(handling ) as handling ,sum(nips ) as nips ,sum(slips ) as slips ,sum(struckagainst ) as struckagainst ,sum(struckby ) as struckby ,sum(other ) as other , hazardSite from " . $mDbName . ".hazards_risk_category_summary_report where site_id =" . $siteId . " and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by hazardSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type, sum(burn) as burn ,sum(cuts ) as cuts ,sum(dropped ) as dropped ,sum(handling ) as handling ,sum(nips ) as nips ,sum(slips ) as slips ,sum(struckagainst ) as struckagainst ,sum(struckby ) as struckby ,sum(other ) as other , hazardSite from " . $mDbName . ".hazards_risk_category_summary_report where site_id =" . $siteId . " and hazard_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by hazardSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }


    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();

    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {
            $currRow = $row;
            $currRow['total'] = $currRow['burn'] + $currRow['cuts'] + $currRow['dropped'] + $currRow['handling'] +$currRow['nips'] + $currRow['slips'] + $currRow['struckagainst'] + $currRow['struckby'] +  $currRow['other'];
            if (count($totalCurrRow) === 0) {
                $totalCurrRow = $currRow;
            } else {
                $totalCurrRow = addInTotal($currRow, $totalCurrRow);
            }

            $emparray[] = array_map('utf8_encode', $currRow);
        } else if ($row['type'] === 'Previous Period') {
            $prevRow = $row;
             $prevRow['total'] = $prevRow['burn'] + $prevRow['cuts'] + $prevRow['dropped'] + $prevRow['handling'] +$prevRow['nips'] + $prevRow['slips'] + $prevRow['struckagainst'] + $prevRow['struckby'] +  $prevRow['other'];
            if (count($totalPrevRow) === 0) {
                $totalPrevRow = $prevRow;
            } else {
                $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);
        }
    }
    if (count($prevRow) === 0 && $prevFromDate !== 'None') {
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if (count($totalPrevRow) === 0) {
            $totalPrevRow = $prevRow;
        } else {
            $totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        }
    }
    if (count($currRow) === 0) {
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
        if (count($totalCurrRow) === 0) {
            $totalCurrRow = $currRow;
        } else {
            $totalCurrRow = addInTotal($currRow, $totalCurrRow);
        }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {

        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}
$totalCurrRow['hazardSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['hazardSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['hazardSite'] = $currRow['hazardSite'];
    $varRow['burn'] = $currRow['burn'] - $prevRow['burn'];
    $varRow['cuts'] = $currRow['cuts'] - $prevRow['cuts'];
    $varRow['dropped'] = $currRow['dropped'] - $prevRow['dropped'];
    $varRow['handling'] = $currRow['handling'] - $prevRow['handling'];
    $varRow['nips'] = $currRow['nips'] - $prevRow['nips'];
    $varRow['slips'] = $currRow['slips'] - $prevRow['slips'];
    $varRow['struckagainst'] = $currRow['struckagainst'] - $prevRow['struckagainst'];
    $varRow['struckby'] = $currRow['struckby'] - $prevRow['struckby'];
    $varRow['other'] = $currRow['other'] - $prevRow['other'];
    $varRow['total'] = $currRow['total'] - $prevRow['total'];
    return $varRow;
}

function addInTotal($row, $totalPrevRow) {
    $totalPrevRow['burn'] += $row['burn'];
    $totalPrevRow['cuts'] += $row['cuts'];
    $totalPrevRow['dropped'] += $row['dropped'];
    $totalPrevRow['handling'] += $row['handling'];
    $totalPrevRow['nips'] += $row['nips'];
    $totalPrevRow['slips'] += $row['slips'];
    $totalPrevRow['struckagainst'] += $row['struckagainst'];
    $totalPrevRow['struckby'] += $row['struckby'];
    $totalPrevRow['other'] += $row['other'];
    $totalPrevRow['total'] += $row['total'];
    return $totalPrevRow;
}

function createEmptyRow($type, $site) {
    $varRow = array();
    $varRow['type'] = $type;
    $varRow['hazardSite'] = $site;
    $varRow['burn'] = 0;
    $varRow['cuts'] = 0;
    $varRow['dropped'] = 0;
    $varRow['handling'] = 0;
    $varRow['nips'] = 0;
    $varRow['slips'] = 0;
    $varRow['struckagainst'] = 0;
    $varRow['struckby'] = 0;
    $varRow['other'] = 0;
    $varRow['total'] = 0;
    return $varRow;
}

?>