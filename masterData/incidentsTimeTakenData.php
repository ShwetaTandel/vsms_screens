<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePHP.php');

if (isset($_GET['siteIds'])) {
    $siteIds = $_GET['siteIds'];
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$sql = "SELECT  * FROM " . $mDbName . ".incident where site_id in (" . $siteIds . ") and incident.status not in( '_DELETED') and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incident.incident_number order by incident_number desc";
ChromePhp::log($sql);
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
//create an array
$emparray = array();
while ($row = mysqli_fetch_assoc($result)) {
    $curr = $row;
    $incidentId = $row['incident_number'];
    $incidentReportedTime = '';
    $incidentReportedBy = '';
    $incidentReported = "SELECT TIME_FORMAT(incidentReported, '%H:%i') as incidentReported, updated_by FROM " . $mDbName . ".proc_time_incident_reported where incident_number =" . $incidentId;
    $incidentReportedData = mysqli_query($connection, $incidentReported);
    while ($mInnerRow = mysqli_fetch_assoc($incidentReportedData)) {
        $incidentReportedTime = $mInnerRow['incidentReported'];
        $incidentReportedBy = $mInnerRow['updated_by'];
    }
    $curr['incidentReportedTime'] = $incidentReportedTime;
    $curr['incidentReportedBy'] = $incidentReportedBy;
    
    $irReportedTime = '';
    $irReportedBy = '';
    $irReported = "SELECT TIME_FORMAT(initialReportSubmitted, '%H:%i') as initialReportSubmitted, updated_by FROM " . $mDbName . ".proc_time_ir_submitted where incident_number =" . $incidentId;
    $irReportedData = mysqli_query($connection, $irReported);
    while ($mInnerRow = mysqli_fetch_assoc($irReportedData)) {
        $irReportedTime = $mInnerRow['initialReportSubmitted'];
        $irReportedBy       =$mInnerRow['updated_by'];
    }
    $curr['irReportedTime'] = $irReportedTime;
    $curr['irReportedBy'] = $irReportedBy;
    
    $irApprovedTime = '';
    $irApprovedBy = '';
    $irApproved = "SELECT TIME_FORMAT(initialReporApproved, '%H:%i') as initialReporApproved, updated_by FROM " . $mDbName . ".proc_time_ir_approved where incident_number =" . $incidentId;
    $irApprovedData = mysqli_query($connection, $irApproved);
    while ($mInnerRow = mysqli_fetch_assoc($irApprovedData)) {
        $irApprovedTime = $mInnerRow['initialReporApproved'] ;
        $irApprovedBy = $mInnerRow['updated_by'];
    }
    $curr['irApprovedTime'] = $irApprovedTime;
     $curr['irApprovedBy'] = $irApprovedBy;
    
    $fiSubmittedTime = '';
    $fiSubmittedBy = '';
    $fiSubmitted = "SELECT TIME_FORMAT(fiSubmitted, '%H:%i') as fiSubmitted, updated_by FROM " . $mDbName . ".proc_time_fi_submitted where incident_number =" . $incidentId;
    $fiSubmittedData = mysqli_query($connection, $fiSubmitted);
    while ($mInnerRow = mysqli_fetch_assoc($fiSubmittedData)) {
        $fiSubmittedTime = $mInnerRow['fiSubmitted'];
        $fiSubmittedBy= $mInnerRow['updated_by'];
    }
    $curr['fiSubmittedTime'] = $fiSubmittedTime;
      $curr['fiSubmittedBy'] = $fiSubmittedBy;
    
    $fiApprovedTime = '';
    $fiApprovedBy = '';
    $fiApproved = "SELECT TIME_FORMAT(fiApproved, '%H:%i') as fiApproved, updated_by FROM " . $mDbName . ".proc_time_fi_approved where incident_number =" . $incidentId;
    $fiApprovedData = mysqli_query($connection, $fiApproved);
    while ($mInnerRow = mysqli_fetch_assoc($fiApprovedData)) {
        $fiApprovedTime = $mInnerRow['fiApproved'];
        $fiApprovedBy = $mInnerRow['updated_by'];
    }
    $curr['fiApprovedTime'] = $fiApprovedTime;
    $curr['fiApprovedBy'] = $fiApprovedBy;
    
    $incidentClosedTime = '';
     $incidentClosedBy = '';
    $incidentClosed = "SELECT TIME_FORMAT(incidentClosed, '%H:%i') as incidentClosed, updated_by FROM " . $mDbName . ".proc_time_incident_closed where incident_number =" . $incidentId;
    $incidentClosedData = mysqli_query($connection, $incidentClosed);
    while ($mInnerRow = mysqli_fetch_assoc($incidentClosedData)) {
        $incidentClosedTime = $mInnerRow['incidentClosed'];
        $incidentClosedBy = $mInnerRow['updated_by'];
    }
    $curr['incidentClosedTime'] = $incidentClosedTime;
     $curr['incidentClosedBy'] = $incidentClosedBy;
    array_walk_recursive($curr, function (&$item) {
        $item = mb_convert_encoding($item, "UTF-8");
    });

    $emparray[] = $curr;
}
echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
?>