<?php

session_start();
include ('../config/phpConfig.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",",$_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();
$sitesql = "select id, code from ". $mDbName . ".site where id in (".$_GET['siteIds'].");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
 while ($row = mysqli_fetch_assoc($siteResult)) {
     $siteNames[$row['id']] = $row['code'];
     
 }
 
 
foreach ($siteIds as &$siteId) {
    
       $currSql = "select  'Current Period' as type, sum(early) as early, sum(midday) as midday, sum(eve) as eve, sum(night) as night, incidentSite from " . $mDbName . ".time_of_day_summary_report where site_id =" . $siteId . " and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incidentSite ";
    $prevSql = '';
    $mainSql = $currSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevSql = "select 'Previous Period' as type, sum(early) as early, sum(midday) as midday, sum(eve) as eve, sum(night) as night,   incidentSite from " . $mDbName . ".time_of_day_summary_report where site_id =" . $siteId . " and incident_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by incidentSite ";
        $mainSql = $currSql . ' UNION ' . $prevSql;
    }

    
    $mainResult = mysqli_query($connection, $mainSql) or die("Error in Selecting " . mysqli_error($connection));
    $prevRow = array();
    $currRow = array();
    
    $varRow = array();
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if($row['type'] === 'Current Period'){
            $currRow = $row;
            
            $currRow['total'] = $currRow['early'] +   $currRow['midday'] +   $currRow['eve'] +   $currRow['night'] ;
            if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
            
            $emparray[] = array_map('utf8_encode', $currRow);
            

        }else if($row['type'] === 'Previous Period'){
            $prevRow = $row;
            $prevRow['total'] = $prevRow['early'] +   $prevRow['midday'] +   $prevRow['eve'] +   $prevRow['night'];
             if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
            $emparray[] = array_map('utf8_encode', $prevRow);

        }
    }
    if(count($prevRow) ===0 && $prevFromDate !== 'None'){
        $prevRow = createEmptyRow('Previous Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $prevRow);
        if(count($totalPrevRow) === 0){
                $totalPrevRow = $prevRow;
            }else{
                $totalPrevRow = addInTotal($prevRow,$totalPrevRow);
            }
    }
    if(count($currRow ) === 0){
        $currRow = createEmptyRow('Current Period', $siteNames[$siteId]);
        $emparray[] = array_map('utf8_encode', $currRow);
         if(count($totalCurrRow) === 0){
                $totalCurrRow = $currRow;
            }else{
                $totalCurrRow = addInTotal($currRow,$totalCurrRow);
            }
    }
    if (count($prevRow) != 0 && count($currRow) != 0) {
        
        $varRow = calcVariance($prevRow, $currRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
    
}
$totalCurrRow['incidentSite'] = "Total";
$totalCurrRow['type'] = "Current Period";
$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None') {
    $totalPrevRow['incidentSite'] = "Total";
    $totalPrevRow['type'] = "Previous Period";
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow){
   $varRow = array();
   $varRow['type']= 'Variance';
   $varRow['incidentSite']= $currRow['incidentSite'];
    $varRow['early'] = $currRow['early'] - $prevRow['early'];
   $varRow['midday'] = $currRow['midday'] - $prevRow['midday'];
   $varRow['eve'] = $currRow['eve'] - $prevRow['eve'];
   $varRow['night'] = $currRow['night'] - $prevRow['night'];
   
   $varRow['total'] = $currRow['total'] - $prevRow['total'];
   return $varRow;
   
}
function addInTotal($row, $totalPrevRow){
   $totalPrevRow['early'] += $row['early'];
   $totalPrevRow['midday'] += $row['midday'];
   $totalPrevRow['eve'] += $row['eve'];
   $totalPrevRow['night'] += $row['night'];
   
   $totalPrevRow['total'] += $row['total'];
   return $totalPrevRow;
}
function createEmptyRow($type, $site){
   $varRow = array();
   $varRow['type']= $type;
   $varRow['incidentSite'] = $site;
   $varRow['early'] = 0;
   $varRow['midday'] = 0;
   $varRow['eve'] = 0;
   $varRow['night'] = 0;
   
   $varRow['total'] = 0;
   return $varRow;

}
?>