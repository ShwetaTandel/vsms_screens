<?php

include ('../config/phpConfig.php');
$data = $_GET['data'];
if ($data == 'users') {
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".users order by last_name, first_name asc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'site') {
    $userId = $_GET['userId'];
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".users_sites where user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'approvers') {
    $userId = $_GET['userId'];
    //fetch table rows from mysql db
    $sql = "SELECT * FROM " . $mDbName . ".approvers where is_active =true and user_id=" . $userId . ";";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
} elseif ($data === 'markinactive') {
    $userId = $_GET['userId'];
    $sql1 = "UPDATE " . $mDbName . ".users set active = false where id=" . $userId . ";";
    $result1 = mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));
    
    $sql2 = "UPDATE " . $mDbName . ".approvers set is_active = false where user_id=" . $userId . ";";
    $result2 = mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));

     $sql3 = "UPDATE " . $mDbName . ".recipients set is_active = false where user_id=" . $userId . ";";
    $result3 = mysqli_query($connection, $sql3) or die("Error in Selecting " . mysqli_error($connection));

    echo 'OK';
}elseif ($data === 'removerecipients') {
    $siteId = $_GET['siteId'];
    $userIds = $_GET['userIds'];
    //delete rows from mysql db

    $sql = "DELETE  FROM " . $mDbName . ".recipients where id in (" . $userIds . ");";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    echo $sql.'OK';
}elseif ($data === 'changepwd') {
    $pwd = $_GET['pwd'];
    $userName = $_GET['username'];
    
    $sql= "UPDATE ".$mDbName. ".users set password='".crypt($pwd, 'PASSWORD_DEFAULT')."', updated_at ='".date ('Y-m-d H:i:s')."' where email_id='".$userName."';";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    
    if($result === TRUE){
        echo "OK";
    }
}elseif ($data === 'siteapprovers') {
    $siteId = $_GET['siteId'];
    
   // $sql = "SELECT * FROM " . $mDbName . ".users, ".$mDbName.".users_sites where users_sites.user_id = users.id and site_id = ".$siteId." and is_approver = true and active=true";
     $sql = "SELECT * FROM " . $mDbName . ".users, ".$mDbName.".approvers where approvers.user_id = users.id and site_id = ".$siteId." and is_active = true and active=true";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
}elseif ($data === 'siterecipients') {
    $siteId = $_GET['siteId'];
     $group = $_GET['group'];
    
    $sql = "SELECT * FROM " . $mDbName . ".users, ".$mDbName.".recipients where recipients.user_id = users.id and site_id = ".$siteId." and is_active = true and group_name = '".$group."';";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
}/*elseif ($data === 'actionowners') {
    $siteId = $_GET['siteId'];
    
    $sql = "SELECT * FROM " . $mDbName . ".users, ".$mDbName.".users_sites where users_sites.user_id = users.id and site_id = ".$siteId." and is_action_owner = true";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    echo json_encode($emparray);
}*/
elseif($data ==='actionowners'){
    $sql = "SELECT * FROM " . $mDbName . ".site";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $sites = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['id'] = $row['id'];
        $curr['code'] = $row['code'];
        $siteId = $curr['id'];
        $owners = array();
        $mDetailQuery = "SELECT users.id, first_name, last_name, manager_name FROM " . $mDbName . ".users, ".$mDbName.".users_sites where users_sites.user_id = users.id and site_id = ".$siteId." and is_action_owner = true";
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $owners[] = $mInnerRow;
        }
        $curr['owners'] = $owners;
        $sites[] = $curr;
    }
    echo json_encode($sites);
}

//close the db connection
mysqli_close($connection);
?>