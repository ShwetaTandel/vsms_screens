<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
$userId = $_SESSION['vsmsUserData']['id'];
$sitedIds = join(",", $_SESSION['vsmsUserData']['sites']);
ChromePhp::log($_SESSION['vsmsUserData']['sites']);
$filter = $_GET['filter'];
$hid =0;
if(isset($_GET['hid'])){
    $hid =    $_GET['hid']; 
}
//fetch table rows from mysql db

if ($filter === 'SITE') {
    $sql = "SELECT hazard.* , display_status,code, description, concat(first_name, ' ', last_name) as approver FROM " . $mDbName . ".hazard join " . $mDbName . ".site on  hazard.site_id = site.id join " . $mDbName . ".hazard_status on hazard.status = hazard_status.status left outer join " . $mDbName . ".users on users.id = hazard.curr_approver_id where hazard.status!='_DELETED' and site_id in (" . $sitedIds . ") order by hazard_number desc;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
} else if ($filter === 'HAZARD') {
    $sql = "SELECT hazard.*, site.code,concat(first_name,' ', last_name) as approver_name FROM " . $mDbName . ".hazard," . $mDbName . ".site , " . $mDbName . ".users where hazard.site_id = site.id and users.id = hazard.curr_approver_id and hazard_number = " . $hid;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {

        $curr = $row;
        $photos = array();
        $counterMeasures = array();
        $allActions = array();
        $files = array();
        $mDetailsQuery1 = "SELECT * FROM " . $mDbName . ".hazard_photo_files where hazard_id =" . $hid;
        $mDetailData1 = mysqli_query($connection, $mDetailsQuery1);
        while ($mInnerRow1 = mysqli_fetch_assoc($mDetailData1)) {
            $photos[] = $mInnerRow1;
        }
        
        $mDetailsQuery2 = "SELECT * FROM " . $mDbName . ".actions where action_type = 'Hazard-Countermeasure' and action_source='Hazard' and report_id =" . $hid;
        $mDetailData2 = mysqli_query($connection, $mDetailsQuery2);
        while ($mInnerRow2 = mysqli_fetch_assoc($mDetailData2)) {
            $counterMeasures[] = $mInnerRow2;
        }
        
        $mDetailsQuery3 = "SELECT concat('AC',actions.id) as id, action_type as category, action as description , code as site, concat(first_name, ' ', last_name) as owner, action_status.description as status, deadline FROM " . $mDbName . ".actions, " . $mDbName . ".action_status, " . $mDbName . ".site, " . $mDbName . ".users where actions.status = action_status.status and   actions.action_owner = users.id and actions.action_site = site.id and action_source='Hazard' and report_id =" . $hid;
        $mDetailData3 = mysqli_query($connection, $mDetailsQuery3);
        while ($mInnerRow3 = mysqli_fetch_assoc($mDetailData3)) {
            $allActions[] = $mInnerRow3;
        }
        
         $mDetailsQuery4 = "SELECT * FROM " . $mDbName . ".hazard_other_files where hazard_id =" . $hid;
        $mDetailData4 = mysqli_query($connection, $mDetailsQuery4);
        while ($mInnerRow4 = mysqli_fetch_assoc($mDetailData4)) {
            $files[] = $mInnerRow4;
        }
        $curr['counterMeasures'] = $counterMeasures;
         $curr['photos'] = $photos;
         $curr['allActions'] = $allActions;
           $curr['files'] = $files;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
}else if ($filter === 'APPROVER') {
    $userId = $_GET['approverId'];
    $sql = "SELECT  hazard.*, code, description FROM " . $mDbName . ".site  join " . $mDbName . ".hazard  on hazard.site_id = site.id join " . $mDbName . ".hazard_status  on hazard.status = hazard_status.status where curr_approver_id = " . $userId . " and hazard.status in('L1_SUBMITTED', 'L2_SAVED') order by hazard_date desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
}

echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>