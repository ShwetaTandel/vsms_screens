<?php

if (isset($_SESSION['vsmsUserData'])) {
    $siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
}
function authenticateHazardIps($ip) {
    
    global $mDbName, $connection;
    $sql = "SELECT * FROM " . $mDbName . ".hazard_tabs_ips where ip_address = '".$ip."' Limit 1";
    $securityToken = "";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
      $data = array();
    //$flag = false;
    while ($row = mysqli_fetch_assoc($result)) {
      //  $flag = true;
        $data[] = $row;
        $securityToken = encrypt($ip. date("Ymd"),"tHeApAcHe6410111");
        $sqUpdateToken = "UPDATE " . $mDbName . ".hazard_tabs_ips set security_token = '".$securityToken."' where ip_address = '".$ip."'";
        mysqli_query($connection, $sqUpdateToken) or die("Error in Selecting " . mysqli_error($connection));
        $data['security_token'] = $securityToken;
    }
   
    return $data;
}
function encrypt($data, $key) {
    return base64_encode(openssl_encrypt($data, "aes-128-ecb", $key, OPENSSL_RAW_DATA));
}

//function getSecurityToken($data){
//    $key  = 'ABC';
//    //$data = '1234567';
//    $alg  = MCRYPT_BLOWFISH;
//    $mode = MCRYPT_MODE_ECB;
//    $blocksize = mcrypt_get_block_size('blowfish', 'ecb'); // get block size 
//    $pkcs = $blocksize - (strlen($data) % $blocksize); // get pkcs5 pad length 
//    $data.= str_repeat(chr($pkcs), $pkcs);
//    $encrypted_data = mcrypt_encrypt($alg, $key, $data, $mode);
//    $phpgeneratedtoken  = base64_encode($encrypted_data);
//    ChromePhp::log($phpgeneratedtoken);
//    return $phpgeneratedtoken;
//}
function getActionOwners() {
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT * FROM " . $mDbName . ".site";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $sites = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['id'] = $row['id'];
        $curr['code'] = $row['code'];
        $siteId = $curr['id'];
        $owners = array();
        $mDetailQuery = "SELECT users.id, first_name, last_name, manager_name FROM " . $mDbName . ".users, " . $mDbName . ".users_sites where users_sites.user_id = users.id and site_id = " . $siteId . " and is_action_owner = true and active=true order by first_name asc";
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $owners[] = $mInnerRow;
        }
        $curr['owners'] = $owners;
        $sites[] = $curr;
    }
    return $sites;
}

function getSiteApprovers($siteId, $level, $userId) {
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT user_id, first_name, last_name FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  level = '" . $level . "' and site_id = " . $siteId . " and active = true and is_active=true ";//and users.id!=" .$userId;
    ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $approvers = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $approvers[] = $row;
    }

    return $approvers;
}

function getSiteApproversForHazard() {
    // Load Hazard approvers
    global $mDbName, $connection;
    $sql = "SELECT * FROM " . $mDbName . ".site";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $sites = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['id'] = $row['id'];
        $curr['code'] = $row['code'];
        $siteId = $curr['id'];
        $approvers = array();
        $mDetailQuery = "SELECT user_id, first_name, last_name,level,site_id FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  site_id = " . $siteId . " and level in ('L2') and active = true and is_active=true";
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $approvers[] = $mInnerRow;
        }
        $curr['approvers'] = $approvers;
        $sites[] = $curr;
    }
    return $sites;
}
function getSiteApproversForGRA() {
    // Load GRA  approvers
    global $mDbName, $connection;
    $sql = "SELECT * FROM " . $mDbName . ".site";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $sites = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = array();
        $curr['id'] = $row['id'];
        $curr['code'] = $row['code'];
        $siteId = $curr['id'];
        $approvers = array();
        $mDetailQuery = "SELECT user_id, first_name, last_name,level,site_id, level FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  site_id = " . $siteId . " and active = true and is_active=true";
        $mDetailData = mysqli_query($connection, $mDetailQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mDetailData)) {
            $approvers[] = $mInnerRow;
        }
        $curr['approvers'] = $approvers;
        $sites[] = $curr;
    }
    return $sites;
}

function getPreviousApprover($incidentId) {
    // Load action owners
    global $mDbName, $connection;
   // $sql = "SELECT user_id, first_name, last_name,level,site_id FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  site_id = " . $siteId . " and active = true and is_active=true group by site_id";
    $sql = "SELECT updated_by_user_id as preApproverId FROM " . $mDbName . ".incident_history," . $mDbName . ".users where incident_number = " . $incidentId . " and status = 'APPROVED'  and users.id = incident_history.updated_by_user_id order by incident_history.id desc limit 1";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $approverId = "";
    while ($row = mysqli_fetch_assoc($result)) {
        $approverId = $row['preApproverId'];
    }
    ChromePhp::log($approverId);
    return $approverId;
}


function getApproversBySite($siteId) {
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT user_id, first_name, last_name,level,site_id FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  site_id = " . $siteId . " and active = true and is_active=true group by site_id";
    ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $approvers = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $approvers[] = $row;
    }

    return $approvers;
}

function getPendingIncidentsCount($approverId) {
    // Load action owners
    global $mDbName, $connection;
    $cnt = 0;
     $emparray = array();
    $sql = "SELECT  count(*) as cnt FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  where curr_approver_id = " . $approverId . " and status in('L2_SUBMITTED', 'L2_EDITED', 'L3_EDITED','L3_SUBMITTED' , 'L3_APPROVED', 'L4_SAVED', 'L4_REJECTED') ";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
  
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row['cnt'];
    }
    
    $sql1 = "SELECT  count(*) as cnt FROM " . $mDbName . ".site  join " . $mDbName . ".hazard  on hazard.site_id = site.id  where curr_approver_id = " . $approverId . " and status in('L1_SUBMITTED', 'L2_SAVED') ";
    $result1 = mysqli_query($connection, $sql1) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
   
    while ($row = mysqli_fetch_assoc($result1)) {
        $emparray[]= $row['cnt'];
    }
    
     $sql2 = "SELECT  count(*) as cnt FROM " . $mDbName . ".site  join " . $mDbName . ".risk_assessment  on risk_assessment.site_id = site.id  where curr_approver_id = " . $approverId . " and status in('L2_SUBMITTED', 'L2_EDITED', 'L3_EDITED','L2_APPROVED' , 'L3_APPROVED', 'L4_EDITED') ";
     $result2 = mysqli_query($connection, $sql2) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
   
    while ($row = mysqli_fetch_assoc($result2)) {
        $emparray[]= $row['cnt'];
    }
 
    
    return $emparray;
}

function getOutstandingActions($ownerId) {
    // Load action owners
    global $mDbName, $connection;
    $sql = "SELECT count(*) as cnt FROM " . $mDbName . ".outstandingactions where action_owner =  " . $ownerId . " and actionStatus in ('ASSIGNED' , 'RE-ASSIGNED','RE-OPENED','RE-SCHEDULED') ORDER BY estimated_completion_date asc;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row['cnt'];
    }
    return $emparray;
}

function getLastIncidentDays() {
    // Load action owners
    global $mDbName, $connection, $siteIds;
    $sql = "select DATEDIFF( now(),incident_date) as cnt from " . $mDbName . ".incident where status!='_DELETED' and site_id in (" . $siteIds . ") order by incident_date desc Limit 1";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row['cnt'];
    }
    return $emparray;
}

function getUserLevels($userId) {
    // Load action owners
    global $mDbName, $connection;

    $sql = "select site_id, level from " . $mDbName . ".approvers where user_id=" . $userId . " and is_active = true group by site_id, level order by site_id;";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
    return $emparray;
}

function getUserSitesLevel($userId) {
    // Load action owners
    global $mDbName, $connection;

    $sql = "SELECT code, level FROM " . $mDbName . ".users,  " . $mDbName . ".site, " . $mDbName . ".approvers where approvers.user_id=users.id and approvers.site_id= site.id and users.id =" . $userId . " and users.active = true and is_active = true ";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    $temp = "";
    while ($row = mysqli_fetch_assoc($result)) {
        $temp = $row['code'] . " - " . $row['level'];
        $emparray[] = $temp;
    }

    return implode(',', $emparray);
}

function getMaxUserLevel($userId) {
    // Load action owners
    global $mDbName, $connection, $siteIds;


    $sql = "select max(level) as maxLevel from users, approvers where is_active=true and users.id= approvers.user_id  and site_id in (" . $siteIds . ")  and user_id =" . $userId;
    $maxLevel = '';
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $maxLevel = $row['maxLevel'];
    }
    return $maxLevel;
}

function getHaradsReportedThisMonth() {
    // Load action owners
    global $mDbName, $connection, $siteIds;
    //  $sql = "SELECT count(*) as cnt, EXTRACT( YEAR_MONTH FROM incident_date ) as incidentMonth, EXTRACT( YEAR_MONTH FROM now() ) as currentmonth FROM ". $mDbName . ".incident where status!='_DELETED' and site_id in (".$siteIds.") GROUP BY EXTRACT( YEAR_MONTH FROM incident_date ) ORDER BY incidentMonth desc limit 2";
    $sql = "SELECT count(*) as cnt FROM " . $mDbName . ".hazard WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and YEAR(hazard_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(hazard_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) "
            . "Union SELECT count(*) as cnt FROM  " . $mDbName . ".hazard WHERE status!='_DELETED' and site_id in (" . $siteIds . ") AND YEAR(hazard_date) = YEAR(CURRENT_DATE) AND MONTH(hazard_date) = MONTH(CURRENT_DATE);";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    $hazardsThisMonth = 0;
    $hazardsLastMonth = 0;
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        if ($i == 0) {
            $hazardsLastMonth = $row['cnt'];
        } else {
            $hazardsThisMonth = $row['cnt'];
        }
        $i++;
    }
    if ($i === 1) {
        $emparray [] = $hazardsLastMonth;
        $emparray [] = $hazardsLastMonth;
    } else {
        $emparray [] = $hazardsThisMonth;
        $emparray [] = $hazardsLastMonth;
    }

    return $emparray;
}

function getIncidentOccurredThisMonth() {
    // Load action owners
    global $mDbName, $connection, $siteIds;
    //  $sql = "SELECT count(*) as cnt, EXTRACT( YEAR_MONTH FROM incident_date ) as incidentMonth, EXTRACT( YEAR_MONTH FROM now() ) as currentmonth FROM ". $mDbName . ".incident where status!='_DELETED' and site_id in (".$siteIds.") GROUP BY EXTRACT( YEAR_MONTH FROM incident_date ) ORDER BY incidentMonth desc limit 2";
    $sql = "SELECT count(*) as cnt FROM " . $mDbName . ".incident WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and YEAR(incident_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(incident_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) "
            . "Union SELECT count(*) as cnt FROM  " . $mDbName . ".incident WHERE status!='_DELETED' and site_id in (" . $siteIds . ") AND YEAR(incident_date) = YEAR(CURRENT_DATE) AND MONTH(incident_date) = MONTH(CURRENT_DATE);";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    $incidentThisMonth = 0;
    $incidentLastMonth = 0;
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        if ($i == 0) {
            $incidentLastMonth = $row['cnt'];
        } else {
            $incidentThisMonth = $row['cnt'];
        }
        $i++;
    }
    if ($i == 1) {
        $emparray [] = $incidentLastMonth;
        $emparray [] = $incidentLastMonth;
    } else {
        $emparray [] = $incidentThisMonth;
        $emparray [] = $incidentLastMonth;
    }
    return $emparray;
}

function getGRAThisMonth(){
    // Load action owners
    global $mDbName, $connection, $siteIds;
    //$sql = "SELECT count(*) as cnt, EXTRACT( YEAR_MONTH FROM incident_date ) as incidentMonth, EXTRACT( YEAR_MONTH FROM now() ) as currentmonth FROM ". $mDbName . ".incident where status!='_DELETED' and site_id in (".$siteIds.") and incident_type in('Serious Accident', 'Accident', 'Accident*') GROUP BY EXTRACT( YEAR_MONTH FROM incident_date ) ORDER BY incidentMonth desc limit 2";

    $sql = "SELECT count(*) as cnt FROM " . $mDbName . ".risk_assessment WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and YEAR(gra_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(gra_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) "
            . "Union SELECT count(*) as cnt FROM  " . $mDbName . ".risk_assessment WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and YEAR(gra_date) = YEAR(CURRENT_DATE) AND MONTH(gra_date) = MONTH(CURRENT_DATE);";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    $graThisMonth = 0;
    $graLastMonth = 0;
    $i = 0;

    while ($row = mysqli_fetch_assoc($result)) {
        if ($i == 0) {
            $graLastMonth = $row['cnt'];
        } else {
            $graThisMonth = $row['cnt'];
        }
        $i++;
    }
    if ($i == 1) {
        $emparray [] = $graLastMonth;
        $emparray [] = $graLastMonth;
    } else {
        $emparray [] = $graThisMonth;
        $emparray [] = $graLastMonth;
    }
    return $emparray;
}

function getInjuriestOccurredThisMonth() {
    // Load action owners
    global $mDbName, $connection, $siteIds;
    //$sql = "SELECT count(*) as cnt, EXTRACT( YEAR_MONTH FROM incident_date ) as incidentMonth, EXTRACT( YEAR_MONTH FROM now() ) as currentmonth FROM ". $mDbName . ".incident where status!='_DELETED' and site_id in (".$siteIds.") and incident_type in('Serious Accident', 'Accident', 'Accident*') GROUP BY EXTRACT( YEAR_MONTH FROM incident_date ) ORDER BY incidentMonth desc limit 2";

    $sql = "SELECT count(*) as cnt FROM " . $mDbName . ".incident WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and incident_type in('Serious Accident', 'Accident', 'Accident*') and YEAR(incident_date) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(incident_date) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) "
            . "Union SELECT count(*) as cnt FROM  " . $mDbName . ".incident WHERE status!='_DELETED' and site_id in (" . $siteIds . ") and incident_type in('Serious Accident', 'Accident', 'Accident*') AND YEAR(incident_date) = YEAR(CURRENT_DATE) AND MONTH(incident_date) = MONTH(CURRENT_DATE);";

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    $injuriesThisMonth = 0;
    $injuriesLastMonth = 0;
    $i = 0;

    while ($row = mysqli_fetch_assoc($result)) {
        if ($i == 0) {
            $injuriesLastMonth = $row['cnt'];
        } else {
            $injuriesThisMonth = $row['cnt'];
        }
        $i++;
    }
    if ($i == 1) {
        $emparray [] = $injuriesLastMonth;
        $emparray [] = $injuriesLastMonth;
    } else {
        $emparray [] = $injuriesThisMonth;
        $emparray [] = $injuriesLastMonth;
    }
    return $emparray;
}
