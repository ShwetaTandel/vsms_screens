<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
$filter = $_GET['filter'];
$siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
$siteNames ='';
if (isset($_GET['siteIds'])) {
    $siteIds = $_GET['siteIds'];
}
if (isset($_GET['siteNames'])) {
    $siteNames = explode(',', $_GET['siteNames']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}

$specialResult = false;
//fetch table rows from mysql db

if ($filter === 'PENDINGAPPROVAL') {
    $sql = "SELECT  incident.incident_number, site_id, code,location, incident_date,incident_type,incident.status,display_status,incident_status.description as statusDesc, concat(first_name,' ', last_name) as approver_name, curr_approver_id, incident.updated_at, incident.updated_by, '' as isOverdue, submit_by,fi_submit_by, gm_approval_by FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  left outer join  " . $mDbName . ".users on incident.curr_approver_id = users.id  join " . $mDbName . ".incident_status  on incident.status = incident_status.status join " . $mDbName . ".incident_deadlines on incident_deadlines.incident_number= incident.incident_number where site_id in (" . $siteIds . ") and incident.status in( 'L2_SUBMITTED','L2_EDITED', 'L3_SUBMITTED', 'L3_EDITED','L3_APPROVED', 'L4_SAVED') order by incident_number desc";
}else if ($filter === 'HAZARDSPENDINGAPPROVAL') {
    $sql = "SELECT  hazard.hazard_number, site_id, code,location, hazard_date,hazard.status,display_status,hazard_status.description as statusDesc, concat(first_name,' ', last_name) as approver_name, curr_approver_id, hazard.updated_at, hazard.updated_by, '' as isOverdue FROM " . $mDbName . ".site  join " . $mDbName . ".hazard  on hazard.site_id = site.id  left outer join  " . $mDbName . ".users on hazard.curr_approver_id = users.id  join " . $mDbName . ".hazard_status  on hazard.status = hazard_status.status where site_id in (" . $siteIds . ") and hazard.status in( 'L1_SUBMITTED','L2_SAVED') order by hazard_number desc";
} else if ($filter === 'INCIDENTSNOTCLOSED') {
    $sql = "SELECT  incident.incident_number, site_id, code,location, incident_date,incident_type,incident.status,display_status,incident_status.description as statusDesc, concat(first_name,' ', last_name) as approver_name, curr_approver_id, incident.updated_at, incident.updated_by, '' as isOverdue, submit_by,fi_submit_by, gm_approval_by FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  left outer join  " . $mDbName . ".users on incident.curr_approver_id = users.id  join " . $mDbName . ".incident_status  on incident.status = incident_status.status join " . $mDbName . ".incident_deadlines on incident_deadlines.incident_number= incident.incident_number where site_id in (" . $siteIds . ") and incident.status not in( '_CLOSED','_DELETED') group by incident.incident_number order by incident_number desc";
     $specialResult = true;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $incidentId = $row['incident_number'];
        $totalCnt = 0;
        $mOutStandingActionCntQuery = "SELECT count(actions.id) as outstandingactions FROM " . $mDbName . ".actions where incident_number =" . $incidentId . " and status not in ('', 'CANCELLED','CLOSED');";
        $mOutStandingActionCntData = mysqli_query($connection, $mOutStandingActionCntQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mOutStandingActionCntData)) {
            $totalCnt = $mInnerRow['outstandingactions'];
        }
        $curr['outstandingactions'] = $totalCnt;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });

        $emparray[] = $curr;
    }
     
      
} else if ($filter === 'HAZARDSCLOSED' || $filter === 'ALLHAZARDS' || $filter === 'EXPORTALLHAZARDS') {
    $dateFilter =" and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59'" ;
    $status = $filter === 'HAZARDSCLOSED' ? "( '_CLOSED','L2_APPROVED')" : "( '_CLOSED','L2_APPROVED', 'L2_SAVED', 'L1_SUBMITTED')";
    
    $sql = "SELECT  hazard.*, DAYNAME(hazard_date) as dayOfWeek, code,hazard.status,display_status,hazard_status.description as statusDesc,"
            ." concat(first_name,' ', last_name) as approver_name,"
            ." hazard.updated_at,hazard.created_by, hazard.updated_by FROM " 
            . $mDbName . ".site  join " . $mDbName . ".hazard  on hazard.site_id = site.id  left outer join  " 
            . $mDbName . ".users on hazard.curr_approver_id = users.id  join " 
            . $mDbName . ".hazard_status  on hazard.status = hazard_status.status where site_id in (" . $siteIds . ") and hazard.status in"
            . $status.$dateFilter. " group by hazard.hazard_number order by hazard_number desc";
     $specialResult = true;
      ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $hazardId = $row['hazard_number'];
        $totalCnt = 0;
          $openCnt = 0;
        $actionQuery = "select report_id, sum(case when actions.status in ('CREATED','ASSIGNED', 'RE-SCHEDULED', 'RE-OPENED') THEN 1 ELSE 0 END) as openactions, "
                ."sum(case when actions.status not in ('CANCELLED') THEN 1 ELSE 0 END) as totalactions from " . $mDbName . ".actions where report_id =" 
                . $hazardId . " and action_source = 'Hazard'";
       
        $actionQueryData = mysqli_query($connection, $actionQuery);
        while ($mInnerRow = mysqli_fetch_assoc($actionQueryData)) {
            $openCnt = $mInnerRow['openactions'];
             $totalCnt = $mInnerRow['totalactions'];
        }
        $curr['totalactions'] = $totalCnt;
         $curr['openactions'] = $openCnt;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });

        $emparray[] = $curr;
    }
     
      
} else if ($filter === 'DELETEDINCIDENTS') {

    $sql = "SELECT incident.incident_number, site.code, location, incident_date, incident_type,incident_history.updated_by, incident_history.updated_at, substring(comments,LOCATE(': ', comments)+1,length(comments)) as comments from " . $mDbName . ".incident join " . $mDbName . ".site on incident.site_id = site.id join " . $mDbName . ".incident_history on incident_history.incident_number = incident.incident_number where site_id in (" . $siteIds . ") and incident.status = '_DELETED' and incident_history.status = 'DELETED' and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59';";
}else if ($filter === 'DELETEDHAZARDS') {

    $sql = "SELECT hazard.hazard_number, site.code, location, hazard_date, hazard.created_by,hazard_history.updated_by, hazard_history.updated_at, substring(comments,LOCATE(': ', comments)+1,length(comments)) as comments from " . $mDbName . ".hazard join " . $mDbName . ".site on hazard.site_id = site.id join " . $mDbName . ".hazard_history on hazard_history.hazard_id = hazard.hazard_number where site_id in (" . $siteIds . ") and hazard.status = '_DELETED' and hazard_history.status = 'DELETED' and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59';";
}else if ($filter === 'INCIDENTSBYNAME') {
    $sql = "SELECT incident.incident_number, code, incident_date, incident_type, person_name, date(person_dob) as person_dob,employment_status, is_vehicle_involved, date(employment_start_date) as employment_start_date, date(time_on_job_start_date) as time_on_job_start_date FROM " . $mDbName . ".incident_person_details, " . $mDbName . ".incident, " . $mDbName . ".site where incident_person_details.incident_number = incident.incident_number and incident.site_id= site.id and site_id in (" . $siteIds . ") and incident.status != '_DELETED' and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59';";
} else if ($filter === 'EXPORTHISTORICDATA') {
    $sql = "Select * from " . $mDbName . ".incident_historic_data where site in ('".join("','",$siteNames)."')and STR_TO_DATE(incident_date,'%d/%m/%Y') between '" . $fromDate . "' and '" . $toDate ."';";
    
}else if ($filter === 'DAREPORT') {
    $sql = "SELECT incident.incident_number, code, incident_date, incident_type, incident_history.updated_by, incident_history.updated_at,incident_detail,  person_name, date(person_dob) as person_dob, employment_status, is_vehicle_involved, date(employment_start_date) as employment_start_date, date(time_on_job_start_date) as time_on_job_start_date, if((location_of_injury='None' OR location_of_injury=''), '0','1') as is_injured, location_of_injury, prev_incident_date_reference, is_vehicle_involved, da_test_required, da_test_agreed, da_passed, da_tested_by FROM incident_person_details, incident, site, incident_history where incident_history.incident_number = incident.incident_number and incident_person_details.incident_number = incident.incident_number and incident.site_id= site.id and incident_history.status = 'SUBMITTED' and level = 'L2'  and site_id in (" . $siteIds . ") and incident.status != '_DELETED' and da_test_required is true and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incident_number, person_name;";
}else if ($filter === 'DAMAGECOST') {
    $sql = "SELECT code,sum( estimated_cost) as damagecost, sum( estimated_cost_other) as damagecostother FROM vsms.incident join site on site.id = incident.site_id and site_id in (" . $siteIds . ") and incident.status != '_DELETED' and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by site_id;";
}else if ($filter === 'ALLINCIDENTENTSREPORT') {
    $specialResult = true;
    $sql = "SELECT  incident.incident_number, site_id, code,location, incident_date,incident_type,incident.status,display_status,incident_status.description as statusDesc, concat(first_name,' ', last_name) as approver_name, curr_approver_id, incident.updated_at, incident.updated_by, '' as isOverdue, submit_by,fi_submit_by, gm_approval_by , incident.created_at , incident.created_by, timediff(incident_date, incident.created_at) as diff FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  left outer join  " . $mDbName . ".users on incident.curr_approver_id = users.id  join " . $mDbName . ".incident_status  on incident.status = incident_status.status join " . $mDbName . ".incident_deadlines on incident_deadlines.incident_number= incident.incident_number  where site_id in (" . $siteIds . ") and incident.status not in( '_DELETED') and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by incident.incident_number order by incident_number desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $incidentId = $row['incident_number'];
        $totalCnt = 0;
        $mTotalActionCntQuery = "SELECT count(actions.id) as totalCnt FROM " . $mDbName . ".actions where incident_number =" . $incidentId . " and status !='';";
        $mTotalActionCntData = mysqli_query($connection, $mTotalActionCntQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mTotalActionCntData)) {
            $totalCnt = $mInnerRow['totalCnt'];
        }
        $curr['totalCnt'] = $totalCnt;
        
        $oustandingCnt = 0;
        $mOutActionCntQuery = "SELECT count(actions.id) as outstandingactions FROM " . $mDbName . ".actions where incident_number =" . $incidentId . " and status not in ('CANCELLED', 'CLOSED', '');";
        $mOutActionCntData = mysqli_query($connection, $mOutActionCntQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mOutActionCntData)) {
            $oustandingCnt = $mInnerRow['outstandingactions'];
        }
        $curr['outstandingactions'] = $oustandingCnt;
        
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });

        $emparray[] = $curr;
    }
} else if ($filter === 'EXPORTINCIDENTENTSREPORT') {
    $specialResult = true;
    $sql = "select incident.incident_number, incident_detail,incident.status,incident_status.description,code, location, location_other, date(incident_date) as incidentDate, dayname(incident_date) as dayOfWeek, time(incident_date) as incidentTime," .
            "incident_type,incident.action_type,action_type_other,area, lighting,temperature,noise,surface_type ,surface_condition, is_person_involved,person_name, TIMESTAMPDIFF(YEAR, person_dob, incident_date) AS age1," .
            "DATE_FORMAT(FROM_DAYS(DATEDIFF(incident_date, person_dob)),   '%y Years') AS ageYears,DATE_FORMAT(FROM_DAYS(DATEDIFF(incident_date, person_dob)),   '%m Months') AS ageMonths,DATE_FORMAT(FROM_DAYS(DATEDIFF(incident_date, person_dob)),   '%d Days') AS ageDays,".
            "employment_status, TIMESTAMPDIFF(DAY, employment_start_date, incident_date) AS los, TIMESTAMPDIFF(DAY, time_on_job_start_date,incident_date) AS toj,any_previous_incidents,prev_incident_date_reference,is_vehicle_involved," .
            "vehicle_type,vehicle_reg_no,was_person_removed_from_truck,stillage_type,location_of_injury,treatment_administered,ppe_worn,was_ppe_req_level,estimated_cost,is_activity_standard_process,da_test_required,da_passed," .
            "is_ssow_suitable,ssow_comments,is_ra_suitable,ra_comments,is_mhe_training_required,person_concentraion,person_experience,person_training,machine_condition,machine_maintenance,machine_facility," .
            "method_ssow,method_ruling,method_risk_assessment,environment_floor,environment_lighting,environment_condition, category, category_level,type,act_or_condition,is_dangerous_occurence, injured_person_name,days_lost," .
            "isRIDDOR, concat(first_name, ' ', last_name) as currApproverName,incident.updated_by, incident.updated_at,display_status,'' as isOverdue, submit_by,fi_submit_by, gm_approval_by, incident_person_details.id as person_id " .
            "from " . $mDbName . ".incident join " . $mDbName . ".site on incident.site_id = site.id " .
            "join " . $mDbName . ".incident_deadlines on incident.incident_number = incident_deadlines.incident_number " .
            "join " . $mDbName . ".incident_status on incident.status = incident_status.status " .
            "left outer join " . $mDbName . ".users on incident.curr_approver_id = users.id " .
            "left outer join " . $mDbName . ".incident_person_details on incident.incident_number =incident_person_details.incident_number " .
            "left outer join  " . $mDbName . ".incident_full_investigation_details on incident_full_investigation_details.incident_number =  incident.incident_number " .
            "left outer join  " . $mDbName . ".incident_injured_person_details on (incident_injured_person_details.injured_person_name =  incident_person_details.person_name and incident_injured_person_details.incident_number= incident_person_details.incident_number) " .
            " where incident.status != '_DELETED' and site_id in (" . $siteIds . ") and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' order by incident.incident_number desc;";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $incidentId = $row['incident_number'];
        $personId = $row['person_id'];
        $outstandingactions = 0;
        $mActionCntQuery = "SELECT count(actions.id) as outstandingactions FROM " . $mDbName . ".actions where incident_number =" . $incidentId . " and status not in ('CANCELLED', 'CLOSED', '');";
        $mActionCntData = mysqli_query($connection, $mActionCntQuery);
        while ($mInnerRow = mysqli_fetch_assoc($mActionCntData)) {
            $outstandingactions = $mInnerRow['outstandingactions'];
        }
        
        if($personId=== null){
            $personId =0;
        }
        $licenses = 0;
        $licenseQuery = "SELECT is_valid as licenseValid FROM " . $mDbName . ".incident_person_license_details where incident_number =" . $incidentId . " and person_id=".$personId;
        $licenseData = mysqli_query($connection, $licenseQuery);
        while ($mInnerRow1 = mysqli_fetch_assoc($licenseData)) {
            $licenses = $mInnerRow1['licenseValid'];
        }
        $curr['outstandingactions'] = $outstandingactions;
        $curr['licenseValid'] = $licenses;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
}



if ($specialResult === false) {
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

//create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
}
echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
