<?php

session_start();
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: 0');
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
$error = false;
$person = '';
$injuredPerson = '';
if (isset($_POST['person'])) {
    $person = $_POST['person'];
}
if (isset($_POST['injuredPerson'])) {
    $injuredPerson = $_POST['injuredPerson'];
}
$incidentId =0;
if (isset($_POST['incidentId'])) {
   $incidentId = $_POST['incidentId'];
}
$hazardId =0;
if (isset($_POST['hazardId'])) {
   $hazardId = $_POST['hazardId'];
}
$riskHazardId =0;
if (isset($_POST['rhid'])) {
   $riskHazardId = $_POST['rhid'];
}
if (isset($_POST['keepRHFiles'])) {
    $keepRHFiles = json_decode($_POST['keepRHFiles']);
}
if (isset($_POST['keepPhotos'])) {
    $keepPhotos = json_decode($_POST['keepPhotos']);
}
if (isset($_POST['keepSSOW'])) {
    $keepSSOW = json_decode($_POST['keepSSOW']);
}
if (isset($_POST['keepRisks'])) {
    $keepRisks = json_decode($_POST['keepRisks']);
}
if (isset($_POST['keepWitness'])) {
    $keepWitness = json_decode($_POST['keepWitness']);
}
if (isset($_POST['keepSupportFiles'])) {
    $keepSupportFiles = json_decode($_POST['keepSupportFiles']);
}
if (isset($_POST['keepHazardPhotos'])) {
    $keepHazardPhotos = json_decode($_POST['keepHazardPhotos']);
}
if (isset($_POST['keepHazardFiles'])) {
    $keepHazardFiles = json_decode($_POST['keepHazardFiles']);
}

if ($incidentId != 0) {
    if ($person !== '') {
             $keepDA = json_decode($_POST['keepDA']);
            //$sql = "delete from " . $mDbName . ".incident_person_da_files where file_name not in  ('".implode("','", $keepDA). "') and person_id = (select id from " . $mDbName . ".incident_person_details where person_name = '".$person."' and incident_number = ".$incidentId.") and incident_number = " . $incidentId;
            $sql = "select id from " . $mDbName . ".incident_person_details where person_name = '".$person."' and incident_number = ".$incidentId.";";
            $personId =-1;
            ChromePhp::log($sql);
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            while ($row = mysqli_fetch_assoc($result)) {
                 $personId = $row['id'];
            }
          foreach ($keepDA as $value) {
            $sql = "INSERT INTO ".$mDbName. ".incident_person_da_files (`person_id`,`incident_number`, `file_name`) VALUES (".$personId.",".$incidentId.", '".$value."');";
              ChromePhp::log($sql);
               $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log("Record Inserted DA");
            }
        }
    } elseif ($injuredPerson !== '') {
             $keepRIDDOR = json_decode($_POST['keepRIDDOR']);
            $sql = "select id from " . $mDbName . ".incident_injured_person_details where injured_person_name = '".$injuredPerson."' and incident_number = ".$incidentId.";";
            $injuredPersonId =-1;
            ChromePhp::log("in riddor ---->".$sql);
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            while ($row = mysqli_fetch_assoc($result)) {
                 $injuredPersonId = $row['id'];
            }
          foreach ($keepRIDDOR as $value) {
            $sql = "INSERT INTO ".$mDbName. ".incident_injured_person_riddor_files (`injured_person_id`,`incident_number`, `file_name`) VALUES (".$injuredPersonId.",".$incidentId.", '".$value."');";
              ChromePhp::log($sql);
               $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log("Record Inserted RIDDOR");
            }
        }
    }else {
        if ($keepPhotos !== '') {
            $sql = "delete from " . $mDbName . ".incident_photos_files where file_name not in ('".implode("','", $keepPhotos). "') and incident_number = " . $incidentId;

            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted 1");
            }
        }
        if ($keepSSOW !== '') {

            $sql = "delete from " . $mDbName . ".incident_ssow_files where file_name not in ('".implode("','", $keepSSOW). "') and incident_number = " . $incidentId;
            
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted 2");
            }
        }
        if ($keepRisks !== '') {

            $sql = "delete from " . $mDbName . ".incident_risk_files where file_name not in('".implode("','", $keepRisks). "') and incident_number = " . $incidentId;
          
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted 3");
            }
        }
        if ($keepWitness !== '') {

            $sql = "delete from " . $mDbName . ".incident_witness_files where file_name not in ('".implode("','", $keepWitness). "') and incident_number = " . $incidentId;
         
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted4");
            }
        }
         if ($keepSupportFiles !== '') {

            $sql = "delete from " . $mDbName . ".incident_support_files where file_name not in ('".implode("','", $keepSupportFiles). "') and incident_number = " . $incidentId;
         
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted5");
            }
        }
    }
}else if($hazardId!=0){
    if ($keepHazardFiles !== '') {

            $sql = "delete from " . $mDbName . ".hazard_other_files where file_name not in ('".implode("','", $keepHazardFiles). "') and hazard_id = " . $hazardId;
         
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted");
            }
        }
        
        if ($keepHazardPhotos !== '') {

            $sql = "delete from " . $mDbName . ".hazard_photo_files where file_name not in ('".implode("','", $keepHazardPhotos). "') and hazard_id = " . $hazardId;
         
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted5");
            }
        }
    
}else if($riskHazardId!=0){
    if ($keepRHFiles !== '') {

            $sql = "delete from " . $mDbName . ".risk_hazard_files where file_name not in ('".implode("','", $keepRHFiles). "') and risk_hazard_id = " . $riskHazardId;
         
            $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if ($result === TRUE) {
                ChromePhp::log($sql."Record deleted");
            }
        }
        
       
    
}
exit("File Uploaded");
?>
