<?php

session_start();
include ('../config/phpConfig.php');
$filter = $_GET['filter'];

if (isset($_GET['siteId'])) {
    $siteId = $_GET['siteId'];
}


if ($filter === 'Users') {
    $sql = "SELECT first_name,  last_name, email_id , '' as level FROM ".$mDbName.".users, ".$mDbName.".users_sites, ".$mDbName.".site where users_sites.user_id = users.id and users_sites.site_id=site.id and site_id =". $siteId." order by first_name, last_name";
    
} else if ($filter === 'Approvers') {
    $sql = "SELECT first_name,  last_name, email_id, level FROM ".$mDbName.".users, ".$mDbName.".approvers where  is_active = true and approvers.user_id=users.id and  site_id =". $siteId." order by first_name, last_name";
} else if ($filter === 'Action Owners') {

    $sql = "SELECT first_name,  last_name, email_id, '' as level  FROM ".$mDbName.".users, ".$mDbName.".users_sites, ".$mDbName.".site where users_sites.user_id = users.id and is_action_owner = true and users_sites.site_id=site.id and site_id =". $siteId." order by first_name, last_name";
    
}
$result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

//create an array
$emparray = array();
while ($row = mysqli_fetch_assoc($result)) {
    $emparray[] = array_map('utf8_encode', $row);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
