
<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

if (isset($_GET['siteIds'])) {
    $siteIds = explode(",", $_GET['siteIds']);
}
$fromDate = '';
if (isset($_GET['fromDate'])) {
    $fromDate = $_GET['fromDate'];
}
$toDate = '';
if (isset($_GET['toDate'])) {
    $toDate = $_GET['toDate'];
}
$prevToDate = '';
if (isset($_GET['prevToDate'])) {
    $prevToDate = $_GET['prevToDate'];
}
$prevFromDate = '';
if (isset($_GET['prevFromDate'])) {
    $prevFromDate = $_GET['prevFromDate'];
}


$emparray = array();
$totalPrevRow = array();
$totalCurrRow = array();
$totalVarRow = array();
$siteNames = array();

$sitesql = "select id, code from " . $mDbName . ".site where id in (" . $_GET['siteIds'] . ");";

$siteResult = mysqli_query($connection, $sitesql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($siteResult)) {
    $siteNames[$row['id']] = $row['code'];
}


$totalCurrRow['hazardsReported'] = 0;
$totalCurrRow['avgClosureTime'] = 0;

$totalCurrRow['type'] = 'Current Period';
$totalCurrRow['hazardSite'] = 'Total';

$totalPrevRow['hazardsReported'] = 0;
$totalPrevRow['avgClosureTime'] = 0;

$totalPrevRow['type'] = 'Previous Period';
$totalPrevRow['hazardSite'] = 'Total';

$totalVarRow['hazardsReported'] = 0;
$totalVarRow['avgClosureTime'] = 0;

$totalVarRow['type'] = 'Variance';
$totalVarRow['hazardSite'] = 'Total';

foreach ($siteIds as &$siteId) {
    $prevRow = array();
    $currRow = array();
    $varRow = array();

    $currRow['hazardsReported'] = 0;
    $currRow['avgClosureTime'] = 0;
    
    $currRow['type'] = 'Current Period';
    $currRow['hazardSite'] = $siteNames[$siteId];

    $prevRow['hazardsReported'] = 0;
    $prevRow['avgClosureTime'] = 0;
    
    $prevRow['type'] = 'Previous Period';
    $prevRow['hazardSite'] = $siteNames[$siteId];




    ///Query for getting hazards reported
    $currHazardReportedSql = "select count(*) as hazardsReported, 'Current Period' as type from " . $mDbName . ".hazard where hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' and status = '_CLOSED' and site_id = " .$siteId;
    $prevHazardReportedSql = '';
    $mainHazardReportedSql = $currHazardReportedSql;

    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevHazardReportedSql = "select count(*) as hazardsReported, 'Previous Period' as type from " . $mDbName . ".hazard where hazard_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' and status = '_CLOSED' and site_id = " .$siteId;
        $mainHazardReportedSql = $currHazardReportedSql . ' UNION ' . $prevHazardReportedSql;
    }
    $mainResult = mysqli_query($connection, $mainHazardReportedSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult)) {
        if ($row['type'] === 'Current Period') {

            $currRow['hazardsReported'] = $row['hazardsReported'];
        } else if ($row['type'] === 'Previous Period') {

            $prevRow['hazardsReported'] = $row['hazardsReported'];
        }
    }
    /////hazard closure times by
    $currAvgClosureSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".hazard_avg_closure_time where  hazard_site = " .$siteId. " and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by hazard_number";
    $prevAvgClosureSql = '';
    $mainAvgClosureSql = $currAvgClosureSql;
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        $prevAvgClosureSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".hazard_avg_closure_time where  hazard_site = " .$siteId. " and hazard_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by hazard_number";
        $mainAvgClosureSql = $currAvgClosureSql . ' UNION ' . $prevAvgClosureSql;
    }
    ChromePhp::log($mainAvgClosureSql);
    $mainResult8 = mysqli_query($connection, $mainAvgClosureSql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($mainResult8)) {
        if ($row['type'] === 'Current Period') {
            $currRow['avgClosureTime'] = $row['avgClosureTime'];
        } else if ($row['type'] === 'Previous Period') {
            $prevRow['avgClosureTime'] = $row['avgClosureTime'];
        }
    }


    //$totalCurrRow = addInTotal($currRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $currRow);
    if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
        //$totalPrevRow = addInTotal($prevRow, $totalPrevRow);
        $emparray[] = array_map('utf8_encode', $prevRow);
  
        $varRow = calcVariance($prevRow, $currRow);
        //$totalVarRow = addInTotal($varRow, $totalVarRow);
        $emparray[] = array_map('utf8_encode', $varRow);
    }
}

$currTotalAvgClosureSql = "Select 'Current Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".hazard_avg_closure_time where hazard_site in (" . $_GET['siteIds'] . ") and hazard_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by hazard_number";
$prevTotalAvgClosureSql = '';
$mainTotalAvgClosureSql = $currTotalAvgClosureSql;
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $prevTotalAvgClosureSql = "Select 'Previous Period' as type, TIME_FORMAT(sec_to_time(avg(avgClosureTime)), '%H:%i:%s') as avgClosureTime from " . $mDbName . ".hazard_avg_closure_time where hazard_site in (" . $_GET['siteIds'] . ") and hazard_date between '" . $prevFromDate . "' and '" . $prevToDate . " 23:59:59' group by hazard_number";
    $mainTotalAvgClosureSql = $currTotalAvgClosureSql . ' UNION ' . $prevTotalAvgClosureSql;
}
ChromePhp::log($mainTotalAvgClosureSql);
$mainResult8 = mysqli_query($connection, $mainTotalAvgClosureSql) or die("Error in Selecting " . mysqli_error($connection));
while ($row = mysqli_fetch_assoc($mainResult8)) {
    if ($row['type'] === 'Current Period') {
        $totalCurrRow['avgClosureTime'] = $row['avgClosureTime'];
    } else if ($row['type'] === 'Previous Period') {
        $totalPrevRow['avgClosureTime'] = $row['avgClosureTime'];
    }
}



$emparray[] = array_map('utf8_encode', $totalCurrRow);
if ($prevFromDate !== 'None' && $prevToDate !== 'None') {
    $emparray[] = array_map('utf8_encode', $totalPrevRow);
    $totalVarRow = calcVariance($totalPrevRow, $totalCurrRow);
    $emparray[] = array_map('utf8_encode', $totalVarRow);
}

echo json_encode($emparray);
//close the db connection
mysqli_close($connection);

function calcVariance($prevRow, $currRow) {
    $varRow = array();
    $varRow['type'] = 'Variance';
    $varRow['hazardSite'] = $currRow['hazardSite'];
    $varRow['hazardsReported'] = $currRow['hazardsReported'] - $prevRow['hazardsReported'];
    $varRow['avgClosureTime'] = secToHR(timeToSeconds($currRow['avgClosureTime']) - timeToSeconds($prevRow['avgClosureTime']));
    
    return $varRow;
}

function addInTotal($row, $totalPrevRow) {
    $totalPrevRow['hazardsReported'] += $row['hazardsReported'];
    $totalPrevRow['avgClosureTime'] += $row['avgClosureTime'];
    
    return $totalPrevRow;
}

function timeToSeconds(string $time)
{
    $arr = explode(':', $time);
    if (count($arr) == 3) {
        return $arr[0] * 3600 + $arr[1] * 60 + $arr[2];
    } else {
        return $arr[0] * 60 + $arr[1];
    }
}
function secToHR($seconds) {
  $hours = floor($seconds / 3600);
  $minutes = floor(($seconds / 60) % 60);
  $seconds = $seconds % 60;
  return "$hours:$minutes:$seconds";
}

?>