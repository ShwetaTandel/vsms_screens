<?php
session_start();
include ('../config/phpConfig.php');
$filter = $_GET['filter'];

//fetch table rows from mysql db

if ($filter === 'SITE') {
    $sitedIds = join(",", $_SESSION['vsmsUserData']['sites']);
    // $sql = "SELECT * FROM ". $mDbName . ".incident,". $mDbName .".site,". $mDbName .".users where incident.site_id = site.id and incident.curr_approver_id = users.id and site_id in (" .$sitedIds. ") and status != '_DELETED';";
    $sql = "SELECT  incident_number, site_id, code,location, incident_date,incident_type,incident.status,display_status,incident_status.description as statusDesc, concat(first_name,' ', last_name) as approver_name, curr_approver_id FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  left outer join  " . $mDbName . ".users on incident.curr_approver_id = users.id  join " . $mDbName . ".incident_status  on incident.status = incident_status.status where site_id in (" . $sitedIds . ") and incident.status != '_DELETED' order by incident_number desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
} else if ($filter === 'INCIDENT') {
    $incidentId = $_GET['iid'];
    
    //$sql = "SELECT incident.*, site.code, incident_status.display_status as displayStatus, concat(first_name, ' ', last_name ) as approver_name FROM " . $mDbName . ".incident join " . $mDbName . ".Site on incident.site_id = site.id join  " . $mDbName . ".incident_status on incident_status.status = incident.status left outer join " . $mDbName . ".users on incident.curr_approver_id = users.id where incident.incident_number = ". $incidentId;
    $sql = "SELECT incident.*, site.code, incident_status.display_status as displayStatus, concat(first_name, ' ', last_name ) as approver_name,submit_reminder_by,submit_by,fi_reminder,fi_submit_by,gm_approval_reminder,gm_approval_by FROM " . $mDbName . ".incident join " . $mDbName . ".Site on incident.site_id = site.id join  " . $mDbName . ".incident_status on incident_status.status = incident.status left outer join " . $mDbName . ".users on incident.curr_approver_id = users.id join incident_deadlines on " . $mDbName . ".incident_deadlines.incident_number = incident.incident_number where incident.incident_number = ". $incidentId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $persons = array();
        $actions = array();
        $licenses = array();
        $photos = array();
        $ssows = array();
        $risks = array();
        $witnessStmnts = array();
        $fullInvestigation;
        $whyBlocks = array();
        $otherCounterMeasures = array();
        $injuredPersons = array();
        $allActions = array();
        $supportFiles = array();
        

        $mDetailsQuery1 = "SELECT * FROM " . $mDbName . ".incident_person_details where incident_number =" . $incidentId;
        $mDetailData1 = mysqli_query($connection, $mDetailsQuery1);
        while ($mInnerRow1 = mysqli_fetch_assoc($mDetailData1)) {
            $innerCurrRow = $mInnerRow1;
            $dafiles = array();
            $mInnerDetailsQuery1 = "SELECT file_name FROM " . $mDbName . ".incident_person_da_files where person_id =" . $mInnerRow1['id'] . " and incident_number=" . $incidentId;
            $mInnerDetailData1 = mysqli_query($connection, $mInnerDetailsQuery1);
            while ($mInnerInnerRow1 = mysqli_fetch_assoc($mInnerDetailData1)) {
                $dafiles [] = $mInnerInnerRow1;
            }
            $innerCurrRow['dafiles'] = $dafiles;
            $persons[] = $innerCurrRow;
        }
        $mDetailsQuery2 = "SELECT * FROM " . $mDbName . ".actions where action_type = 'Incident-Containment' and action_source ='Incident' and report_id =" . $incidentId;
        $mDetailData2 = mysqli_query($connection, $mDetailsQuery2);
        while ($mInnerRow2 = mysqli_fetch_assoc($mDetailData2)) {
            $actions[] = $mInnerRow2;
        }
        $mDetailsQuery3 = "SELECT incident_person_license_details.id as licenseId,person_name, person_id, expiry_date, is_valid, file_name FROM " . $mDbName . ".incident_person_license_details," . $mDbName . ".incident_person_details where incident_person_details.id = incident_person_license_details.person_id and incident_person_details.incident_number =" . $incidentId;
        $mDetailData3 = mysqli_query($connection, $mDetailsQuery3);
        while ($mInnerRow3 = mysqli_fetch_assoc($mDetailData3)) {
            $licenses[] = $mInnerRow3;
        }
        $mDetailsQuery4 = "SELECT * FROM " . $mDbName . ".incident_photos_files where incident_number =" . $incidentId;
        $mDetailData4 = mysqli_query($connection, $mDetailsQuery4);
        while ($mInnerRow4 = mysqli_fetch_assoc($mDetailData4)) {
            $photos[] = $mInnerRow4;
        }

        $mDetailsQuery5 = "SELECT * FROM " . $mDbName . ".incident_ssow_files where incident_number =" . $incidentId;
        $mDetailData5 = mysqli_query($connection, $mDetailsQuery5);
        while ($mInnerRow5 = mysqli_fetch_assoc($mDetailData5)) {
            $ssows[] = $mInnerRow5;
        }
        $mDetailsQuery6 = "SELECT * FROM " . $mDbName . ".incident_risk_files where incident_number =" . $incidentId;
        $mDetailData6 = mysqli_query($connection, $mDetailsQuery6);
        while ($mInnerRow6 = mysqli_fetch_assoc($mDetailData6)) {
            $risks[] = $mInnerRow6;
        }
        $mDetailsQuery7 = "SELECT * FROM " . $mDbName . ".incident_witness_files where incident_number =" . $incidentId;
        $mDetailData7 = mysqli_query($connection, $mDetailsQuery7);
        while ($mInnerRow7 = mysqli_fetch_assoc($mDetailData7)) {
            $witnessStmnts[] = $mInnerRow7;
        }

        $mDetailsQuery8 = "SELECT * FROM " . $mDbName . ".incident_full_investigation_details where incident_number =" . $incidentId;
        $mDetailData8 = mysqli_query($connection, $mDetailsQuery8);
        while ($mInnerRow8 = mysqli_fetch_assoc($mDetailData8)) {
            $fullInvestigation = $mInnerRow8;
        }

        $mDetailsQuery9 = "SELECT * FROM " . $mDbName . ".incident_full_investigation_why_details where incident_number =" . $incidentId;
        $mDetailData9 = mysqli_query($connection, $mDetailsQuery9);
        while ($mInnerRow9 = mysqli_fetch_assoc($mDetailData9)) {
            //$whyBlocks[] = $mInnerRow9;
            $innerCurrRow2 = $mInnerRow9;
            $counterMeasures = array();
            $mInnerDetailsQuery2 = "SELECT * FROM " . $mDbName . ".actions where report_sub_id = " . $innerCurrRow2['id'] . " and action_source = 'Incident' and report_id=" . $incidentId;
            $mInnerDetailData2 = mysqli_query($connection, $mInnerDetailsQuery2);
            while ($mInnerInnerRow2 = mysqli_fetch_assoc($mInnerDetailData2)) {
                $counterMeasures [] = $mInnerInnerRow2;
            }
            $innerCurrRow2['counterMeasures'] = $counterMeasures;
            $whyBlocks[] = $innerCurrRow2;
        }

        $mDetailsQuery10 = "SELECT * FROM " . $mDbName . ".actions where action_type != 'Incident-Containment' and report_sub_id is null and action_source='Incident' and report_id =" . $incidentId;
        $mDetailData10 = mysqli_query($connection, $mDetailsQuery10);
        while ($mInnerRow10 = mysqli_fetch_assoc($mDetailData10)) {
            $otherCounterMeasures[] = $mInnerRow10;
        }

        $mDetailsQuery11 = "SELECT * FROM " . $mDbName . ".incident_injured_person_details where incident_number =" . $incidentId;
        $mDetailData11 = mysqli_query($connection, $mDetailsQuery11);
        while ($mInnerRow11 = mysqli_fetch_assoc($mDetailData11)) {
            $innerCurrRow11 = $mInnerRow11;
            $riddorFiles = array();
            $mInnerDetailsQuery11 = "SELECT file_name FROM " . $mDbName . ".incident_injured_person_riddor_files where injured_person_id =" . $mInnerRow11['id'] . " and incident_number=" . $incidentId;
            $mInnerDetailData11 = mysqli_query($connection, $mInnerDetailsQuery11);
            while ($mInnerInnerRow11 = mysqli_fetch_assoc($mInnerDetailData11)) {
                $riddorFiles [] = $mInnerInnerRow11;
            }
            $innerCurrRow11['riddorfiles'] = $riddorFiles;
            $injuredPersons[] = $innerCurrRow11;
        }

        //$mDetailsQuery12 = "SELECT concat('AC',actions.id) as id, action_type as category, action as description , code as site, concat(first_name, ' ', last_name) as owner, status , deadline FROM " . $mDbName . ".actions, " . $mDbName . ".site, " . $mDbName . ".users where actions.action_owner = users.id and actions.action_site = site.id and incident_number =".$incidentId;
        $mDetailsQuery12 = "SELECT concat('AC',actions.id) as id, action_type as category, action as description , code as site, concat(first_name, ' ', last_name) as owner, action_status.description as status, deadline FROM " . $mDbName . ".actions, " . $mDbName . ".action_status, " . $mDbName . ".site, " . $mDbName . ".users where actions.status = action_status.status and   actions.action_owner = users.id and actions.action_site = site.id and action_source='Incident' and report_id =" . $incidentId;
        $mDetailData12 = mysqli_query($connection, $mDetailsQuery12);
        while ($mInnerRow12 = mysqli_fetch_assoc($mDetailData12)) {
            $allActions[] = $mInnerRow12;
        }
        
         $mDetailsQuery13 = "SELECT * FROM " . $mDbName . ".incident_support_files where incident_number =" . $incidentId;
        $mDetailData13 = mysqli_query($connection, $mDetailsQuery13);
        while ($mInnerRow13 = mysqli_fetch_assoc($mDetailData13)) {
            $supportFiles[] = $mInnerRow13;
        }

        $curr['persons'] = $persons;
        $curr['actions'] = $actions;
        $curr['licenses'] = $licenses;
        $curr['photos'] = $photos;
        $curr['ssows'] = $ssows;
        $curr['risks'] = $risks;
        $curr['witness'] = $witnessStmnts;
        $fullInvestigation['otherCounterMeasures'] = $otherCounterMeasures;
        $fullInvestigation['whyblocks'] = $whyBlocks;
        $curr['fullInvestigation'] = $fullInvestigation;
        $curr['injuredPersons'] = $injuredPersons;
        $curr['allActions'] = $allActions;
        $curr['supportFiles'] = $supportFiles;

        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
} else if ($filter === 'DELETE_INCIDENT') {
    $incidentId = $_GET['iid'];
    $sql = "UPDATE " . $mDbName . ".incident set status = '_DELETED' where incident.incident_number = " . $incidentId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if ($result === TRUE) {
        $emparray[] = 'Record Deleted';
    }
} else if ($filter === 'APPROVER') {
    $userId = $_GET['approverId'];
    $sql = "SELECT  incident_number, code,location, incident_date,incident_type,incident.status,incident_status.description as statusDesc FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id join " . $mDbName . ".incident_status  on incident.status = incident_status.status where curr_approver_id = " . $userId . " and incident.status in('L2_SUBMITTED', 'L2_EDITED', 'L3_SUBMITTED','L3_EDITED', 'L3_APPROVED', 'L4_SAVED', 'L4_REJECTED') order by incident_date desc";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
} else if ($filter === 'APPROVERCOUNT') {
    $userId = $_GET['approverId'];
    $sql = "SELECT  count(*) as cnt FROM " . $mDbName . ".site  join " . $mDbName . ".incident  on incident.site_id = site.id  where curr_approver_id = " . $userId . " and status in('L2_SUBMITTED', 'L2_SAVED') ";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row['cnt'];
    }
} else if ($filter === 'APPROVERBYLEVEL') {
    $level = $_GET['level'];
    $site = $_GET['site'];
    $sql = "SELECT user_id, first_name, last_name FROM " . $mDbName . ".approvers, " . $mDbName . ".users where approvers.user_id = users.id and  site_id = " . $site . " and level= '" . $level . "' and active = true and is_active=true";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
} else if ($filter === 'MARKCLOSED') {
    $emparray = array();
    $incidentId = $_GET['iid'];
    $actionsCnt = -1;
    $sql = "SELECT  count(*) as cnt FROM " . $mDbName . ".actions where status not in('CLOSED', 'CANCELLED', '') and action_source ='Incident' and report_id =" . $incidentId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    while ($row = mysqli_fetch_assoc($result)) {
        $actionsCnt = $row['cnt'];
    }
    if (intval($actionsCnt) === 0) {
        $sql = "UPDATE " . $mDbName . ".incident set status = '_CLOSED' where incident_number =" . $incidentId;
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    }
}
echo json_encode($emparray);
//close the db connection
mysqli_close($connection);
?>