<?php

session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
$filter = $_GET['filter'];
$graId = 0;
if (isset($_GET['graid'])) {
    $graId = $_GET['graid'];
}
//fetch table rows from mysql db

if ($filter === 'SITE') {
    $sitedIds = join(",", $_SESSION['vsmsUserData']['sites']);
    //$sql = "SELECT risk_assessment.* , date(gra_date) as graDate, date(review_date) as reviewDate,display_status,code, description, concat(first_name, ' ', last_name) as approver FROM " . $mDbName . ".risk_assessment join " . $mDbName . ".site on  risk_assessment.site_id = site.id join " . $mDbName . ".gra_status on risk_assessment.status = gra_status.status left outer join " . $mDbName . ".users on users.id = risk_assessment.curr_approver_id where risk_assessment.status!='_DELETED' and site_id in (" . $sitedIds . ") order by gra_id desc;";
    $sql = "SELECT b.* , (select group_concat('RA',gra_id) from risk_assessment a where a.ref_gra_id = b.gra_id) as cnt,date(gra_date) as graDate, date(review_date) as reviewDate,display_status,code, description, concat(first_name, ' ', last_name) as approver FROM " . $mDbName . ".risk_assessment b join " . $mDbName . ".site on  b.site_id = site.id join " . $mDbName . ".gra_status on b.status = gra_status.status left outer join " . $mDbName . ".users on users.id = b.curr_approver_id where b.status!='_DELETED' and site_id in (" . $sitedIds . ") order by b.gra_id desc;";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = array_map('utf8_encode', $row);
    }
} else if ($filter === 'GRA') {
    $sql = "SELECT risk_assessment.* , display_status,approver_level,code, description, concat(first_name, ' ', last_name) as approver FROM " . $mDbName . ".risk_assessment join " . $mDbName . ".site on  risk_assessment.site_id = site.id join " . $mDbName . ".gra_status on risk_assessment.status = gra_status.status left outer join " . $mDbName . ".users on users.id = risk_assessment.curr_approver_id where risk_assessment.status!='_DELETED' and gra_id = " . $graId;
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $curr = $row;
        $hazards = array();
        $allActions = array();
        $files = array();
        $mDetailsQuery1 = "SELECT * FROM " . $mDbName . ".risk_hazards where gra_id =" . $graId;
        $mDetailData1 = mysqli_query($connection, $mDetailsQuery1);
        while ($mInnerRow1 = mysqli_fetch_assoc($mDetailData1)) {
            $innerCurrRow2 = $mInnerRow1;
            $hazardId = $innerCurrRow2['id'];
            $counterMeasures = array();
            $files = array();
            $mInnerDetailsQuery2 = "SELECT * FROM " . $mDbName . ".actions where action_source = 'Risk Assessment' and report_sub_id=" . $hazardId . " and report_id=" . $graId;
            $mInnerDetailData2 = mysqli_query($connection, $mInnerDetailsQuery2);
            while ($mInnerInnerRow2 = mysqli_fetch_assoc($mInnerDetailData2)) {
                $counterMeasures [] = $mInnerInnerRow2;
            }
            $innerCurrRow2['actions'] = $counterMeasures;
            
            
             $mInnerDetailsQuery4 = "SELECT count(*) as cnt FROM " . $mDbName . ".actions where action_source = 'Risk Assessment' and status in ('ASSIGNED','RE-ASSIGNED','RE-OPENED','RE-SCHEDULED') and report_sub_id=" . $hazardId . " and report_id=" . $graId;
            $mInnerDetailData4 = mysqli_query($connection, $mInnerDetailsQuery4);
            while ($mInnerInnerRow4 = mysqli_fetch_assoc($mInnerDetailData4)) {
                $innerCurrRow2['openActionsCnt'] = $mInnerInnerRow4['cnt'];
            }
            
            $mInnerDetailsQuery3 = "SELECT * FROM " . $mDbName . ".risk_hazard_files where risk_hazard_id=" . $hazardId;
            $mInnerDetailData3 = mysqli_query($connection, $mInnerDetailsQuery3);
            while ($mInnerInnerRow3 = mysqli_fetch_assoc($mInnerDetailData3)) {
                $files [] = $mInnerInnerRow3;
            }
            $innerCurrRow2['files'] = $files;
            $hazards[] = $innerCurrRow2;
        }
        $mDetailsQuery2 = "SELECT concat('AC',actions.id) as id, action_type as category, action as description , code as site, concat(first_name, ' ', last_name) as owner, action_status.description as status, deadline FROM " . $mDbName . ".actions, " . $mDbName . ".action_status, " . $mDbName . ".site, " . $mDbName . ".users where actions.status = action_status.status and   actions.action_owner = users.id and actions.action_site = site.id and action_source='Risk Assessment' and report_id =" . $graId;
        $mDetailData2 = mysqli_query($connection, $mDetailsQuery2);
        while ($mInnerRow2 = mysqli_fetch_assoc($mDetailData2)) {
            $allActions[] = $mInnerRow2;
        }
        
        $curr['riskHazards'] = $hazards;
        $curr['allActions'] = $allActions;
        $curr['files'] = $files;
        array_walk_recursive($curr, function (&$item) {
            $item = mb_convert_encoding($item, "UTF-8");
        });
        $emparray[] = $curr;
    }
} else if ($filter === 'APPROVER') {
    $userId = $_GET['approverId'];
    $sql = "SELECT  risk_assessment.*, date(gra_date) as gra_date, code FROM " . $mDbName . ".site  join " . $mDbName . ".risk_assessment  on risk_assessment.site_id = site.id join " . $mDbName . ".gra_status  on risk_assessment.status = gra_status.status where curr_approver_id = " . $userId . " and risk_assessment.status in('L2_SUBMITTED', 'L2_EDITED', 'L2_APPROVED', 'L3_EDITED', 'L3_APPROVED', 'L4_EDITED') order by gra_date desc";
    ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    //create an array
    $emparray = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $emparray[] = $row;
    }
}

echo json_encode($emparray);

//close the db connection
mysqli_close($connection);
?>