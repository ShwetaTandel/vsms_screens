<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';
set_time_limit(300);
$jobId = 1;
$jobType = $_GET['jobType'];
$email = new EmailHelper();
$mailTos = array();
$ccTo = array();

if ($jobType === 'Incident') {
    $currTime = intVal(date("H"));
    echo $currTime;
    if($currTime >=0 && $currTime < 8){
      $jobId = 1;  
    }else if($currTime >=8 && $currTime < 16){
        $jobId = 2;  
    }else if ($currTime >= 16){
        $jobId = 3;  
    }
    $sql = "SELECT * FROM " . $mDbName . ".incidents_batch where site_id = 11;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    sendIncidentJobEmails($record);
}else if ($jobType === 'Action'){
    $sql = "SELECT * FROM " . $mDbName . ".actions_batch where displaysite = 'TSTCODE' and dayDiff <=1"; 
       $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }

    sendActionJobEmails($record);
}

function sendActionJobEmails($record){
     global $email, $mailTos, $ccTo;
     ChromePhp::log(count($record));
      for ($i = 0; $i < count($record); $i++) {
          
        $timeDiff = intval($record[$i]['timeDiff']);
        $dayDiff = intval($record[$i]['dayDiff']);
        $action_type = $record[$i]['action_type'];
        $ownerId = $record[$i]['action_owner'];
        $siteId = $record[$i]['siteId'];
        $actionId = $record[$i]['id'];
        $levels = array();
        $link = '';
        if($dayDiff < 0 ){
            //Send escalation
            $mailTos = getEmailByUserId($ownerId, $siteId);
             array_push($levels, 'L2');
             $ccTo = getEmailByLevels($siteId, $levels);
             $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E32');
            
        }else if ($dayDiff === 0  || ($dayDiff ===1 && $timeDiff <= 24)){
            
            if($action_type!== 'Incident-Containment'){
            //Send reminder
                $ccTo = array();
               $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=Incident";
                $mailTos = getEmailByUserId($ownerId, $siteId);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E31');
            }
        }
      
      }
}

function sendIncidentJobEmails($record) {
    global $email, $mailTos, $ccTo,$jobId;

    for ($i = 0; $i < count($record); $i++) {
        ChromePhp::log($record[$i]);
        $timeDiff = intval($record[$i]['diff']);
        $siteId = $record[$i]['site_id'];
        $dayofweek = $record[$i]['dayofweek'];
        $status = $record[$i]['status'];
        $today = $record[$i]['today'];
        $levels = array();
        $link = '';
        if ($status == "L1_CREATED" || $status == "L1_SAVED" || $status == "L2_REJECTED") {
            if ($timeDiff >= 8 && $timeDiff < 24) {// SEND AUTOREMINDERS
                array_push($levels, 'L1');
                $mailTos = getEmailByLevels($siteId, $levels);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E21');
            } else if ($timeDiff >= 24) {
                if ($today === 'Monday' && $dayofweek === 'Friday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 48) >= 8 && ($timeDiff - 48) < 24) {
                        // SEND AUTOREMINDERS to All L1 Approvers
                        array_push($levels, 'L1');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E21');
                    } else {
                        //escalation to L1 & L4 
                        array_push($levels, 'L1','L2', 'L4');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E22');
                    }
                } else if ($today === 'Monday' && $dayofweek === 'Saturday') {
                    if (($timeDiff - 24) >= 8 && ($timeDiff - 24) < 24) {
                        // SEND AUTOREMINDERS
                        array_push($levels, 'L1');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E21');
                    } else {
                        //escalation
                        array_push($levels, 'L1','L2', 'L4');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E22');
                    }
                } else {
                    array_push($levels, 'L1','L2', 'L4');
                    $mailTos = getEmailByLevels($siteId, $levels);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E22');
                }
            }
        } else if ($status === 'L2_SUBMITTED' || $status === 'L2_EDITED') {
            if ($timeDiff >= 8 && $timeDiff < 24) {// SEND AUTOREMINDERS
                $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AIR&iid=" . $record[$i]['incident_number'];
                $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E23');
            } else if ($timeDiff >= 24) {
                if ($dayofweek === 'Friday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 48) >= 8 && ($timeDiff - 48) < 24) {
                        // SEND AUTOREMINDERS
                        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AIR&iid=" . $record[$i]['incident_number'];
                        $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E23');
                    } else {
                        //escalation
                        array_push($levels, 'L2', 'L3');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E24');
                    }
                } else if ($dayofweek === 'Saturday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 24) >= 8 && ($timeDiff - 24) < 24) {
                        // SEND AUTOREMINDERS
                        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AIR&iid=" . $record[$i]['incident_number'];
                        $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E23');
                    } else {
                        //escalation
                        array_push($levels, 'L2', 'L3');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E24');
                    }
                } else {
                    array_push($levels, 'L2', 'L3');
                    $mailTos = getEmailByLevels($siteId, $levels);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E24');
                }
            }
        } else if ($status === 'L2_APPROVED' || $status === 'L3_SAVED' || $status === 'L3_REJECTED') {
            if ($timeDiff >= 36 && $timeDiff < 72) {// SEND AUTOREMINDERS
                array_push($levels, 'L2');
                $mailTos = getEmailByLevels($siteId, $levels);
                $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=FIR&iid=" . $record[$i]['incident_number'];
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E25');
            } else if ($timeDiff >= 72) {
                if ($dayofweek === 'Friday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 48) >= 36 && ($timeDiff - 48) < 72) {
                        // SEND AUTOREMINDERS
                        if ($jobId === '1') {
                            $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=FIR&iid=" . $record[$i]['incident_number'];
                            array_push($levels, 'L2');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E25');
                        }
                    } else {
                        //escalation
                        if ($jobId !== '2') {
                            array_push($levels, 'L2', 'L3');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E26');
                        }
                    }
                } else if ($dayofweek === 'Saturday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 24) >= 36 && ($timeDiff - 24) < 72) {
                        // SEND AUTOREMINDERS
                        if ($jobId === '1') {
                            $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=FIR&iid=" . $record[$i]['incident_number'];
                            array_push($levels, 'L2');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E25');
                        }
                    } else {
                        //escalation
                        if ($jobId !== '2') {
                            array_push($levels, 'L2', 'L3');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E26');
                        }
                    }
                } else {
                    //escalation
                    if ($jobId !== '2') {
                        array_push($levels, 'L2', 'L3');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E26');
                    }
                }
            }
        } else if ($status === 'L3_SUBMITTED' || $status === 'L3_EDITED') {
            if ($timeDiff >= 96 && $timeDiff < 168) {// SEND AUTOREMINDERS
                if ($jobId === 1) {
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $record[$i]['incident_number'];
                    $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E27');
                }
            } else if ($timeDiff >= 168) {
                if ($dayofweek === 'Friday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 48) >= 96 && ($timeDiff - 48) < 168) {
                        // SEND AUTOREMINDERS
                        if ($jobId === 1) {
                            $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $record[$i]['incident_number'];
                            $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E27');
                        }
                    } else {
                        //escalation
                        if ($jobId === '1') {
                            array_push($levels, 'L4', 'L3');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E28');
                        }
                    }
                } else if ($dayofweek === 'Saturday') {// case where incident happened previous friday and batch running on monday
                    if (($timeDiff - 24) >= 96 && ($timeDiff - 24) < 168) {
                        // SEND AUTOREMINDERS
                        if ($jobId === 1) {
                            $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $record[$i]['incident_number'];
                            $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E27');
                        }
                    } else {
                        //escalation
                        if ($jobId === '1') {
                            array_push($levels, 'L4', 'L3');
                            $mailTos = getEmailByLevels($siteId, $levels);
                            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E28');
                        }
                    }
                } else {
                    if ($jobId === '1') {
                        array_push($levels, 'L4', 'L3');
                        $mailTos = getEmailByLevels($siteId, $levels);
                        $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E28');
                    }
                }
            }
        }
    }
}

function getEmailByLevels($siteId, $levels) {
    global $mDbName, $connection, $serverName;

    if ($serverName === "vantecapps" || $siteId !== '11') {//Site id for TSTCOODE is 11
        $sql = "select email_id as email from " . $mDbName . ".approvers, " . $mDbName . ".users where  is_active=true and users.id= approvers.user_id and  level in ('" . implode("','", $levels) . "') and site_id =" . $siteId;
    } else {
        $sql = "select concat('test',email_id) as email from " . $mDbName . ".approvers, " . $mDbName . ".users where  is_active=true and users.id= approvers.user_id and  level in ('" . implode("','", $levels) . "') and site_id =" . $siteId;
    }
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }

    return $record;
}

function getEmailByUserId($userId, $siteId) {
    global $mDbName, $connection;
    if ($siteId !== '11') {
        $sql = "SELECT email_id as email FROM " . $mDbName . ".users where users.id =" . $userId;
    } else {
        $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".users where users.id=" . $userId;
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }

    return $record;
}

?>
