<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';
set_time_limit(300);
$jobId = 1;
$jobType = $_GET['jobType'];
$email = new EmailHelper();



if ($jobType === 'Incident') {
    $currTime = date("G");
    echo $currTime;
    if ($currTime >= 0 && $currTime < 8) {
        $jobId = 1;
    } else if ($currTime >= 8 && $currTime < 16) {
        $jobId = 2;
    } else if ($currTime >= 16) {
        $jobId = 3;
    }
    $sql = "SELECT * FROM " . $mDbName . ".incidents_batch;";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    sendIncidentJobEmails($record);
} else if ($jobType === 'Action') {
    $sql = "SELECT * FROM " . $mDbName . ".actions_batch where dayDiff <=1";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    sendActionJobEmails($record);
}else if ($jobType === 'Hazard') {
    $sql = "SELECT * FROM " . $mDbName . ".hazard_batch where timeDiff >= 24";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row;
    }
    sendHazardJobEmails($record);
}

function sendActionJobEmails($record) {
    global $email;
    for ($i = 0; $i < count($record); $i++) {
        $mailTos = array();
        $ccTo = array();
        $timeDiff = intval($record[$i]['timeDiff']);
        $dayDiff = intval($record[$i]['dayDiff']);
        $action_type = $record[$i]['action_type'];
        $ownerId = $record[$i]['action_owner'];
        $siteId = $record[$i]['siteId'];
        $actionId = $record[$i]['id'];
        $actionSource =  $record[$i]['action_source'];
        $deadline = new DateTime($record[$i]['estimated_completion_date']);
        $levels = array();
        $link = '';
        if ($dayDiff < 0 || ($dayDiff ===0 && $deadline < new DateTime())) {
            //Send escalation
            $mailTos = getEmailByUserId($ownerId, $siteId);
            array_push($levels, 'L2');
            $ccTo = getEmailByLevels($siteId, $levels);
            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E32');
        } else if (($dayDiff === 0 && $deadline >=new DateTime()) || ($dayDiff === 1 && $timeDiff <= 24)) {
            if ($action_type !== 'Incident-Containment') {
                //Send reminder
                $ccTo = array();
                if($actionSource === 'Incident'){
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=Incident";
                }else if($actionSource === 'Hazard'){
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=Hazard";;
                }else{
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=Other";;
                }
                $mailTos = getEmailByUserId($ownerId, $siteId);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E31');
            }
        }
    }
}

function sendHazardJobEmails($record) {
    global $email;
    for ($i = 0; $i < count($record); $i++) {
        $mailTos = array();
        $ccTo = array();
        $timeDiff = intval($record[$i]['timeDiff']);
        $siteId = $record[$i]['site_id'];
        $hazardId = $record[$i]['hazard_number'];
        $currApproverId = $record[$i]['curr_approver_id'];
        $levels = array();
        $link = '';
        if ($timeDiff >= 24 && $timeDiff < 72) {
            //Send escalation
            $mailTos = getEmailByUserId($currApproverId, $siteId);
            $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportHazard.php?action=HI&hid=" .$hazardId;
            $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E29');
        } else if ($timeDiff >= 72) {
               array_push($levels, 'L2', 'L3');
               $ccTo = getEmailByLevels($siteId, $levels);
             $mailTos = getEmailByUserId($currApproverId, $siteId);
              $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E30');
        }
    }
}

function sendIncidentJobEmails($record) {
    global $email, $jobId;
    echo count($record);
    for ($i = 0; $i < count($record); $i++) {
        $status = $record[$i]['status'];
        $siteId = $record[$i]['site_id'];
        $today = new DateTime();
        $levels = array();
        $ccTo = array();
        $mailTos = array();
        $link = '';
        if ($status == "L1_CREATED" || $status == "L1_SAVED" || $status == "L2_REJECTED") {
            $submitBy = new DateTime($record[$i]['submit_by']);
            $submitReminder = new DateTime($record[$i]['submit_reminder_by']);
            if($today >= $submitReminder && $today < $submitBy) {
                // SEND AUTO REMINDERS
                array_push($levels, 'L1');
                $mailTos = getEmailByLevels($siteId, $levels);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E21');
            } else if($today >= $submitBy) {
                //escalation to L1 & L4 
                array_push($levels, 'L1', 'L2', 'L4');
                $mailTos = getEmailByLevels($siteId, $levels);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E22');
            }
        } else if ($status === 'L2_SUBMITTED' || $status === 'L2_EDITED') {
            $submitBy = new DateTime($record[$i]['submit_by']);
            $submitReminder = new DateTime($record[$i]['submit_reminder_by']);
            if ($today >= $submitReminder && $today < $submitBy) {
                // SEND AUTO REMINDERS
               
               $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AIR&iid=" . $record[$i]['incident_number'];
                $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E23');
            } else if ($today >= $submitBy) {
                //escalation
                array_push($levels, 'L2', 'L3');
                $mailTos = getEmailByLevels($siteId, $levels);
                $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E24');
            }
        } else if ($status === 'L2_APPROVED' || $status === 'L3_SAVED' || $status === 'L3_REJECTED') {
            $fisubmitBy = new DateTime($record[$i]['fi_submit_by']);
            $fisubmitReminder = new DateTime($record[$i]['fi_reminder']);

            if ($today >= $fisubmitReminder && $today < $fisubmitBy){// SEND AUTO REMINDERS
                if ($jobId === 1) {
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=FIR&iid=" . $record[$i]['incident_number'];
                    array_push($levels, 'L2');
                    $mailTos = getEmailByLevels($siteId, $levels);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E25');
                }
            } else if ($today >= $fisubmitBy) {
                //escalation
                if ($jobId !== 2) {
                    array_push($levels, 'L2', 'L3');
                    $mailTos = getEmailByLevels($siteId, $levels);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E26');
                }
            }
        } else if ($status === 'L3_SUBMITTED' || $status === 'L3_EDITED') {
            $gmsubmitBy = new DateTime($record[$i]['gm_approval_by']);
            $gmsubmitReminder = new DateTime($record[$i]['gm_approval_reminder']);
            if ($today >= $gmsubmitReminder && $today < $gmsubmitBy) {// SEND 
                if ($jobId === 1) {
                    $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $record[$i]['incident_number'];
                    $mailTos = getEmailByUserId($record[$i]['curr_approver_id'], $siteId);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E27');
                }
            } else if ($today >= $gmsubmitBy) {
                if ($jobId === 1) {
                    array_push($levels, 'L4', 'L3');
                    $mailTos = getEmailByLevels($siteId, $levels);
                    $email->sendJobEmails($record[$i], $link, $mailTos, $ccTo, 'E28');
                }
            }
        }
    }
}

function getEmailByLevels($siteId, $levels) {
    global $mDbName, $connection, $serverName;

    if ($serverName === "vantecapps") {//Site id for TSTCOODE is 11
        $sql = "select email_id as email from " . $mDbName . ".approvers, " . $mDbName . ".users where is_active=true and active=true and users.id= approvers.user_id and  level in ('" . implode("','", $levels) . "') and site_id =" . $siteId;
    } else {
        $sql = "select concat('test',email_id) as email from " . $mDbName . ".approvers, " . $mDbName . ".users where is_active=true and active=true and users.id= approvers.user_id and  level in ('" . implode("','", $levels) . "') and site_id =" . $siteId;
    }

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }

    return $record;
}

function getEmailByUserId($userId, $siteId) {
    global $mDbName, $connection, $serverName;
     if ($serverName === "vantecapps") {
        $sql = "SELECT email_id as email FROM " . $mDbName . ".users where active=true and users.id =" . $userId;
    } else {
        $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".users where active=true and users.id=" . $userId;
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }

    return $record;
}

?>
