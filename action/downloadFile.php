<?php 
session_start();
header('Cache-Control: no-cache, no-store, must-revalidate');   
header('Expires: 0');
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');

date_default_timezone_set('Europe/London');

function download($path)
{
// calculate filename
	if (file_exists($path)) 
	{
        // send headers to browser to initiate file download
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$path);
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        readfile($path);
	}
	else
	{
		exit ("File:".$path." not found");
	}
	return true;
}

$filename = $_GET['fileName'];
$incidentId = 0;
if(isset($_GET['iid'])){
    $incidentId = $_GET['iid'];
}
$actionId = 0;

if(isset($_GET['aid'])){
    $actionId = $_GET['aid'];
}
$hid= 0;
if(isset($_GET['hid'])){
    $hid = $_GET['hid'];
}
$riskHazardId= 0;
if(isset($_GET['rhid'])){
    $riskHazardId = $_GET['rhid'];
}
$fileType = $_GET['type'];
if($actionId !=0){
    $path = "$dir". $fileType."/".$actionId ."/".$filename;
    
}else if($incidentId!=0){
    $path = "$dir". $incidentId ."/".$fileType."/".$filename;
}else if($hid!=0){
    $path = $hazardDir .$hid ."/".$fileType."/".$filename;
}else if($riskHazardId!=0){
    $path = $graDir .$riskHazardId ."/".$filename;
}

$download = download($path);

 
// $upload = "Test";
if($download)
{	
	$result = "";
	$result .= "<div>".$filename."</div>"; 
	$result .= " "; 
	echo "<div id='fileStatus'>";
	echo $result;
	echo "</div>";
	echo "<div id='errorStatus'>";
	echo "DOWNLOADED";
	echo "</div>";
	
	$error = false;
}
else 
{ 
	echo "<div id='fileStatus'>";
	echo $result;
	echo "</div>";
	echo "<div id='errorStatus'>";
	echo "FAILED";
	echo "</div>";
}
?>

<script>
	window.top.window.uploadEnd();
</script>