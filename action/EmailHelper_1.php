<?php

class EmailHelper {

    function sendJobEmails($data, $link, $mailTos, $ccTo, $emailType) {

        $mailfrom = "vsms@vanteceurope.com";//"system-vsms@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com"; //anastasiia-saad.eq@vantec-gl.com
        $subject = "";
        $mailHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                    <div>';
        $mailFooter = '<p style="font:12px italic;">This is an automated email please do not reply.</p>
                      </div>
                      </body>
                      </html>';
        $mail_content = "";
        $attachement = "";

        if ($emailType == 'E21') {
            $deadline = new DateTime($data['submit_by']);
            $subject = "REMINDER: IR" . $data['incident_number'] . " - Initial Report Pending";
            $mail_content = $mailHeader . '<br/>
                              <h3>Initial Report for IR' . $data['incident_number'] . ' must be submitted to the Site Management before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>You can access the report via Your Site Incidents tab on VSMS Home Screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E22') {
            $deadline = new DateTime($data['submit_by']);
            $subject = "REMINDER: " . $data['incidentSite'] . ": IR" . $data['incident_number'] . " - Initial Report Overdue";
            $mail_content = $mailHeader . '<br/>
                              <h3>Initial Report for IR' . $data['incident_number'] . ' is now overdue. It should have been submitted to the Site Management before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>Please submit the Initial Report immediately.</p>
                              <p>You can access the report via Your Site Incidents tab on VSMS Home Screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E23') {
            $deadline = new DateTime($data['submit_by']);
            $subject = "REMINDER: IR" . $data['incident_number'] . " - Approve Initial Report";
            $mail_content = $mailHeader . '<br/>
                              <h3>Initial Report for IR' . $data['incident_number'] . ' must be approved before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>Please follow this <a href ="' . $link . '">link</a> to approve or reject the report. Alternatively, it can be accessed via Pending Incidents tab on Pending Approvals screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E24') {
            $deadline = new DateTime($data['submit_by']);
            $subject = "REMINDER: " . $data['incidentSite'] . ": IR" . $data['incident_number'] . " - Initial Report Approval Overdue";
            $mail_content = $mailHeader . '<br/>
                              <h3>Approval of the Initial Report for IR' . $data['incident_number'] . ' is now overdue. It should have been approved by ' . $data['currApproverName'] . ' before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>Please ensure the Initial Report is approved immediately.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E25') {
            $deadline = new DateTime($data['fi_submit_by']);
            $subject = "REMINDER: IR" . $data['incident_number'] . " - Full Investigation Pending";
            $mail_content = $mailHeader . '<br/>
                              <h3>Full Investigation for IR' . $data['incident_number'] . ' must be submitted to the GM before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>You can edit the report by following this <a href ="' . $link . '">link</a>. Alternatively, it can be accessed via Your Site Incidents tab on VSMS Home screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E26') {
            $deadline = new DateTime($data['fi_submit_by']);
            $subject = "REMINDER: " . $data['incidentSite'] . ": IR" . $data['incident_number'] . " - Full Investigation Overdue";
            $mail_content = $mailHeader . '<br/>
                              <h3>Full Investigation for IR' . $data['incident_number'] . ' is now overdue. It should have been submitted to the GM before ' . $deadline->format('d-m-Y H:i:s') . '.</h3>
                              <p>Please submit the Full Investigation immediately.</p>
                             <p>You can access the report via Your Site Incidents tab on VSMS Home Screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E27') {
            $subject = "REMINDER: IR" . $data['incident_number'] . " - GM Approval Pending";
            $mail_content = $mailHeader . '<br/>
                              <h3>Full Investigation approval pending for IR' . $data['incident_number'] . '.</h3>
                              <p>Please follow the <a href ="' . $link . '">link</a> to approve or reject the report. Alternatively, it can be accessed via Pending Incidents tab on Pending Approvals screen.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType == 'E28') {
            $deadline = new DateTime($data['gm_approval_by']);
            $incidentDate = new DateTime($data['incident_datetime']);

            $subject = "REMINDER: " . $data['incidentSite'] . ": IR" . $data['incident_number'] . " - GM Approval Overdue";
            $mail_content = $mailHeader . '<br/>
                              <h3>Please be informed that the Full Investigation approval pending for IR' . $data['incident_number'] . ' by ' . $data['currApproverName'] . '.</h3>
                              <p>The incident took place on ' . $incidentDate->format('d-m-Y H:i:s') . '</p>
                              <p>Please ensure the Full Investigation is approved as soon as possible.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType === 'E31') {
            $incident = 'IR' . $data['incident_number'];
            $deadline = new DateTime($data['estimated_completion_date']);
            $displaySite = $data['displaysite'];
            if ($data['incident_number'] === null) {
                $incident = 'N/A';
                $displaySite = 'N/A';
            }
            $subject = "REMINDER: AC" . $data['id'] . " - Outstanding Action";
            $mail_content = $mailHeader . '<br/>
                              <h3>Please be informed that the deadline for below action is in less than 24 hours:</h3>
                              <span style="font:bold">Report ID:</span><span>' . $incident . '<br/>
                              <span style="font:bold">Report Site:</span><span> ' . $displaySite . '<br/>
                              <span style="font:bold">Action Description:</span><span> ' . $data['action'] . '<br/>
                              <span style="font:bold">Deadline:</span><span> ' . $deadline->format('d-m-Y H:i:s') . '<br/>
                              <span style="font:bold">Owner:</span><span> ' . $data['actionOwnerName'] . '<br/>
                              <p>Please follow the <a href ="' . $link . '">link</a> to complete or re-schedule the action.</p>
                              <br/>' . $mailFooter;
        } else if ($emailType === 'E32') {
            $incident = 'IR' . $data['incident_number'];
            $deadline = new DateTime($data['estimated_completion_date']);
            $displaySite = $data['displaysite'];
            if ($data['incident_number'] === null) {
                $incident = 'N/A';
                $displaySite = 'N/A';
            }
            $subject = "AC" . $data['id'] . " - Overdue Action";
            $mail_content = $mailHeader . '<br/>
                              <h3>Please be informed that the below action is now overdue:</h3>
                              <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                              <span style="font:bold">Report Site:</span><span> ' . $displaySite . '<br/>
                              <span style="font:bold">Action Description:</span><span> ' . $data['action'] . '<br/>
                              <span style="font:bold">Deadline:</span><span> ' . $deadline->format('d-m-Y H:i:s') . '<br/>
                              <span style="font:bold">Owner:</span><span> ' . $data['actionOwnerName'] . '<br/>
                              <p>Please ensure the action is completed or re-scheduled as soon as possible.</p>
                              <br/>' . $mailFooter;
        }
        $uid = md5(uniqid(time()));
        $ccEmails = implode(",", $ccTo);
        if ($attachement != '') {
            // $filename = 'Purchase Order ' . $order['purchaseOrderNumber'] . '.pdf';
            $content = chunk_split(base64_encode($attachement));
        }
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($ccEmails != '') {
            $header .= "Cc: " . $ccEmails . "\r\n";
        }
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if ($attachement != '') {
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= "$content" . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        }
        $emails = implode(",", $mailTos);

        if (mail($emails, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function sendActionEmails($action, $link, $mailTos, $ccTo, $emailType, $comments, $prevOwner) {
         $mailfrom = "vsms@vanteceurope.com";// = "system-vsms@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com"; //anastasiia-saad.eq@vantec-gl.com
        $subject = "";
        $mailHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                    <div>';
        $mailFooter = '<p style="font:12px italic;">This is an automated email please do not reply.</p>
                      </div>
                      </body>
                      </html>';
        $mail_content = "";
        $attachement = "";
        $incident = "";
        $site = "";
        $subjectSite = "";
        if ($action[0]->incident_number == '') {
            $incident = 'N/A';
            $site = 'N/A';
            $subjectSite = "";
        } else {
            $incident = "IR" . $action[0]->incident_number;
            $site = $action[0]->incidentsite;
            $subjectSite = $action[0]->incidentsite . ':';
        }
        $assignedDate = DateTime::createFromFormat('Y-m-d H:i:s', $action[0]->assigned_at)->format('d-m-Y H:i:s');
        $deadline = DateTime::createFromFormat('Y-m-d H:i:s', $action[0]->deadline)->format('d-m-Y H:i:s');
        $updatedDate = DateTime::createFromFormat('Y-m-d H:i:s', $action[0]->updated_at)->format('d-m-Y H:i:s');
        if ($emailType == 'E16') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Outstanding Action";
            $mail_content = $mailHeader . '<br/>
                                <h3>You have been assigned the below action by ' . $action[0]->assigned_by . ' on ' . $assignedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Deadline:</span><span> ' . $deadline . '<br/>
                                <p>Please follow this <a href ="' . $link . '">link</a> to complete the action.</a>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E19') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Action Cancelled";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been cancelled by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Comments:</span><span> ' . $comments . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E18') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Action Closed";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been closed by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Comments:</span><span> ' . $comments . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E20') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Action Re-Opened";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been re-opened by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">New Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">New Owner:</span><span> ' . $action[0]->actionOwner . '<br/>
                                <span style="font:bold">Reason for re-opening:</span><span> ' . $comments . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E39') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Outstanding Action ";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been re-opened and assigned to you by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">New Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">Reason for re-opening:</span><span> ' . $comments . '<br/>
                                <p>Please follow this <a href ="' . $link . '">link</a> to complete the action.</a>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E38') {
            $newDeadline = DateTime::createFromFormat('Y-m-d H:i:s', $action[0]->estimated_completion_date)->format('d-m-Y H:i:s');
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Action Re-Scheduled";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been re-scheduled by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Original Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">Comments:</span><span> ' . $comments . '<br/>
                                <span style="font:bold">New Estimated Completion Date:</span><span> ' . $newDeadline . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E17') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Action Re-Assigned";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below action has been re-assigned by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">Comments:</span><span> ' . $comments . '<br/>
                                <span style="font:bold">Previous Owner:</span><span> ' . $prevOwner . '<br/>
                                <span style="font:bold">New Owner:</span><span> ' . $action[0]->actionOwner . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E37') {
            $subject = "" . $subjectSite . " AC" . $action[0]->id . " - Outstanding Action";
            $mail_content = $mailHeader . '<br/>
                                <h3>You have been re-assigned the below action by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">Comments:</span><span> ' . $comments . '<br/>
                                <span style="font:bold">Previous Owner:</span><span> ' . $prevOwner . '<br/>
                                <p>Please follow this <a href ="' . $link . '">link</a> to complete the action.</a>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E40') {
            $subject = "AC" . $action[0]->id . " - New Additional Action";
            $mail_content = $mailHeader . '<br/>
                                <h3>The below additional action has been raised by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Deadline:</span><span> ' . $deadline . '<br/>
                                <span style="font:bold">Owner:</span><span> ' . $action[0]->actionOwner . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E41') {
            $subject = "AC" . $action[0]->id . " - New Additional Action";
            $mail_content = $mailHeader . '<br/>
                                <h3>A new additional action has been assigned to you by ' . $action[0]->updated_by . ' on ' . $updatedDate . ' </h3>
                                <span style="font:bold">Action ID:</span><span> AC' . $action[0]->id . '<br/>
                                <span style="font:bold">Report ID:</span><span> ' . $incident . '<br/>
                                <span style="font:bold">Report Site:</span><span> ' . $site . '<br/>
                                <span style="font:bold">Action Category:</span><span> ' . $action[0]->action_type . '<br/>
                                <span style="font:bold">Action Description:</span><span> ' . $action[0]->action . '<br/>
                                <span style="font:bold">Deadline:</span><span> ' . $deadline . '<br/>
                                <p>Please follow this <a href ="' . $link . '">link</a> to complete the action.</a>
                                <br/>' . $mailFooter;
        }
        $uid = md5(uniqid(time()));
        $ccEmails = implode(",", $ccTo);
        if ($attachement != '') {
            // $filename = 'Purchase Order ' . $order['purchaseOrderNumber'] . '.pdf';
            $content = chunk_split(base64_encode($attachement));
        }
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($ccEmails != '') {
            $header .= "Cc: " . $ccEmails . "\r\n";
        }
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if ($attachement != '') {
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= "$content" . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        }
        $emails = implode(",", $mailTos);

        if (mail($emails, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function sendIncidentEmails($incident, $link, $mailTos, $ccTo, $emailType, $comments) {
         $mailfrom = "vsms@vanteceurope.com";// = "system-vsms@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com";
        $subject = "";
        $mailHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                    <div>';
        $mailFooter = '<p style="font:12px italic;">This is an automated email please do not reply.</p>
                      </div>
                      </body>
                      </html>';
        $mail_content = "";
        $attachement = "";
        $incidentDate = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->incident_date)->format('d-m-Y H:i:s');

        if ($emailType == 'E01') {
            $deadline = new DateTime($incident[0]->submit_by);
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Reported";
            $mail_content = $mailHeader . '<br/>
                                <h3>Incident Report has been created:</h3>
                                <span style="font:bold">Report ID:</span><span> IR' . $incident[0]->incident_number . '<br/>
                                <span style="font:bold">Site:</span><span> ' . $incident[0]->code . '<br/>
                                <span style="font:bold">Location:</span><span> ' . $incident[0]->location . ' ' . $incident[0]->location_other . '<br/>
                                <span style="font:bold">Incident Date and Time:</span><span> ' . $incidentDate . '<br/>
                                <span style="font:bold"> Incident Type:</span><span> ' . $incident[0]->incident_type . '<br/>
                                <span style="font:bold"> Incident Brief:</span><span> ' . $incident[0]->incident_brief . '<br/>
                                <span style="font:bold">Created By:</span><span> ' . $incident[0]->created_by . '<br/>
                                <p>Initial Report must be submitted to the Site Management by the reporter of the incident before ' . $deadline->format('d-m-Y H:i:s') . '.</p>
                                <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E01_1') {
            $deadline = new DateTime($incident[0]->submit_by);
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Serious Event Reported";
            $mail_content = $mailHeader . '<br/>
                                <h3><span style="font:bold;color:red">Serious Incident Report has been created:</span></h3>
                                <span style="font:bold">Report ID:</span><span> IR' . $incident[0]->incident_number . '<br/>
                                <span style="font:bold">Site:</span><span> ' . $incident[0]->code . '<br/>
                                <span style="font:bold">Location:</span><span> ' . $incident[0]->location . ' ' . $incident[0]->location_other . '<br/>
                                <span style="font:bold">Incident Date and Time:</span><span> ' . $incidentDate . '<br/>
                                <span style="font:bold"> Incident Type:</span><span> ' . $incident[0]->incident_type . '<br/>
                                <span style="font:bold"> Incident Brief:</span><span> ' . $incident[0]->incident_brief . '<br/>
                                <span style="font:bold">Created By:</span><span> ' . $incident[0]->created_by . '<br/>
                                <p>Initial Report must be submitted to the Site Management by the reporter of the incident before ' . $deadline->format('d-m-Y H:i:s') . '.</p>
                                <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E02') {
            $deadline = new DateTime($incident[0]->fi_submit_by);
            $submittedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Approval Required";
            $mail_content = $mailHeader . '<br/>
                                <h3>Initial Report for IR' . $incident[0]->incident_number . ' has been submitted by ' . $incident[0]->updated_by . ' on ' . $submittedDateTime . '.</h3>
                                <p>Please follow this <a href ="' . $link . '">link</a> to approve or reject the report. Alternatively, it can be accessed via Pending Incidents tab on Pending Approvals screen.</p>
                                <p>Please be reminded that once the Initial Report has been approved, the Full Investigation must be completed by ' . $deadline->format('d-m-Y H:i:s') . '.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E03') {
            $deadline = new DateTime($incident[0]->fi_submit_by);
            if ($comments !== '') {
                $comments = "Comments :" . $comments;
            }
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Initial Report Approved";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Initial Report for IR' . $incident[0]->incident_number . ' has been approved by ' . $incident[0]->updated_by . ' on ' . $updatedAt . '.</h3>
                                 <p>' . $comments . '</p>
                                <p>Full Investigation must be submitted to the GM by the Site Management before ' . $deadline->format('d-m-Y H:i:s') . '</p>
                                <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E04') {
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Vehicle Involved";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Please be informed that an Incident with a vehicle involved has been reported, and a refresher training might be requested as a result.</h3>
                                  <span style="font:bold">Report ID:</span><span> IR' . $incident[0]->incident_number . '<br/>
                                <span style="font:bold">Site:</span><span> ' . $incident[0]->code . '<br/>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E05') {
            $deadline = new DateTime($incident[0]->submit_by);
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Initial Report Review Required";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Initial Report for IR' . $incident[0]->incident_number . ' has been rejected by ' . $incident[0]->updated_by . ' on ' . $updatedAt . ' due to the following:</h3>
                                <p>' . $comments . '</p>
                                <p>Initial Report must be re-submitted to the Site Management before ' . $deadline->format('d-m-Y H:i:s') . '</p>
                                <p>Please follow this <a href ="' . $link . '">link</a> to review and re-submit the report. Alternatively, it can be accessed via Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E06') {
            $submittedDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Approval Required";
            $mail_content = $mailHeader . '<br/>
                                <h3>Full Investigation for IR' . $incident[0]->incident_number . ' has been submitted by ' . $incident[0]->updated_by . ' on ' . $submittedDateTime . '.</h3>
                                <p>Please follow this <a href ="' . $link . '">link</a> to approve or reject the report. Alternatively, it can be accessed via Pending Incidents tab on Pending Approvals screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E07') {

            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Full Investigation Approved";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            if ($comments !== '') {
                $comments = "Comments :" . $comments;
            }

            $mail_content = $mailHeader . '<br/>
                                <h3>Full Investigation for IR' . $incident[0]->incident_number . ' has been approved by ' . $incident[0]->updated_by . ' on ' . $updatedAt . '.</h3>
                                <p>' . $comments . '</p>
                                <p>This Incident Report is now pending H&S sign off.</p>
                                <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E08') {

            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Pending Sign Off";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            if ($comments !== '') {
                $comments = "Comments :" . $comments;
            }
            $mail_content = $mailHeader . '<br/>
                                <h3>Full Investigation for IR' . $incident[0]->incident_number . ' has been approved by ' . $incident[0]->updated_by . ' on ' . $updatedAt . '.</h3>
                                  <p>' . $comments . '</p>
                                <p>This Incident Report is now pending your sign off.</p>
                                <p>Please follow this <a href ="' . $link . '">link</a> to complete the sign off and close the report. Alternatively, it can be accessed via Pending Incidents tab on Pending Approvals screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E09') {
            $deadline = new DateTime($incident[0]->fi_submit_by);
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Full Investigation Review Required";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Full Investigation for IR' . $incident[0]->incident_number . ' has been rejected by ' . $incident[0]->updated_by . ' on ' . $updatedAt . ' due to the following:</h3>
                                <p>' . $comments . '</p>
                                <p>Full Investigation must be re-submitted to the GM before ' . $deadline->format('d-m-Y H:i:s') . '</p>
                                <p>Please follow this <a href ="' . $link . '">link</a> to review and re-submit the report. Alternatively, it can be accessed via Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E10') {
            $status = 'Outstanding Actions';
            if ($incident[0]->status === 'L4_APPROVED') {
                $status = 'Outstanding Actions';
            } else if ($incident[0]->status === '_CLOSED') {
                $status = 'Closed';
            }
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Signed Off";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Incident Report for IR' . $incident[0]->incident_number . ' has been signed off by ' . $incident[0]->updated_by . ' on ' . $updatedAt . '.</h3>
                                <p>The report is currently in ' . $status . ' status.</p>
                               <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E11') {
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Report Deleted";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Incident Report IR' . $incident[0]->incident_number . ' has been deleted by ' . $incident[0]->updated_by . ' on ' . $updatedAt . ' due to the following:</h3>
                                <p>' . $comments . '</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E34') {

            if ($comments !== '') {
                $comments = "Comments :" . $comments;
            }
            $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Report Re-assigned";
            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Incident Report IR' . $incident[0]->incident_number . ' approval has been re-assigned to ' . $incident[0]->approver_name . ' by ' . $incident[0]->updated_by . ' on ' . $updatedAt . ' and no longer requires any action from you.</h3>
                                      <p>' . $comments . '</p>
                                <p>You can view the report by clicking "View" button on Your Site Incidents tab on VSMS Home Screen.</p>
                                <br/>' . $mailFooter;
        } else if ($emailType == 'E41') {

            $line = ' approval ';
            $act = ' approve or reject ';
            if ($incident[0]->displayStatus === 'L2') {
                $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Report Approval Re-assigned";
            } else if ($incident[0]->displayStatus === 'L3') {
                $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Full Investigation Approval Re-assigned";
            } else if ($incident[0]->displayStatus === 'L4') {
                $subject = "" . $incident[0]->code . ": IR" . $incident[0]->incident_number . " - Incident Report Sign off  Re-assigned";
                $line = ' sign off ';
                $act = ' sign off ';
            }
            if ($comments !== '') {
                $comments = "Comments :" . $comments;
            }

            $updatedAt = DateTime::createFromFormat('Y-m-d H:i:s', $incident[0]->updated_at)->format('d-m-Y H:i:s');
            $mail_content = $mailHeader . '<br/>
                                <h3>Incident Report IR' . $incident[0]->incident_number . $line . 'has been re-assigned to you by ' . $incident[0]->updated_by . ' on ' . $updatedAt . '.</h3>
                                <p>' . $comments . '</p>
                                <p>Please go to Pending Approvals screen to' . $act . 'this report .</p>
                                <br/>' . $mailFooter;
        }
        $uid = md5(uniqid(time()));
        $ccEmails = implode(",", $ccTo);
        if ($attachement != '') {
            // $filename = 'Purchase Order ' . $order['purchaseOrderNumber'] . '.pdf';
            $content = chunk_split(base64_encode($attachement));
        }
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($ccEmails != '') {
            $header .= "Cc: " . $ccEmails . "\r\n";
        }
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\r\n";
        if ($emailType == 'E01_1' || $emailType == 'E01') {
            $header.= "X-Priority: 1 \n";
            $header.= "X-MSMail-Priority: High \n"; //$header.=  "Priority: Urgent\n";
            $header.= "Importance: High";
        }

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if ($attachement != '') {
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= "$content" . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        }
        $emails = implode(",", $mailTos);

        if (mail($emails, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function sendPasswordResetLink($userId, $emailAddress, $link) {
         $mailfrom = "vsms@vanteceurope.com";// = "system-vsms@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com";
        $subject = "Password reset link for VSMS";
        $mail_content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                        <div>
                                <p>Hi ' . $userId . ',</p>
                                <p>Please click <a href ="' . $link . '">here</a> to reset your password. The link is valid for 10 minutes.</p>
                                <p></p>
                                <p style="font:12px italic;">If you have not requested a password reset, please alert the VSMS admin team about the same.<br/></p>
                        </div>
                    </body>
                    </html>';
        $uid = md5(uniqid(time()));
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        if (mail($emailAddress, $subject, $nmessage, $header)) {
            return "SENT";
        } else {
            return "FAILED";
        }
    }

    function sendDAJobEmail($mailTos) {
         $mailfrom = "vsms@vanteceurope.com";// = "system-vsms@vantec-gl.com";
        $BccTo = "shweta-tandel.ce@vantec-gl.com";
        $filePath = "VSMS DA Report.xls";
        $mailHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    </head>
                    <body>
                    <div>';
        $mailFooter = '<p style="font:12px italic;">This is an automated email please do not reply.</p>
                      </div>
                      </body>
                      </html>';
        $mail_content = "";
        
        $subject = "Weekly D&A Report";
        $mail_content = $mailHeader . '<br/>
                                <h3>Please find the D&A report attached for time period ' . date('d-m-Y', strtotime("-7 days")) . ' to ' . date("d-m-Y") . '.</h3>
                                <br/>' . $mailFooter;

        $uid = md5(uniqid(time()));
        
            $filename = 'DA_Report_' . date('d-m-Y', strtotime("-7 days")) . '_to_'. date("d-m-Y") .'.xls';
            $content = $this->getDAReportAttachment($filePath);
        
        // header
        $header = "From: " . $mailfrom . "\r\n";
        if ($BccTo != '') {
            $header .= "Bcc: " . $BccTo . "\r\n";
        }
        $header .= "Reply-To: \r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\r\n";
        // message & attachment
        $nmessage = "--" . $uid . "\r\n";
        $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
        $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $nmessage .= $mail_content . "\r\n\r\n";
        $nmessage .= "--" . $uid . "\r\n";
        
            $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
            $nmessage .= "Content-Transfer-Encoding: base64\r\n";
            $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
            $nmessage .= "$content" . "\r\n\r\n";
            $nmessage .= "--" . $uid . "--";
        
        //$emails = implode(",", $mailTos);
        //echo "emails".$mailTos;
        if (mail($mailTos, $subject, $nmessage, $header)) {
           
            unlink($filePath);
            return "SENT";
            
        } else {
            return "FAILED";
        }
    }

    function getDAReportAttachment($filePath) {
        //$file = "C://Users//standel//Downloads//VSMS DA Report.xls"; 
        $content = file_get_contents($filePath);
        $content = chunk_split(base64_encode($content));
        return $content;
    }

 

}
