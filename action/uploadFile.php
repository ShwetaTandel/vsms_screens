<?php

session_start();
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: 0');
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
$error = false;
$file = $_FILES['file']['name'];
$incidentId = 0;
$actionId = 0;
$hid = 0;
$riskHazardId = 0;
if(isset($_POST['iid'])){
    $incidentId = $_POST['iid'];    
}
if(isset($_POST['actionid'])){
    $actionId = $_POST['actionid'];    
}
if(isset($_POST['hid'])){
    $hid = $_POST['hid'];    
}
if(isset($_POST['rhid'])){
    $riskHazardId = $_POST['rhid'];    
}
$fileType = "";
if(isset($_POST['type'])){
    $fileType = $_POST['type'];
}
ChromePhp::log("Hid is ".$hid. $fileType);

if($incidentId != 0){
    //Upload files to specific folder
    
     $path = $dir . $incidentId . '/'.$fileType .'/';
      if (!is_dir($path)) {
        mkdir($path, 0777, true);
      }
    $filePath = $path . $file;

    if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
            $sql = '';
        if($fileType === 'license'){
            $person = $_POST['person'];
            $sql = "update INTO ".$mDbName. ".incident_person_license_files (`person_id`, `incident_number`, `file_name`) VALUES ((select id from ".$mDbName. ".incident_person_details where person_name='".$person. "' and incident_number=".$incidentId. "),".$incidentId.", '".$file."');";
        }else if($fileType === 'DA'){
           $person = $_POST['person'];
            $sql = "INSERT INTO ".$mDbName. ".incident_person_da_files (`person_id`, `incident_number`, `file_name`) VALUES ((select id from ".$mDbName. ".incident_person_details where person_name='".$person. "' and incident_number=".$incidentId. "),".$incidentId.", '".$file."');";
        }else if($fileType === 'RIDDOR'){
           $person = $_POST['person'];
            $sql = "INSERT INTO ".$mDbName. ".incident_injured_person_riddor_files (`injured_person_id`, `incident_number`, `file_name`) VALUES ((select id from ".$mDbName. ".incident_injured_person_details where injured_person_name='".$person. "' and incident_number=".$incidentId. "),".$incidentId.", '".$file."');";
            ChromePhp::log("IN UPLOAD RIDDOR ->>>>".$sql);
        } else if($fileType === 'ssow'){
            $sql = "INSERT INTO ".$mDbName. ".incident_ssow_files (`incident_number`, `file_name`) VALUES (".$incidentId.", '".$file."');";
        }else if($fileType === 'Photos'){
            $sql = "INSERT INTO ".$mDbName. ".incident_photos_files (`incident_number`, `file_name`) VALUES (".$incidentId.", '".$file."');";
        }else if($fileType === 'RiskAssesment'){
             $sql = "INSERT INTO ".$mDbName. ".incident_risk_files (`incident_number`, `file_name`) VALUES (".$incidentId.", '".$file."');";
            
        }else if($fileType === 'WitnesStatement'){
            $sql = "INSERT INTO ".$mDbName. ".incident_witness_files (`incident_number`, `file_name`) VALUES (".$incidentId.", '".$file."');";
            
        }else if($fileType === 'SupportFiles'){
            $sql = "INSERT INTO ".$mDbName. ".incident_support_files (`incident_number`, `file_name`) VALUES (".$incidentId.", '".$file."');";
            ChromePhp::log("IN UPLOAD SUPOORT FILES ->>>>".$sql);
        }
            
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if($result === TRUE){
                ChromePhp::log("Record updated");            
            }
        }
}else if ($actionId!=0){
      $path = $actionDir . $actionId . '/';
      if (!is_dir($path)) {
        mkdir($path, 0777, true);
      }
      $filePath = $path . $file;
      if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
        if($fileType === 'Photos'){
            $sql = "INSERT INTO ".$mDbName. ".actions_photos (`action_id`, `file_name`) VALUES (".$actionId.", '".$file."');";
            ChromePhp::log("Record updated in photos");  
        }else{
            $sql = "INSERT INTO ".$mDbName. ".actions_files (`action_id`, `file_name`) VALUES (".$actionId.", '".$file."');";
        }
         $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if($result === TRUE){
                ChromePhp::log("Record updated");            
            }
        
    }

}else if ($hid!=0){
      $path = $hazardDir . $hid . '/'. $fileType.'/';
      if (!is_dir($path)) {
        mkdir($path, 0777, true);
      }
      $filePath = $path . $file;
        ChromePhp::log("File path".$filePath);  
      if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
        if($fileType === 'Photos'){
            $sql = "INSERT INTO ".$mDbName. ".hazard_photo_files (`hazard_id`, `file_name`) VALUES (".$hid.", '".$file."');";
            ChromePhp::log("Record updated in photos");  
        }else{
            $sql = "INSERT INTO ".$mDbName. ".hazard_other_files (`hazard_id`, `file_name`) VALUES (".$hid.", '".$file."');";
        }
         $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
            if($result === TRUE){
                ChromePhp::log("Record updated");            
            }
        
    }

}else if ($riskHazardId!=0){
    $path = $graDir . $riskHazardId . '/';
    if (!is_dir($path)) {
        mkdir($path, 0777, true);
    }
    $filePath = $path . $file;
    ChromePhp::log("File path" . $filePath);
    if (move_uploaded_file($_FILES['file']['tmp_name'], $filePath)) {
        $sql = "INSERT INTO " . $mDbName . ".risk_hazard_files (`risk_hazard_id`, `file_name`) VALUES (" . $riskHazardId . ", '" . $file . "');";
        ChromePhp::log("Record updated in risk hazard files");
        $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
        if ($result === TRUE) {
            ChromePhp::log("Record updated");
        }
    }
}
exit("File Uploaded");
?>
