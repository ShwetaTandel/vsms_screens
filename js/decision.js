 
        function ask(question) {
            if (!question)
                return;
           // var answer = prompt(question.question + '\n' + question.answers.map(({ answer }, i) => '\n' + i + ' ' + answer));
            var answer = prompt(question.question + '\n' + '(Enter 0 for Yes and 1 for No)'+'\n'+question.answers.map(({ answer }, i) => '\n' + i + ' ' + answer));
            if (answer in question.answers) {
                var val = answer === '1' ? 'No': 'Yes';
                decisionWay = decisionWay + "\n" + question.question + " - " + val + ". \r";
                if (question.answers[answer].comment) {
                    alert(question.answers[answer].comment);
                    document.getElementById('vantecWayDecision').value = question.answers[answer].comment;
                    document.getElementById('decisionTree').innerHTML  = decisionWay;
                    decisionWay ="";
                    return;
                }
                ask(question.answers[answer]);
            }
        }
        var decisionWay = "";

        var tree = {
            "question": "Were the actions intended?",
            "answers": [
                {
                    "answer": "Yes",
                    "question": "Were the results intended?",
                    "answers": [
                        {
                            "answer": "Yes",
                            "comment": "SABOTAGE, MALICIOUS DAMAGE"
                        },
                        {
                            "answer": "No",
                            "question": "Did the individual fully understand the relevant procedures?",
                            "answers": [
                                {
                                    "answer": "Yes",
                                    "question": "Did the individual have lapse in memory/concentration?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                             "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                                
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },{
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                 
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Procedures clear, reasonable, workable and applicable?",
                                            "answers": [
                                                {
                                                    "answer": "Yes",
                                                    "comment": "RECKLESS VIOLATION"
                                                },
                                                {
                                    "answer": "No",
                                    "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                                
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },{
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "answer": "No",
                                    "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                                
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },{
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                            "answer": "No",
                            "question": "Did the individual fully understand the relevant procedures?",
                            "answers": [
                                {
                                    "answer": "Yes",
                                    "question": "Did the individual have lapse in memory/concentration?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                             "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                                
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },{
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                 
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Procedures clear, reasonable, workable and applicable?",
                                            "answers": [
                                                {
                                                    "answer": "Yes",
                                                    "comment": "RECKLESS VIOLATION"
                                                },
                                               {
                                    "answer": "No",
                                    "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                                
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },{
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "answer": "No",
                                    "question": "Would the peer group have behaved in the same manner?",
                                    "answers": [
                                        {
                                            "answer": "Yes",
                                            "comment": "NONE RESPONSIBLE ERROR - Group Training Required"
                                        },
                                        {
                                            "answer": "No",
                                            "question": "Was training, selection or experience defective?",
                                            "answers": [
                                              
                                                {
                                                    "answer": "Yes",
                                                    "comment": "SYSTEM INDUCED ERROR"
                                                },
                                                  {
                                                    "answer": "No",
                                                    "comment": "POSSIBLE NEGLIGENT ERROR"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
            ]
        },
        question = tree;
        
       
    