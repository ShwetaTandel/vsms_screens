function populateGRA(data) {

    $("#graId").val(data.gra_id);
    
    $("#site").val(data.site_id);
    $("#areaAssessed").val(data.area_assessed);
    $("#gaOwner").val(data.owner);
    $("#gaAssessor").val(data.assessor);
    $("#gaReference").val(data.reference);
    if (data.gra_date !== null && data.gra_date !== '') {
           $('#gaDate').val(new Date(data.gra_date).toISOString().split('T')[0]);
       }
   if (data.review_date !== null && data.review_date !== '') {
            $('#gaRevisionDate').val(new Date(data.review_date).toISOString().split('T')[0]);
        }
   $("#gaDate").max = new Date().toISOString().split("T")[0];
    populateHazards(data.riskHazards);
    populateApprovers($("#site"));
    
    if(data.curr_approver_id !== ''){
     $("#approver").val(data.curr_approver_id);
   }else{
       $("#approver").val('-1');
   }
}

function populateHazards(riskHazards) {
    //populate the 1st roww
    if(riskHazards.length !==0){
    $('#row0').find('.riskHazard').html(riskHazards[0].hazard_detail);
    $('#row0').find('.riskHazardId').val(riskHazards[0].id);
    $('#row0').find('.personAtRisk').html(riskHazards[0].persons_at_risk);
    $('#row0').find('.existingControls').html(riskHazards[0].existing_controls);
    if(riskHazards[0].is_futher_actions_required === '1'){
        $('#row0').find('.furtherActions').html("Yes");
    }else{
          $('#row0').find('.furtherActions').html("No");
    }
    $('#row0').find('.openActions').html(riskHazards[0].openActionsCnt);
    
    
    $('#row0').find('.riskRatingS').html(riskHazards[0].risk_rating_s);
    $('#row0').find('.riskRatingL').html(riskHazards[0].risk_rating_l);
    $('#row0').find('.riskRatingR').html(parseInt(riskHazards[0].risk_rating_l * riskHazards[0].risk_rating_s));
    $('#row0').find('.newRiskRatingS').html(riskHazards[0].new_risk_rating_s);
    $('#row0').find('.newRiskRatingL').html(riskHazards[0].new_risk_rating_l);
    $('#row0').find('.newRiskRatingR').html(parseInt(riskHazards[0].new_risk_rating_l * riskHazards[0].new_risk_rating_s));
    //populate the first row
    for (var i = 1; i < riskHazards.length; i++) {
        var data = $("#row0").clone(true).appendTo("#hazardTb");
        data.find("span").html('');
        var id = "row" + i;
        $('#hazardTb tr:last').attr("id", id);
        id = "#" + id;
        $(id).find('.riskHazard').html(riskHazards[i].hazard_detail);
        $('#row' + i).find('.riskHazardId').val(riskHazards[i].id);
        $('#row' + i).find('.personAtRisk').html(riskHazards[i].persons_at_risk);
        
        $('#row' + i).find('.existingControls').html(riskHazards[i].existing_controls);
        if (riskHazards[i].is_futher_actions_required === '1') {
            $('#row' + i).find('.furtherActions').html("Yes");
        } else {
            $('#row' + i).find('.furtherActions').html("No");
        }
        $('#row' + i).find('.openActions').html(riskHazards[i].openActionsCnt);
        $('#row' + i).find('.riskRatingS').html(riskHazards[i].risk_rating_s);
        $('#row' + i).find('.riskRatingL').html(riskHazards[i].risk_rating_l);
        $('#row' + i).find('.riskRatingR').html(parseInt(riskHazards[i].risk_rating_l * riskHazards[i].risk_rating_s));
        $('#row' + i).find('.newRiskRatingS').html(riskHazards[i].new_risk_rating_s);
        $('#row' + i).find('.newRiskRatingL').html(riskHazards[i].new_risk_rating_l);
        $('#row' + i).find('.newRiskRatingR').html(parseInt(riskHazards[i].new_risk_rating_l * riskHazards[i].new_risk_rating_s));
    }
       $('#hazardTb tr').find('.riskColor').each(function(index) {
                $(this).addClass(getColor(parseInt($(this).text())));
        });
    }
}

function showHazard(elem, data, graId) {
    $('#hazardModal').find("input,textarea,select").val('');
    $('#hazardModal').find(".slider").val(1);
    $('#hazardModal').find(".sliderval").val(1);
    for (var ind = 1; ind <= $("#tab_action tbody tr").length; ind++) {
        $('#actionRow' + ind).remove();
    }
    if (data !== null) {
        var id = $(elem).closest('tr').attr('id').substring(3);
        var risk = data[0].riskHazards[id];
        $('#hazardDetails').val(risk.hazard_detail);
        $('#hazardId').val(risk.id);
      
        $('#personsAtRisk').val(risk.persons_at_risk);
        $('#existingControls').val(risk.existing_controls);
        $('#riskRatingS').val(parseInt(risk.risk_rating_s));
        $('#valriskRatingS').val(parseInt(risk.risk_rating_s));
        $('#riskRatingL').val(parseInt(risk.risk_rating_l));
        $('#valriskRatingL').val(parseInt(risk.risk_rating_l));
        var riskR = parseInt(risk.risk_rating_s) * parseInt(risk.risk_rating_l);
        $('#riskRatingR').val(riskR);
        document.getElementById("valriskRatingR").value = riskR;
        $('#newRiskRatingS').val(parseInt(risk.new_risk_rating_s));
        $('#valnewRiskRatingS').val(parseInt(risk.new_risk_rating_s));
        $('#newRiskRatingL').val(parseInt(risk.new_risk_rating_l));
        $('#valnewRiskRatingL').val(parseInt(risk.new_risk_rating_l));
        var riskR = parseInt(risk.new_risk_rating_s) * parseInt(risk.new_risk_rating_l);
        $('#newRiskRatingR').val(riskR);
        document.getElementById("valnewRiskRatingR").value = riskR;
        
        if(risk.is_futher_actions_required === '1'){
           $('#isFurtherActionRequired').prop('checked',true);
           $("#tab_action").show();
           populateActions(risk.actions);
        }else{
           $('#isFurtherActionRequired').prop('checked',false);
           $("#tab_action").hide();
        }
        populateFiles(risk.files, risk.id, 'showFiles', 'Files', 'Attached Files');
    }
    $('#modalGraId').val(graId);
    $('#hazardModal').modal('show');
}

function calcRisk(elem) {
    var id = $(elem).attr('id');
    document.getElementById("val" + id).value = $(elem).val();
    var riskS = parseInt($("#riskRatingS").val());
    var riskR = parseInt($("#riskRatingL").val());
    document.getElementById('riskRatingR').value = riskS * riskR;
    document.getElementById("valriskRatingR").value = riskS * riskR;
    ;
}

function calcNewRisk(elem) {
    var id = $(elem).attr('id');
    document.getElementById("val" + id).value = $(elem).val();
    var riskS = parseInt($("#newRiskRatingS").val());
    var riskR = parseInt($("#newRiskRatingL").val());
    document.getElementById('newRiskRatingR').value = riskS * riskR;
    document.getElementById("valnewRiskRatingR").value = riskS * riskR;
    ;
}

function populateActions(actions) {

    $('#actionRow0').find("input,textarea,select").val('');
    $('#actionRow0').find(".actionId").val('0');
    
    for (var i = 0; i < actions.length; i++) {
        if (i > 0 && actions.length !== $("#tab_action tbody tr").length) {
            var data = $("#tab_action tr:eq(1)").clone(true).appendTo("#tab_action");
            //var rowCount = $('#tab_action tr').length - 1;
            $('#tab_action tr:last').attr("id", "actionRow" + i);
            data.find("input").val('');
        }

        $('#actionRow' + i).find('.action').val(actions[i].action);
        $('#actionRow' + i).find('.actionSite').val(actions[i].action_site);
        $('#actionRow' + i).find('.actionId').val("AC" + actions[i].id);
        $('#actionRow' + i).find('.actionSubType').val(actions[i].action_sub_type);
        if (actions[i].deadline !== null && actions[i].deadline !== '') {
            $('#actionRow' + i).find('.deadline').val(new Date(actions[i].deadline).toISOString().split('T')[0]);
        }

        populateOwners($('#actionRow' + i).find('.actionSite')[0]);
        $('#actionRow' + i).find('.actionOwner').val(actions[i].action_owner);
        $('#actionRow' + i).find('.actionStatus').val(actions[i].status);
    }
}

function validateHazard() {
    
    var valid = true;
    var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    var checkEmptyInput = $("#hazardModal").find("mandatory").filter(function () {
        if ($(this).val() === "")
            return $(this);
    }).length;
     if (checkEmptyInput > 0) {
        alert("All the fields are mandatory, please enter all the data before saving.");
        valid =false;
        return false;
    }
    if($('#isFurtherActionRequired').is(':checked')){
        var actionInput = $("#actionTableRow").find("input[type=text]:visible,input[type=date],textarea,select").filter(function () {
        if ($(this).val() === "")
            return $(this);
         }).length;
         if(actionInput > 0){
              alert("Please enter all the fields for each of the action or at least add one action if Further actions are required is selected");
              valid =false;
              return false;
    
         }
        var dateInput = $("#actionTableRow").find("input[type=date]").filter(function () {
        if ($(this).val() !== '' && (moment($(this).val(), dateFormats, true).isValid() === false || new Date($(this).val()) < currentDate))
            return $(this);
         }).length;
         if(dateInput > 0){
             alert("Invalid date in action deadline, it has to be a future date");
             valid = false;
             return false;
    
         }
    }else{
        if ($("#riskRatingL").val() !== $("#newRiskRatingL").val() 
           || $("#riskRatingS").val() !== $("#newRiskRatingS").val() 
           || $("#riskRatingR").val() !== $("#newRiskRatingR").val()) {
             alert("If no further actions are required the Risk Rating and New Risk rating should remain the same.");
             valid = false;
             return false;
        }
    } 
    if($("#riskRatingS").val() !== $("#newRiskRatingS").val()){
        valid = confirm("Ratings differ between Severity and New Severity, are you sure you want to continue?");
    }
    
   
    if(valid){
        submitHazardData();
    }
    return false;
}

function validateGRA(filter, action){
    var valid = true;
     var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
    var checkEmptyInput = $("#gaSummData").find(".mandatory").filter(function () {
    if ($(this).val() === "")
            return $(this);
    }).length;
    var date = new Date($("#gaDate").val()); 
    date.setFullYear(date.getFullYear() + 1);
    if (checkEmptyInput > 0) {
      alert("Please enter all the mandortry fields before submitting the Risk Assessment");
      valid = false;       
    }else if($('#row0').find('.riskHazardId').val() === '0'){
      alert("Please add atleast one Risk Hazard in order to proceed.")   ;
      valid = false;
    }else if(new Date($("#gaRevisionDate").val()) <= new Date($("#gaDate").val())){
      alert("Next review date should be after the Revision date")   ;
      valid = false;
        
    }else if(new Date($("#gaRevisionDate").val()) > date){
      alert("Next review date should not be any later "+moment(date).format('DD/MM/YYYY') )   ;
      valid = false;
        
    }else if((document.getElementById('approver').value === '-1' || document.getElementById('approver').value === '') && action!=='L4APPROVE'){
      alert("Please select an approver.")   ;
      valid = false;
    }
    if(valid){
        submitGRAData(filter);
    }
    return valid;
}

function uploadFiles(rhid) {
    var elem = $('#showFiles').find('.file');
    var keepPhotos = [];
    for (var i = 0; i < elem.length; i++) {
        var text3 = elem[i].innerText;
        if (text3.indexOf('\n') !== -1) {
            keepPhotos.push(text3.substring(2, text3.length));
        } else {
            keepPhotos.push(text3.substring(1, text3.length));
        }
    }

    var form_data = new FormData();
    form_data.append('keepRHFiles', JSON.stringify(keepPhotos));
    form_data.append('rhid', rhid);
    var photoFiles = $('#attachFiles')[0].files;
    $.ajax({
        url: '../masterData/deleteFileRecords.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            console.log(response);
            callAjaxForUpload(rhid, photoFiles);


        }
    });
}
function callAjaxForUpload(rhid, fileList) {
    console.log("callAjaxForUpload-> ->" + fileList.length);
    for (var x = 0; x < fileList.length; x++) {
        var file_data = fileList[x];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('rhid', rhid);
        $.ajax({
            url: '../action/uploadFile.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            async: false,
            success: function (response) {
                console.log(x + " number File uploaded for " + type);
            }
        });
    }

}

function populateFiles(files, rhid, divId, title) {
    $('#' + divId).empty();
    $('#' + divId).append('<div class="col-md-2"><label class="control-label">' + title + '</label></div>');
    for (var i = 0; i < files.length; i++) {
        var link = "../action/downloadFile.php?rhid=" + rhid + "&fileName=" + encodeURIComponent(files[i].file_name);
        var newElement = '<div class="alert alert-gray alert-dismissible file" >' +
                '<button type="button" class="close" data-dismiss="alert">' + '&times;' + '</button>' +
                '<a href="' + link + '" class="alert-link">' + files[i].file_name + '</a>' +
                '</div>';
        $('#' + divId).append(newElement);
    }

}



function buildActionsTable(myList) {

    var columns = addAllColumnHeaders(myList);

    for (var i = 0; i < myList.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = myList[i][columns[colIndex]];
            if (cellValue === null) {
                cellValue = "";
            }
            row$.append($('<td/>').html(cellValue));
        }
        $("#actionsDataTable").append(row$);
    }
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(myList)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    // for (var i = 0; i < myList.length; i++) {
    var arr = ["id", "category", "description", "site", "owner", "deadline", "status"];
    for (var col in arr) {
        //if ($.inArray(key, columnSet) == -1) {
        columnSet.push(arr[col]);
        headerTr$.append($('<th/>').html(arr[col].toUpperCase()));
        //}
    }

    $("#actionsDataTable").append(headerTr$);

    return columnSet;
}

function getColor(value){
    var color = "";
    if(value === 1){
       color = "one";
    }else if(value === 2){
        color = "two";
    }else if(value === 3){
        color = "three";
    }else if(value === 4){
        color = "four";
    }else if(value === 5){
       color = "five";
    }else if(value === 6){
       color = "six";
    }else if(value === 8){
        color = "eight";
    }else if(value === 9){
        color = "nine";
    }else if(value === 10){
        color = "ten";
    }else if(value === 12){
        color = "twelve";
    }else if(value === 15){
        color = "fifteen";
    }else if(value === 16){
        color = "sixteen";
    }else if(value === 20){
        color = "twenty";
    }else if(value === 25){
        color = "twentyfive";
    }
    return color;
}