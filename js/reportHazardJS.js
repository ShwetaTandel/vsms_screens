/* JAva script functions to handle the Hazard page*/

//Get day of the week
function getDay(date) {
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var n = weekday[new Date(date).getDay()];
    return n;
}
function validate(action) {

    var valid = true;
    var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    var hazardDate = moment($('#hazardDateTime').data('date'), 'DD-MM-YYYY').format(moment.HTML5_FMT.DATE);
    $('#counterMeasureRequiredError').css('display', 'none');

    if (document.getElementById('hazardSite').value === '-1' || document.getElementById('hazardLocation').value === '-1'
            || $('#hazardDateTime').data('date') === undefined || document.getElementById('hazardDescription').value === ''
            || document.getElementById('makeItSafe').value === '-1' || document.getElementById('anyWitness').value === '-1') {
        alert("Please enter all the fields marked required.");

        valid = false;
    } else if (document.getElementById('makeItSafe').value === 'Yes' && document.getElementById('makeItSafeWhyWhat').value === '') {
        alert("Please enter What you did to make it safe.");
        valid = false;
    } else if (document.getElementById('makeItSafe').value === 'No' && document.getElementById('makeItSafeWhyWhat').value === '') {
        alert("Please enter Why haven't you done anything to make it safe.");
        valid = false;
    } else if (document.getElementById('anyWitness').value === 'Yes' && document.getElementById('anyWitnessWho').value === '') {
        alert("Please enter the details of the witnesses");
        valid = false;
    } else if (action === 'NEW' && document.getElementById('approver').value === '-1') {
        alert("Please select who you want to submit the hazard report to.");
        valid = false;
    }
    if (valid === false) {
        $('#hazardTab').trigger('click');
        return valid;
    }
    //Validate the Hazard Investigation
    if (action === 'L2_APPROVED') {
        if (document.getElementById('actOrCondition').value === "-1"
                || document.getElementById('riskCategory').value === "-1" || document.getElementById('hazardActions').value === '' || document.getElementById('actionsNeeded').value=== '') {
            alert("Please enter all the fields marked required.");
            $('#hazardInvestigationTab').trigger('click');
            valid = false;
        }

        if (document.getElementById('actionsNeeded').value === "Yes") {
            var totalOtherMeasures = $('.clonedCounterMeasure').length;
            
          
            for (var otherCnt = 1; otherCnt <= totalOtherMeasures; otherCnt++) {
                document.getElementById('counterMeasureValidation' + otherCnt).style.display = 'none';
                if ($('#counterMeasure' + otherCnt).val() !== '') {
                    if ($('#counterMeasureType' + otherCnt).val() === '-1'
                            || $('#counterMeasureSite' + otherCnt).val() === '-1' || $('#counterMeasureOwner' + otherCnt).val() === '-1' || $('#counterMeasureDeadline' + otherCnt).val() === '') {

                        valid = false;
                        
                        document.getElementById('counterMeasureValidation' + otherCnt).style.display = 'block';
                    }
                }else if ($('#counterMeasure' + otherCnt).val() === '' && otherCnt===1){
                      $('#counterMeasureRequiredError').css('display', 'block');
                      valid = false;
                }
                if ($('#counterMeasureDeadline' + otherCnt).is(':disabled') === false) {

                    if ($('#counterMeasureDeadline' + otherCnt).val() !== '' && (moment($('#counterMeasureDeadline' + otherCnt).val(), dateFormats, true).isValid() === false || new Date($('#counterMeasureDeadline' + otherCnt).val()) < currentDate)) {
                        valid = false;
                        
                        document.getElementById('counterMeasureDateValidation').style.display = 'block';
                    }
                }


            }
           
        }
    }
    if (valid) {
        if (confirm("Are you sure you want to submit this Hazard Report?")) {
            submitHazard(action);
        } else {
            valid = false;
        }
    }
    return valid;
}

function changeWhatWhyLabel() {
    if (document.getElementById('makeItSafe').value === 'Yes') {
        $("#lblWhatWhy").html("What?<span style='color: red'>*</span>");

    } else if (document.getElementById('makeItSafe').value === 'No') {
        document.getElementById('lblWhatWhy').innerText === "Why?<span style='color: red'>*</span>";
        $("#lblWhatWhy").html("Why?<span style='color: red'>*</span>");

    }
}
function showActions() {
     
    if (document.getElementById('actionsNeeded').value === 'Yes') {
        document.getElementById('actionsDiv').style.display = "block";

    } else {
       alert("Alert: After selecting NO any actions added till now will get deleted once the Hazard is saved/submitted.");
       document.getElementById('actionsDiv').style.display = "none";
       $('#counterMeasureRequiredError').css('display', 'none');
    }
}


function addCounterMeasure() {
    var num = $('.clonedCounterMeasure').length, // how many "duplicatable" input fields we currently have
            newNum = num + 1, // the numeric ID of the new input field being added
            newElem = $('#counterMeasureDiv' + num).clone();

    newElem = newElem.attr('id', 'counterMeasureDiv' + newNum).fadeIn('slow');
    newElem.find('.counterMeasureValidation').attr('id', 'counterMeasureValidation' + newNum).attr('name', 'counterMeasureValidation' + newNum).css('display', 'none');
    newElem.find('.counterMeasureIdLabel').attr('id', 'counterMeasureIdLabel' + newNum).attr('name', 'counterMeasureIdLabel' + newNum).html("Action ID:");
    newElem.find('.counterMeasureId').val('0').attr('id', 'counterMeasureId' + newNum).attr('name', 'counterMeasureId' + newNum);
    newElem.find('.counterMeasure').val('').attr('id', 'counterMeasure' + newNum).attr('name', 'counterMeasure' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureType').val('-1').attr('id', 'counterMeasureType' + newNum).attr('name', 'counterMeasureType' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureStatus').val('').attr('id', 'counterMeasureStatus' + newNum).attr('name', 'counterMeasureStatus' + newNum);
    newElem.find('.counterMeasureDeadline').val('').attr('id', 'counterMeasureDeadline' + newNum).attr('name', 'counterMeasureDeadline' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureSite').val('-1').attr('id', 'counterMeasureSite' + newNum).attr('name', 'counterMeasureSite' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureOwner').val('-1').attr('id', 'counterMeasureOwner' + newNum).attr('name', 'counterMeasureOwner' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureManager').val('').attr('id', 'counterMeasureManager' + newNum).attr('name', 'counterMeasureManager' + newNum);
    $('#counterMeasureDiv' + num).after(newElem);
    $('#btnDelCounter').attr('disabled', false);
}
function uploadHazardFiles(iid) {

    var photoFiles = $('#attachPhotos')[0].files;
    var otherFiles = $('#attachFiles')[0].files;
  
    var elem1 = $('#showPhotos').find('.file');
    var keepHazardPhotos = [];
    for (var i = 0; i < elem1.length; i++) {
        var text = elem1[i].innerText;
        if (text.indexOf('\n') !== -1) {
            keepHazardPhotos.push(text.substring(2, text.length));
        } else {
            keepHazardPhotos.push(text.substring(1, text.length));
        }

    }
    
    var elem2 = $('#showFiles').find('.file');
    var keepHazardFiles = [];
    for (var i = 0; i < elem2.length; i++) {
        var text = elem2[i].innerText;
        if (text.indexOf('\n') !== -1) {
            keepHazardFiles.push(text.substring(2, text.length));
        } else {
            keepHazardFiles.push(text.substring(1, text.length));
        }

    }
    callAjaxForDeleteAndUploadFiles(keepHazardFiles, keepHazardPhotos,  iid, otherFiles, photoFiles)

}
function callAjaxForUpload(hid, fileType, fileList){
    
    for (var x = 0; x < fileList.length; x++) {
        var file_data = fileList[x];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('hid', hid);
        form_data.append('type', fileType);
        $.ajax({
            url: '../action/uploadFile.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            async: false,
            success: function (response) {
                console.log(x + " number File uploaded for " + fileType);
            }
        });
    }
    
}
function callAjaxForDeleteAndUploadFiles(keepHazardFiles, keepHazardPhotos,  hazardId, fileList, photoList) {

    var form_data = new FormData();
    form_data.append('keepHazardFiles', JSON.stringify(keepHazardFiles));
    form_data.append('keepHazardPhotos', JSON.stringify(keepHazardPhotos));
    
    form_data.append('hazardId', hazardId);
    $.ajax({
        url: '../masterData/deleteFileRecords.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            console.log(response);
              callAjaxForUpload(hazardId,'Photos', photoList );
                callAjaxForUpload(hazardId, 'OtherFiles', fileList );

        }
    });


}

function populatePageFromHazardData(data) {

    var status = data.status;
    document.getElementById("hazardSite").value = data.site_id;

    $("#hazardLocation option:contains(" + data.location + ")").attr('selected', 'selected');
    $("#hazardLocation").attr('disabled', true);
    $("#hazardSite").attr('disabled', true);
    $("#hazardDateTime").datetimepicker({
        format: 'DD/MM/YYYY HH:mm:ss',
        date: new Date(data.hazard_date)
    });
    document.getElementById('hazardDateTime').value = new Date(data.hazard_date);
    $('#hazardDateTime').datetimepicker('disable');
    document.getElementById('hazardDay').value = getDay(new Date(data.hazard_date));
    document.getElementById("exactLocation").value = data.location_other;
    $("#exactLocation").attr('disabled', true);
    
    document.getElementById("hazardDescription").value = data.hazard_description;

    document.getElementById("makeItSafe").value = data.have_done_anything_to_make_it_safe;
    if (data.have_done_anything_to_make_it_safe === 'Yes') {
        document.getElementById("makeItSafeWhyWhat").value = data.what_is_done;
    } else {
        document.getElementById("makeItSafeWhyWhat").value = data.why_not_done;
    }
    changeWhatWhyLabel();
    document.getElementById("anyWitness").value = data.any_witness;
    document.getElementById("anyWitnessWho").value = data.witness_name;
    document.getElementById("improvementSuggestion").value = data.improvement_suggestions;
    document.getElementById("reporterName").value = data.reporter_name;
    document.getElementById("reporterDepartment").value = data.reporter_department;
    populateHazardFiles(data.photos, data.hazard_number, 'showPhotos', 'Photos', 'Attached Photos');
    if(status === 'L2_SAVED' || status === 'L2_APPROVED' || status === '_CLOSED'){
        document.getElementById('actOrCondition').value = data.act_or_condition;
        document.getElementById('riskCategory').value = data.risk_category;
        document.getElementById('hazardComments').value = data.hazard_comments;
        document.getElementById('hazardActions').value = data.hazard_actions;
        document.getElementById('actionsNeeded').value = data.further_action_needed;
         populateHazardFiles(data.files, data.hazard_number, 'showFiles', 'OtherFiles', 'Attached Files');
        if(data.further_action_needed === 'Yes'){
          document.getElementById('actionsDiv').style.display = 'block';
            var otherMeasures = data.counterMeasures;
            for (var y = 0; y < otherMeasures.length; y++) {
                var cntr = y + 1;
                if (cntr > 1) {
                    addCounterMeasure();
                }
                $('#counterMeasureId' + cntr).val(otherMeasures[y].id);
                $('#counterMeasure' + cntr).val(otherMeasures[y].action);
                $('#counterMeasureType' + cntr).val(otherMeasures[y].action_sub_type);
                if (otherMeasures[y].deadline !== null && otherMeasures[y].deadline !== '') {
                    $('#counterMeasureDeadline' + cntr).val(new Date(otherMeasures[y].deadline).toISOString().split('T')[0]);
                }
                $('#counterMeasureSite' + cntr).val(otherMeasures[y].action_site);
                populateOwnersForCounterMeasure($('#counterMeasureSite' + cntr));
                $('#counterMeasureOwner' + cntr).val(otherMeasures[y].action_owner);
                populateCounterMeasureManager($('#counterMeasureOwner' + cntr));
                if (data.status === 'L2_SAVED' || data.status === 'L2_APPROVED') {
                    //Update status and action if
                    if (otherMeasures[y].action !== null && otherMeasures[y].action !== '') {

                        if (otherMeasures[y].status !== 'CREATED') {
                            var lower = otherMeasures[y].status.toLowerCase();
                            $('#counterMeasure' + cntr).prop("disabled", true);
                            $('#counterMeasureType' + cntr).prop("disabled", true);
                            $('#counterMeasureDeadline' + cntr).prop("disabled", true);
                            $('#counterMeasureSite' + cntr).prop("disabled", true);
                            $('#counterMeasureOwner' + cntr).prop("disabled", true);
                            $('#counterMeasureIdLabel' + cntr).html("Action ID: AC" + otherMeasures[y].id);
                            $('#counterMeasureStatus' + cntr).val(lower.charAt(0).toUpperCase() + lower.slice(1));
                        }
                    }

                }
            }
        }
        
        
        
    }
}

function populateHazardFiles(files, hid, divId, type, title) {
    $('#' + divId).empty();
    $('#' + divId).append('<div class="col-md-2"><label class="control-label">' + title + '</label></div>')
    for (var i = 0; i < files.length; i++) {
        var link = "../action/downloadFile.php?hid=" + hid + "&fileName=" + encodeURIComponent(files[i].file_name) + "&type=" + type;
        var newElement = '<div class="alert alert-gray alert-dismissible file" >' +
                '<button type="button" class="close" data-dismiss="alert">' + '&times;' + '</button>' +
                '<a href="' + link + '" class="alert-link">' + files[i].file_name + '</a>' +
                '</div>';
        $('#' + divId).append(newElement);
    }

}

function buildActionsTable(myList) {

    var columns = addAllColumnHeaders(myList);

    for (var i = 0; i < myList.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = myList[i][columns[colIndex]];
            if (cellValue === null) {
                cellValue = "";
            }
            row$.append($('<td/>').html(cellValue));
        }
        $("#actionsDataTable").append(row$);
    }
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(myList)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    // for (var i = 0; i < myList.length; i++) {
    var arr = ["id", "category", "description", "site", "owner", "deadline", "status"];
    for (var col in arr) {
        //if ($.inArray(key, columnSet) == -1) {
        columnSet.push(arr[col]);
        headerTr$.append($('<th/>').html(arr[col].toUpperCase()));
        //}
    }

    $("#actionsDataTable").append(headerTr$);

    return columnSet;
}

function discardChanges() {
    if (confirm("Are you sure you want to discard all changes made to this form? This Hazard Report will not be saved.")) {
        window.open("home.php", "_self");
    }
}