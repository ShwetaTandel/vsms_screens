/* 
 Javascript finctions to handle all use cases on report incident page
 */
//Calculate difference between to dates
function getAge(dateString, compareTo) {
    //MM/DD/YYYY   
    var now = new Date(compareTo.substring(6, 10),
            compareTo.substring(3, 5) - 1,
            compareTo.substring(0, 2)
            );
    var yearNow = now.getYear();
    var monthNow = now.getMonth();
    var dateNow = now.getDate();

    /*        var dob = new Date(dateString.substring(6, 10),
     dateString.substring(0, 2) - 1,
     dateString.substring(3, 5)
     );*/
    var dob = new Date(dateString.substring(6, 10),
            dateString.substring(3, 5) - 1,
            dateString.substring(0, 2)
            );


    var yearDob = dob.getYear();
    var monthDob = dob.getMonth();
    var dateDob = dob.getDate();
    var age = {};
    var ageString = "";
    var yearString = "";
    var monthString = "";
    var dayString = "";


    yearAge = yearNow - yearDob;

    if (monthNow >= monthDob)
        var monthAge = monthNow - monthDob;
    else {
        yearAge--;
        var monthAge = 12 + monthNow - monthDob;
    }

    if (dateNow >= dateDob)
        var dateAge = dateNow - dateDob;
    else {
        monthAge--;
        var dateAge = 31 + dateNow - dateDob;

        if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
        }
    }

    age = {
        years: yearAge,
        months: monthAge,
        days: dateAge
    };

    if (age.years > 1)
        yearString = " years";
    else
        yearString = " year";
    if (age.months > 1)
        monthString = " months";
    else
        monthString = " month";
    if (age.days > 1)
        dayString = " days";
    else
        dayString = " day";


    if ((age.years > 0) && (age.months > 0) && (age.days > 0))
        ageString = age.years + yearString + " " + age.months + monthString + " " + age.days + dayString;
    else if ((age.years === 0) && (age.months === 0) && (age.days > 0))
        ageString = age.days + dayString;
    else if ((age.years > 0) && (age.months === 0) && (age.days === 0))
        ageString = age.years + yearString;
    else if ((age.years > 0) && (age.months > 0) && (age.days === 0))
        ageString = age.years + yearString + " " + age.months + monthString;
    else if ((age.years === 0) && (age.months > 0) && (age.days > 0))
        ageString = age.months + monthString + " " + age.days + dayString;
    else if ((age.years > 0) && (age.months === 0) && (age.days > 0))
        ageString = age.years + yearString + " " + age.days + dayString;
    else if ((age.years === 0) && (age.months > 0) && (age.days === 0))
        ageString = age.months + monthString;
    else
        ageString = "Error";

    return ageString;
}
//Get date format
function getFormattedDate(dateString) {
    if (dateString === null || dateString === '') {
        return '';
    }
    var date = new Date(dateString);
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return  day + '/' + month + '/' + year;
    //return year + '-' + month + '-' + day;
}

//Get day of the week
function getDay(date) {
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var n = weekday[new Date(date).getDay()];
    return n;
}
//populate time diff and display
function populateTimeDiff(elem, dateString, field) {
    var startDate = getFormattedDate(dateString);
    var compareDate = getFormattedDate(moment($('#incidentDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss'));
    var age = getAge(startDate,compareDate);
    if (age !== 'Error') {
        var id = $(elem).attr('id');
        var num = id.split('_')[1];
        if (field === "DOB") {
            document.getElementById('personAge_' + num).value = age;
        } else if (field === "ESD") {
            document.getElementById('personLoS_' + num).value = age;
        } else if (field === "TOJ") {
            document.getElementById('personTOJ_' + num).value = age;
        }
    }

}

function showDates(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];
    //var num = id.substring(id.length - 1, id.length);
    if ($(elem).val() !== 'Employee' && $(elem).val() !== 'Agency') {

        $('#personESD_' + num).find('.person-esd-2').val('');
        $('#personTOJSD_' + num).find('.person-jsd-2').val('');

        $('#personESD_' + num).datetimepicker("disable");
        $('#personTOJSD_' + num).datetimepicker("disable");
        $('#personLoS_' + num).val('');
        $('#personTOJ_' + num).val('');

    } else {
        $('#personESD_' + num).datetimepicker("enable");
        $('#personTOJSD_' + num).datetimepicker("enable");
    }

}

function enableDateAndRef(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];
    if ($(elem).is(':checked')) {
        $("#personDateAndRef_" + num).removeAttr('disabled');
    } else {
        $("#personDateAndRef_" + num).val('');
        $("#personDateAndRef_" + num).attr('disabled', 'disabled');
    }
}
function enableSSOWComments(elem) {
    if ($(elem).val() === 'Yes') {
        $('#commentsSSOW').removeAttr('disabled');
    } else {
        $('#commentsSSOW').val('');
        $('#commentsSSOW').attr('disabled', 'disabled');
    }

}
function enableRAComments(elem) {
    if ($(elem).val() === 'No') {
        $('#commentsRA').removeAttr('disabled');
    } else {
        $('#commentsRA').val('');
        $('#commentsRA').attr('disabled', 'disabled');
    }

}
function enableVantecWay(elem) {

    if ($(elem).is(':checked')) {
        $("#vantecWayDecision").val("ILL HEALTH");
        $("#decisionTree").html("");
        $("#vantecWayComments").val("");
        $("#vantecWayDecision").attr("disabled", true);
    } else {
        $("#vantecWayDecision").val("");

        $("#vantecWayDiv :input").attr("disabled", false);
    }

}
function enableVehicleField(elem) {
    var id = $(elem).attr('id');
    //var cnt = id.substring(id.length - 1, id.length);
    var cnt = id.split('_')[1];
    if ($(elem).is(':checked')) {
        $("#personVehicleType_" + cnt).removeAttr('disabled');
        $("#personVehicleReg_" + cnt).removeAttr('disabled');
        $("#personRemovedToggle_" + cnt).removeAttr('disabled');
        

        //Also add the current person name in the license details box
        $('.licensePersonName').each(function () {
            var value = $("#personName_" + cnt).val() === '' ? ('Person ' + cnt) : $("#personName_" + cnt).val();
            $(this).append('<option value ="' + ('Person ' + cnt) + '">' + value + '</option>');
        });
    } else {
        $("#personVehicleType_" + cnt).attr('disabled', 'disabled');
        $("#personVehicleType_" + cnt).val('-1');
        $("#personVehicleReg_" + cnt).attr('disabled', 'disabled');
        $("#personVehicleReg_" + cnt).val('');
        $("#personVehicleTypeComments_" + cnt).attr('disabled', 'disabled');
        $("#personVehicleTypeComments_" + cnt).val('');
        $("#personRemovedToggle_" + cnt).prop('checked', false);
        $("#personRemovedToggle_" + cnt).next().html("&cross;");
        $("#personRemovedToggle_" + cnt).parent().removeClass('.btn-primary');
        $("#personRemovedToggle_" + cnt).parent().addClass('btn-danger');
        $("#personRemovedToggle_" + cnt).attr('disabled', 'disabled');

        $('.licensePersonName').each(function () {
            //var value = (flag === 'NEW') ? 'Person ' + cnt: $("#personName" + cnt).val();
            var value = 'Person ' + cnt;
            $(this).find("option[value='" + value + "']").remove();
        });
    }
}
function enableOtherVehicleComments(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];
    if($(elem).val() === 'Other'){
        
        $("#personVehicleTypeComments_" + num).removeAttr('disabled');
        
    }else{
        $("#personVehicleTypeComments_" + num).val('');
        $("#personVehicleTypeComments_" + num).attr('disabled', 'disabled');
    }
    
}
function enableDAFields(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];

    if (id.split('_')[0] === 'personDAFormReqToggle') {
        if ($(elem).is(':checked')) {
            $("#personDATestRefusedToggle_" + num).removeAttr('disabled');
            $("#personDAPassToggle_" + num).removeAttr('disabled');
            $("#personDAForm_" + num).removeAttr('disabled');
            $("#personDATestedBy_" + num).removeAttr('disabled');
             $("#personDAFormNotReqComments_" + num).val('');
             $("#personDAFormNotReqComments_" + num).attr('disabled', 'disabled');
        } else {
            $("#personDAFormNotReqComments_" + num).removeAttr('disabled');
             $("#personDAForm_" + num).attr('disabled', 'disabled');
            $("#personDAForm_" + num).val('');
            $("#personDATestedBy_" + num).val('-1');
            $("#personDATestedBy_" + num).attr('disabled', 'disabled');
            populateToggle('personDAPassToggle_'+num, '0');
            populateToggle('personDATestRefusedToggle_'+num, '0');
            $("#personDAPassToggle_" + num).attr('disabled', 'disabled');
            $("#personDATestRefusedToggle_" + num).attr('disabled', 'disabled');

            

        }
    }else if (id.split('_')[0] === 'personDATestRefusedToggle') {
        if ($(elem).is(':checked')) {
            $("#personDAPassToggle_" + num).removeAttr('disabled');
            $("#personDAForm_" + num).removeAttr('disabled');
            $("#personDATestedBy_" + num).removeAttr('disabled');
        }else{
            $("#personDAForm_" + num).attr('disabled', 'disabled');
            $("#personDAForm_" + num).val('');
            $("#personDATestedBy_" + num).val('-1');
            $("#personDATestedBy_" + num).attr('disabled', 'disabled');
            populateToggle('personDAPassToggle_'+num, '0');
            $("#personDAPassToggle_" + num).attr('disabled', 'disabled');
          
        }
    }
}

function enableRIDDOR(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];
    if ($(elem).is(':checked')) {
        $("#injuredPersonRiddorRef_" + num).removeAttr('disabled');
        $("#injuredPersonHSEReportedDate_" + num).datetimepicker('enable');
        $("#injuredPersonRIDDORForm_" + num).removeAttr('disabled');

    } else {
        $("#injuredPersonRiddorRef_" + num).attr('disabled', 'disabled');
        $("#injuredPersonRiddorRef_" + num).val('');
        $("#injuredPersonHSEReportedDate_" + num).find(".person-hse-date-2").val('');
        $("#injuredPersonHSEReportedDate_" + num).datetimepicker('disable');
        $("#injuredPersonRIDDORForm_" + num).val('');
        $("#injuredPersonRIDDORForm_" + num).attr('disabled', 'disabled');


    }

}



function discardChanges() {
    if (confirm("Are you sure you want to discard all changes made to this form? This Incident Report will not be saved.")) {
        window.open("home.php", "_self");
    }
}
function validate(action) {
    var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
    var errorTab = '';
    var isVehicleInvolved = false;
    var submitAction = 'L1_CREATED'; // This the Incident submission
    if (action === 'IR') {
        submitAction = 'L2_SUBMITTED'; //This Initial Report Submit
    }
    if (action === 'L2_EDITED') {
        submitAction = 'L2_EDITED'; //This is L2 Approver editing
    } else if (action === 'L2_APPROVED') {
        submitAction = 'L2_APPROVED'; //This is L2 Approver approving
    } else if (action === 'L3_SAVED') {
        submitAction = 'L3_SAVED'; //This is L2 Approver Saving Full Investigation
    } else if (action === 'L3_SUBMITTED') {
        submitAction = 'L3_SUBMITTED'; //This is L2 Approver SUBMITTING Full Investigation
    } else if (action === 'L3_EDITED') {
        submitAction = 'L3_EDITED'; //This is L2 Approver SUBMITTING Full Investigation
    } else if (action === 'L3_APPROVED') {
        submitAction = 'L3_APPROVED'; //This is L2 Approver SUBMITTING Full Investigation
    } else if (action === 'L4_SAVED') {
        submitAction = 'L4_SAVED'; //This is H&S offices
    } else if (action === 'L4_APPROVED') {
        submitAction = 'L4_APPROVED'; //This is last sign off
    }

    var valid = true;
    var currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0);
    var incidentDate = moment($('#incidentDateTime').data('date'), 'DD-MM-YYYY').format(moment.HTML5_FMT.DATE);
    

    if (document.getElementById('incidentSite').value === '-1' || document.getElementById('incidentLocation').value === '-1' || $('#incidentDateTime').data('date') === undefined 
       || document.getElementById('incidentBrief').value === '' || document.getElementById('eventSeverity').value === '-1') {
        alert("Please enter all the fields marked required.");
        $('#incidentTab').trigger('click');
        return false;
    }

    var locationElement = document.getElementById("incidentLocation");
    var location = locationElement.options [locationElement.selectedIndex].text;
    if (location === 'Other') {
        if (document.getElementById('exactLocation').value === "") {
            alert("Please enter exact location or choose any other location from the list.")
            $('#incidentTab').trigger('click');
            valid = false;
        }
    }
   
    if ($("input:radio[name=incidentType]:checked").val() === undefined) {
        valid = false;
        alert("Please answer all the questions, in order to determine the Incident Type.");
        $('#incidentTab').trigger('click');
    } else {
        // check if all questions are answered
        var question3Ans, question4Ans = "";
        if (document.getElementById('question3Div').style.display === 'block') {
            question3Ans = $("input:radio[name=answer3]:checked").val();
            if (question3Ans === undefined) {
                valid = false;
                alert("Please answer all the questions, in order to determine the Incident Type.");
                $('#incidentTab').trigger('click');
            }
        }
        if (document.getElementById('question4Div').style.display === 'block') {
            question4Ans = $("input:radio[name=answer4]:checked").val();
            if (question4Ans === undefined) {
                valid = false;
                alert("Please answer all the questions, in order to determine the Incident Type.");
                $('#incidentTab').trigger('click');
            }
        }
    }



    //Do all the Initila report validations
    if (action !== 'NEW') {
        $('#licenseValidationError').css('display', 'none');
        $('#licenseValidityError').css('display', 'none');
        $('#licenseDateValidationError').css('display', 'none');
        $('#actionValidationError').css('display', 'none');
        $('#actionFieldsRequiredValidationError').css('display', 'none');
        $('#messageValidationError').css('display', 'none');
        $('#generalValidationError').css('display', 'none');
        $('#procesValidationError').css('display', 'none');
        $('#approverValidationError').css('display', 'none');
        $('#personValidationError').css('display', 'none');
        $('#processValidationError').css('display', 'none');
        $('#areaValidationError').css('display', 'none');
        $('#witnessFileValidationError').css('display', 'none');

        if (document.getElementById('incidentDetail').value === '') {
            $('#areaValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('actionType').value === '-1' || document.getElementById('actionType').value === '') {
            $('#areaValidationError').css('display', 'block');
            valid = false;
        }

        if (document.getElementById('incidentArea').value === '-1' || document.getElementById('incidentArea').value === '') {
            $('#areaValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('lighting').value === '-1' || document.getElementById('lighting').value === '') {
            $('#generalValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('temperature').value === '-1' || document.getElementById('temperature').value === '') {
            $('#generalValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('noise').value === '-1' || document.getElementById('noise').value === '') {
            $('#generalValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('surfaceCondition').value === '-1' || document.getElementById('surfaceCondition').value === '') {
            $('#generalValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('surfaceType').value === '-1' || document.getElementById('surfaceType').value === '') {
            $('#generalValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('typeOfStillage').value === '-1' || document.getElementById('typeOfStillage').value === '') {
            $('#processValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('immdCause').value === '') {
            $('#processValidationError').css('display', 'block');
            valid = false;
        }
        if (document.getElementById('actionType').value === 'Other') {
            if (document.getElementById('otherActionType').value === '') {
                $('#generalValidationError').css('display', 'block');
                valid = false;
            }
        }

        var isPersonInvolved = $("input:radio[name=isPersonInvolved]:checked").val();
        if (isPersonInvolved === undefined) {
            valid = false;
            $('#personValidationError').css('display', 'block');
        }
        //Validate person details 
        if (isPersonInvolved === 'Yes') {
            var totalPersons = $('.clonedInput').length;
            for (var cnt = 1; cnt <= totalPersons; cnt++) {
                $('#vehicleValidationError_' + cnt).css('display', 'none');
                $('#daFormValidationError_' + cnt).css('display', 'none');
                $('#prevIncidentsValidationError_' + cnt).css('display', 'none');
                $('#injuryValidationError_' + cnt).css('display', 'none');
                $('#personGeneralValidationError_' + cnt).css('display', 'none');
                $('#personDateValidationError_' + cnt).css('display', 'none');

                //Previous incidents
                var ele = $('#personName_' + cnt);
                if (ele.val() === '') {
                    $('#personGeneralValidationError_' + cnt).css('display', 'block');
                    valid = false;
                }
                var empStatus = $('#personEmpStatus_' + cnt);
                if (empStatus.val() === '-1') {
                    $('#personGeneralValidationError_' + cnt).css('display', 'block');
                    valid = false;
                }
                ele = $('#personPPEWorn_' + cnt);
                if (ele.val().length === 0) {
                    $('#personGeneralValidationError_' + cnt).css('display', 'block');
                    valid = false;
                }

                ele = $('#personDOB_' + cnt).find('.person-dob-2');
                if (ele.val() === '') {
                    $('#personGeneralValidationError_' + cnt).css('display', 'block');
                    valid = false;
                }
                if (empStatus.val() === 'Employee' || empStatus.val() === 'Agency') {
                    var eleJSD = $('#personTOJSD_' + cnt).find('.person-jsd-2');

                    if (eleJSD.val() === '') {
                        $('#personGeneralValidationError_' + cnt).css('display', 'block');
                        valid = false;
                    }

                    var eleESD = $('#personESD_' + cnt).find('.person-esd-2');

                    if (eleESD.val() === '') {
                        $('#personGeneralValidationError_' + cnt).css('display', 'block');
                        valid = false;
                    }
                    if (moment(eleJSD.val(), "DD/MM/YYYY") < moment(eleESD.val(), "DD/MM/YYYY")) {
                        $('#personDateValidationError_' + cnt).css('display', 'block');
                        valid = false;
                    }

                }


                ele = $('#personPrevIncidentsToggle_' + cnt);
                if ($(ele).is(':checked')) {
                    if ($('#personDateAndRef_' + cnt).val() === '') {
                        valid = false;
                        $('#prevIncidentsValidationError_' + cnt).css('display', 'block');
                    }
                }
                //Vehicle fields
                ele = $('#personVehicleInvolvedToggle_' + cnt);
                if ($(ele).is(':checked')) {
                    isVehicleInvolved = true;


                    if ($('#personVehicleType_' + cnt).val() === '-1' || $('#personVehicleReg_' + cnt).val() === '') {
                        valid = false;
                        $('#vehicleValidationError_' + cnt).css('display', 'block');
                    }
                    var licenseAttached = true;
                    var personFound = false;
                    $('#tab_license tbody tr').each(function (i, element) {
                        var html = $(this).html();
                        if (html !== '')
                        {
                            var license = $(this).find('.licenseFile')[0];
                            var person = $(this).find('.licensePersonName option:selected').text();
                            var licenseLink = $(this).find('.fileLinks').children();
                            var expiryDate = $(this).find('.licenseExpiryDate').val();
                            var isValid = $(this).find('.licenseValid').val();
                            if (person === '' || person === '-1' || isValid === '-1' || expiryDate === '') {
                                valid = false;
                                $('#licenseValidationError').css('display', 'block');
                            }
                            if (person === $('#personName_' + cnt).val()) {
                                personFound = true;
                                if (license.files.length + licenseLink.length === 0) {
                                    licenseAttached = false;
                                }
                            }
                            if (moment(expiryDate, dateFormats, true).isValid() === false) {
                                valid = false;
                                $('#licenseDateValidationError').css('display', 'block');
                            }
                        }
                    });
                    if (licenseAttached === false || personFound === false) {
                        valid = false;
                        $('#licenseValidationError').css('display', 'block');
                    }
                }
                ele = $('input[name=isLocationOfInjury_' + cnt + ']');
                //ele = $('#personLocationInjury_' + cnt);
                if ($(ele).is(':disabled') === false) {
                    if ($('#injuryYes_' + cnt).is(':checked')) {
                        if ($("input:checkbox[name=personLocationInjury_" + cnt + "]:checked").length === 0) {
                            valid = false;
                            $('#injuryValidationError_' + cnt).css('display', 'block');
                        }
                    }
                }
                ele = $('#personTreatment_' + cnt);
                if ($(ele).is(':disabled') === false) {
                    if ($(ele).val().length === 0) {
                        valid = false;
                        $('#injuryValidationError_' + cnt).css('display', 'block');
                    }
                }
                ele = $('#personTreatmentDetails_' + cnt);
                if ($(ele).is(':disabled') === false) {
                    if ($(ele).val() === '') {
                        valid = false;
                        $('#injuryValidationError_' + cnt).css('display', 'block');
                    }
                }
                ele = $('#personDAFormReqToggle_' + cnt);
                if ($(ele).is(':checked')) {
                    var eleTestReq = $('#personDATestRefusedToggle_' + cnt);
                    if ($(eleTestReq).is(':checked')) {
                        var fileInput = $('#personDAForm_' + (cnt))[0];
                        var elem0 = $('#showDAFiles_' + (cnt)).find('.file');
                        if (fileInput.files.length + elem0.length === 0 || $('#personDATestedBy_' + (cnt)).val()=== '-1') {
                            valid = false;
                            $('#daFormValidationError_' + cnt).css('display', 'block');
                        }    
                    }
                    
                }
            }
        }
        var ele = $('#keyPoints');
        if ($(ele).is(':disabled') === false) {
            if ($(ele).val() === '') {
                valid = false;
                $('#messageValidationError').css('display', 'block');
            }
        }

        ele = $('#mainMessage');
        if ($(ele).is(':disabled') === false) {
            if ($(ele).val() === '') {
                valid = false;
                $('#messageValidationError').css('display', 'block');
            }
        }
        var witnessfileInput = $('#attachWitnessStmts')[0];
        var showWitnessFiles = $('#showWitnessStmts').find('.file');
        if (witnessfileInput.files.length + showWitnessFiles.length === 0) {
            valid = false;
            $('#witnessFileValidationError').css('display', 'block');
        }
        $('#tab_license tbody tr').each(function (i, element) {
            var html = $(this).html();
            if (html !== '')
            {
                var license = $(this).find('.licenseFile')[0];
                var person = $(this).find('.licensePersonName option:selected').text();
                var licenseLink = $(this).find('.fileLinks').children();
                var expiry = $(this).find(".licenseExpiryDate").val();
                var licenseValid = $(this).find(".licenseValid").val();

                if (person !== "") {
                    if (license.files.length + licenseLink.length === 0) {
                        valid = false;
                        $('#licenseValidationError').css('display', 'block');
                    }
                    if (expiry === '') {
                        valid = false;
                        $('#licenseValidationError').css('display', 'block');
                    }
                    if (licenseValid === '') {
                        valid = false;
                        $('#licenseValidationError').css('display', 'block');
                    }
                    if (new Date(expiry) < new Date(incidentDate) && licenseValid !== 'No') {
                        valid = false;
                        $('#licenseValidityError').css('display', 'block');

                    } else if (new Date(expiry) >= new Date(incidentDate) && licenseValid !== 'Yes') {
                        valid = false;
                        $('#licenseValidityError').css('display', 'block');

                    }


                }
            }
        });
        var actionCnt = 0;
        var actionValid = true;
        $('#tab_action tbody tr').each(function (i, tr) {
            var html = $(this).html();
            if (html !== '')
            {

                var action = $(this).find('.containmentAction').val();
                var actionSite = $(this).find('.containmentSite').val();
                var actionOwner = $(this).find('.actionOwner').val();
                if (action === '' || actionSite === '-1' || actionOwner === '' || actionOwner === '-1') {
                    actionValid = false;

                }
                actionCnt++;

            }
        });
        if (actionCnt === 1 && actionValid === false) {
            valid = false;
            // $('#actionValidationError').css('display', 'block');
            $('#actionFieldsRequiredValidationError').css('display', 'block');
        } else if (actionCnt > 1 && actionValid === false) {
            valid = false;
            $('#actionFieldsRequiredValidationError').css('display', 'block');
        }
        if (valid === false) {
            errorTab = 'initialReportTab';
        }
        //Validation for fiull investigation
        if (action === 'L3_EDITED' || action === 'L3_SUBMITTED' || action === 'L3_APPROVED' || action === 'L4_APPROVED' || action === 'L4_SAVED') {
            var newActionAdded = false;
            document.getElementById('eventSummaryValidation').style.display = 'none';
            document.getElementById('radioButtonsValidation').style.display = 'none';
            document.getElementById('commentsValidation').style.display = 'none';
            document.getElementById('commentsRequiredValidation').style.display = 'none';
            document.getElementById('otherCounterMeasureDateValidation').style.display = 'none';
            document.getElementById('mheReqValidation').style.display = 'none';
             document.getElementById('vantecWayValidation').style.display = 'none';
             document.getElementById('investigationTeamValidation').style.display = 'none';


            //Required validation
            if (document.getElementById('eventSummary').value === '') {
                valid = false;
                document.getElementById('eventSummaryValidation').style.display = 'block';
            }

            var totalWhys = $('.clonedWhy').length;
            for (var whyCnt = 1; whyCnt <= totalWhys; whyCnt++) {
                document.getElementById('whyBlockValidation_' + whyCnt).style.display = 'none';
                document.getElementById('counterMeasureValidationA_' + whyCnt).style.display = 'none';
                document.getElementById('counterMeasureValidationB_' + whyCnt).style.display = 'none';
                document.getElementById('counterMeasureValidationC_' + whyCnt).style.display = 'none';
                document.getElementById('counterMeasureDateValidation_' + whyCnt).style.display = 'none';
                if ($('#why1_' + whyCnt).val() === '' || $('#why2_' + whyCnt).val() === '' || $('#why3_' + whyCnt).val() === '' || $('#rootCauseA_' + whyCnt).val() === '') {
                    document.getElementById('whyBlockValidation_' + whyCnt).style.display = 'block';
                    valid = false;
                }
                if ($('#counterMeasureA_' + whyCnt).val() === '' || $('#counterMeasureTypeA_' + whyCnt).val() === '-1'
                        || $('#counterMeasureSiteA_' + whyCnt).val() === '-1' || $('#counterMeasureOwnerA_' + whyCnt).val() === '-1' || $('#counterMeasureDeadlineA_' + whyCnt).val() === '') {

                    valid = false;
                    document.getElementById('counterMeasureValidationA_' + whyCnt).style.display = 'block';
                }
                if ($('#rootCauseB_' + whyCnt).val() !== '') {
                    //check if all fields are filled up
                    if ($('#counterMeasureB_' + whyCnt).val() === '' || $('#counterMeasureTypeB_' + whyCnt).val() === '-1'
                            || $('#counterMeasureSiteB_' + whyCnt).val() === '-1' || $('#counterMeasureOwnerB_' + whyCnt).val() === '-1' || $('#counterMeasureDeadlineB_' + whyCnt).val() === '') {

                        valid = false;
                        document.getElementById('counterMeasureValidationB_' + whyCnt).style.display = 'block';
                    }
                }
                if ($('#rootCauseC_' + whyCnt).val() !== '') {
                    //check if all fields are filled up
                    if ($('#counterMeasureC_' + whyCnt).val() === '' || $('#counterMeasureTypeC_' + whyCnt).val() === '-1'
                            || $('#counterMeasureSiteC_' + whyCnt).val() === '-1' || $('#counterMeasureOwnerC_' + whyCnt).val() === '-1' || $('#counterMeasureDeadlineC_' + whyCnt).val() === '') {

                        valid = false;
                        document.getElementById('counterMeasureValidationC_' + whyCnt).style.display = 'block';
                    }
                }
                if ($('#counterMeasureB_' + whyCnt).val() !== '' && ($('#counterMeasureTypeB_' + whyCnt).val() === '-1' || $('#counterMeasureDeadlineB_' + whyCnt).val() === ''
                        || $('#counterMeasureSiteB_' + whyCnt).val() === '-1' || $('#counterMeasureOwnerB_' + whyCnt).val() === '-1')) {
                    valid = false;
                    document.getElementById('counterMeasureValidationB_' + whyCnt).style.display = 'block';
                }
                if ($('#counterMeasureC_' + whyCnt).val() !== '' && ($('#counterMeasureTypeC_' + whyCnt).val() === '-1' || $('#counterMeasureDeadlineC_' + whyCnt).val() === ''
                        || $('#counterMeasureSiteC_' + whyCnt).val() === '-1' || $('#counterMeasureOwnerC_' + whyCnt).val() === '-1')) {
                    valid = false;
                    document.getElementById('counterMeasureValidationC_' + whyCnt).style.display = 'block';
                }

                if ($('#counterMeasureDeadlineA_' + whyCnt).is(':disabled') === false) {

                    if ($('#counterMeasureDeadlineA_' + whyCnt).val() !== '' && (moment($('#counterMeasureDeadlineA_' + whyCnt).val(), dateFormats, true).isValid() === false || new Date($('#counterMeasureDeadlineA_' + whyCnt).val()) < currentDate)) {
                        valid = false;
                        document.getElementById('counterMeasureDateValidation_' + whyCnt).style.display = 'block';
                    }
                }
                if ($('#counterMeasureDeadlineB_' + whyCnt).is(':disabled') === false) {

                    if ($('#counterMeasureDeadlineB_' + whyCnt).val() !== '' && (moment($('#counterMeasureDeadlineB_' + whyCnt).val(), dateFormats, true).isValid() === false || new Date($('#counterMeasureDeadlineB_' + whyCnt).val()) < currentDate)) {
                        valid = false;
                        document.getElementById('counterMeasureDateValidation_' + whyCnt).style.display = 'block';
                    }
                }
                if ($('#counterMeasureDeadlineC_' + whyCnt).is(':disabled') === false) {

                    if ($('#counterMeasureDeadlineC_' + whyCnt).val() !== '' && (moment($('#counterMeasureDeadlineC_' + whyCnt).val(), dateFormats, true).isValid() === false || new Date($('#counterMeasureDeadlineC_' + whyCnt).val()) < currentDate)) {
                        valid = false;
                        document.getElementById('counterMeasureDateValidation_' + whyCnt).style.display = 'block';
                    }
                }
                if ($('#counterMeasureA_' + whyCnt).is(':disabled') === false && $('#counterMeasureA_' + whyCnt).val() !== '') {
                    newActionAdded = true;
                }
                if ($('#counterMeasureB_' + whyCnt).is(':disabled') === false && $('#counterMeasureB_' + whyCnt).val() !== '') {
                    newActionAdded = true;
                }
                if ($('#counterMeasureC_' + whyCnt).is(':disabled') === false && $('#counterMeasureC_' + whyCnt).val() !== '') {
                    newActionAdded = true;
                }


            }
            var totalOtherMeasures = $('.clonedCounterMeasure').length;
            for (var otherCnt = 1; otherCnt <= totalOtherMeasures; otherCnt++) {
                document.getElementById('otherCounterMeasureValidation' + otherCnt).style.display = 'none';
                if ($('#otherCounterMeasure' + otherCnt).val() !== '') {
                    if ($('#otherCounterMeasureType' + otherCnt).val() === '-1'
                            || $('#otherCounterMeasureSite' + otherCnt).val() === '-1' || $('#otherCounterMeasureOwner' + otherCnt).val() === '-1' || $('#otherCounterMeasureDeadline' + otherCnt).val() === '') {

                        valid = false;
                        document.getElementById('otherCounterMeasureValidation' + otherCnt).style.display = 'block';
                    }
                }
                if ($('#otherCounterMeasureDeadline' + otherCnt).is(':disabled') === false) {

                    if ($('#otherCounterMeasureDeadline' + otherCnt).val() !== '' && (moment($('#otherCounterMeasureDeadline' + otherCnt).val(), dateFormats, true).isValid() === false || new Date($('#otherCounterMeasureDeadline' + otherCnt).val()) < currentDate)) {
                        valid = false;
                        document.getElementById('otherCounterMeasureDateValidation').style.display = 'block';
                    }
                }
                if ($('#otherCounterMeasure' + otherCnt).is(':disabled') === false && $('#otherCounterMeasure' + otherCnt).val() !== '') {
                    newActionAdded = true;
                }

            }
            if ($("#raSuitable").val() === '' || $("#raSuitable").val() === '-1') {
                valid = false;
                document.getElementById('commentsValidation').style.display = 'block';
            }
            if ($("#ssowSuitable").val() === '' || $("#ssowSuitable").val() === '-1') {
                valid = false;
                document.getElementById('commentsValidation').style.display = 'block';
            }

            if ($("#raSuitable").val() === 'No' && $("#commentsRA").val() === '') {
                valid = false;
                document.getElementById('commentsRequiredValidation').style.display = 'block';
            }
            if ($("#ssowSuitable").val() === 'No' && $("#commentsSSOW").val() === '') {
                valid = false;
                document.getElementById('commentsRequiredValidation').style.display = 'block';
            }
            if (isVehicleInvolved) {
                if ($('#mheTrainingReq').val() === -1 || $('#mheTrainingReq').val() === '-1') {
                    valid = false;
                    document.getElementById('mheReqValidation').style.display = 'block';
                }

            }

            if ($("input:radio[name=personConcentration]:checked").val() === undefined || $("input:radio[name=personExperience]:checked").val() === undefined || $("input:radio[name=personTraining]:checked").val() === undefined
                    || $("input:radio[name=machineCondition]:checked").val() === undefined || $("input:radio[name=machineMaintenance]:checked").val() === undefined || $("input:radio[name=machineFacility]:checked").val() === undefined
                    || $("input:radio[name=methodSSOW]:checked").val() === undefined || $("input:radio[name=methodRuling]:checked").val() === undefined || $("input:radio[name=methodRiskAssessment]:checked").val() === undefined
                    || $("input:radio[name=environmentFloor]:checked").val() === undefined || $("input:radio[name=environmentLighting]:checked").val() === undefined || $("input:radio[name=environmentCondition]:checked").val() === undefined) {

                valid = false;
                document.getElementById('radioButtonsValidation').style.display = 'block';
            }
            if(document.getElementById('vantecWayDecision').value === ''){
                valid = false;
                document.getElementById('vantecWayValidation').style.display = 'block';
            }
            if(document.getElementById('vantecWayDecision').value!== 'ILL HEALTH' && document.getElementById('vantecWayComments').value === ''){
                valid = false;
                    document.getElementById('vantecWayValidation').style.display = 'block';
            }
             if(document.getElementById('investigationTeamNames').value === '' ){
                valid = false;
                document.getElementById('investigationTeamValidation').style.display = 'block';
            }


        }
        if (action === 'L3_APPROVED') {
            if (document.getElementById('approver').value === '-1') {
                valid = false;
                document.getElementById('approverValidationError').style.display = 'block';
            }
        }
        if (valid === false && errorTab === '') {
            errorTab = 'fullInvestigationTab';

        }
        if (action === 'L4_APPROVED') {
            //do h&S sign off validations
            document.getElementById('hsRequiredValidatiionError').style.display = 'none';
            if ($('#category').val() === '' || $('#categoryLevel').val() === '' || $('#type').val() === '' || $('#actOrConditon').val() === '' || $('#isDangerousOccurence').val() === '-1' || $('#classification').val() === '') {
                valid = false;
                document.getElementById('hsRequiredValidatiionError').style.display = 'block';
            }
            var totalInjuredPersons = $('.clonedInjuredDetails').length;
            for (var injuredCnt = 1; injuredCnt <= totalInjuredPersons; injuredCnt++) {
                document.getElementById('injuredPersonValidationError_' + injuredCnt).style.display = 'none';
                document.getElementById('injuredPersonDaysLostError_' + injuredCnt).style.display = 'none';
                document.getElementById('injuredPersonDaysLostLTIError_' + injuredCnt).style.display = 'none';
                document.getElementById('injuredPersonValidationRiddorError_' + injuredCnt).style.display = 'none';
                document.getElementById('injuredPersonDaysLostMoreThan7Error_' + injuredCnt).style.display = 'none';

                if ($('#injuredPersonName_' + injuredCnt).val() !== '-1') {
                    if ($('#injuredPersonTreatmentDetails_' + injuredCnt).val() === '' || $('#dayLost_' + injuredCnt).val() === '') {
                        valid = false;
                        document.getElementById('injuredPersonValidationError_' + injuredCnt).style.display = 'block';

                    }
                    if ($('#type').val() === 'LTI' && parseInt($('#dayLost_' + injuredCnt).val()) <= 0){
                        valid = false;
                        document.getElementById('injuredPersonDaysLostLTIError_' + injuredCnt).style.display = 'block';
                        
                    }
                    else if (parseInt($('#dayLost_' + injuredCnt).val()) < 0) {
                        valid = false;
                        document.getElementById('injuredPersonDaysLostError_' + injuredCnt).style.display = 'block';

                    }
                    
                    if (parseInt($('#dayLost_' + injuredCnt).val()) > 7 && $('#isRIDDORToggle_' + injuredCnt).is(':checked') === false) {
                        valid = false;
                        document.getElementById('injuredPersonDaysLostMoreThan7Error_' + injuredCnt).style.display = 'block';

                    } else if ($('#isRIDDORToggle_' + injuredCnt).is(':checked')) {
                        var fileList = $('#showRIDDORFiles_' + (injuredCnt)).find('.file');
                        var fileInput = $('#injuredPersonRIDDORForm_' + (injuredCnt))[0];
                        if ($('#injuredPersonRiddorRef_' + injuredCnt).val() === '' || $('#injuredPersonHSEReportedDate_' + cnt).find('.person-hse-date-2').val() === '' || (fileInput.files.length + fileList.length === 0)) {
                            valid = false;
                            document.getElementById('injuredPersonValidationRiddorError_' + injuredCnt).style.display = 'block';
                        }

                    }
                }

            }
        }
        if (submitAction.split('_')[1] === 'SUBMITTED') {
            if (document.getElementById('approver').value === '-1') {
                $('#approverValidationError').css('display', 'block');
                valid = false;
            }


        }
        if (valid === false && errorTab === '') {
            errorTab = 'hsTab';

        }
    }
    var message = "Are you sure you want to submit the Incident Report?";
    if (action === 'L2_EDITED') {
        message = "Are you sure you want the save the changes made to Initial Report?"
    } else if (action === 'L3_EDITED') {
        message = "Are you sure you want the save the changes made to Full Investigation?"
    } else if (action === 'L2_APPROVED') {
        message = "Are you sure you want to approve this Initial Report? This action cannot be undone. If you have any comments, please add them in the box below (Maximum allowed 500 characters):"
    } else if (action === 'L3_APPROVED') {
        message = "Are you sure you want to approve this Full Investigation? This action cannot be undone. If you have any comments, please add them in the box below (Maximum allowed 500 characters):"
    } else if (action === 'L3_SAVED') {
        message = "Are you sure you want to save the changes made to Full Investigation?"
    } else if (action === 'L4_SAVED' && newActionAdded) {
        message = "Are you sure you want to save the changes made to H&S Sign Off section? All new countermeasures will be submitted to the selected owners once you click OK";
    } else if (action === 'L4_SAVED') {
        message = "Are you sure you want to save the changes made to H&S Sign Off section?"
    } else if (action === 'L4_APPROVED') {
        message = "Are you sure you want to close off this Incident Report? This action cannot be undone.";
    }
    if (valid) {
        if (action === 'L2_APPROVED' || action === 'L3_APPROVED') {
            //var comment = prompt(message, '');
            if (action === 'L2_APPROVED') {
                $('#promptMessage').html('Are you sure you want to approve Initial Report? This action cannot be undone. If you have any comments please add them in the box below (Maximum allowed 500 characters)');
            } else {
                $('#promptMessage').html('Are you sure you want to approve Full Investigation? This action cannot be undone. If you have any comments please add them in the box below (Maximum allowed 500 characters)');
            }


            $('#promptAction').val(submitAction);

            $('#promptModal').modal('show');

        } else {
            if (confirm(message)) {
                submitIncidentData(submitAction, '');
            } else {
                valid = false;
            }
        }
    } else {
        if (action !== 'NEW') {
            alert('Make sure you have entered all fields marked required before submitting the Incident Data.');
            $('#' + errorTab).trigger('click');
        }
    }

    return valid;
}

function populatePageFromData(data) {
    document.getElementById("incidentSite").value = data.site_id;

    $("#incidentLocation option:contains(" + data.location + ")").attr('selected', 'selected');
    $("#incidentSite").attr('disabled', true);
    $("#incidentDateTime").datetimepicker({
        format: 'DD/MM/YYYY HH:mm:ss',
        date: new Date(data.incident_date)
    });

    if (data.curr_approver_id === "") {
        if ($('#approver').children('option').length === 2) {
            $('#approver option').eq(1).prop('selected', true);
        }
    } else {
        var hasValue = !!$('#approver > option[value="' + data.curr_approver_id + '"]').length;
        if (hasValue)
            document.getElementById('approver').value = data.curr_approver_id;
        else
            document.getElementById('approver').value = "-1";
    }


    document.getElementById('incidentDateTime').value = new Date(data.incident_date);
    $('#incidentDateTime').datetimepicker('disable');


    //$('#incidentDateTime').data("DateTimePicker").date(moment(new Date ).format('DD/MM/YYYY HH:mm'));
    document.getElementById('incidentDay').value = getDay(new Date(data.incident_date));
    document.getElementById("exactLocation").value = data.location_other;
    document.getElementById("incidentBrief").value = data.incident_brief;
    document.getElementById("eventSeverity").value = data.event_severity;
    
    $("input[name=incidentType][value='" + data.incident_type + "']").prop("checked", true);
    showIncidentDesc(data.incident_type);

    document.getElementById('question1Div').style.display = "block";
    document.getElementById('question1Label').innerText = data.question1_text;
    $("input[name=answer1][value='" + data.question1_answer + "']").prop("checked", true);

    if (data.question2_text !== '') {
        document.getElementById('question2Div').style.display = "block";
        document.getElementById('question2Label').innerText = data.question2_text;
        $("input[name=answer2][value='" + data.question2_answer + "']").prop("checked", true);
    }
    if (data.question3_text !== '') {
        document.getElementById('question3Div').style.display = "block";
        document.getElementById('question3Label').innerText = data.question3_text;
        $("input[name=answer3][value='" + data.question3_answer + "']").prop("checked", true);
    }
    if (data.question4_text !== '') {
        document.getElementById('question4Div').style.display = "block";
        document.getElementById('question4Label').innerText = data.question4_text;
        $("input[name=answer4][value='" + data.question4_answer + "']").prop("checked", true);
    }

    // Populate initial reposrt data 
    document.getElementById('incidentDetail').value = data.incident_detail;
    document.getElementById('actionType').value = data.action_type;
    document.getElementById('incidentArea').value = data.area;
    document.getElementById('otherActionType').value = data.action_type_other;
    document.getElementById('lighting').value = data.lighting;
    document.getElementById('temperature').value = data.temperature;
    document.getElementById('noise').value = data.noise;
    document.getElementById('surfaceType').value = data.surface_type;
    document.getElementById('surfaceCondition').value = data.surface_condition;
    document.getElementById('immdCause').value = data.immediate_cause;
    document.getElementById('estimatedCost').value = data.estimated_cost;
    document.getElementById('estimatedCostOther').value = data.estimated_cost_other;
    document.getElementById('typeOfStillage').value = data.stillage_type;
    document.getElementById('keyPoints').value = data.key_points;
    document.getElementById('mainMessage').value = data.main_message;
     document.getElementById('graId').value = data.gra_id;

    if (data.is_person_involved === '1') {
        $("#personYes").prop("checked", true);
    } else if (data.is_person_involved === '0') {
        $("#personNo").prop("checked", true);
    }
    showFirstPerson();
    populateToggle("activityStandard", data.is_activity_standard_process);
    if (data.incident_type === 'Serious Accident' || data.incident_type === 'Accident*' || data.incident_type === 'Incident*' || data.incident_type === 'Near Miss*') {
        $("#keyPoints").prop('disabled', false);
        $("#mainMessage").prop('disabled', false);
    }
    //populate actions

    populateActions(data);


    //Get the persons data populated
    if (data.is_person_involved === '1') {
        var persons = data.persons;
        for (var i = 0; i < persons.length; i++) {
            if (i > 0) {
                addPerson('EDIT');
            }
            populatePerson(i + 1, persons[i], data.incident_type);

        }
        populateLicense(data);
    }
    //Populate photos files
    populateFiles(data.photos, data.incident_number, 'showPhotos', 'Photos', 'Attached Photos');
    populateFiles(data.ssows, data.incident_number, 'showSSOW', 'ssow', 'Attached SSoW Files');
    populateFiles(data.risks, data.incident_number, 'showRisks', 'RiskAssesment', 'Attached Risk Assessments Files');
    populateFiles(data.witness, data.incident_number, 'showWitnessStmts', 'WitnesStatement', 'Attached Witness Files');
    populateFiles(data.supportFiles, data.incident_number, 'showSupportDocuments', 'supportFiles', 'Attached supporting documents');

    populateFullInvestigation(data);
    populateHSData(data);

}
function populateFullInvestigation(data) {
    var fi = data.fullInvestigation;
    if (fi.id !== null && fi.id !== undefined) {
        $('#fiId').val(fi.id);
    }
    $('#eventSummary').val(fi.event_summary);
    $('#commentsSSOW').val(fi.ssow_comments);
    $('#commentsRA').val(fi.ra_comments);
    fi.is_ra_suitable === '1' ? $('#raSuitable').val('Yes') : (fi.is_ra_suitable === '0' ? $('#raSuitable').val('No') : $('#raSuitable').val('-1'));

    fi.is_ssow_suitable === '1' ? $('#ssowSuitable').val('Yes') : (fi.is_ssow_suitable === '0' ? $('#ssowSuitable').val('No') : $('#ssowSuitable').val('-1'));

    fi.is_mhe_training_required === '1' ? $('#mheTrainingReq').val('Yes') : (fi.is_mhe_training_required === '0' ? $('#mheTrainingReq').val('No') : $('#mheTrainingReq').val('-1'));
    $("input[name=environmentCondition][value='" + fi.environment_condition + "']").prop("checked", true);
    $("input[name=environmentFloor][value='" + fi.environment_floor + "']").prop("checked", true);
    $("input[name=environmentLighting][value='" + fi.environment_lighting + "']").prop("checked", true);
    $("input[name=machineCondition][value='" + fi.machine_condition + "']").prop("checked", true);
    $("input[name=machineFacility][value='" + fi.machine_facility + "']").prop("checked", true);
    $("input[name=machineMaintenance][value='" + fi.machine_maintenance + "']").prop("checked", true);
    $("input[name=methodRiskAssessment][value='" + fi.method_risk_assessment + "']").prop("checked", true);
    $("input[name=methodRuling][value='" + fi.method_ruling + "']").prop("checked", true);
    $("input[name=methodSSOW][value='" + fi.method_ssow + "']").prop("checked", true);
    $("input[name=personConcentration][value='" + fi.person_concentraion + "']").prop("checked", true);
    $("input[name=personExperience][value='" + fi.person_experience + "']").prop("checked", true);
    $("input[name=personTraining][value='" + fi.person_training + "']").prop("checked", true);

    var whyBlocks = fi.whyblocks;
    for (var i = 0; i < whyBlocks.length; i++) {
        var cnt = i + 1;
        if (i > 0) {
            addWhy();
        }
        /*populate why blocks*/
        $('#whyid_' + cnt).val(whyBlocks[i].id);
        $('#why1_' + cnt).val(whyBlocks[i].why_1);
        $('#why2_' + cnt).val(whyBlocks[i].why_2);
        $('#why3_' + cnt).val(whyBlocks[i].why_3);
        $('#why4_' + cnt).val(whyBlocks[i].why_4);
        $('#why5_' + cnt).val(whyBlocks[i].why_5);
        $('#rootCauseA_' + cnt).val(whyBlocks[i].root_cause_a);
        $('#rootCauseB_' + cnt).val(whyBlocks[i].root_cause_b);
        $('#rootCauseC_' + cnt).val(whyBlocks[i].root_cause_c);
        var cm = whyBlocks[i].counterMeasures;
        for (var z = 0; z < 3; z++) {
            var tag = cm[z].counter_measure_number;
            $('#counterMeasure' + tag + 'Id_' + cnt).val(cm[z].id);
            $('#counterMeasure' + tag + '_' + cnt).val(cm[z].action);
            $('#counterMeasureType' + tag + '_' + cnt).val(cm[z].action_sub_type);
            if (cm[z].deadline !== null && cm[z].deadline !== '') {
                $('#counterMeasureDeadline' + tag + '_' + cnt).val(new Date(cm[z].deadline).toISOString().split('T')[0]);
            }
            $('#counterMeasureSite' + tag + '_' + cnt).val(cm[z].action_site);
            populateOwnersForCounterMeasure($('#counterMeasureSite' + tag + '_' + cnt));
            $('#counterMeasureOwner' + tag + '_' + cnt).val(cm[z].action_owner);
            populateCounterMeasureManager($('#counterMeasureOwner' + tag + '_' + cnt));
            if (data.status === 'L3_SUBMITTED' || data.status === 'L3_EDITED' || data.status === 'L3_APPROVED' || data.status === 'L4_SAVED' || data.status === 'L4_APPROVED' || data.status === 'L3_REJECTED' || data.status === 'L3_SAVED') {
                //Update status and action if
                if (cm[z].action !== null && cm[z].action !== '') {
                    //disable all this shit
                    if (cm[z].status !== 'CREATED') {
                        var lower = cm[z].status.toLowerCase();
                        $('#counterMeasure' + tag + '_' + cnt).prop('disabled', true);
                        $('#counterMeasureType' + tag + '_' + cnt).prop('disabled', true);
                        $('#counterMeasureDeadline' + tag + '_' + cnt).prop('disabled', true);
                        $('#counterMeasureSite' + tag + '_' + cnt).prop('disabled', true);
                        $('#counterMeasureOwner' + tag + '_' + cnt).prop('disabled', true);
                        $('#counterMeasure' + tag + 'IdLabel_' + cnt).html("Action ID: AC" + cm[z].id);
                        //$('#counterMeasureStatus' + tag + '_' + cnt).val(cm[z].status);
                        $('#counterMeasureStatus' + tag + '_' + cnt).val(lower.charAt(0).toUpperCase() + lower.slice(1));

                    }
                }

            }
        }

    }

    var otherMeasures = fi.otherCounterMeasures;
    for (var y = 0; y < otherMeasures.length; y++) {
        var cntr = y + 1;
        if (cntr > 1) {
            addCounterMeasure();
        }
        $('#otherCounterMeasureId' + cntr).val(otherMeasures[y].id);
        $('#otherCounterMeasure' + cntr).val(otherMeasures[y].action);
        $('#otherCounterMeasureType' + cntr).val(otherMeasures[y].action_sub_type);
        if (otherMeasures[y].deadline !== null && otherMeasures[y].deadline !== '') {
            $('#otherCounterMeasureDeadline' + cntr).val(new Date(otherMeasures[y].deadline).toISOString().split('T')[0]);
        }
        $('#otherCounterMeasureSite' + cntr).val(otherMeasures[y].action_site);
        populateOwnersForCounterMeasure($('#otherCounterMeasureSite' + cntr));
        $('#otherCounterMeasureOwner' + cntr).val(otherMeasures[y].action_owner);
        populateCounterMeasureManager($('#otherCounterMeasureOwner' + cntr));
        if (data.status === 'L3_SUBMITTED' || data.status === 'L3_EDITED' || data.status === 'L3_APPROVED' || data.status === 'L4_SAVED' || data.status === 'L4_APPROVED' || data.status === 'L3_REJECTED' || data.status === 'L3_SAVED') {
            //Update status and action if
            if (otherMeasures[y].action !== null && otherMeasures[y].action !== '') {

                if (otherMeasures[y].status !== 'CREATED') {
                    var lower = otherMeasures[y].status.toLowerCase();
                    $('#otherCounterMeasure' + cntr).prop("disabled", true);
                    $('#otherCounterMeasureType' + cntr).prop("disabled", true);
                    $('#otherCounterMeasureDeadline' + cntr).prop("disabled", true);
                    $('#otherCounterMeasureSite' + cntr).prop("disabled", true);
                    $('#otherCounterMeasureOwner' + cntr).prop("disabled", true);
                    $('#otherCounterMeasureIdLabel' + cntr).html("Action ID: AC" + otherMeasures[y].id);
                    $('#otherCounterMeasureStatus' + cntr).val(lower.charAt(0).toUpperCase() + lower.slice(1));
                }
            }

        }
    }
    populateToggle('isIllHealth', fi.vantec_way_decision=== 'ILL HEALTH'? '1': '0');
    enableVantecWay($('#isIllHealth'));
        $('#vantecWayDecision').val(fi.vantec_way_decision);
    $('#investigationTeamNames').val(fi.investigation_team_names);
    
    $('#vantecWayComments').val(fi.vantec_way_comments);
    $('#decisionTree').html(fi.vantec_way_decision_tree);

    

}
function populateFiles(files, iid, divId, type, title) {
    $('#' + divId).empty();
    $('#' + divId).append('<div class="col-md-2"><label class="control-label">' + title + '</label></div>')
    for (var i = 0; i < files.length; i++) {
        var link = "../action/downloadFile.php?iid=" + iid + "&fileName=" + encodeURIComponent(files[i].file_name) + "&type=" + type;
        var newElement = '<div class="alert alert-gray alert-dismissible file" >' +
                '<button type="button" class="close" data-dismiss="alert">' + '&times;' + '</button>' +
                '<a href="' + link + '" class="alert-link">' + files[i].file_name + '</a>' +
                '</div>';
        $('#' + divId).append(newElement);
    }

}
function populateToggle(id, flag) {
    var elem = $("#" + id);
    if(document.getElementById(id).classList.contains('badgebox')){
        if (flag === '1') {
            elem.removeAttr('disabled');
            elem.prop('checked', true);
            elem.next().html("&check;");
            elem.parent().removeClass('btn-danger');
            elem.parent().addClass('btn-primary');
        } else {
            elem.prop('checked', false);
            elem.next().html("&cross;");
            elem.parent().removeClass('.btn-primary');
            elem.parent().addClass('btn-danger');
        }
    }else if(document.getElementById(id).classList.contains('badgeboxReverse')){
          if (flag === '1') {
            elem.removeAttr('disabled');
            elem.prop('checked', true);
             elem.next().html("&check;");
            elem.parent().removeClass('.btn-primary');
            elem.parent().addClass('btn-danger');
           
        } else {
            elem.prop('checked', false);
            elem.next().html("&cross;");
            elem.parent().removeClass('btn-danger');
            elem.parent().addClass('btn-primary');
        }
    }else if(document.getElementById(id).classList.contains('badgeboxneutral')){
          if (flag === '1') {
            elem.removeAttr('disabled');
            elem.prop('checked', true);
             elem.next().html("&check;");
             elem.parent().removeClass('btn-secondary');
            elem.parent().addClass('btn-dark');
           
        } else {
            elem.prop('checked', false);
            elem.next().html("&cross;");
             elem.parent().removeClass('btn-dark');
            elem.parent().addClass('btn-secondary');
        }
    }
}
function changeIncident(elem) {
    return false;

}
function disableBodyParts(elem) {
    var id = $(elem).attr('id');
    var num = id.split('_')[1];

    if ($('#injuryYes_' + num).is(':checked')) {
        $('input[name=personLocationInjury_' + num + ']').prop('disabled', false);


    } else {
        $('input[name=personLocationInjury_' + num + ']').prop('checked', false);
        $('input[name=personLocationInjury_' + num + ']').prop('disabled', true);

    }

}
function showFirstPerson() {
    if (document.getElementById('personYes').checked) {
        document.getElementById('personDetails_1').style.display = 'block';
        document.getElementById('personButtons').style.display = 'block';
        document.getElementById('licenseTitle').style.display = 'block';
        document.getElementById('licenseDetailsDiv').style.display = 'block';

    } else {
        document.getElementById('personDetails_1').style.display = 'none';
        document.getElementById('personButtons').style.display = 'none';
        document.getElementById('licenseTitle').style.display = 'none';
        document.getElementById('licenseDetailsDiv').style.display = 'none';
        //Remove added persons
        var num = $('.clonedInput').length;
        for (var i = 2; i <= num; i++) {
            $('#personDetails_' + num).slideUp('slow', function () {
                $(this).remove();
            });
        }
        //Remove added license
        $('#tab_license tbody tr').each(function (i, tr) {
            if (i > 1) {
                $("#delete_license").click();
            }
        });
        $('.licensePersonName').each(function () {
            $(this).find("option").remove().end().append('<option value="-1"></option>');
        });
        $('#personDetails_1').find('input[type=text]').val('');
        $("#personDetails_1").find('input[type=checkbox]:checked').click();
        $('#showDAFiles_1').empty();
    }
}
function populateIncident() {

    document.getElementById('incidentTypeDesc').style.display = 'none';
    $('input:radio[name=answer3]').each(function () {
        $(this).prop('disabled', false);
    });
    $('input:radio[name=answer4]').each(function () {
        $(this).prop('disabled', false);
    });
    if (document.getElementById('answer1Yes').checked) { //Q1 - YES
        document.getElementById('question2Div').style.display = "block";
        document.getElementById('question2Label').innerText = "Did the event result in serious injury or a fatality?";
        if (document.getElementById('answer2Yes').checked) { //Q2 - YES
            document.getElementById('seriousAccident').checked = true;
            document.getElementById('question3Div').style.display = "none";
            document.getElementById('question4Div').style.display = "none";
        } else if (document.getElementById('answer2No').checked) { //Q2 NO
            $('input:radio[name=incidentType]').each(function () {
                $(this).prop('checked', false);
            });
            document.getElementById('question3Div').style.display = "block";
            document.getElementById('question3Label').innerText = "Could the event have resulted in a serious injury or a fatality?";
            if (document.getElementById('answer3Yes').checked) { //Q3 YES
                document.getElementById('accidentStar').checked = true;
                document.getElementById('question4Div').style.display = "none";
            } else if (document.getElementById('answer3No').checked) { //Q3 NO
                document.getElementById('accident').checked = true;
                document.getElementById('question4Div').style.display = "none";
            }
        }
    } else if (document.getElementById('answer1No').checked) { //Q1 - NO
        document.getElementById('question2Div').style.display = "block";
        document.getElementById('question2Label').innerText = "Was someone taken ill?";
        if (document.getElementById('answer2Yes').checked) { //Q2 - YES
            document.getElementById('question3Div').style.display = "block";
            document.getElementById('question3Label').innerText = "Have you been notified of this occupational illness by a medical professional?"
            if (document.getElementById('answer3Yes').checked) { //Q3 - YES
                document.getElementById('occupationalDiesease').checked = true;
                document.getElementById('question4Div').style.display = "none";
            } else if (document.getElementById('answer3No').checked) { //Q3 - NO
                document.getElementById('question4Div').style.display = "block";
                document.getElementById('question4Label').innerText = "Did the illness suddenly begin at work?"
                if (document.getElementById('answer4Yes').checked) { //Q4 -YES
                    document.getElementById('illHealth').checked = true;
                } else if (document.getElementById('answer4No').checked) { //Q4 - NO
                    document.getElementById('illHealthStar').checked = true;
                }
            }
        } else if (document.getElementById('answer2No').checked) {//Q2 NO
            document.getElementById('question3Div').style.display = "block";
            document.getElementById('question3Label').innerText = "Did damage occur as a result?"
            if (document.getElementById('answer3Yes').checked) { //Q3 - YES
                document.getElementById('question4Div').style.display = "block";
                document.getElementById('question4Label').innerText = "Could the event have resulted in a serious injury or a fatality?"
                if (document.getElementById('answer4Yes').checked) { //Q4 -YES
                    document.getElementById('incidentStar').checked = true;
                } else if (document.getElementById('answer4No').checked) { //Q4 - NO
                    document.getElementById('incident').checked = true;
                }
            } else if (document.getElementById('answer3No').checked) { //Q3 - NO
                document.getElementById('question4Div').style.display = "block";
                document.getElementById('question4Label').innerText = "Could the event have resulted in a serious injury or a fatality?"
                if (document.getElementById('answer4Yes').checked) { //Q4 -YES
                    document.getElementById('nearMissStar').checked = true;
                } else if (document.getElementById('answer4No').checked) { //Q4 - NO
                    document.getElementById('nearMiss').checked = true;
                }
            }
        }
    }
    var incidentType = $("input:radio[name=incidentType]:checked").val();
    showIncidentDesc(incidentType);

    if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident') {
        
        $(".injuryYes").each(function () {
            $(this).prop('disabled', false);
            
        });
         $(".injuryNo").each(function () {
            $(this).prop('disabled', false);
            
        });
        $(".person-location").each(function () {
            $(this).prop('disabled', false);
            
        });
    } else {
        $(".injuryYes").each(function () {
            $(this).prop('checked', false);
            $(this).prop('disabled', true);
            
        });
         $(".injuryNo").each(function () {
             $(this).prop('checked', false);
            $(this).prop('disabled', true);
            
        });

        $(".person-location").each(function () {
            $(this).prop('disabled', true);
             $(this).prop('checked', false);
           
           
        });

    }
    if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident' || incidentType === 'Occupational Disease' || incidentType === 'Ill Health*' || incidentType === 'Ill Health') {
        $(".person-treatment-details").each(function () {
            $(this).val('');
            $(this).prop('disabled', false);
        });
        $(".person-treatment").each(function () {
            $(this).prop('disabled', false);
            $(this).selectpicker('refresh');
        });
    } else {
        $(".person-treatment-details").each(function () {
            $(this).prop('disabled', true);
        });
        $(".person-treatment").each(function () {
            $(this).prop('disabled', true);
            $(this).val('');
            if ($(this).find('.filter-option-inner-inner').length > 0) {
                $(this).find('.filter-option-inner-inner')[0].innerText = 'Nothing selected';
            }
            $(this).selectpicker('refresh');
        });
    }
    if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Incident*' || incidentType === 'Near Miss*') {
        $("#keyPoints").prop('disabled', false);
        $("#mainMessage").prop('disabled', false)
    } else {
        $("#keyPoints").val('');
        $("#mainMessage").val('');
        $("#keyPoints").prop('disabled', true);
        $("#mainMessage").prop('disabled', true)
        
    }
}
function showIncidentDesc(incidentType) {
    if (incidentType === 'Serious Accident') {
        document.getElementById('incidentTypeDesc').innerText = 'An event which resulted in a serious or fatal injury. Fast Response may be required.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Accident') {
        document.getElementById('incidentTypeDesc').innerText = 'An event which resulted in a minor (non serious or fatal) injury and could not have resulted in a serious injury or a fatality.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Accident*') {
        document.getElementById('incidentTypeDesc').innerText = 'An event which resulted in a minor (non serious or fatal) injury but could have resulted in a serious injury or a fatality*. Fast Response may be required.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Occupational Disease') {
        document.getElementById('incidentTypeDesc').innerText = 'Disease or disorder that is caused by the work or working conditions and has been diagnosed by a medical professional.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Ill Health') {
        document.getElementById('incidentTypeDesc').innerText = 'Ill health condition that began in the workplace.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Ill Health*') {
        document.getElementById('incidentTypeDesc').innerText = 'Ill health condition that began outside of work. Ensure the person affected has stated (written statement) that the ill health did not begin whilst at work.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Incident') {
        document.getElementById('incidentTypeDesc').innerText = 'An event which caused damage and could not have resulted in a serious injury or a fatality.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Incident*') {
        document.getElementById('incidentTypeDesc').innerText = 'An event which caused damage and could have resulted in a serious injury or a fatality*. Fast Response may be required.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Near Miss') {
        document.getElementById('incidentTypeDesc').innerText = 'An event where injury or damage to plant, facility etc was narrowly avoided and which could not have resulted in a serious injury or a fatality.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    } else if (incidentType === 'Near Miss*') {
        document.getElementById('incidentTypeDesc').innerText = 'An event where injury or damage to plant, facility etc was narrowly avoided and which could have resulted in a serious injury or a fatality*. Fast Response may be required.';
        document.getElementById('incidentTypeDesc').style.display = 'block';

    }
}

function loadActionOwnersData() {

    //Get action owners data from all sites
    $.ajax({
        url: "../masterData/usersData.php?data=actionowners",
        type: 'GET',
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        success: function (response, textstatus) {
            console.log(response);
            return response;
        }
    });
}
function addCounterMeasure() {
    var num = $('.clonedCounterMeasure').length, // how many "duplicatable" input fields we currently have
            newNum = num + 1, // the numeric ID of the new input field being added
            newElem = $('#otherCounterMeasureDiv' + num).clone();

    newElem = newElem.attr('id', 'otherCounterMeasureDiv' + newNum).fadeIn('slow');
    newElem.find('.otherCounterMeasureValidation').attr('id', 'otherCounterMeasureValidation' + newNum).attr('name', 'otherCounterMeasureValidation' + newNum).css('display', 'none');
    newElem.find('.otherCounterMeasureIdLabel').attr('id', 'otherCounterMeasureIdLabel' + newNum).attr('name', 'otherCounterMeasureIdLabel' + newNum).html("Action ID:");
    newElem.find('.otherCounterMeasureId').val('0').attr('id', 'otherCounterMeasureId' + newNum).attr('name', 'otherCounterMeasureId' + newNum);
    newElem.find('.otherCounterMeasure').val('').attr('id', 'otherCounterMeasure' + newNum).attr('name', 'otherCounterMeasure' + newNum).prop("disabled", false);
    newElem.find('.otherCounterMeasureType').val('-1').attr('id', 'otherCounterMeasureType' + newNum).attr('name', 'otherCounterMeasureType' + newNum).prop("disabled", false);
    newElem.find('.otherCounterMeasureStatus').val('').attr('id', 'otherCounterMeasureStatus' + newNum).attr('name', 'otherCounterMeasureStatus' + newNum);
    newElem.find('.otherCounterMeasureDeadline').val('').attr('id', 'otherCounterMeasureDeadline' + newNum).attr('name', 'otherCounterMeasureDeadline' + newNum).prop("disabled", false);
    newElem.find('.otherCounterMeasureSite').val('-1').attr('id', 'otherCounterMeasureSite' + newNum).attr('name', 'otherCounterMeasureSite' + newNum).prop("disabled", false);
    newElem.find('.otherCounterMeasureOwner').val('-1').attr('id', 'otherCounterMeasureOwner' + newNum).attr('name', 'otherCounterMeasureOwner' + newNum).prop("disabled", false);
    newElem.find('.otherCounterMeasureManager').val('').attr('id', 'otherCounterMeasureManager' + newNum).attr('name', 'otherCounterMeasureManager' + newNum);
    $('#otherCounterMeasureDiv' + num).after(newElem);
    $('#btnDelCounter').attr('disabled', false);
}

function addInjuredPerson() {
    var num = $('.clonedInjuredDetails').length, // how many "duplicatable" input fields we currently have
            newNum = num + 1, // the numeric ID of the new input field being added
            newElem = $('#injuredPerson_' + num).clone();

    newElem = newElem.attr('id', 'injuredPerson_' + newNum).fadeIn('slow');
    newElem.find('.injured-person-id').attr('id', 'injuredPersonId_' + newNum).attr('name', 'injuredPersonId_' + newNum).val('0');
    newElem.find('.injured-person-name').attr('id', 'injuredPersonName_' + newNum).attr('name', 'injuredPersonName_' + newNum);
    newElem.find('.injured-person-riddor-toggle').attr('id', 'isRIDDORToggle_' + newNum).attr('name', 'isRIDDORToggle_' + newNum);
    newElem.find('.injured-person-treatment-details').val('').attr('id', 'injuredPersonTreatmentDetails_' + newNum).attr('name', 'injuredPersonTreatmentDetails_' + newNum);
    newElem.find('.injured-person-days-lost').val('').attr('id', 'dayLost_' + newNum).attr('name', 'dayLost_' + newNum);
    newElem.find('.injured-person-riddor-ref').attr('id', 'injuredPersonRiddorRef_' + newNum).attr('name', 'injuredPersonRiddorRef_ena' + newNum).val('');
    newElem.find('.injured-person-riddor-form').attr('id', 'injuredPersonRIDDORForm_' + newNum).attr('name', 'injuredPersonRIDDORForm_' + newNum).val('');
    newElem.find('.injuredPersonValidationError').attr('id', 'injuredPersonValidationError_' + newNum).attr('name', 'injuredPersonValidationError_' + newNum).css('display', 'none');
    newElem.find('.injuredPersonValidationRiddorError').attr('id', 'injuredPersonValidationRiddorError_' + newNum).attr('name', 'injuredPersonValidationRiddorError_' + newNum).css('display', 'none');
    newElem.find('.injuredPersonDaysLostError').attr('id', 'injuredPersonDaysLostError_' + newNum).attr('name', 'injuredPersonDaysLostError_' + newNum).css('display', 'none');
    newElem.find('.injuredPersonDaysLostLTIError').attr('id', 'injuredPersonDaysLostLTIError_' + newNum).attr('name', 'injuredPersonDaysLostLTIError_' + newNum).css('display', 'none');
    newElem.find('.injuredPersonDaysLostMoreThan7Error').attr('id', 'injuredPersonDaysLostMoreThan7Error_' + newNum).attr('name', 'injuredPersonDaysLostMoreThan7Error_' + newNum).css('display', 'none');
    newElem.find('.injured-person-riddor-files').attr('id', 'showRIDDORFiles_' + newNum).attr('name', 'showRIDDORFiles_' + newNum);
    newElem.find('.person-hse-date-1').attr('id', 'injuredPersonHSEReportedDate_' + newNum).attr('name', 'injuredPersonHSEReportedDate_' + newNum);
    newElem.find('.person-hse-date-2').attr('id', 'injuredPersonHSEReportedDate_' + newNum).attr('name', 'injuredPersonHSEReportedDate_' + newNum).attr('data-target', '#injuredPersonHSEReportedDate_' + newNum).val('');
    newElem.find('.person-hse-date-3').attr('data-target', '#injuredPersonHSEReportedDate_' + newNum);
    newElem.find('.bootstrap-select').replaceWith(function () {
        return $('select', this);
    });
    $('#injuredPerson_' + num).after(newElem);
    $('#showRIDDORFiles_' + newNum).empty();
    //Set all Datepickers
    $("#injuredPersonHSEReportedDate_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: false,
        // maxDate: moment(),
        userCurrent: false,
        locale: moment.locale('en', {week: {dow: 1}})
    });
    $('#injuredPersonHSEReportedDate_' + newNum).datetimepicker("disable");
    $("#injuredPerson_" + newNum).find('.badgeBox').each(function () {
        $(this).prop('checked', false);
        $(this).next().html("&cross;");
        $(this).parent().removeClass('.btn-primary');
        $(this).parent().addClass('btn-danger');
    });
    $('#injuredPersonRiddorRef_' + newNum).prop('disabled', true);
    $('#injuredPersonRIDDORForm_' + newNum).prop('disabled', true);
    $('#btnDelInjuredPerson').attr('disabled', false);

    $('.badgebox').click(function () {
        if ($(this).is(':checked')) {
            $(this).next().html("&check;");
            $(this).parent().removeClass('btn-danger');
            $(this).parent().addClass('btn-primary');
        } else {
            $(this).next().html("&cross;");
            $(this).parent().removeClass('btn-primary');
            $(this).parent().addClass('btn-danger');
        }
    });
}

function addWhy() {
    var num = $('.clonedWhy').length, // how many "duplicatable" input fields we currently have
            newNum = num + 1, // the numeric ID of the new input field being added
            newElem = $('#block5Why' + num).clone();
    newElem = newElem.attr('id', 'block5Why' + newNum).fadeIn('slow');
    // manipulate the name/id values of the input inside the new element
    newElem.find('.whyTitle').attr('id', 'whyTitle' + newNum).attr('name', 'whyTitle' + newNum).html('5 Whys Block ' + newNum + ' (Why did this happen?)');
    newElem.find('.whyid').val('0').attr('id', 'whyid_' + newNum).attr('name', 'whyid_' + newNum);
    newElem.find('.why1').val('').attr('id', 'why1_' + newNum).attr('name', 'why1_' + newNum);
    newElem.find('.why2').val('').attr('id', 'why2_' + newNum).attr('name', 'why2_' + newNum);
    newElem.find('.why3').val('').attr('id', 'why3_' + newNum).attr('name', 'why3_' + newNum);
    newElem.find('.why4').val('').attr('id', 'why4_' + newNum).attr('name', 'why4_' + newNum);
    newElem.find('.why5').val('').attr('id', 'why5_' + newNum).attr('name', 'why5_' + newNum);
    newElem.find('.rootCauseA').val('').attr('id', 'rootCauseA_' + newNum).attr('name', 'rootCauseA_' + newNum);
    newElem.find('.rootCauseB').val('').attr('id', 'rootCauseB_' + newNum).attr('name', 'rootCauseB_' + newNum);
    newElem.find('.rootCauseC').val('').attr('id', 'rootCauseC_' + newNum).attr('name', 'rootCauseC_' + newNum);
    newElem.find('.whyBlockValidation').val('').attr('id', 'whyBlockValidation_' + newNum).attr('name', 'whyBlockValidation_' + newNum);
    newElem.find('.counterMeasureDateValidation').val('').attr('id', 'counterMeasureDateValidation_' + newNum).attr('name', 'counterMeasureDateValidation_' + newNum);

    newElem.find('.counterMeasureValidationA').attr('id', 'counterMeasureValidationA_' + newNum).attr('name', 'counterMeasureValidationA_' + newNum);
    newElem.find('.counterMeasureValidationB').attr('id', 'counterMeasureValidationB_' + newNum).attr('name', 'counterMeasureValidationB_' + newNum);
    newElem.find('.counterMeasureValidationC').attr('id', 'counterMeasureValidationC_' + newNum).attr('name', 'counterMeasureValidationC_' + newNum);


    newElem.find('.counterMeasureAId').val('0').attr('id', 'counterMeasureAId_' + newNum).attr('name', 'counterMeasureAId_' + newNum);
    newElem.find('.counterMeasureBId').val('0').attr('id', 'counterMeasureBId_' + newNum).attr('name', 'counterMeasureBId_' + newNum);
    newElem.find('.counterMeasureCId').val('0').attr('id', 'counterMeasureCId_' + newNum).attr('name', 'counterMeasureCId_' + newNum);

    newElem.find('.counterMeasureAIdLabel').attr('id', 'counterMeasureAIdLabel_' + newNum).attr('name', 'counterMeasureAIdLabel_' + newNum).html('Action ID:');
    newElem.find('.counterMeasureBIdLabel').attr('id', 'counterMeasureBIdLabel_' + newNum).attr('name', 'counterMeasureBIdLabel_' + newNum).html('Action ID:');
    newElem.find('.counterMeasureCIdLabel').attr('id', 'counterMeasureCIdLabel_' + newNum).attr('name', 'counterMeasureCIdLabel_' + newNum).html('Action ID:');


    newElem.find('.counterMeasureA').val('').attr('id', 'counterMeasureA_' + newNum).attr('name', 'counterMeasureA_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureB').val('').attr('id', 'counterMeasureB_' + newNum).attr('name', 'counterMeasureB_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureC').val('').attr('id', 'counterMeasureC_' + newNum).attr('name', 'counterMeasureC_' + newNum).prop("disabled", false);

    newElem.find('.counterMeasureStatusA').val('').attr('id', 'counterMeasureStatusA_' + newNum).attr('name', 'counterMeasureStatusA_' + newNum);
    newElem.find('.counterMeasureStatusB').val('').attr('id', 'counterMeasureStatusB_' + newNum).attr('name', 'counterMeasureStatusB_' + newNum);
    newElem.find('.counterMeasureStatusC').val('').attr('id', 'counterMeasureStatusC_' + newNum).attr('name', 'counterMeasureStatusC_' + newNum);

    newElem.find('.counterMeasureTypeA').val('-1').attr('id', 'counterMeasureTypeA_' + newNum).attr('name', 'counterMeasureTypeA_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureTypeB').val('-1').attr('id', 'counterMeasureTypeB_' + newNum).attr('name', 'counterMeasureTypeB_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureTypeC').val('-1').attr('id', 'counterMeasureTypeC_' + newNum).attr('name', 'counterMeasureTypeC_' + newNum).prop("disabled", false);

    newElem.find('.counterMeasureDeadlineA').val('').attr('id', 'counterMeasureDeadlineA_' + newNum).attr('name', 'counterMeasureDeadlineA_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureDeadlineB').val('').attr('id', 'counterMeasureDeadlineB_' + newNum).attr('name', 'counterMeasureDeadlineB_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureDeadlineC').val('').attr('id', 'counterMeasureDeadlineC_' + newNum).attr('name', 'counterMeasureDeadlineC_' + newNum).prop("disabled", false);

    newElem.find('.counterMeasureSiteA').val('-1').attr('id', 'counterMeasureSiteA_' + newNum).attr('name', 'counterMeasureSiteA_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureSiteB').val('-1').attr('id', 'counterMeasureSiteB_' + newNum).attr('name', 'counterMeasureSiteB_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureSiteC').val('-1').attr('id', 'counterMeasureSiteC_' + newNum).attr('name', 'counterMeasureSiteC_' + newNum).prop("disabled", false);

    newElem.find('.counterMeasureOwnerA').val('-1').attr('id', 'counterMeasureOwnerA_' + newNum).attr('name', 'counterMeasureOwnerA_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureOwnerB').val('-1').attr('id', 'counterMeasureOwnerB_' + newNum).attr('name', 'counterMeasureOwnerB_' + newNum).prop("disabled", false);
    newElem.find('.counterMeasureOwnerC').val('-1').attr('id', 'counterMeasureOwnerC_' + newNum).attr('name', 'counterMeasureOwnerC_' + newNum).prop("disabled", false);

    newElem.find('.counterMeasureManagerA').val('').attr('id', 'counterMeasureManagerA_' + newNum).attr('name', 'counterMeasureManagerA_' + newNum);
    newElem.find('.counterMeasureManagerB').val('').attr('id', 'counterMeasureManagerB_' + newNum).attr('name', 'counterMeasureManagerB_' + newNum);
    newElem.find('.counterMeasureManagerC').val('').attr('id', 'counterMeasureManagerC_' + newNum).attr('name', 'counterMeasureManagerC_' + newNum);
    $('#block5Why' + num).after(newElem);
    $('#btnDelWhy').attr('disabled', false);
}
function addPerson(flag) {
    // create the new element via clone(), and manipulate it's ID using newNum value
    var num = $('.clonedInput').length, // how many "duplicatable" input fields we currently have
            newNum = num + 1, // the numeric ID of the new input field being added
            newElem = $('#personDetails_' + num).clone();
    newElem = newElem.attr('id', 'personDetails_' + newNum).fadeIn('slow');
    // manipulate the name/id values of the input inside the new element
    newElem.find('.heading-reference').attr('id', 'reference_' + newNum).attr('name', 'reference_' + newNum).html('Person ' + newNum);
    newElem.find('.person-name').val('').attr('id', 'personName_' + newNum).attr('name', 'personName_' + newNum);
    newElem.find('.person-dob-1').attr('id', 'personDOB_' + newNum).attr('name', 'personDOB_' + newNum);
    newElem.find('.person-dob-2').attr('id', 'personDOB_' + newNum).attr('name', 'personDOB_' + newNum).attr('data-target', '#personDOB_' + newNum).val('');
    newElem.find('.person-dob-3').attr('data-target', '#personDOB_' + newNum);
    newElem.find('.person-age').val('').attr('id', 'personAge_' + newNum).attr('name', 'personAge_' + newNum);
    newElem.find('.person-emp-status').attr('id', 'personEmpStatus_' + newNum).attr('name', 'personEmpStatus_' + newNum).val('');
    newElem.find('.person-esd-1').attr('id', 'personESD_' + newNum).attr('name', 'personESD_' + newNum);
    newElem.find('.person-esd-2').attr('id', 'personESD_' + newNum).attr('name', 'personESD_' + newNum).attr('data-target', '#personESD_' + newNum).val('');
    newElem.find('.person-esd-3').attr('data-target', '#personESD_' + newNum);
    newElem.find('.person-los').attr('id', 'personLoS_' + newNum).attr('name', 'personLoS_' + newNum).val('');
    newElem.find('.person-jsd-1').attr('id', 'personTOJSD_' + newNum).attr('name', 'personTOJSD_' + newNum);
    newElem.find('.person-jsd-2').attr('id', 'personTOJSD_' + newNum).attr('name', 'personTOJSD_' + newNum).attr('data-target', '#personTOJSD_' + newNum).val('');
    newElem.find('.person-jsd-3').attr('data-target', '#personTOJSD_' + newNum);
    newElem.find('.person-toj').attr('id', 'personTOJ_' + newNum).attr('name', 'personTOJ_' + newNum).val('');
    newElem.find('.person-prev-inc-toggle').attr('id', 'personPrevIncidentsToggle_' + newNum).attr('name', 'personPrevIncidentsToggle_' + newNum);
    newElem.find('.person-date-ref').attr('id', 'personDateAndRef_' + newNum).attr('name', 'personDateAndRef_' + newNum).val('').attr('disabled', 'disabled');;
    newElem.find('.person-veh-inv-toggle').attr('id', 'personVehicleInvolvedToggle_' + newNum).attr('name', 'personVehicleInvolvedToggle_' + newNum);
    newElem.find('.person-vehicle-type').attr('id', 'personVehicleType_' + newNum).attr('name', 'personVehicleType_' + newNum).val('').attr('disabled', 'disabled');
    newElem.find('.person-vehicle-type-comments').attr('id', 'personVehicleTypeComments_' + newNum).attr('name', 'personVehicleTypeComments_' + newNum).val('').attr('disabled', 'disabled');
    newElem.find('.person-vehicle-reg').attr('id', 'personVehicleReg_' + newNum).attr('name', 'personVehicleReg_' + newNum).val('').attr('disabled', 'disabled');
    newElem.find('.person-removed-toggle').attr('id', 'personRemovedToggle_' + newNum).attr('name', 'personRemovedToggle_' + newNum);
    newElem.find('.person-license').attr('id', 'personLicense_' + newNum).attr('name', 'personLicense_' + newNum).val('');
    newElem.find('.person-lic-exp-1').attr('id', 'personLicenseExpiry_' + newNum).attr('name', 'personLicenseExpiry_' + newNum);
    newElem.find('.person-lic-exp-2').attr('id', 'personLicenseExpiry_' + newNum).attr('name', 'personLicenseExpiry_' + newNum).attr('data-target', '#personLicenseExpiry_' + newNum);
    newElem.find('.person-lic-exp-3').attr('data-target', '#personLicenseExpiry_' + newNum);
    newElem.find('.person-lic-toggle').attr('id', 'personLicenseInDateToggle_' + newNum).attr('name', 'personLicenseInDateToggle_' + newNum);

    newElem.find('.person-location').attr('id', 'personLocationInjury_' + newNum).attr('name', 'personLocationInjury_' + newNum);
    newElem.find('.injuryYes').attr('id', 'injuryYes_' + newNum).attr('name', 'isLocationOfInjury_' + newNum);
    newElem.find('.injuryYes').next().attr('for', 'injuryYes_' + newNum);
    newElem.find('.injuryNo').attr('id', 'injuryNo_' + newNum).attr('name', 'isLocationOfInjury_' + newNum);
    newElem.find('.injuryNo').next().attr('for', 'injuryNo_' + newNum);

    newElem.find('.person-treatment').attr('id', 'personTreatment_' + newNum).attr('name', 'personTreatment_' + newNum);
    newElem.find('.person-treatment').next().attr('data-id', 'personTreatment_' + newNum);
    newElem.find('.person-ppe-worn').attr('id', 'personPPEWorn_' + newNum).attr('name', 'personPPEWorn_' + newNum);
    newElem.find('.person-ppe-worn').next().attr('data-id', 'personPPEWorn_' + newNum);
    newElem.find('.person-ppe-req-level-toggle').attr('id', 'personPPEOfReqLevelToggle_' + newNum).attr('name', 'personPPEOfReqLevelToggle_' + newNum);
    newElem.find('.person-treatment-details').attr('id', 'personTreatmentDetails_' + newNum).attr('name', 'personTreatmentDetails_' + newNum).val('');
    newElem.find('.person-da-form-req-toggle').attr('id', 'personDAFormReqToggle_' + newNum).attr('name', 'personDAFormReqToggle_' + newNum);
    newElem.find('.person-da-form-not-req-comments').attr('id', 'personDAFormNotReqComments_' + newNum).attr('name', 'personDAFormNotReqComments_' + newNum).val('').removeAttr('disabled');
    newElem.find('.person-da-test-refused-toggle').attr('id', 'personDATestRefusedToggle_' + newNum).attr('name', 'personDATestRefusedToggle_' + newNum).attr('disabled', 'disabled');
    newElem.find('.person-da-tested-by').attr('id', 'personDATestedBy_' + newNum).attr('name', 'personDATestedBy_' + newNum).val('-1').attr('disabled', 'disabled');
    newElem.find('.person-da-pass-toggle').attr('id', 'personDAPassToggle_' + newNum).attr('name', 'personDAPassToggle_' + newNum).attr('disabled', 'disabled');
    newElem.find('.person-da-form').attr('id', 'personDAForm_' + newNum).attr('name', 'personDAForm_' + newNum).val('').attr('disabled', 'disabled');
    newElem.find('.person-da-files').attr('id', 'showDAFiles_' + newNum);

    newElem.find('.vehicleValidationError').attr('id', 'vehicleValidationError_' + newNum).css('display', 'none');
    newElem.find('.daFormValidationError').attr('id', 'daFormValidationError_' + newNum).css('display', 'none');
    newElem.find('.prevIncidentsValidationError').attr('id', 'prevIncidentsValidationError_' + newNum).css('display', 'none');
    newElem.find('.injuryValidationError').attr('id', 'injuryValidationError_' + newNum).css('display', 'none');
    newElem.find('.personGeneralValidationError').attr('id', 'personGeneralValidationError_' + newNum).css('display', 'none');
    newElem.find('.personDateValidationError').attr('id', 'personDateValidationError_' + newNum).css('display', 'none');

    newElem.find('.bootstrap-select').replaceWith(function () {
        return $('select', this);
    });
    //Add the new DIV
    
   var incidentDateTime = moment($('#incidentDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss').toJSON();


    $('#personDetails_' + num).after(newElem);
    $('#showDAFiles_' + newNum).empty();
    $('#btnDelPerson').attr('disabled', false);

    //Set all Datepickers
    $("#personDOB_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: false,
        maxDate: new Date(incidentDateTime),
        viewMode: 'years',
        date:'',
        userCurrent: false,
        locale: moment.locale('en', {week: {dow: 1}})
    });
    $("#personDOB_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "DOB");
    });
    $("#personESD_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: false,
        maxDate: new Date(incidentDateTime),
        date:'',
        userCurrent: false,
        locale: moment.locale('en', {week: {dow: 1}})
    });
    $("#personESD_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "ESD");
    });
    $("#personTOJSD_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        defaultDate: false,
        maxDate: new Date(incidentDateTime),
        date:'',
        userCurrent: false,
        locale: moment.locale('en', {week: {dow: 1}})
    });
    $("#personTOJSD_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "TOJ");
    });
     newElem.find('.person-esd-2').val('');
     newElem.find('.person-jsd-2').val('');
     newElem.find('.person-dob-2').val('');

    if(flag === 'NEW'){
        $("#personDetails_" + newNum).find('.badgebox').each(function () {
            $(this).prop('checked', false);
            $(this).next().html("&cross;");
            $(this).parent().removeClass('.btn-primary');
            $(this).parent().addClass('btn-danger');
        });
         $("#personDetails_" + newNum).find('.badgeboxReverse').each(function () {
            $(this).prop('checked', false);
            $(this).next().html("&cross;");
            $(this).parent().removeClass('btn-danger');
            $(this).parent().addClass('btn-primary');
        });
    }
    //Uncheck radio buttons
    $('input[name="isLocationOfInjury_' + newNum + '"]').prop('checked', false);

    $('input[name=personLocationInjury_' + newNum + ']').prop('checked', false);
    $('input[name=personLocationInjury_' + newNum + ']').prop('disabled', true);
    //Set all select boxes
    $('[name="personPPEWorn_' + newNum + '"]').selectpicker('refresh');
    $('[name="personTreatment_' + newNum + '"]').selectpicker('refresh');
    $('[name="personLocationInjury_' + newNum + '"]').selectpicker('refresh');
    //Attach even to check boxes
    //$('input[type=checkbox]').click(function () {
    $('.badgebox').click(function () {
        if ($(this).is(':checked')) {
            $(this).next().html("&check;");
            $(this).parent().removeClass('btn-danger');
            $(this).parent().addClass('btn-primary');
            console.log("I am in function checked");
        } else {
            $(this).next().html("&cross;");
            $(this).parent().removeClass('btn-primary');
            $(this).parent().addClass('btn-danger');
            console.log("I am in function not checked");
        }
    });
    
    $('#vehicleValidationError_' + newNum).css('display', 'none');
    $('#daFormValidationError_' + newNum).css('display', 'none');
    $('#prevIncidentsValidationError_' + newNum).css('display', 'none');
    $('#injuryValidationError_' + newNum).css('display', 'none');
    if(flag === 'NEW'){
        $('html, body').animate({
                scrollTop: $("#personDetails_" + newNum).offset().top
            }, 1000);
    }

}
function populateInjuredPersonNames(elem, personArray) {
    for (var i = 0; i < personArray.length; i++) {
        $(elem).append('<option value ="' + personArray[i].person_name + '">' + personArray[i].person_name + '</option>');
    }
}
function populateHSData(data) {
    var injuredPersons = data.injuredPersons;
    $('#category').val(data.category);
    $('#hsSummary').val(data.hs_summary);
    $('#categoryLevel').val(data.category_level);
    $('#type').val(data.type);
    $('#actOrConditon').val(data.act_or_condition);
    $('#classification').val(data.classification);
    $('#isDangerousOccurence').val(data.is_dangerous_occurence === '1' ? 'Yes' : data.is_dangerous_occurence === '0' ? 'No' : '-1');
    
    populateInjuredPersonNames($('#injuredPersonName_1'), data.persons);
    for (var tmp = 0; tmp < injuredPersons.length; tmp++) {
        var newNum = tmp + 1;
        if (tmp > 0) {
            addInjuredPerson();
        }
        var newElem = $('#injuredPerson_' + newNum);
        newElem.find('.injured-person-id').val(injuredPersons[tmp].id);
        //populateInjuredPersonNames($('#injuredPersonName_'+newNum), data.persons);
        newElem.find('.injured-person-name').val(injuredPersons[tmp].injured_person_name);
        newElem.find('.injured-person-treatment-details').val(injuredPersons[tmp].further_details_and_treatment);
        newElem.find('.injured-person-days-lost').val(injuredPersons[tmp].days_lost);
        newElem.find('.injured-person-riddor-ref').val(injuredPersons[tmp].riddor_reference);
        populateToggle('isRIDDORToggle_' + newNum, injuredPersons[tmp].isRIDDOR);
        enableRIDDOR($('#isRIDDORToggle_' + newNum));
        var HSEDate = injuredPersons[tmp].hse_reported_date !== '' ? new Date(injuredPersons[tmp].hse_reported_date) : '';
        $("#injuredPersonHSEReportedDate_" + newNum).datetimepicker({
            format: 'DD/MM/YYYY',
            date: HSEDate,
            defaultDate: false,
            useCurrent: false,
            maxDate: moment(),
            locale: moment.locale('en', {week: {dow: 1}})
        });
        if (injuredPersons[tmp].hse_reported_date !== null && injuredPersons[tmp].hse_reported_date !== '') {
            newElem.find('.person-hse-date-2').val(getFormattedDate(HSEDate));
        }
        $('#showRIDDORFiles_' + (newNum)).empty();
        populateFiles(injuredPersons[tmp].riddorfiles, data.incident_number, 'showRIDDORFiles_' + newNum, 'RIDDOR', 'Attached RIDDOR Files');
    }

}
function populatePerson(newNum, data) {
    //Populate the person based on data
    var newElem = $('#personDetails_' + newNum);
    newElem.find('.person-name').val(data.person_name);
    newElem.find('.person-emp-status').val(data.employment_status);
    showDates($('#personEmpStatus_' + newNum));
    populateToggle("personPrevIncidentsToggle_" + newNum, data.any_previous_incidents);
    enableDateAndRef($("#personPrevIncidentsToggle_" + newNum));
    newElem.find('.person-date-ref').val(data.prev_incident_date_reference);
    populateToggle('personVehicleInvolvedToggle_' + newNum, data.is_vehicle_involved);
    enableVehicleField($('#personVehicleInvolvedToggle_' + newNum));
    newElem.find('.person-vehicle-type').val(data.vehicle_type);
    newElem.find('.person-vehicle-type-comments').val(data.vehicle_type_other);
    enableOtherVehicleComments($('#personVehicleType_' + newNum))
    
    newElem.find('.person-vehicle-reg').val(data.vehicle_reg_no);
    populateToggle('personRemovedToggle_' + newNum, data.was_person_removed_from_truck);
    if (data.is_injury_involved === '1') {
        $("#injuryYes_" + newNum).prop("checked", true);
        $('input[name=personLocationInjury_' + newNum + ']').prop('disabled', false);
    } else if (data.is_injury_involved === '0') {
        $("#injuryNo_" + newNum).prop("checked", true);
        $('input[name=personLocationInjury_' + newNum + ']').prop('checked', false);
        $('input[name=personLocationInjury_' + newNum + ']').prop('disabled', true);
    }

    var injuries = data.location_of_injury.split(',');
    for (var z = 0; z < injuries.length; z++) {
        $("input[value='" + injuries[z] + "']").prop('checked', true);
    }



    newElem.find('.person-treatment').val(data.treatment_administered.split(','));
    if (newElem.find('.filter-option-inner-inner')[0] !== undefined) {
        newElem.find('.filter-option-inner-inner')[0].innerText = data.treatment_administered;
    }
    newElem.find('.person-ppe-worn').val(data.ppe_worn.split(','));
    if (newElem.find('.filter-option-inner-inner')[1] !== undefined) {
        newElem.find('.filter-option-inner-inner')[1].innerText = data.ppe_worn;
    }
    populateToggle('personPPEOfReqLevelToggle_' + newNum, data.was_ppe_req_level);
    newElem.find('.person-treatment-details').val(data.treatment_details);
    populateToggle('personDAFormReqToggle_' + newNum, data.da_test_required);
    populateToggle('personDATestRefusedToggle_' + newNum, data.da_test_agreed);
    populateToggle('personDAPassToggle_' + newNum, data.da_passed);
    newElem.find('.person-da-tested-by').val(data.da_tested_by);
    newElem.find('.person-da-form-not-req-comments').val(data.da_test_not_req_reason);
    //enableDAFields($('#personDAFormReqToggle' + newNum));
    if (data.da_test_required === '1') {
        $('#personDAFormNotReqComments_' + newNum).attr('disabled', 'disabled');
        $('#personDAFormNotReqComments_' + newNum).val('');
        $('#personDATestRefusedToggle_' + newNum).removeAttr('disabled');
         if(data.da_test_agreed === '0'){
            $('#showDAFiles_' + (newNum)).empty();
            $('#personDAForm_' + newNum).attr('disabled', 'disabled');
            $('#personDAPassToggle_' + newNum).attr('disabled', 'disabled');
            $('#personDATestedBy_' + newNum).attr('disabled', 'disabled');
        }else{
            $('#personDAPassToggle_' + newNum).removeAttr('disabled');
            $('#personDAForm_' + newNum).removeAttr('disabled');
            $('#personDATestedBy_' + newNum).removeAttr('disabled');
            $('#showDAFiles_' + (newNum)).empty();
            populateFiles(data.dafiles, data.incident_number, 'showDAFiles_' + newNum, 'DA', 'Attached D&A Files');
        }
    } else {
        $('#personDAFormNotReqComments_' + newNum).removeAttr('disabled');
        $('#personDATestRefusedToggle_' + newNum).attr('disabled', 'disabled');
    }
   

    populateToggle('personDAPassToggle_' + newNum, data.da_passed);
    //Set all Datepickers
    var incidentDateTime = moment($('#incidentDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss').toJSON();
    var DOB = data.person_dob !== '' ? new Date(data.person_dob) : '';
    $("#personDOB_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        date: DOB,
        defaultDate: false,
        useCurrent: false,
        maxDate: new Date(incidentDateTime)
    });
    $("#personDOB_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "DOB");
    });
    if (data.person_dob !== null && data.person_dob !== '') {
        newElem.find('.person-dob-2').val(getFormattedDate(DOB));
        populateTimeDiff($("#personDOB_" + newNum), data.person_dob, "DOB");
    }
    $("#incidentDateTime").datetimepicker({
        format: 'DD/MM/YYYY HH:mm:ss',
        date: new Date(data.incident_date)
    });



    var ESD = data.employment_start_date !== '' ? new Date(data.employment_start_date) : '';
    $("#personESD_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        date: ESD,
        defaultDate: false,
        useCurrent: false,
        maxDate: new Date(incidentDateTime)
    });
    $("#personESD_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "ESD");
    });
    if (data.employment_start_date !== null && data.employment_start_date !== '') {
        newElem.find('.person-esd-2').val(getFormattedDate(ESD));
        populateTimeDiff($("#personESD_" + newNum), data.employment_start_date, "ESD");
    }
    var toJSD = data.time_on_job_start_date !== '' ? new Date(data.time_on_job_start_date) : '';
    $("#personTOJSD_" + newNum).datetimepicker({
        format: 'DD/MM/YYYY',
        date: toJSD,
        defaultDate: false,
        useCurrent: false,
        maxDate: new Date(incidentDateTime)
    });
    $("#personTOJSD_" + newNum).on("change.datetimepicker", function (e) {
        populateTimeDiff(this, e.date, "TOJ");
    });
    if (data.time_on_job_start_date !== null && data.time_on_job_start_date !== '') {
        newElem.find('.person-jsd-2').val(getFormattedDate(toJSD));
        populateTimeDiff($("#personTOJSD_" + newNum), data.time_on_job_start_date, "TOJ");
    }
    var incidentType = $("input:radio[name=incidentType]:checked").val();
    //Show hide fields

    if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident') {
        $("input[name=isLocationOfInjury_" + newNum).prop('disabled', false);
        //$("#personLocationInjury_" + newNum).prop('disabled', false);
        //$("#personLocationInjury_" + newNum).selectpicker('refresh');
    }
    if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident' || incidentType === 'Occupational Disease' || incidentType === 'Ill Health*' || incidentType === 'Ill Health') {
        $("#personTreatmentDetails_" + newNum).prop('disabled', false);
        $("#personTreatment_" + newNum).prop('disabled', false);
        $("#personTreatment_" + newNum).selectpicker('refresh');
    }
}

function uploadFiles(iid) {
    //Upload all the license files.
    $('#tab_license tbody tr').each(function (i, element) {
        var html = $(this).html();
        if (html !== '')
        {
            var ele = $(this).find('.licenseFile')[0];
            var person = $(this).find('.licensePersonName option:selected').text();
            if (person !== '') {
                var fileList = ele.files;
                callAjaxForUpload(iid, person, 'license', fileList);
            }
        }
    });

    // upload all the DA files
    var totalPersons = $('.clonedInput').length;
    for (var cnt = 0; cnt < totalPersons; cnt++) {
        var ele = $('#personDAForm_' + (cnt + 1))[0];
        var person = $('#personName_' + (cnt + 1)).val();

        var elem0 = $('#showDAFiles_' + (cnt + 1)).find('.file');

        var keepDA = [];
        for (var i = 0; i < elem0.length; i++) {
            var text0 = elem0[i].innerText;
            if (text0.indexOf('\n') !== -1) {
                keepDA.push(text0.substring(2, text0.length));
            } else {
                keepDA.push(text0.substring(1, text0.length));
            }
        }

        if (person !== '') {
            var fileList = ele.files;

            callAjaxForDAFile(keepDA, iid, person, fileList);


        }
    }

    // upload all the RIDDOR files
    var totalInjuredPersons = $('.clonedInjuredDetails').length;
    for (var cntTmp = 0; cntTmp < totalInjuredPersons; cntTmp++) {
        var elePer = $('#injuredPersonRIDDORForm_' + (cntTmp + 1))[0];
        var injuredPerson = $('#injuredPersonName_' + (cntTmp + 1)).val();

        var elem1 = $('#showRIDDORFiles_' + (cntTmp + 1)).find('.file');

        var keepRIDDOR = [];
        for (var z = 0; z < elem1.length; z++) {
            var text11 = elem1[z].innerText;
            if (text11.indexOf('\n') !== -1) {
                keepRIDDOR.push(text11.substring(2, text11.length));
            } else {
                keepRIDDOR.push(text11.substring(1, text11.length));
            }
        }

        if (injuredPerson !== '-1') {
            var fileListRIDDOR = elePer.files;

            callAjaxForRIDDORFile(keepRIDDOR, iid, injuredPerson, fileListRIDDOR);


        }
    }


    var elem1 = $('#showSSOW').find('.file');
    var keepSSOW = [];
    for (var i = 0; i < elem1.length; i++) {
        var text = elem1[i].innerText;
        if (text.indexOf('\n') !== -1) {
            keepSSOW.push(text.substring(2, text.length));
        } else {
            keepSSOW.push(text.substring(1, text.length));
        }

    }

    var elem2 = $('#showRisks').find('.file');
    var keepRisks = [];
    for (var i = 0; i < elem2.length; i++) {
        var text1 = elem2[i].innerText;
        if (text1.indexOf('\n') !== -1) {
            keepRisks.push(text1.substring(2, text1.length));
        } else {
            keepRisks.push(text1.substring(1, text1.length));
        }
    }
    var elem3 = $('#showWitnessStmts').find('.file');
    var keepWitness = [];
    for (var i = 0; i < elem3.length; i++) {
        var text2 = elem3[i].innerText;
        if (text2.indexOf('\n') !== -1) {
            keepWitness.push(text2.substring(2, text2.length));
        } else {
            keepWitness.push(text2.substring(1, text2.length));
        }
    }
    var elem = $('#showPhotos').find('.file');
    var keepPhotos = [];
    for (var i = 0; i < elem.length; i++) {
        var text3 = elem[i].innerText;
        if (text3.indexOf('\n') !== -1) {
            keepPhotos.push(text3.substring(2, text3.length));
        } else {
            keepPhotos.push(text3.substring(1, text3.length));
        }
    }
    
     var elem4 = $('#showSupportDocuments').find('.file');
    var keepSupportFiles = [];
    for (var i = 0; i < elem4.length; i++) {
        var text4 = elem4[i].innerText;
        if (text4.indexOf('\n') !== -1) {
            keepSupportFiles.push(text4.substring(2, text4.length));
        } else {
            keepSupportFiles.push(text4.substring(1, text4.length));
        }
    }

    var ssowFileList = $('#attachSSOW')[0].files;
    var riskFiles = $('#attachRiskAssessments')[0].files;
    var witnessFiles = $('#attachWitnessStmts')[0].files;
    var photoFiles = $('#attachPhotos')[0].files;
    var supportFiles = $('#supportDocuments')[0].files;

    callAjaxForDeleteFiles(keepPhotos, keepRisks, keepWitness, keepSSOW,keepSupportFiles, iid, ssowFileList, riskFiles, witnessFiles, photoFiles,supportFiles);


}
//CALL AJAC FUNCTION FOR UPLOAD
function callAjaxForUpload(iid, person, type, fileList) {
    console.log("callAjaxForUpload->" + type + " ->" + fileList.length);
    for (var x = 0; x < fileList.length; x++) {
        var file_data = fileList[x];
        var form_data = new FormData();
        form_data.append('file', file_data);
        form_data.append('iid', iid);
        form_data.append('type', type);
        form_data.append('person', person);
        $.ajax({
            url: '../action/uploadFile.php',
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            async: false,
            success: function (response) {
                console.log(x + " number File uploaded for " + type);
            }
        });
    }

}

function callAjaxForDeleteFiles(keepPhotos, keepRisks, keepWitness, keepSSOW,keepSupportFiles, incidentId, ssowFileList, riskFiles, witnessFiles, photoFiles, supportFiles) {

    var form_data = new FormData();
    form_data.append('keepPhotos', JSON.stringify(keepPhotos));
    form_data.append('keepRisks', JSON.stringify(keepRisks));
    form_data.append('keepWitness', JSON.stringify(keepWitness));
    form_data.append('keepSSOW', JSON.stringify(keepSSOW));
    form_data.append('keepSupportFiles', JSON.stringify(keepSupportFiles));
    form_data.append('incidentId', incidentId);
    $.ajax({
        url: '../masterData/deleteFileRecords.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            console.log(response);
            callAjaxForUpload(incidentId, '', 'ssow', ssowFileList);
            callAjaxForUpload(incidentId, '', 'RiskAssesment', riskFiles);
            callAjaxForUpload(incidentId, '', 'WitnesStatement', witnessFiles);
            callAjaxForUpload(incidentId, '', 'Photos', photoFiles);
            callAjaxForUpload(incidentId, '', 'SupportFiles', supportFiles);

        }
    });


}

function callAjaxForDAFile(keepDA, iid, person, fileList) {
    var form_data = new FormData();
    form_data.append('person', person);
    form_data.append('keepDA', JSON.stringify(keepDA));
    form_data.append('incidentId', iid);
    console.log("IN DA " + person + JSON.stringify(keepDA) + iid);
    $.ajax({
        url: '../masterData/deleteFileRecords.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            //No upload files
            console.log("IN DA SUCCESS" + response);
            callAjaxForUpload(iid, person, 'DA', fileList);
        }
    });
    console.log("DA file for perosn " + person);

}

function callAjaxForRIDDORFile(keepRIDDOR, iid, injuredPerson, fileList) {
    var form_data = new FormData();
    form_data.append('injuredPerson', injuredPerson);
    form_data.append('keepRIDDOR', JSON.stringify(keepRIDDOR));
    form_data.append('incidentId', iid);
    console.log("IN RIDDOR " + injuredPerson + JSON.stringify(keepRIDDOR) + iid);
    $.ajax({
        url: '../masterData/deleteFileRecords.php',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
            //No upload files
            console.log("IN RIDDOR SUCCESS" + response);
            callAjaxForUpload(iid, injuredPerson, 'RIDDOR', fileList);
        }
    });
    console.log("RIDDOR file for perosn " + injuredPerson);

}

function getDeadline() {
    var deadline = new Date(document.getElementById('incidentDateTime').value);
    if (deadline.getDay() === 5) {
        deadline.setHours(deadline.getHours() + 72);
    } else if (deadline.getDay() === 6) {
        deadline.setHours(deadline.getHours() + 48);
    } else {
        deadline.setHours(deadline.getHours() + 24);
    }
    return deadline;
}

function buildActionsTable(myList) {

    var columns = addAllColumnHeaders(myList);

    for (var i = 0; i < myList.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = myList[i][columns[colIndex]];
            if (cellValue === null) {
                cellValue = "";
            }
            row$.append($('<td/>').html(cellValue));
        }
        $("#actionsDataTable").append(row$);
    }
}

// Adds a header row to the table and returns the set of columns.
// Need to do union of keys from all records as some records may not contain
// all records
function addAllColumnHeaders(myList)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    // for (var i = 0; i < myList.length; i++) {
    var arr = ["id", "category", "description", "site", "owner", "deadline", "status"];
    for (var col in arr) {
        //if ($.inArray(key, columnSet) == -1) {
        columnSet.push(arr[col]);
        headerTr$.append($('<th/>').html(arr[col].toUpperCase()));
        //}
    }

    $("#actionsDataTable").append(headerTr$);

    return columnSet;
}




