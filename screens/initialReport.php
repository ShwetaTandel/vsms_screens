<div class="row">
    <div class="col-md-3 col-lg-9" >
        <div class="controls">
            <label class="control-label">Incident Detail <span style="color: red">*</span></label>
            <textarea rows="3" class="form-control" id="incidentDetail" maxlength="1000" ></textarea>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-4">
        <label class="control-label">Attach Photograph/Sketch <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
        <br/>
        <input class="form-control" type="file" multiple="true" id="attachPhotos">
        

    </div>
    <div class="col-md-2">
        <label class="control-label">Action Type  <span style="color: red">*</span></label>
        <select class="custom-select" id="actionType" >
            <option value="-1"></option>
            <option value="Building">Building</option>
            <option value="Corner">Corner</option>
            <option value="Destacking">Destacking</option>
            <option value="Hydraulics">Hydraulics</option>
            <option value="Loading">Loading</option>
            <option value="Manoeuvre">Manoeuvre</option>
            <option value="Manual Handling">Manual Handling</option>
            <option value="Offloading">Offloading</option>
            <option value="Parking">Parking</option>
            <option value="Picking">Picking</option>
            <option value="Pullaway">Pullaway</option>
            <option value="Putaway">Putaway</option>
            <option value="Reversing">Reversing</option>
            <option value="Stacking">Stacking</option>
            <option value="Transporting">Transporting</option>
            <option value="Turning">Turning</option>
            <option value="Other">Other</option>
        </select>
    </div>
    <div class="col-md-4">
        <label class="control-label">Specify Action Type  <span style="font-style: italic">(if selected Other)</span></label>
        <input class="form-control" id="otherActionType" maxlength="255" >
    </div>
    <div class="col-md-2">
        <label class="control-label">Area  <span style="color: red">*</span></label>
        <select class="custom-select" id="incidentArea" >
            <option value="-1"></option>
            <option value="Site">Site</option>
            <option value="Transport">Transport</option>
            <option value="Site/Transport">Site/Transport</option>
        </select>
    </div>
</div>
<br/>
<div class="row" id="showPhotos"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
    <div class="col-md-2"><label class="control-label">Attached Photographs/Sketches</label></div>
</div>
<div class="row">
    <div id ="areaValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the required fields from the above section of Initial report</Strong></div>
</div>
<hr/>
<h4>Environmental Conditions</h4>
<br/>
<div class="row">
    <div class="col-md-2">
        <label class="control-label">Lighting  <span style="color: red">*</span></label>
        <select class="custom-select" id="lighting" >
            <option value="-1"></option>
            <option value="Excellent">Excellent</option>
            <option value="Satisfactory">Satisfactory</option>
            <option value="Poor">Poor</option>
            <option value="Night">Night</option>
            <option value="Daylight">Daylight</option>
            <option value="Dusk/Dawn">Dusk/Dawn</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Temperature  <span style="color: red">*</span></label>
        <select class="custom-select" id="temperature" >
            <option value="-1"></option>
            <option value="Hot (above 30C)">Hot (above 30C)</option>
            <option value="Normal (13C-30C)">Normal (13C-30C)</option>
            <option value="Cold (below 13C)">Cold (below 13C)</option>
            <option value="Freezing (below 0C)">Freezing (below 0C)</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Noise  <span style="color: red">*</span></label>
        <select class="custom-select" id="noise" >
            <option value="-1"></option>
            <option value="Quiet">Quiet</option>
            <option value="Background">Background</option>
            <option value="Loud">Loud</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Surface Type  <span style="color: red">*</span></label>
        <select class="custom-select" id="surfaceType" >
            <option value="-1"></option>
            <option value="Carpet">Carpet</option>
            <option value="Ceramic Tile">Ceramic Tile</option>
            <option value="Concrete (Painted)">Concrete (Painted)</option>
            <option value="Concrete (Unpainted)">Concrete (Unpainted)</option>
            <option value="Grass">Grass</option>
            <option value="Gravel">Gravel</option>
            <option value="Paving">Paving</option>
            <option value="Soil">Soil</option>
            <option value="Steel">Steel</option>
            <option value="Tarmac">Tarmac</option>
            <option value="Vinyl">Vinyl</option>
            <option value="Wood">Wood</option>
            <option value="Other">Other</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Surface Condition  <span style="color: red">*</span></label>
        <select class="custom-select" id="surfaceCondition" >
            <option value="-1"></option>
            <option value="Greasy/Oily">Greasy/Oily</option>
            <option value="OK - Normal">OK - Normal</option>
            <option value="Boggy">Boggy</option>
            <option value="Wet/Slippery">Wet/Slippery</option>
            <option value="Worn/Uneven">Worn/Uneven</option>
        </select>
    </div>
</div>
<div class="row">
    <div id ="generalValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the required fields from the above section of Initial report</Strong></div>
</div>
<hr/>
<h4>Person(s) Involved</h4>
<br/>
<div class="row">
    <div class="col-3">
        <div class="form-group">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="personYes" name="isPersonInvolved" value="Yes" onchange="showFirstPerson()">
                <label class="custom-control-label" for="personYes">Yes</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" class="custom-control-input" id="personNo" name="isPersonInvolved" value="No" onchange="showFirstPerson()">
                <label class="custom-control-label" for="personNo">No</label>
            </div>
        </div>
    </div>
</div>
<hr/>
<div class="row">
    <div id ="personValidationError" class="showError alert alert-danger" style="display: none"><strong>Please select whether a person was involved in the incident or not?</Strong></div>
</div>

<div class="container-fluid clonedInput" id="personDetails_1" style="display:none">  
    <hr style="border-top: dotted;color: gainsboro" />
    <h4 id="reference_1" name="reference" class="heading-reference">Person 1</h4>

    <div class="row">
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Name <span style="color: red">*</span></label>
                <input type="text" class="form-control person-name"  id="personName_1" name="personName_1[]" maxlength="255" />
            </div>
        </div>
        <div class='col-md-2 col-lg-3'>
            <div class="form-group">
                <label class="control-label">Date of Birth (DD/MM/YYYY)<span style="color: red">*</span></label>
                <div class="input-group date person-dob-1" class="" id="personDOB_1" name="personDOB_1[]" data-target-input="nearest">
                    <input class="form-control datetimepicker-input person-dob-2" type="text" name="personDOB_1[]" id="personDOB_1"  data-target="#personDOB_1"  autocomplete="off" />
                    <div class="input-group-append person-dob-3" data-target="#personDOB_1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Age </label>
                <input type="text" class="form-control person-age" id="personAge_1" name="personAge_1[]" maxlength="10" readonly=""/>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Employment Status <span style="color: red">*</span></label>
                <select class="custom-select person-emp-status" id="personEmpStatus_1" name="personEmpStatus_1[]" onchange="showDates(this);" >
                    <option value="-1"></option>
                    <option value="Employee">Employee</option>
                    <option value="Agency">Agency</option>
                    <option value="Contractor">Contractor</option>
                    <option value="Third Party">Third Party</option>
                    <option value="Visitor/Public">Visitor/Public</option>
                </select>
            </div>
        </div>

    </div>

    <div class="row">
        <div class='col-md-2 col-lg-3'>
            <div class="form-group">
                <label class="control-label">Employment Start Date (DD/MM/YYYY) <span style="color: red">*</span></label>
                <div class="input-group date person-esd-1" id="personESD_1" name="personESD_1[]" data-target-input="nearest">
                    <input class="form-control datetimepicker-input person-esd-2" type="text" name="personESD_1[]" id="personESD_1"  data-target="#personESD_1"  autocomplete="off" />
                    <div class="input-group-append person-esd-3" data-target="#personESD_1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Length of <br/>service </label>

                <input type="text" class="form-control person-los" id="personLoS_1" name="personLoS_1[]" maxlength="255" readonly=""/>
            </div>
        </div>
        <div class='col-md-2 col-lg-3'>
            <div class="form-group">
                <label class="control-label">Time on Job Start Date (DD/MM/YYYY)<span style="color: red">*</span></label>
                <div class="input-group date person-jsd-1" id="personTOJSD_1" name="personTOJSD_1[]" data-target-input="nearest">
                    <input class="form-control datetimepicker-input person-jsd-2" type="text" name="personTOJSD_1[]" id="personTOJSD_1"  data-target="#personTOJSD_1"  autocomplete="off" />
                    <div class="input-group-append person-jsd-3" data-target="#personTOJSD_1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> 
                </div>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Time on <br/> Job </label>
                <br/>
                <input type="text" class="form-control person-toj" id="personTOJ_1" name="personTOJ_1[]" maxlength="255" readonly/>
            </div>
        </div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <br/>
                <label class="btn btn-primary">Previous Incidents 
                    <input type="checkbox" id="personPrevIncidentsToggle_1" class="person-prev-inc-toggle badgeboxReverse" onchange="enableDateAndRef(this)"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
        <div class="col-md-3 col-lg-9">
            <div class="form-group">
                <label class="control-label">Please specify Date & Reference  <span style="color: red">*</span></label>
                <input type="text" class="form-control person-date-ref" id="personDateAndRef_1" name="personDateAndRef_1[]" maxlength="255" disabled="true" />
            </div>

        </div>
    </div>
    <div class="row">
        <div id ="prevIncidentsValidationError_1" class="showError alert alert-danger prevIncidentsValidationError" style="display: none"><strong>Please add details if there were any previous incidents occurred.</Strong></div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger">Vehicle Involved 
                    <input type="checkbox" id="personVehicleInvolvedToggle_1" class="person-veh-inv-toggle badgebox" onchange="enableVehicleField(this);"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
        <div class="col-md-2 col-lg-4">
            <div class="form-group">
                <label class="control-label">Vehicle Type</label>
                <select class="custom-select person-vehicle-type" id="personVehicleType_1" name="personVehicleType_1[]" disabled="true" onchange="enableOtherVehicleComments(this);">
                    <option value="-1"></option>
                    <option value="Counterbalance B1">Counterbalance B1</option>
                    <option value="Counterbalance B2">Counterbalance B2</option>
                    <option value="Reach Truck">Reach Truck</option>
                    <option value="Pallet Truck">Pallet Truck</option>
                    <option value="Tow Tractor">Tow Tractor</option>
                    <option value="HLOP">HLOP</option>
                    <option value="LLOP">LLOP</option>
                    <option value="Straddle Carrier">Straddle Carrier</option>
                    <option value="Side Loader">Side Loader</option>
                    <option value="Car">Car</option>
                    <option value="LGV">LGV</option>
                    <option value="Bendi Truck">Bendi Truck</option>
                    <option value="Scissor Lift">Scissor Lift</option>
                    <option value="Cherry Picker">Cherry Picker</option>
                    <option value="Combi Truck">Combi Truck</option>
                    <option value="MAFI">MAFI</option>
                    <option value="Ergo Mover">Ergo Mover</option>
                    <option value="Other">Other</option>
                </select>
            </div>
        </div>
         <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Vehicle Type - Other (Comments)</label>
                <input type="text" class="form-control person-vehicle-type-comments" id="personVehicleTypeComments_1" name="personVehicleTypeComments_1[]" maxlength="100" disabled="true"/>
            </div>

        </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger" style="margin-top: 5px;">Person removed from truck? 
                    <input type="checkbox" id="personRemovedToggle_1" class="person-removed-toggle badgebox" disabled="true"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>

            </div>
        </div>
        <div class="col-md-3 col-lg-4">
            <div class="form-group">

                <label class="control-label">Vehicle Reg/No <span style="color: red">*</span></label>
                <input type="text" class="form-control person-vehicle-reg" id="personVehicleReg_1" name="personVehicleReg_1[]" maxlength="100" disabled="true"/>
            </div>

        </div>
        
    </div>
    
    <div class="row">
        <div id ="vehicleValidationError_1" class="showError alert alert-danger vehicleValidationError" style="display: none"><strong>Please select all vehicle related fields if vehicle were involved.</Strong></div>
    </div>

    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-3">
            <label>Location of Injury? <span style="color: red">*</span></label>
            <div class="form-group">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input injuryYes" id="injuryYes_1" name="isLocationOfInjury_1" value="Yes" onchange="disableBodyParts(this)" disabled="true">
                    <label class="custom-control-label" for="injuryYes_1">Yes</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input injuryNo" id="injuryNo_1" name="isLocationOfInjury_1" value="No" onchange="disableBodyParts(this)" disabled="true">
                    <label class="custom-control-label" for="injuryNo_1">No</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-lg-3">        
            <label style="font-weight:  bold">Upper Limbs</label><br/>
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Wrist" disabled="true">Right Wrist<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Hand"  disabled="true">Right Hand<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Finger(s)"  disabled="true">Right Finger(s)<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Forearm"  disabled="true">Right Forearm<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Upper arm"  disabled="true">Right Upper Arm<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Shoulder" disabled="true">Right Shoulder<br>      
            <input type="checkbox" class = "person-location"  name="personLocationInjury_1" value="Left Wrist"  disabled="true">Left Wrist<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Hand"  disabled="true">Left Hand<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Finger(s)" disabled="true">Left Finger(s)<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Forearm" disabled="true">Left Forearm<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Upper Arm" disabled="true">Left Upper Arm<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Shoulder" disabled="true">Left Shoulder<br>        
        </div>
        <div class="col-md-2 col-lg-3">  
            <label style="font-weight:  bold">Lower Limbs</label><br/>
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Foot" disabled="true">Right Foot<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Toe(s)" disabled="true">Right Toe(s)<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Ankle" disabled="true">Right Ankle<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Lower Leg" disabled="true">Right Lower Leg<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Knee" disabled="true">Right Knee<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Thigh" disabled="true">Right Thigh<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Foot" disabled="true">Left Foot<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Toe(s)" disabled="true">Left Toe(s)<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Ankle" disabled="true">Left Ankle<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Lower Leg" disabled="true">Left Lower Leg<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Knee" disabled="true">Left Knee<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Thigh" disabled="true">Left Thigh<br>      

        </div>

        <div class="col-md-2 col-lg-3">        
            <label style="font-weight:  bold">Torso</label><br/>
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Waist" disabled="true">Waist<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Middle to Lower Back" disabled="true">Middle to Lower Back<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Upper Back" disabled="true">Upper Back<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Chest" disabled="true">Chest<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="stomach" disabled="true">Stomach<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Groin" disabled="true">Groin<br>        
        </div>
        <div class="col-md-2 col-lg-3">        
            <label style="font-weight:  bold">Head & Face</label><br/>
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Ear" disabled="true">Right Ear<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Right Eye" disabled="true">Right Eye<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Head" disabled="true">Head<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Face" disabled="true">Face<br>      
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Back of Head" disabled="true">Back of Head<br>      
            <input type="checkbox" class = "person-location"name="personLocationInjury_1" value="Left Eye" disabled="true">Left Eye<br>        
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Left Ear" disabled="true">Left Ear<br>        
            <input type="checkbox" class = "person-location" name="personLocationInjury_1" value="Neck" disabled="true">Neck<br>        
        </div>

    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Treatment Administered<span style="color: red">*</span></label>
                <br/>
                <select multiple data-style="bg-white rounded-pill px-4 py-3 shadow-sm" class="selectpicker w-100 person-treatment"  id="personTreatment_1" name="personTreatment_1[]" disabled="true">
                    <?php
                    $string = file_get_contents("../files/treatment.json");
                    $json_a = json_decode($string, true);
                    echo "<option value='-1'></option>";
                    foreach ($json_a as $row) {
                        echo '<option value="' . $row['value'] . '">' . $row['value'] . '</option>';
                    }
                    echo '';
                    ?>
                </select>

            </div>
        </div>
        <div class="col-md-3 col-lg-6">
            <div class="form-group">
                <label class="control-label">Treatment details <span style="color: red">*</span></label>
                <br/>
                <textarea rows="3" class="form-control person-treatment-details" id='personTreatmentDetails_1'  name = "personTreatmentDetails_1[]" disabled="true" maxlength="1000"></textarea>
            </div>
        </div>


    </div>
    <div class="row">
        <div id ="injuryValidationError_1" class="showError alert alert-danger injuryValidationError" style="display: none"><strong>Please specify the required details.</Strong></div>
    </div>

    <div class="row">   
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">PPE Worn<span style="color: red">*</span></label>
                <br/>
                <select multiple data-style="bg-white rounded-pill px-4 py-3 shadow-sm" class="selectpicker w-100 person-ppe-worn" id="personPPEWorn_1" name="personPPEWorn_1[]" >
                    <?php
                    $string = file_get_contents("../files/ppe.json");
                    $json_a = json_decode($string, true);
                    echo "<option value='-1'></option>";
                    foreach ($json_a as $row) {
                        echo '<option value="' . $row['value'] . '">' . $row['value'] . '</option>';
                    }
                    echo '';
                    ?>
                </select>


            </div>
        </div>
        <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger" style="margin-top: 5px;">Was PPE of required level?
                    <input type="checkbox" id="personPPEOfReqLevelToggle_1" class="person-ppe-req-level-toggle badgebox"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>

            </div>
        </div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger">D&A Test required?
                    <input type="checkbox" id="personDAFormReqToggle_1" class="person-da-form-req-toggle badgebox" onchange="enableDAFields(this);"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
         <div class="col-md-3 col-lg-3">
            <div class="form-group">

                <label class="control-label">Why D&A Test not required?</label>
                <input type="text" class="form-control person-da-form-not-req-comments" id="personDAFormNotReqComments_1" name="personDAFormNotReqComments_1[]" maxlength="255"/>
            </div>

        </div>
        
          <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger">D&A Test agreed? 
                    <input type="checkbox" id="personDATestRefusedToggle_1" class="person-da-test-refused-toggle badgebox" disabled="true" onchange="enableDAFields(this)"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger">D&A Pass? 
                    <input type="checkbox" id="personDAPassToggle_1" class="person-da-pass-toggle badgebox" disabled="true"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-md-3 col-lg-3">
        <label class="control-label">D&A tested by?<span style="color: red">*</span></label>
        <select class="custom-select person-da-tested-by" id="personDATestedBy_1" name="person-da-tested-by" disabled="true">
            <option value="-1"></option>
            <option value="Recruitment agency">Recruitment agency</option>
            <option value="Contractor/Third party">Contractor/Third party</option>
            <option value="In-house">In-house</option>
        </select>
    </div>
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <label class="control-label">Attach D&A Form  <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span><span style="color: red">*</span></label>
                <input type="file" class="form-control person-da-form" id="personDAForm_1" name="personDAForm_1[]" multiple disabled="true"/>
            </div>
        </div>

    </div>
    <div class="row">
        <div id ="daFormValidationError_1" class="showError alert alert-danger daFormValidationError" style="display: none"><strong>Please select all D&A related fields if D&A test is required.</Strong></div>
    </div>
    <br/>
    <div class="row person-da-files" id="showDAFiles_1"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
        <div class="col-md-2"><label class="control-label">Attached D&AForms</label></div>
    </div>
    <br/>
    <div class="row">
        <div id ="personGeneralValidationError_1" class="showError alert alert-danger personGeneralValidationError" style="display: none"><strong>Please enter all the required fields for the person.</Strong></div>
        <div id ="personDateValidationError_1" class="showError alert alert-danger personDateValidationError" style="display: none"><strong>Time on Job Start Date cannot be earlier than Employment Start Date.</Strong></div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
</div>
<br/>


<div class="row" id="personButtons" style="display: none">
    <div class="col-md-4">

        <div id="add-del-buttons">
            <input type="button" class="btn btn-primary" id="btnAddPerson" value="Add Person">
            <input type="button" class="btn btn-danger" id="btnDelPerson" value="Remove Person">
        </div>

    </div> 
</div>
<hr/>
<h4 id="licenseTitle" style="display: none">Licence Details</h4>
<br/>
<div class="row" id="licenseDetailsDiv" style="display: none"> 
    <div class="col-md-12">
        <label class="control-label">Licence Details<span style="color: red">*</span></label>
        <table class="table table-bordered table-hover" id="tab_license">
            <thead>
            <th>Person Name</th>
            <th>Attach Licence  <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span> </th>
            <th>Licence Expiry Date</th>
            <th>Valid?</th>
            </thead>
            <tbody>
                <tr id='licenseRow0'>
                    <td width="15%"> 
                        <input type="hidden" name='licenseId[]'  class="licenseId" value="0"/>
                        <select class="custom-select licensePersonName" name="licensePersonName[]" >
                            <option value="-1"></option>

                        </select>
                    </td>
                    <td width="25%"><span class="fileLinks" style="display: none"></span><input type="file" name='licenseFile[]'  class="form-control licenseFile" /></td>
                    <td width="20%"><input type="date" id="licenseExpiryDate" name='licenseExpiryDate[]'  class="form-control licenseExpiryDate" maxlength="50"/></td>
                    <td width="15%"> <select class="custom-select licenseValid" name="licenseValid[]">
                            <option value="-1"></option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </td>
                </tr>
                <tr id='licenseRow1'></tr>
            </tbody>
        </table>
        <button id="add_license" class="btn btn-primary pull-left" type="button">+</button>
        <button id='delete_license' class="pull-right btn btn-danger" type="button">-</button>
        <br/>
        <br/>
    </div>
</div>
<div class="row">
    <div id ="licenseValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all licence related fields  or clear selections.</Strong></div>
    <div id ="licenseDateValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter correct license expiry date.</Strong></div>
    <div id ="licenseValidityError" class="showError alert alert-danger" style="display: none"><strong>Please choose appropriate licence validity according to the expiry date entered.</Strong></div>
</div>
<hr/>
<h4>Process</h4>
<br/>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <br/>
            <label class="btn btn-danger" style="margin-top: 5px">Was activity standard process? 
                <input type="checkbox" id="activityStandard" class="badgebox"/>
                <span class="badge">&cross;</span>
            </label>
            <span style="color: red">*</span>
        </div>
    </div>
    <div class="col-md-2">
        <label class="control-label">Pack Type <span style="color: red">*</span></label>
        <select class="custom-select" id="typeOfStillage">
            <option value="-1"></option>
            <option value="Half Size">Half Size</option>
            <option value="Normal">Normal</option>
            <option value="Oversized">Oversized</option>
            <option value="None">None</option>
        </select>
    </div>
    <div class="col-md-3">
        <label class="control-label">Estimated Cost (VEU only)</label>
        <br/>
        <input class="form-control"  type="number" step="0.01" min="0.00" value="0.00" id="estimatedCost"/>
    </div>
     <div class="col-md-3">
        <label class="control-label">Estimated Cost (Other)</label>
        <br/>
        <input class="form-control"  type="number" step="0.01" min="0.00" value="0.00" id="estimatedCostOther"/>
    </div>
</div>
<div class="row">
    <div class="col-md-8">
        <label class="control-label">Immediate Cause<span style="color: red">*</span></label>
        <br/>
        <textarea id="immdCause" class="form-control" rows="2" maxlength="1000" ></textarea>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Risk Assessment</label>
            <br/>
            <select class="custom-select" id="graId" required>
                <?php
                include ('../config/phpConfig.php');
             
                if (mysqli_connect_errno()) {
                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                }
                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.risk_assessment where site_id='.   $data[0]->site_id);
                echo "<option value=-1></option>";
                while ($row = mysqli_fetch_array($result)) {
                    echo '<option value="' . $row['gra_id'] . '">' ."RA". $row['gra_id'] . '</option>';
                }
                echo '';
                mysqli_close($con);
                ?>
            </select>
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div id ="processValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the required fields in the Process section of Initial report</Strong></div>
</div>
<div class="row">
    <div class="col-md-3">
        <label class="control-label">Attach Safe Systems of Work (SSoW)  <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
        <br/>
        <input type="file" multiple="true" class="form-control" id="attachSSOW"/>
    </div>
    <div class="col-md-3">
        <label class="control-label">Attach Risk Assessments (RA) <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
        <br/>
        <input type="file" multiple="true" class="form-control" id="attachRiskAssessments"/>
    </div>
    <div class="col-md-3">
        <label class="control-label">Attach Witness Statements  <span style="color: red">*</span> <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
        <br/>
        <input type="file" multiple="true" class="form-control" id="attachWitnessStmts"/>
    </div>
     <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Attach additional supporting documents  <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
                <input type="file" class="form-control support_documents" id="supportDocuments" name="supportDocuments" multiple/>
            </div>
        </div>

</div>
<br/>
<div class="row" id="showSSOW"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
    <div class="col-md-2"><label class="control-label">Attached SSoW Files</label></div>
</div>
<br/>
<div class="row" id="showRisks"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
    <div class="col-md-2"><label class="control-label">Attached Risk Assessments Files</label></div>
</div>
<br/>
<div class="row" id="showWitnessStmts"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
    <div class="col-md-2"><label class="control-label">Attached Witness Statements Files</label></div>
</div>
<br/>
<div class="row" id="showSupportDocuments">
    <div class="col-md-2"><label class="control-label">Attached supporting documents</label></div>
</div>
<div class="row">
    <div id ="witnessFileValidationError" class="showError alert alert-danger witnessFileValidationError" style="display: none"><strong>Please attach at least one Witness statement.</Strong></div>
</div>

<hr/>
<h4>Fast Response</h4>
<br/>
<div class="row"> 

    <div class="col-md-6">
        <label class="control-label">Key Points<span style="color: red">*</span></label>
        <br/>
        <textarea id="keyPoints" class="form-control" rows="2" maxlength="1000" disabled="true"></textarea>
    </div>
    <div class="col-md-6">
        <label class="control-label">Main Message<span style="color: red">*</span></label>
        <br/>
        <textarea id="mainMessage" class="form-control" rows="2" maxlength="1000" disabled="true"></textarea>
    </div>

</div>
<hr/>

<div class="row">
    <div id ="messageValidationError" class="showError alert alert-danger" style="display: none"><strong>Please enter Key points & Main message.</Strong></div>
</div>
<br/>

<hr/>
<h4>Immediate Containment</h4>
<br/>
<div class="row"> 
    <div class="col-md-12">
        <label class="control-label">Immediate Containment Action<span style="color: red">*</span></label>
        <label class="control-label pull-right " id='containmentDeadline' style="color: red">Deadline for all containments is :</label>
        <table class="table table-bordered table-hover" id="tab_action">
            <thead>
            <th style="display:none" id="actionCol1">Action ID</th>
            <th >Immediate Containment Action</th>
            <th >Site</th>
            <th >Owner</th>
            <th >Manager</th>
            <th style="display:none" id="actionCol2">Status</th>
            </thead>
            <tbody>
                <tr id='actionRow0'>
                    <td class='containmentIdCol' style="display:none"><input type="text" value="0" name = "containmentId[]" class="form-control containmentId"  readonly="true"/></td>
                    <td ><textarea rows = "1" name='containmentAction[]'  class="form-control containmentAction" maxlength="250"></textarea></td>
                    <td > <select class="custom-select containmentSite" name="containmentSite[]" onchange="populateOwners(this)">
                            <?php
                            include ('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                            echo "<option value='-1'></option>";
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </td>
                    <td >
                        <select class="custom-select actionOwner" name="actionOwner[]"  onchange="populateManager(this)">
                            <option value=""></option>
                        </select>
                    </td>
                    <td ><input type="text" name='manager[]'  class="form-control manager" maxlength="250" readonly=""/></td>
                    <td class='containmentStatusCol' style="display:none"><input type="text" value="" name = "containmentStatus[]" class="form-control containmentStatus"  readonly="true"/></td>

                </tr>
                <tr id='actionRow1'></tr>
            </tbody>
        </table>
        <button id="add_action" class="btn pull-left btn-primary" type="button">+</button>
        <button id='delete_action' class="pull-right btn btn-danger" type="button">-</button>
        <br/>
        <br/>
    </div>
</div>
<hr/>
<div class="row">
    <div id ="actionValidationError" class="showError alert alert-danger" style="display: none"><strong>At least add one action.</Strong></div>
    <div id ="actionFieldsRequiredValidationError" class="showError alert alert-danger" style="display: none"><strong> Please enter all the required fields above.</Strong></div>
</div>

