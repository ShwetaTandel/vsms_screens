<?php if ($action === "NEW") { ?>
    <ul class="nav nav-tabs container" role="tablist" id="myTabs">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" id="hazardTab" href="#hazardReport"><span style="font-size: large;font-weight: bold">Hazard Details</span></a>
        </li>
    </ul>

<?php } elseif ($action === 'HI') { ?>
    <ul class="nav nav-tabs container" role="tablist"  id="myTabs">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" id="hazardTab" href="#hazardReport"><span style="font-size: large;font-weight: bold">Hazard Details</span></a>
        </li>
        <li class="nav-item" >
            <a class="nav-link" data-toggle="tab" id="hazardInvestigationTab" href="#hazardInvestigation"><span style="font-size: large;font-weight: bold">Hazard Investigation</span></a>
        </li>
        <li class="nav-item active"  >
            <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
        </li>
    </ul>
<?php } elseif ($action === 'VIEW') { ?>
    <ul class="nav nav-tabs container" role="tablist"  id="myTabs">



        <li class="nav-item active">
            <a class="nav-link" data-toggle="tab" id="hazardTab" href="#hazardReport"><span style="font-size: large;font-weight: bold">Hazard Details</span></a>
        </li>
        <?php if ($data[0]->status === 'L2_APPROVED' || $data[0]->status === '_CLOSED') { ?>
            <li class="nav-item" >
                <a class="nav-link" data-toggle="tab" id="hazardInvestigationTab" href="#hazardInvestigation"><span style="font-size: large;font-weight: bold">Hazard Investigation</span></a>
            </li>
        <?php } if ($data[0]->status === 'L2_APPROVED' || $data[0]->status === '_CLOSED') { ?>
           <li class="nav-item"  >
            <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
        </li>
        <?php } ?>



    </ul>
<?php } ?>





