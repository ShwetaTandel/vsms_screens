<h4>Hazard Classification</h4>
 <br/>
<div class="row">
     <div class="col-md-3 col-lg-3">
     <label class="control-label">Act or Condition<span style="color: red">*</span></label>
        <select class="custom-select" id="actOrCondition" >
            <option value="-1"></option>
            <option value="Act">Act</option>
            <option value="Condition">Condition</option>
            <option value="Act/Condition">Act/Condition</option>
        </select>
    </div>
     <div class="col-md-3 col-lg-3">
        <label class="control-label">Risk Category  <span style="color: red">*</span></label>
        <select class="custom-select" id="riskCategory" >
            <option value="-1"></option>
            <option value="Burn, Scalds">Burn, Scalds</option>
            <option value="Cuts and Abrasions">Cuts and Abrasions</option>
            <option value="Dropped Product">Dropped Product</option>
            <option value="Handling and Lifting">Handling and Lifting</option>
            <option value="Nips, Traps and Strains">Nips, Traps and Strains</option>
            <option value="Slips, Trips and Falls">Slips, Trips and Falls</option>
            <option value="Struck Against">Struck Against</option>
            <option value="Struck By">Struck By</option>
            <option value="Other">Other</option>
        </select>
    </div>
    <div class="col-md-3 col-lg-6">
            <div class="form-group">
                <label class="control-label">Comments</label>
                <br/>
                <textarea rows="3" class="form-control" id='hazardComments'  name = "hazardComments" maxlength="1000"></textarea>
            </div>
        </div>
</div>
 <hr/>

 <h4>Hazard Containment</h4>
  <br/>
 <div class="row">
      <div class="col-md-3 col-lg-8">
            <div class="form-group">
                <label class="control-label">What actions have been taken to contain/reduce the risk?<span style="color: red">*</span></label>
                <br/>
                <textarea rows="3" class="form-control" id='hazardActions'  name = "hazardActions" maxlength="1000"></textarea>
            </div>
        </div>
    <div class="col-md-3 col-lg-4">
            <div class="form-group">
                <label class="control-label">Attach Files <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
                <input type="file" class="form-control" id="attachFiles" name="attachFiles[]" multiple/>
            </div>
        </div>
     
 </div>
 
<div class="row"  id="showFiles"  <?php if ($action === "L1_SUBMITTED") { ?>style="display:none"<?php } ?>>
    <div class="col-md-2" >
            <label class="control-label">Attached Files</label>
        </div>
</div>
 <div class="row">
     <div class="col-md-3 col-lg-4">
        <label class="control-label">Are further countermeasures required?<span style="color: red">*</span></label>
        <select class="custom-select" id="actionsNeeded" onchange="showActions()" >
            <option value=""></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
 </div>
 
<hr/>
<div id="actionsDiv" style="display: none">
 <h4>Add Actions</h4>
  <br/>
<div class="clonedCounterMeasure" id="counterMeasureDiv1" >
    <div class="row" >

        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label counterMeasureIdLabel" id="counterMeasureIdLabel1" style="font-weight: bold">Action ID:</label>
                <input type="hidden" value="0" id = "counterMeasureId1" class = "counterMeasureId"/>
            </div>
            <div class="controls">
                <label class="control-label">Countermeasures </label>
                <textarea rows ="3" class="form-control counterMeasure" id="counterMeasure1" maxlength="250" ></textarea>
            </div>
        </div>
        <div class="col-md-3 col-lg-8" >
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Type</label>
                    <select class="custom-select counterMeasureType" id="counterMeasureType1">
                        <option value="-1"></option>
                        <option value="Behaviour">Behaviour</option>
                        <option value="Condition">Condition</option>
                        <option value="Equipment">Equipment</option>
                        <option value="Training">Training</option>
                        <option value="Procedure">Procedure</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Deadline</label>
                    <input type="date" id="counterMeasureDeadline1" name="counterMeasureDeadline1" class="form-control counterMeasureDeadline"/>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Status</label>
                    <input type="text" id="counterMeasureStatus1" name="counterMeasureStatus" class="form-control counterMeasureStatus" disabled="true"/>

                </div>

            </div>

            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Site</label>
                    <select class="custom-select counterMeasureSite" id="counterMeasureSite1" onchange="populateOwnersForCounterMeasure($(this))">
                        <?php
                        include ('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                        echo "<option value='-1'></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                        }
                        echo '';
                        mysqli_close($con);
                        ?>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Owner</label>
                    <select class="custom-select counterMeasureOwner" id="counterMeasureOwner1" onchange="populateCounterMeasureManager(this)">
                        <option value="-1"></option>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Manager</label>
                    <input type="text" id='counterMeasureManager1' class="form-control counterMeasureManager" readonly="true"/>

                </div>
            </div>
          
        </div>
       
    </div>
    <br/>
       <div class="row">
            <div id ="counterMeasureValidation1" class="counterMeasureValidation showError alert alert-danger" style="display: none"><strong>Please enter/clear all fields in Other Countermeasure added.</Strong></div>
        </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <br/>
</div>
  <div class="row">
    <div id ="counterMeasureDateValidation" class="counterMeasureDateValidation showError alert alert-danger" style="display: none"><strong>Please enter correct dates.</Strong></div>
    <div id ="counterMeasureRequiredError" class="showError alert alert-danger" style="display: none"><strong>At least add one Countermeasure Action, if you have selected Yes for further actions needed.</Strong></div>
</div>
<div class="row" id="counterButtons" >
    <div class="col-md-12">

        <div>
            <input type="button" class="btn btn-blue" id="btnAddCounter" value="+">
            <input type="button" class="btn btn-red pull-right" id="btnDelCounter" value="-" disabled="true">
        </div>

    </div> 
</div>
  </div>