<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$filter = 'NEW';
$actionId = 0;
$source = 'Other';
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}
if (isset($_GET['id'])) {
    $actionId = $_GET['id'];
    $serviceActionUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/actionsData.php?filter=ACTIONDETAILS&rowID=" . $actionId;
    $actionData = json_decode(file_get_contents($serviceActionUrl));
     
    if ($actionData === null || empty($actionData)) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
    if ($filter === 'OPEN' && $actionData[0]->action_owner !== $_SESSION['vsmsUserData']['id']) {
        echo "<h1>You are not authorized to access this action. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    } else if ($filter === 'OPEN' && ($actionData[0]->status === 'CLOSED' || $actionData[0]->status === 'CANCELLED')) {
        echo "<h1>Looks like this action is no longer pending for your input. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
   
}
if (isset($_GET['source'])) {
    $source = $_GET['source'];
}
include '../masterData/generalMasterData.php';
$actionOwners = getActionOwners();
?>
<html>
    <head>
        <title>VSMS - Report Incident</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>

        <script src="../js/tempusdominus-bootstrap-4.min.js"></script>
        <link href="../css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/bootstrap-select.min.js"></script>
        <script src="../js/IEFixes.js"></script>

        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
            .card-header .fa {
                transition: .3s transform ease-in-out;
            }
            .card-header .collapsed .fa {
                transform: rotate(-90deg);

            } 
            #overlay >img{

                position:absolute;
                top:500px;
                left: 1000px;
                z-index:10;


            }
            #modal {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px; right:20px;
                bottom:20px; left:20px;
            }
            #container{
                position:relative;
                display:none;
            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;
            }

            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

        </style>
    </head>
    <body >
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center" id = "heading"></h1>      
            </div>
        </div>
        <form action="" method="post">
            <div id="overlay">
                <img src="http://cdn.nirmaltv.com/images/generatorphp-thumb.gif" alt="Wait" alt="Loading" />
                <div id="modal">
                    Saving Action Data......
                </div>
            </div>
            <div class="container" style="background-color: whitesmoke; border-style: groove">
                <div id="reportDetailsMain">
                    <br/>
                    <h4>Report Details</h4>
                    <a  href="" id="reportIdLink" style="font-weight: bold"></a>
                    <input type="hidden" id="reportId" value="0"/>
                    <br/>
                    <div class="row" id="reportDetails">
                        <div class="col-md-2 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Is this action linked to an existing Incident or Hazard Report?<span style="color: red">*</span></label>
                                <div class="input-group">
                                    <select class="form-control" id="isActionConnected" onchange="populateReportType(this)">
                                        <option value="-1"></option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-2">
                            <div class="form-group">
                                <label class="control-label">Report Type<span style="color: red">*</span></label>
                                <br/>    <br/>
                                <select class="form-control" id="reportType"  disabled="true" onchange="populateReportIds()">
                                    <option value="-1"></option>
                                    <option value="Incident">Incident</option>
                                    <option value="Hazard">Hazard</option>

                                </select>

                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site<span style="color: red">*</span></label>
                                <br/>    <br/>
                                <select class="form-control" id="reportSite"  onchange="populateReportIds()" disabled="true">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users_sites,' . $mDbName . '.site where users_sites.site_id = site.id and users_sites.user_id = ' . $_SESSION['vsmsUserData']['id']);
                                    echo "<option value='-1'></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Report ID<span style="color: red">*</span></label>
                                <br/>    <br/>
                                <div class="input-group">
                                    <select class="form-control" id="reportIdSelect" disabled="true" onchange="populateIncidentData()">
                                        <option value="-1"></option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row" >
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="site" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="location" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Date & Time</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="reportDateTime" disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Report Status</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="reportStatus" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr style="border-top: dotted;color: gainsboro" />
                </div>
                <div>
                    <h4>Action Details</h4>
                    <div class="row">

                        <div class="col-md-2 col-lg-4">
                            <div class="form-group">
                                <label class="control-label">Action Category</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="actionType" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3 pull-right" style="display: none" id="actionSourceDiv">
                            <div class="form-group">
                                <label class="control-label">Action Source<span style="color: red">*</span></label>
                                <div class="input-group">
                                    <select class="custom-select" id="actionSource" >
                                        <option value="-1"></option>
                                        <option value="H&S Audit">H&S Audit</option>
                                        <option value="Safety Tour">Safety Tour</option>
                                        <option value="Change Assessment">Change Assessment</option>
                                        <option value="Risk Assessment">Risk Assessment</option>
                                        <option value="COSHH Assessment">COSHH Assessment</option>
                                        <option value="Display Screen Equipment Assessment">Display Screen Equipment Assessment</option>
                                        <option value="Manual Handling Audit">Manual Handling Audit</option>
                                        <option value="Driving Standard Audit">Driving Standard Audit</option>
                                        <option value="Covid-19 Concern">Covid-19 Concern</option>
                                        <option value="ISO 45001">ISO 45001</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row" >

                        <div class="col-md-3 col-lg-4" >
                            <div class="controls">
                                <label class="control-label" id="labelActionId" style="font-weight: bold">Action ID:</label>

                            </div>
                            <div class="controls">
                                <label class="control-label">Action Description<span style="color: red">*</span></label>
                                <textarea rows ="3" class="form-control" id="actionDesc" maxlength="250" disabled="true"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-8" >
                            <div  class="row">
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label">Action Type<span style="color: red">*</span></label>
                                    <select class="custom-select" id="actionSubType" disabled="true">
                                        <option value="-1"></option>
                                        <option value="Behaviour">Behaviour</option>
                                        <option value="Condition">Condition</option>
                                        <option value="Equipment">Equipment</option>
                                        <option value="Training">Training</option>
                                        <option value="Procedure">Procedure</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label">Deadline<span style="color: red">*</span></label>
                                    <input type="date" id="deadline" name="deadline" class="form-control" disabled="true"/>

                                </div>
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label"> Status</label>
                                    <input type="text" id="actionStatus" name="actionStatus" class="form-control" disabled="true"/>

                                </div>
                            </div>
                            <div  class="row">
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label">Select Site<span style="color: red">*</span></label>
                                    <select class="custom-select" id="actionSite" disabled="true" onchange="populateOwnersForCounterMeasure($(this))">
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                                        echo "<option value='-1'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label">Select Owner<span style="color: red">*</span></label>
                                    <select class="custom-select" id="actionOwner" disabled="true" onchange="populateCounterMeasureManager($(this))">

                                    </select>
                                </div>
                                <div class="col-md-3 col-lg-4">
                                    <label class="control-label">Manager</label>
                                    <input type="text" id='actionOwnerManager' class="form-control" readonly="true"/>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-lg-6">
                            <label class="control-label">Attach Photo</label>

                            <input type="file" id="actionPhoto" multiple="true" name="actionPhoto" class="form-control" disabled="true"/>
                        </div>
                    </div>
                    <hr style="border-top: dotted;color: gainsboro" />
                </div>
                <div id="assignedDiv">
                    <h4>(Re)Assigned By</h4>
                    <div class="row">
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Name</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="assignedBy" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Date</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="assignedDate" maxlength="255" disabled="true"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr style="border-top: dotted;color: gainsboro" />


                    <div class="row" id='chooseActionDiv'>
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Choose what to do with this action<span style="color: red">*</span></label>
                                <div class="input-group">
                                    <select class="form-control" onchange="showActions(this)" id="actionTaken">
                                        <option value="-1"></option>
                                        <option value="COMPLETE">Claim this action complete</option>
                                        <option value="CHANGE">Change estimated completion date</option>
                                        <option value="COMMENTS">Add Comments</option>
                                        <option value="CANCEL">Cancel this action</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id='chooseCommentsDiv'>
                        <div class="col-md-2 col-lg-6" id="actionCommentsDiv">
                            <div class="form-group">
                                <label class="control-label">Comments<span style="color: red">*</span></label>
                                <div class="input-group">
                                    <textarea class="form-control" rows="2" maxlength="500" id="actionComments"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-2 col-lg-3' style="display:none" id="dateDiv">
                            <div class="form-group">
                                <label class="control-label">Estimated Completion Date(DD/MM/YYYY)<span style="color: red">*</span></label>
                                <div class="input-group date completionDate1"  id="completionDate" name="completionDate" data-target-input="nearest">
                                    <input class="form-control datetimepicker-input completionDate2" type="text" name="completionDate" id="completionDate"  data-target="#completionDate"  autocomplete="off"/>
                                    <div class="input-group-append completionDate3" data-target="#completionDate" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3" style="display:none" id="fileDiv">
                            <div class="form-group">
                                <label class="control-label">Attach Files</label>
                                <br/> <br/>
                                <div class="input-group">
                                    <input type="file" id="actionFiles" class="form-control" multiple="true"/>
                                </div>
                            </div>
                        </div >

                    </div>
                </div>
                <div class="col-md-2 col-lg-3" id="showActionFiles"  style="display: none">
                </div>
                <hr/>
                <div class="container" id="commentsTableContainer" style="display: none">
                    <h4>Added Comments</h4>
                    <table id="commentsTable"></table>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div style="margin-top: 2rem">
                            <div class="pull-right">
                                <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()" <?php if ($filter === "VIEW") { ?>style="display:none"<?php } ?>></input>
                                <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate()" <?php if ($filter === "VIEW") { ?>style="display:none"<?php } ?>></input>
                                <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('home.php?show=A', '_self')" <?php if ($filter !== "VIEW") { ?>style="display:none"<?php } ?>></input>
                            </div>
                        </div>
                    </div>
                </div>
              
                <br/><br/>
            </div>
        </form>
    </body>
    <script>
        $(document).ready(function () {
            var actionId = <?php echo $actionId ?>;
            var source = '<?php echo $source ?>';
            $("#completionDate").datetimepicker({
                format: 'DD-MM-YYYY',
                locale: moment.locale('en', {week: {dow: 1}}),
                useCurrent: false,
                minDate: moment()
            });
            if (actionId !== 0) {
                var url = '';
                 if (source === 'Incident') {
                    url = "../masterData/actionsData.php?filter=ACTIONDATA&rowID=" + actionId;
                }else if (source === 'Hazard') {
                    url = "../masterData/actionsData.php?filter=ACTIONDATAHAZARD&rowID=" + actionId;
                }else if (source === 'GRA') {
                    url = "../masterData/actionsData.php?filter=ACTIONDATAGRA&rowID=" + actionId;
                }else {
                    url = "../masterData/actionsData.php?filter=ACTIONDATAOTHER&rowID=" + actionId;
                }
                $.ajax({
                    url: url,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        poupulateActionData(response);

                    }});
            }
            else {
                showActionForm();
            }
        });
        function populateReportType(elem) {
            if ($(elem).val() === 'Yes') {
                $('#reportType').attr('disabled', false);
                $('#reportSite').attr('disabled', false);
                $('#reportIdSelect').attr('disabled', false);
                $('#actionPhoto').attr('disabled', true);
               
                $('#actionSourceDiv').hide();
            } else {
                $('#reportIdSelect').empty().append('<option selected="selected" value="-1"></option>');
                $('#reportType').attr('disabled', true);
                $('#reportIdSelect').attr('disabled', true);
                $('#reportType').val('-1');
                $('#reportSite').val('-1');
                $('#site').val('');
                $('#location').val('');
                $('#reportDateTime').val('');
                $('#reportStatus').val('');
                $('#reportSite').attr('disabled', true);
                $('#actionType').val('Countermeasure');
                $('#actionPhoto').attr('disabled', false);
                $('#actionSourceDiv').show();
            }

        }
        function populateIncidentData() {
            if ($('#reportType').val() === 'Incident') {
                var iid = $('#reportIdSelect').val();
                $.ajax({
                    url: "../masterData/actionsData.php?filter=INCIDENTDATA&iid=" + iid,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        var incident = JSON.parse(response);
                        document.getElementById('site').value = incident[0].code;
                        document.getElementById('location').value = incident[0].location;
                        document.getElementById('reportStatus').value = incident[0].description;
                        document.getElementById('reportDateTime').value = new Date(incident[0].incident_date).toLocaleDateString() +' ' +incident[0].incident_date.split(' ')[1];;

                    }});
            }else  if ($('#reportType').val() === 'Hazard') {
                var iid = $('#reportIdSelect').val();
                $.ajax({
                    url: "../masterData/actionsData.php?filter=HAZARDDATA&iid=" + iid,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        var hazard = JSON.parse(response);
                        document.getElementById('site').value = hazard[0].code;
                        document.getElementById('location').value = hazard[0].location;
                        document.getElementById('reportStatus').value = hazard[0].description;
                        document.getElementById('reportDateTime').value = new Date(hazard[0].hazard_date).toLocaleDateString() +' ' +hazard[0].hazard_date.split(' ')[1];;

                    }});
            }
        }
        function populateReportIds() {
            var site = $('#reportSite').val();
            var reportType = $('#reportType').val();
            var filter = reportType === 'Incident' ? "SITEINCIDENTS" : "SITEHAZARDS";
            var actionType = reportType === 'Incident' ? 'Incident-Countermeasure-Additional' : 'Hazard-Countermeasure-Additional'
             $('#actionType').val(actionType);
            var selectList = document.getElementById("reportIdSelect");
            $(selectList).attr('disabled', false);
            $(selectList).empty();
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            option.innerHTML = "";
             selectList.add(option);

            $.ajax({
                url: "../masterData/actionsData.php?filter=" + filter + "&siteId=" + site,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    var incidents = JSON.parse(response);
                    for (var i = 0; i < incidents.length; i++) {
                        var newOption = document.createElement('option');
                        newOption.setAttribute("value", incidents[i].reportId);
                        if (reportType === 'Incident') {
                            newOption.innerHTML = 'IR' + incidents[i].reportId;
                        } else if (reportType === 'Hazard'){
                            newOption.innerHTML = 'HZ' + incidents[i].reportId;
                        }
                         selectList.add(newOption);
                    }
                }});
        }
        function showActionForm() {
            document.getElementById('heading').innerText = 'CREATE NEW ACTION';
            $('#actionDesc').attr('disabled', false);
            $('#actionSubType').attr('disabled', false);
            $('#actionSubType').attr('disabled', false);
            $('#deadline').attr('disabled', false);
            $('#actionSite').attr('disabled', false);
            $('#actionOwner').attr('disabled', false);
            document.getElementById('assignedDiv').style.display = 'none';

        }
        function poupulateActionData(response) {
            var source = '<?php echo $source ?>';
            var actionData = JSON.parse(response)[0];
            var filter = '<?php echo $filter ?>';
            //if (actionData.actionStatus === 'Re-opened' || actionData.actionStatus === 'Closed' ||) {
            document.getElementById('showActionFiles').style.display = 'block';
            document.getElementById('commentsTableContainer').style.display = 'block';
            populateActionFiles(actionData.files, actionData.photos, actionData.id);
            //}

            console.log(actionData);
            if (filter === 'OPEN') {
                document.getElementById('heading').innerText = 'OUTSTANDING ACTION';
            } else {
                document.getElementById('heading').innerText = 'VIEW ACTION';
                document.getElementById('chooseActionDiv').style.display = 'none';
                document.getElementById('chooseCommentsDiv').style.display = 'none';
                if (actionData.actionStatus === 'Cancelled') {
                    document.getElementById('chooseCommentsDiv').style.display = 'block';
                    document.getElementById('actionCommentsDiv').style.display = 'block';
                    $('#actionComments').val(actionData.comments);
                    $('#actionComments').attr('disabled', true);
                } else if (actionData.actionStatus === 'Closed') {
                    document.getElementById('chooseCommentsDiv').style.display = 'block';
                    document.getElementById('actionCommentsDiv').style.display = 'block';
                    $('#actionComments').val(actionData.comments);
                    $('#actionComments').attr('disabled', true);


                }
            }
            if (source === 'Incident' || source === 'Hazard' ||source === 'GRA' ) {
                document.getElementById('reportDetails').style.display = 'none';
                document.getElementById('site').value = actionData.reportsite;
               // document.getElementById('location').value = actionData.location;
               $('#location').val(actionData.location);
                document.getElementById('reportDateTime').value =  new Date(actionData.reportDate).toLocaleDateString() +' ' +actionData.reportDate.split(' ')[1];
                
                document.getElementById('reportStatus').value = actionData.reportStatus;
                document.getElementById('reportId').value = actionData.reportId;
                 if (source === 'Incident'){
                   document.getElementById('reportIdLink').innerText = 'Report ID: IR' + actionData.reportId;
                    document.getElementById('reportIdLink').href = "reportIncident.php?action=VIEW&iid=" + actionData.reportId.substring();
                 }else   if (source === 'Hazard'){
                      document.getElementById('reportIdLink').innerText = 'Report ID: HZ' + actionData.reportId;
                       document.getElementById('reportIdLink').href = "reportHazard.php?action=VIEW&hid=" + actionData.reportId.substring();
                 }else   if (source === 'GRA'){
                      document.getElementById('reportIdLink').innerText = 'Report ID: RA' + actionData.reportId;
                       document.getElementById('reportIdLink').href = "genericRiskAssessment.php?action=VIEW&graid=" + actionData.reportId.substring();
                 }
               
            } else {
                document.getElementById('reportDetails').style.display = 'none';
                document.getElementById('reportDetailsMain').style.display = 'none';
                document.getElementById('actionSourceDiv').style.display = 'block';
                $("#actionSource").attr("disabled", "disabled");
            }
            document.getElementById('actionSource').value = actionData.action_source;
            document.getElementById('actionType').value = actionData.action_type;
            if (actionData.action_type.includes('Containment')) {
                $("#actionTaken option[value='CHANGE']").attr("disabled", "disabled");
            }
            document.getElementById('labelActionId').innerText = "Action ID: " + actionData.id;
            document.getElementById('actionDesc').value = actionData.action;
            document.getElementById('actionSubType').value = actionData.action_sub_type;
            document.getElementById('deadline').value = new Date(actionData.deadline).toISOString().split('T')[0];
            document.getElementById('actionStatus').value = actionData.actionStatus;
            document.getElementById('actionSite').value = actionData.action_site;
            var option = document.createElement("option");
            option.text = actionData.actionOwner;
            document.getElementById('actionOwner').add(option);
            document.getElementById('actionOwnerManager').value = actionData.manager_name;
            document.getElementById('assignedBy').value = actionData.assigned_by;
            document.getElementById('assignedDate').value = new Date(actionData.assigned_at).toLocaleDateString() +' ' +actionData.assigned_at.split(' ')[1];

            populateActionComments();
        }
        function populateOwnersForCounterMeasure(elem) {

            var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
            var containmentSite = elem.val();
            var owners = '';
            $('#actionOwner').attr('disabled', false);
            var ownerList = $('#actionOwner');
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === containmentSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateCounterMeasureManager(elem) {
            document.getElementById('actionOwnerManager').value = $('#actionOwner').find('option:selected').attr('data-divid') === undefined ? '' : $('#actionOwner').find('option:selected').attr('data-divid');
        }
        function showActions(elem) {
            var action = $(elem).val();
            if (action === 'COMPLETE') {
                document.getElementById('fileDiv').style.display = 'block';
                document.getElementById('dateDiv').style.display = 'none';
            } else if (action === 'CANCEL') {
                document.getElementById('fileDiv').style.display = 'none';
                document.getElementById('dateDiv').style.display = 'none';
            } else if (action === 'CHANGE') {
                document.getElementById('fileDiv').style.display = 'none';
                document.getElementById('dateDiv').style.display = 'block';
            } else if (action === 'COMMENTS') {
                document.getElementById('fileDiv').style.display = 'none';
                document.getElementById('dateDiv').style.display = 'none';
            }
        }
        function validate() {
            var valid = true;
            var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
            var currentDate = new Date();
            currentDate.setHours(0, 0, 0, 0);
            var action = $('#actionTaken').val();
            var filter = '<?php echo $filter ?>';
            if (filter !== 'NEW') {
                if (action === '-1') {
                    alert('Choose what to do with this action');
                    valid = false;
                }

                var comments = document.getElementById('actionComments').value;
                if (action === 'COMPLETE' || action === 'CANCEL' || action === 'COMMENTS') {
                    if (comments === '') {
                        valid = false;
                        alert('Please add comments.');
                    }
                } else if (action === 'CHANGE') {
                    if (comments === '' && $('#completionDate').data('date') === undefined) {
                        valid = false;
                        alert('Please add comments and estimated completion date.');
                        return;
                    }
                    if (comments === '') {
                        valid = false;
                        alert('Please add comments.');
                        return;
                    }
                    if ($('#completionDate').data('date') === undefined) {
                        valid = false;
                        alert('Please add estimated completion date.');
                        return;
                    }
                }
            } else {
                if ($('#isActionConnected').val() === 'Yes' && ($('#reportId').val() === '-1')) {
                    valid = false;
                    alert("Please choose Report ID , if you want to link it to any report.");
                    return false;

                }

                if ($('#actionDesc').val() === '' || $('#actionType').val() === '' || $('#deadline').val() === '' || $('#actionSite').val() === '-1' || $('#actionOwner').val() === '-1') {
                    valid = false;
                    alert("Please enter all fields marked mandatory.");
                    return false;

                } else {
                    if (moment($('#deadline').val(), dateFormats, true).isValid() === false || new Date($('#deadline').val()) < currentDate) {
                        alert("Invalid Date");
                        valid = false;
                        return false;

                    }
                }
                if ($('#isActionConnected').val() === 'No' && ($('#actionSource').val() === '-1')) {
                    valid = false;
                    alert("Please select field Action source.");
                    return false;

                }

            }
            if (valid === true) {
                var message = "Are you sure you want to submit this Action to the selected owner?";
                if (action === 'COMPLETE') {
                    message = "Are you sure you want to claim this Action as complete?";
                } else if (action === 'CANCEL') {
                    message = "Are you sure you want to cancel this Action?";
                } else if (action === 'CHANGE') {
                    message = "Are you sure you want to change the estimated completion date of this Action?"
                } else if (action === 'COMMENTS') {
                    message = "Are you sure you want to save the action with the new comments?"
                }
                if (confirm(message)) {
                    submitActionData();
                }
            }

            return valid;
        }
        function submitActionData() {
            var filter = '<?php echo $filter ?>';
            var actionModel = {};
            var reportId = 0;
            var createdByUserId = <?php echo $_SESSION['vsmsUserData']['id'] ?>;
            var action = filter;
             var actionSource = '<?php echo $source ?>';
              
            if (filter !== 'NEW') {
                var comments = document.getElementById('actionComments').value;
                var completionDateTime = moment($('#completionDate').data('date'), 'DD-MM-YYYY').toJSON();
                var actionId = <?php echo $actionId ?>;
                reportId = document.getElementById('reportId').value;
                action = $('#actionTaken').val();
                actionModel = {"comments": comments, "filter": action, "estimatedCompletionDate": completionDateTime, 
                               "id": actionId, "reportId": reportId, "createdByUserId": createdByUserId,"actionSource": actionSource};
            } else {
                //NEW Flow
                reportId = $('#isActionConnected').val() === 'Yes' ? document.getElementById('reportIdSelect').value : 0;
                actionSource = reportId !==0 ? document.getElementById('reportType').value:document.getElementById('actionSource').value;
                 
                actionModel = {
                    "id": 0,
                    "action": document.getElementById('actionDesc').value,
                    "actionSite": document.getElementById('actionSite').value,
                    "actionType": document.getElementById('actionType').value,
                    "actionSubType": document.getElementById('actionSubType').value,
                    "actionOwner": document.getElementById('actionOwner').value,
                    "deadline": document.getElementById('deadline').value,
                    "incidentWhyId": '-1',
                    "counterMeasureNumber": '',
                    "filter": filter,
                    "createdByUserId": createdByUserId,
                    "reportId": reportId,
                    "comments": "",
                    "actionSource": actionSource

                }
            }

            console.log(actionModel);
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(actionModel));
            form_data.append('function', 'saveAction');
            form_data.append('connection', vsmsservice);


            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    console.log(response);
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        if(actionSource!== 'Incident' && actionSource !=='Hazard'){
                            actionSource = "Other";
                        }
                        var windowLocation = 'actionMessage.php?action=' + filter + '&actionId=' + res[1] + '&id=' + reportId+'&source='+actionSource;
                        sendActionEmails(res[1], reportId, action, actionSource);
                        if (filter !== 'NEW') {
                            windowLocation = 'actionMessage.php?action=' + action + '&actionId=' + res[1] + '&id=' + reportId+'&source='+actionSource;
                            uploadActionFiles(res[1], '');
                        } else {
                            uploadActionFiles(res[1], 'Photos');
                        }

                        $("#overlay").fadeIn("slow");
                        var delay = 3000;
                        setTimeout(function ()
                        {
                            $("#overlay").fadeOut("fast");
                            $('#container').fadeIn();
                            alert('Action Data Saved');
                            window.location.href = windowLocation;
                        }, delay);
                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }});




        }

        function sendActionEmails(actionId, reportId, filter, source) {
            var func = '';
            var type = '';
            if (filter === 'CANCEL') {
                func = 'ActionCancelled';
                type = 'Notification';
//            } else if (filter === 'COMPLETE') {
//                func = 'ActionComplete';
//                type = 'Notification';
              } else if (filter === 'CHANGE') {
                func = 'ActionRescheduled';
                type = 'Notification';

            } else if (filter === 'NEW') {
                func = 'NewActionAssigned';
                type = 'Both';

            }
            if (filter !== 'COMPLETE'){
                $.ajax({
                    type: "GET",
                    url: url = "sendEmails.php?reportId=" + reportId + "&actionId=" + actionId + "&function=" + func + "&type=" + type+ "&source="+source,
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
        }

        function uploadActionFiles(actionid, fileType) {
            var ele;
            if (fileType === 'Photos') {
                ele = $('#actionPhoto')[0];
            } else {
                ele = $('#actionFiles')[0];
            }
            console.log("uploadActionFiles->" + ele.files.length);
            for (var x = 0; x < ele.files.length; x++) {
                var file_data = ele.files[x];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('actionid', actionid);
                if (fileType === 'Photos') {
                    form_data.append('type', fileType);
                }
                $.ajax({
                    url: '../action/uploadFile.php',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    async: false,
                    success: function (response) {
                        console.log(x + " number File uploaded for ");
                    }
                });
            }


        }

        function discardChanges() {
            if (confirm("Are you sure you want to discard all changes made to this form? This action will not be saved.")) {
                window.open("showOutstandingActions.php", "_self");
            }
        }
        function populateActionFiles(files, photos, actionId) {
            $('#showActionFiles').empty();
            $('#showActionFiles').append('<label class="control-label">' + "Attached Files/Photos" + '</label>')
            for (var i = 0; i < files.length; i++) {
                var link = "../action/downloadFile.php?aid=" + actionId + "&fileName=" + encodeURIComponent(files[i].file_name) + "&type=actions";
                var newElement = '<div class="alert alert-gray alert-dismissible file" >' +
                        '<a href="' + link + '" class="alert-link">' + files[i].file_name + '</a>' +
                        '</div>';
                $('#showActionFiles').append(newElement);
            }
            for (var i = 0; i < photos.length; i++) {
                var link = "../action/downloadFile.php?aid=" + actionId + "&fileName=" + encodeURIComponent(photos[i].file_name) + "&type=actions";
                var newElement = '<div class="alert alert-gray alert-dismissible file" >' +
                        '<a href="' + link + '" class="alert-link">' + photos[i].file_name + '</a>' +
                        '</div>';
                $('#showActionFiles').append(newElement);
            }

        }

        function populateActionComments() {
            var actionId = <?php echo $actionId; ?>;
            $.ajax({
                type: "GET",
                url: "../masterData/actionsData.php?filter=ACTIONCOMMENTS&actionId=" + actionId,
                success: function (data) {
                    data = JSON.parse(data);
                    var col = [];
                    for (var i = 0; i < data.length; i++) {
                        for (var key in data[i]) {
                            if (col.indexOf(key) === -1) {
                                col.push(key);
                            }
                        }
                    }

                    // CREATE DYNAMIC TABLE.
                    var table =document.getElementById("commentsTable");

                    // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

                    var tr = table.insertRow(-1);                   // TABLE ROW.

                    for (var i = 0; i < col.length; i++) {
                        var th = document.createElement("th");      // TABLE HEADER.
                        th.innerHTML = col[i];
                        tr.appendChild(th);
                    }

                    // ADD JSON DATA TO THE TABLE AS ROWS.
                    for (var i = 0; i < data.length; i++) {

                        tr = table.insertRow(-1);

                        for (var j = 0; j < col.length; j++) {
                            var tabCell = tr.insertCell(-1);
                            tabCell.innerHTML = data[i][col[j]];
                            
                        }
                    }

                    // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
                    var divContainer = document.getElementById("commentsTableContainer");
                    //divContainer.innerHTML = "";
                    divContainer.appendChild(table);
                }
            });
        }

    </script>
</html>