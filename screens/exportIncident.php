<?php

session_start();
include '../config/ChromePhp.php';
include ('../config/phpConfig.php');
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$iid = $_GET['iid'];
$serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/incidentsData.php?filter=INCIDENT&iid=" . $iid;
$data = json_decode(file_get_contents($serviceUrl));
if ($data == null) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}
ChromePhp::log($data);

require('./incidentReportPDF.php');
$logoFile = "../images/logoTest.jpg";
$logoXPos = 160;
$logoYPos = 0;
$logoWidth = 50;
$COL1X = 10;
$COL2X = 60;
$COL3X = 110;
$COL4X = 160;
$CURR_Y_POS = 40;
$PAGE_HEIGHT = 200;

$incident = $data[0];
$indidentDate = date_create($incident->incident_date);

// Create PDF class
$pdf = new PDF_Invoice('P', 'mm', 'A4');
/* * START HEADER* */
$pdf->AddPage();
$pdf->SetAutoPageBreak(true, 2);
$pdf->AliasNbPages();
$pdf->Image($logoFile, $logoXPos, $logoYPos, $logoWidth);
$pdf->addSeperator();
$pdf->addCompanyName("VANTEC EUROPE LTD.", "3 Infiniti Drive, Hillthorn Business Park, Washington Tyne and Wear, NE37 3HG", "VAT No GB569306809 / Company Reg No 2458961");
/* * END  HEADER* */

//DOCUMENT HEADING
$pdf->addIncidentHeaderTitle($iid);

//INCIDENT DETAILS PAGE
addIncidentDetailsPage();

//PERSON DETAILS - START ON A NEW PAGE
if ($incident->is_person_involved === '1') {
    $pdf->AddPage();
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSubSubHeader("Person Details", $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 5;
    for ($i = 0; $i < count($incident->persons); $i++) {
        addPersonDetails($i);
        ChromePhp::LOG("AFTER ADD PERSON DETAILS YJE " . $CURR_Y_POS);
        if ($CURR_Y_POS > $PAGE_HEIGHT) {
            $pdf->AddPage();
            $CURR_Y_POS = $pdf->GetY() + 10;
        }
    }
    //End of person details
    if (count($incident->licenses) > 0) {
        $pdf->addSubSubHeader("License Details ", $COL1X, $CURR_Y_POS);
        $pdf->Ln();
        $pdf->Ln();
        $licenseHeader = array("Person Name", "License Expiry Date", "Is License Valid?");
        $licenseData = getLicenseData($incident);
        $pdf->CreateLicenseTable($licenseHeader, $licenseData);
    }
}
///ADD PROCESS DETAILS

addProcessDetails();
if ($CURR_Y_POS > $PAGE_HEIGHT) {
    $pdf->AddPage();
    $CURR_Y_POS = $pdf->GetY() + 10;
}
///ADD IMMEDIATE ACTIONS
addImmediateContainment();

//ADD FULL INVESTIGATION DETAILS - NEW PAGE
$pdf->AddPage();
$CURR_Y_POS = $pdf->GetY() + 10;
$pdf->addSubHeader("Full Investigation", $COL1X, $CURR_Y_POS);
$CURR_Y_POS = $pdf->GetY() + 10;
$pdf->addSubSubHeader("5 Why Root Cause Analysis", $COL1X, $CURR_Y_POS);
$pdf->addLineSeperator();
$CURR_Y_POS = $pdf->GetY() + 10;
$pdf->addQuadrapleColumnLabelValue("Event Summary:", $incident->fullInvestigation->event_summary, $COL1X, $CURR_Y_POS);
$CURR_Y_POS = $pdf->GetY() + 10;
for ($i = 0; $i < count($incident->fullInvestigation->whyblocks); $i++) {
    if (($CURR_Y_POS + 100) > $PAGE_HEIGHT) {
        $pdf->AddPage();
        $CURR_Y_POS = 10;
    } else {
        $CURR_Y_POS = $pdf->GetY() + 10;
    }
    addWhyBlock($i);
    //$pdf->addLineSeperator();
    for ($k = 0; $k < 3; $k++) {
        $counterMeasure = $incident->fullInvestigation->whyblocks[$i]->counterMeasures[$k];
        if ($counterMeasure->action !== '') {
            if ($CURR_Y_POS > $PAGE_HEIGHT) {
                $pdf->AddPage();
                $CURR_Y_POS = $pdf->GetY() + 10;
            } else {
                $CURR_Y_POS = $pdf->GetY() + 15;
            }
            addCounterMeasures($counterMeasure);
        }
    }
}
$CURR_Y_POS = $pdf->GetY() + 15;

for ($i = 0; $i < count($incident->fullInvestigation->otherCounterMeasures); $i++) {
    $otherCntM = $incident->fullInvestigation->otherCounterMeasures[$i];
    if ($i === 0 && $otherCntM->action !== '') {

        $pdf->addSubSubHeader("Other Countermeasures:", $COL1X, $CURR_Y_POS);
        $pdf->addLineSeperator();
        $CURR_Y_POS = $pdf->GetY() + 10;
    }
    if ($otherCntM->action !== '') {
        $pdf->addTripleColumnLabelValue("Countermeasure:" . $otherCntM->counter_measure_number, $otherCntM->action, $COL1X, $CURR_Y_POS);
        $pdf->addSingleColumnLabelValue("Countermeasure Type:", $otherCntM->action_sub_type, $COL4X, $CURR_Y_POS);
        
        $CURR_Y_POS = $pdf->GetY() + 15;
		$pdf->addSingleColumnLabelValue("Countermeasure Site:", getSiteName($otherCntM->action_site), $COL1X, $CURR_Y_POS);
        $pdf->addSingleColumnLabelValue("Countermeasure Deadline:", date_create($otherCntM->deadline)->format('d/m/Y'), $COL2X, $CURR_Y_POS);
        $user = getUserAndManager($otherCntM->action_owner);
        $pdf->addSingleColumnLabelValue("Countermeasure Owner:", $user['name'], $COL3X, $CURR_Y_POS);

        $pdf->addSingleColumnLabelValue("Manager:", $user['manager_name'], $COL4X, $CURR_Y_POS);
        $pdf->addDashedSeperator();
        if ($CURR_Y_POS > $PAGE_HEIGHT) {
            $pdf->AddPage();
            $CURR_Y_POS = $pdf->GetY() + 10;
        } else {
            $CURR_Y_POS = $pdf->GetY() + 15;
        }
    }
}
$pdf->addLineSeperator();
if (($CURR_Y_POS + 100) > $PAGE_HEIGHT) {
    $pdf->AddPage();
    $CURR_Y_POS = $pdf->GetY() + 10;
} else {
    $CURR_Y_POS = $pdf->GetY() + 15;
}
addFIDetails();
$pdf->AddPage();
addHSDetails();
$pdf->Output();


/* * **** INDIVIDUAL FUNCTIONS START ****** */

//ADD INCIDENT DETAILS PAGE
function addIncidentDetailsPage() {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $pdf->addSubHeader("Incident Details", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 10;

    $pdf->addSingleColumnLabelValue("Site:", $incident->code, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Location:", $incident->location, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Date & Time:", $indidentDate->format("d/m/Y H:i:s"), $COL3X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Day of the week:", $indidentDate->format("l"), $COL4X, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Brief Details Of Incident:", $incident->incident_brief, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Exact Location:", $incident->location_other, $COL4X, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addDoubleColumnLabelValue($incident->question1_text, $incident->question1_answer, $COL1X, $CURR_Y_POS);
    $pdf->addDoubleColumnLabelValue($incident->question2_text, $incident->question2_answer, $COL3X - 5, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addDoubleColumnLabelValue($incident->question3_text, $incident->question3_answer, $COL1X, $CURR_Y_POS);
    $pdf->addDoubleColumnLabelValue($incident->question4_text, $incident->question4_answer, $COL3X - 5, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSingleColumnLabelValue("Incident Type:", $incident->incident_type, $COL1X, $CURR_Y_POS);

    /* Add Initial Report Details */
    $CURR_Y_POS = $pdf->GetY() + 15;
    $pdf->addSubHeader("Initial Report", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addQuadrapleColumnLabelValue("Incident Detail:", $incident->incident_detail, $COL1X, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 5;
    if ($incident->action_type_other !== '') {
        $pdf->addDoubleColumnLabelValue("Action Type:", $incident->action_type . '-' . $incident->action_type_other, $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Action Type:", $incident->action_type, $COL1X, $CURR_Y_POS);
    }
    $pdf->addSingleColumnLabelValue("Area:", $incident->area, $COL3X, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 5;
    $pdf->addSubSubHeader("Environmental Conditions", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();

    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSingleColumnLabelValue("Lighting:", $incident->lighting, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Temperature:", $incident->temperature, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Noise:", $incident->noise, $COL3X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Surface Type:", $incident->surface_type, $COL4X, $CURR_Y_POS);


    $CURR_Y_POS = $pdf->GetY() + 5;
    $pdf->addSingleColumnLabelValue("Surface Condition:", $incident->surface_condition, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 5;
}

//ADD DETAILS FOR EACH PERSON
function addPersonDetails($i) {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $pdf->addSubSubHeader("Person " . ($i + 1), $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 15;
    $pdf->addSingleColumnLabelValue("Name:", $incident->persons[$i]->person_name, $COL1X, $CURR_Y_POS);
    $age = date_diff($indidentDate, date_create($incident->persons[$i]->person_dob));
    $pdf->addSingleColumnLabelValue("DOB:", date_create($incident->persons[$i]->person_dob)->format("d/m/Y"), $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Age:", $age->format("%Y Year %M Months %d Days"), $COL3X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Employment Status:", $incident->persons[$i]->employment_status, $COL4X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 5;
    $los = "";
    $esd = "";
    if ($incident->persons[$i]->employment_start_date !== '') {
        $los = date_diff($indidentDate, date_create($incident->persons[$i]->employment_start_date))->format("%Y Year %M Months %d Days");
        $esd = date_create($incident->persons[$i]->employment_start_date)->format("d/m/Y");
    }
    $toj = "";
    $tojsd = "";
    if ($incident->persons[$i]->time_on_job_start_date !== '') {
        $toj = date_diff($indidentDate, date_create($incident->persons[$i]->time_on_job_start_date))->format("%Y Year %M Months %d Days");
        $tojsd = date_create($incident->persons[$i]->time_on_job_start_date)->format("d/m/Y");
    }
    $pdf->addSingleColumnLabelValue("Employment Start Date:", $esd, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Length Of Service:", $los, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Time on Job Start Date :", $tojsd, $COL3X, $CURR_Y_POS);

    $pdf->addSingleColumnLabelValue("Time on Job:", $toj, $COL4X, $CURR_Y_POS);

    if ($incident->persons[$i]->any_previous_incidents === '1') {
        $CURR_Y_POS = $pdf->GetY() + 5;
        $pdf->addTripleColumnLabelValue("Previous Incident Details", $incident->persons[$i]->prev_incident_date_reference, $COL1X, $CURR_Y_POS);
    }
    if ($incident->persons[$i]->is_vehicle_involved === '1') {
        $CURR_Y_POS = $pdf->GetY() + 5;
        $val = "No";
        if ($incident->persons[$i]->was_person_removed_from_truck === '1') {
            $val = "Yes";
        }
        $pdf->addDoubleColumnLabelValue("Vehicle Type:", $incident->persons[$i]->vehicle_type . '-' . $incident->persons[$i]->vehicle_type_other, $COL1X, $CURR_Y_POS);

        $pdf->addSingleColumnLabelValue("Vehicle Reg/No:", $incident->persons[$i]->vehicle_reg_no, $COL3X, $CURR_Y_POS);
        $pdf->addSingleColumnLabelValue("Person removed from Truck?", $val, $COL4X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 5;
    if ($incident->persons[$i]->location_of_injury === '') {
        $pdf->addTripleColumnLabelValue("Locaion Of Injury:", "None", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addTripleColumnLabelValue("Locaion Of Injury:", $incident->persons[$i]->location_of_injury, $COL1X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 5;
    if ($incident->persons[$i]->treatment_administered === '') {
        $pdf->addSingleColumnLabelValue("Treatment Administered:", "None", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Treatment Administered:", $incident->persons[$i]->treatment_administered, $COL1X, $CURR_Y_POS);
    }
    if ($incident->persons[$i]->treatment_details === '') {
        $pdf->addSingleColumnLabelValue("Treatment Details:", "None", $COL2X, $CURR_Y_POS);
    } else {
        $pdf->addTripleColumnLabelValue("Treatment Details:", $incident->persons[$i]->treatment_details, $COL2X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 5;
    if ($incident->persons[$i]->ppe_worn === '') {
        $pdf->addDoubleColumnLabelValue("PPE Worn:", "N/A", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addDoubleColumnLabelValue("PPE Worn:", $incident->persons[$i]->ppe_worn, $COL1X, $CURR_Y_POS);
    }
    if ($incident->persons[$i]->was_ppe_req_level === '1') {
        $pdf->addSingleColumnLabelValue("Was PPE of required level?", "Yes", $COL3X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Was PPE of required level?:", "No", $COL3X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 10;
    if ($incident->persons[$i]->da_test_required === '0') {
        $pdf->addSingleColumnLabelValue("D&A Test Required:", "No", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("D&A Test Required:", "Yes", $COL1X, $CURR_Y_POS);
    }
    if ($incident->persons[$i]->da_test_not_req_reason === '') {
        $pdf->addSingleColumnLabelValue("Why D&A Test not required?", "N/A", $COL2X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Why D&A Test not required?", $incident->persons[$i]->da_test_not_req_reason, $COL2X, $CURR_Y_POS);
    }

    if ($incident->persons[$i]->da_test_agreed === '0' && $incident->persons[$i]->da_test_required === '1') {
        $pdf->addSingleColumnLabelValue("D&A Test agreed?", "No", $COL3X, $CURR_Y_POS);
    } else if ($incident->persons[$i]->da_test_agreed === '0' && $incident->persons[$i]->da_test_required === '0') {
        $pdf->addSingleColumnLabelValue("D&A Test agreed?", "N/A", $COL3X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("D&A Test agreed?", "Yes", $COL3X, $CURR_Y_POS);
    }
    if ($incident->persons[$i]->da_passed === '0' && $incident->persons[$i]->da_test_required === '1') {
        $pdf->addSingleColumnLabelValue("D&A Test passed?", "No", $COL4X, $CURR_Y_POS);
    } else if ($incident->persons[$i]->da_passed === '0' && $incident->persons[$i]->da_test_required === '0') {
        $pdf->addSingleColumnLabelValue("D&A Test passed?", "N/A", $COL4X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("D&A Test passed?", "Yes", $COL4X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 5;
    if ($incident->persons[$i]->da_tested_by === '-1') {
        $pdf->addSingleColumnLabelValue("D&A Test by?", "N/A", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("D&A Tested by?", $incident->persons[$i]->da_tested_by, $COL1X, $CURR_Y_POS);
    }
    $CURR_Y_POS = $pdf->GetY() + 5;
}

//ADD PROCESS DETAILS ON INCIDENT DEYAIL PAGE
function addProcessDetails() {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSubSubHeader("Process Details ", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 15;
    if ($incident->is_activity_standard_process === '1') {
        $pdf->addSingleColumnLabelValue("Was activity a standard process?", "Yes", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Was activity a standard process?", "No", $COL1X, $CURR_Y_POS);
    }

    $pdf->addSingleColumnLabelValue("Pack Type:", $incident->stillage_type, $COL3X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Estimated Cost:", $incident->estimated_cost, $COL4X, $CURR_Y_POS);

    $CURR_Y_POS = $pdf->GetY() + 5;
    $pdf->addQuadrapleColumnLabelValue("Immediate Cause:", $incident->immediate_cause, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 5;
    $pdf->addQuadrapleColumnLabelValue("Key Points:", $incident->key_points, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 5;
    $pdf->addQuadrapleColumnLabelValue("Main Message:", $incident->main_message, $COL1X, $CURR_Y_POS);
}

//ADD IMMEDIATE ACTIONS
function addImmediateContainment() {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSubSubHeader("Immediate Containment Details:", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 10;
    for ($i = 0; $i < count($incident->actions); $i++) {
        $action = $incident->actions[$i];
        $pdf->addTripleColumnLabelValue("Action-" . ($i + 1), $action->action, $COL1X, $CURR_Y_POS);
        $CURR_Y_POS = $pdf->GetY() + 5;
        $pdf->addSingleColumnLabelValue("Site:", getSiteName($action->action_site), $COL1X, $CURR_Y_POS);
        $user = getUserAndManager($action->action_owner);
        $pdf->addSingleColumnLabelValue("Action:", $user['name'], $COL2X, $CURR_Y_POS);
        $pdf->addSingleColumnLabelValue("Action:", $user['manager_name'], $COL3X, $CURR_Y_POS);

        $pdf->addDashedSeperator();
        $CURR_Y_POS = $pdf->GetY() + 15;
    }
}

//ADD WHY BLOCK
function addWhyBlock($i) {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $pdf->addSubSubHeader("5 Whys Block " . ($i + 1), $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Why 1:", $incident->fullInvestigation->whyblocks[$i]->why_1, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Why 2:", $incident->fullInvestigation->whyblocks[$i]->why_2, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Why 3:", $incident->fullInvestigation->whyblocks[$i]->why_3, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Why 4:", $incident->fullInvestigation->whyblocks[$i]->why_4, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Why 5:", $incident->fullInvestigation->whyblocks[$i]->why_5, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTitle("Root Causes & Countermeasures:", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 15;
    $pdf->addTripleColumnLabelValue("Root Cause (A):", $incident->fullInvestigation->whyblocks[$i]->root_cause_a, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Root Cause (B):", $incident->fullInvestigation->whyblocks[$i]->root_cause_b, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addTripleColumnLabelValue("Root Cause (C):", $incident->fullInvestigation->whyblocks[$i]->root_cause_c, $COL1X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
}

//ADD COUNTER MEASURES 
function addCounterMeasures($counterMeasure) {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $pdf->addTripleColumnLabelValue("Countermeasure:" . $counterMeasure->counter_measure_number, $counterMeasure->action, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Countermeasure Type:", $counterMeasure->action_sub_type, $COL4X, $CURR_Y_POS);
    
    $CURR_Y_POS = $pdf->GetY() + 15;
	$pdf->addSingleColumnLabelValue("Countermeasure Site:", getSiteName($counterMeasure->action_site), $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Countermeasure Deadline:", date_create($counterMeasure->deadline)->format('d/m/Y'), $COL2X, $CURR_Y_POS);
    $user = getUserAndManager($counterMeasure->action_owner);
    $pdf->addSingleColumnLabelValue("Countermeasure Owner:", $user['name'], $COL3X, $CURR_Y_POS);

    $pdf->addSingleColumnLabelValue("Manager:", $user['manager_name'], $COL4X, $CURR_Y_POS);
    $pdf->addDashedSeperator();
    $CURR_Y_POS = $pdf->GetY() + 5;
}

//ADD FULL INVESTIGATION DETAILS
function addFIDetails() {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    if ($incident->fullInvestigation->is_ssow_suitable === '1') {
        $pdf->addDoubleColumnLabelValue("SSoW Suitable & Sufficient?", "Yes", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addDoubleColumnLabelValue("SSoW Suitable & Sufficient?", "No", $COL1X, $CURR_Y_POS);
    }
    $pdf->addTripleColumnLabelValue("Comments on SSoW", $incident->fullInvestigation->ssow_comments, $COL2X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    if ($incident->fullInvestigation->is_ra_suitable === '1') {
        $pdf->addDoubleColumnLabelValue("RA Suitable & Sufficient?", "Yes", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addDoubleColumnLabelValue("RA Suitable & Sufficient?", "No", $COL1X, $CURR_Y_POS);
    }
    $pdf->addTripleColumnLabelValue("Comments on RA", $incident->fullInvestigation->ra_comments, $COL2X, $CURR_Y_POS);
    $CURR_Y_POS = $pdf->GetY() + 10;
    if ($incident->fullInvestigation->is_mhe_training_required === '1') {
        $pdf->addDoubleColumnLabelValue("Refresher MHE training required?", "Yes", $COL1X, $CURR_Y_POS);
    } else {
        $pdf->addDoubleColumnLabelValue("Refresher MHE training required?", "No", $COL1X, $CURR_Y_POS);
    }
    $pdf->addLineSeperator();
    $CURR_Y_POS+=15;
    $pdf->addSubSubHeader("Person", $COL1X, $CURR_Y_POS);
    $CURR_Y_POS+=5;
    $pdf->addSingleColumnLabelValue("Concentration:", $incident->fullInvestigation->person_concentraion, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Experience:", $incident->fullInvestigation->person_experience, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Training:", $incident->fullInvestigation->person_training, $COL3X, $CURR_Y_POS);
    $CURR_Y_POS+=15;
    $pdf->addSubSubHeader("Machine", $COL1X, $CURR_Y_POS);
    $CURR_Y_POS+=5;
    $pdf->addSingleColumnLabelValue("Condition:", $incident->fullInvestigation->machine_condition, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Maintenance:", $incident->fullInvestigation->machine_maintenance, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Facility:", $incident->fullInvestigation->machine_facility, $COL3X, $CURR_Y_POS);
    $CURR_Y_POS+=15;
    $pdf->addSubSubHeader("Method", $COL1X, $CURR_Y_POS);
    $CURR_Y_POS+=5;
    $pdf->addSingleColumnLabelValue("SSoW:", $incident->fullInvestigation->method_ssow, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Ruling:", $incident->fullInvestigation->method_ruling, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Risk Assessment:", $incident->fullInvestigation->method_risk_assessment, $COL3X, $CURR_Y_POS);
    $CURR_Y_POS+=15;
    $pdf->addSubSubHeader("Environment", $COL1X, $CURR_Y_POS);
    $CURR_Y_POS+=5;
    $pdf->addSingleColumnLabelValue("Floor:", $incident->fullInvestigation->environment_floor, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Lighting:", $incident->fullInvestigation->environment_lighting, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Condition:", $incident->fullInvestigation->environment_condition, $COL3X, $CURR_Y_POS);

    $pdf->addLineSeperator();
}

//GET LICENSE DATA IN ARRAY
function getLicenseData($incident) {
    $licenseData = array();
    for ($row = 0; $row < count($incident->licenses); $row++) {
        for ($col = 0; $col < 3; $col++) {
            if ($col == 0) {
                $licenseData[$row][$col] = $incident->licenses[$row]->person_name;
            } else if ($col == 1) {
                $licenseData[$row][$col] = date_create($incident->licenses[$row]->expiry_date)->format("d/m/Y");
            } else if ($col == 2) {
                if ($incident->licenses[$row]->is_valid === '1') {
                    $licenseData[$row][$col] = "Yes";
                } else {
                    $licenseData[$row][$col] = "No";
                }
            }
        }
    }
    return $licenseData;
}

//ADD H&S SUMMARY DETAILS
function addHSDetails() {
    global $pdf, $CURR_Y_POS, $COL1X, $COL2X, $COL3X, $COL4X, $incident, $indidentDate;
    $CURR_Y_POS = $pdf->GetY() + 10;
    $pdf->addSubHeader("H&S Sign Off Details", $COL1X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS+=10;
    $pdf->addSingleColumnLabelValue("Category:", $incident->category, $COL1X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Category Level:", $incident->category_level, $COL2X, $CURR_Y_POS);
    $pdf->addSingleColumnLabelValue("Type:", $incident->type, $COL3X, $CURR_Y_POS);
    if ($incident->is_dangerous_occurence === '1') {
        $pdf->addSingleColumnLabelValue("Dangerous Occurrence:", "Yes", $COL4X, $CURR_Y_POS);
    } else {
        $pdf->addSingleColumnLabelValue("Dangerous Occurrence:", "No", $COL4X, $CURR_Y_POS);
    }
    $CURR_Y_POS+=15;
    $pdf->addSingleColumnLabelValue("Act or Condition:", $incident->act_or_condition, $COL1X, $CURR_Y_POS);
    $pdf->addTripleColumnLabelValue("H&S Summary:", $incident->hs_summary, $COL2X, $CURR_Y_POS);
    $pdf->addLineSeperator();
    $CURR_Y_POS = $pdf->GetY() + 10;
    if (count($incident->injuredPersons) > 0 && $incident->injuredPersons[0]->injured_person_name !== '-1') {
        $pdf->addSubSubHeader("Person(s) Involved", $COL1X, $CURR_Y_POS);
        $CURR_Y_POS+=10;
        for ($i = 0; $i < count($incident->injuredPersons); $i++) {
            $person = $incident->injuredPersons[$i];

            $pdf->addSingleColumnLabelValue("Name:", $person->injured_person_name, $COL1X, $CURR_Y_POS);

            $pdf->addSingleColumnLabelValue("Days Lost:", $person->days_lost, $COL2X, $CURR_Y_POS);
            if ($person->isRIDDOR === '1') {
                $pdf->addSingleColumnLabelValue("RIDDOR:", "Yes", $COL3X, $CURR_Y_POS);
            } else {
                $pdf->addSingleColumnLabelValue("RIDDOR:", "No", $COL3X, $CURR_Y_POS);
            }

            $pdf->addSingleColumnLabelValue("Riddor Reference:", $person->riddor_reference, $COL4X, $CURR_Y_POS);
            $CURR_Y_POS+=15;
            $hseDate = '';
            if ($person->hse_reported_date !== '') {
                $hseDate = date_create($person->hse_reported_date)->format('d/m/Y');
            }
            $pdf->addSingleColumnLabelValue("Date reported to HSE:", $hseDate, $COL1X, $CURR_Y_POS);
            $pdf->addTripleColumnLabelValue("Further details of Treatment:", $person->further_details_and_treatment, $COL2X, $CURR_Y_POS);
            $CURR_Y_POS = $pdf->GetY() + 10;
        }
    }
    $pdf->addLineSeperator();
}

//GET THE SITE NAME
function getSiteName($siteId) {
    global $mDbName, $con;
    $sql = 'SELECT code FROM ' . $mDbName . '.site where id ="' . $siteId . '";';
    $result = mysqli_query($con, $sql);

    while ($row = mysqli_fetch_array($result)) {
        $code = $row['code'];
    }
    return $code;
}

//GET USER NAME AND MANAGER
function getUserAndManager($userId) {
    global $mDbName, $con;
    $sql = "SELECT concat(first_name,' ', last_name) as name, manager_name FROM " . $mDbName . ".users where id =" . $userId;
    $result = mysqli_query($con, $sql);
    $user = array();
    while ($row = mysqli_fetch_array($result)) {
        $user[] = $row;
    }
    ChromePhp::log($user);
    return $user[0];
}

?>