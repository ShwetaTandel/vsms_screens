
<div class="row">
    <div class="col-md-3 col-lg-12" >
        <div class="controls">
            <h4>5 Why Root Cause Analysis</h4>
            <br/>
            <label class="control-label">Event Summary<span style="color: red">*</span></label>
            <textarea rows="3" class="form-control" id="eventSummary" maxlength="1000" ></textarea>
            <input type="hidden" id="fiId" value="0"/>
        </div>
    </div>
</div>
<div class="row">
    <div id ="eventSummaryValidation" class="showError alert alert-danger" style="display: none"><strong>Please enter Event Summary.</Strong></div>
</div>
<br/>
<div id='block5Why1' class="container-fluid clonedWhy">
    <input type="hidden" value="0" id="whyid_1" class="whyid"/>
    <hr style="height:2px;border:none;color:#333;background-color:#333;" />
    <h4 id="whyTitle1" name="whyTitle1" class="whyTitle">5 Whys Block 1 (Why did this happen?)</h4>


    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label">Why 1<span style="color: red">*</span></label>
                <input type ="text" class="form-control why1" id="why1_1" name="why1_1"  maxlength="250" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label">Why 2<span style="color: red">*</span></label>
                <input type ="text" class="form-control why2" id="why2_1" name="why2_1" maxlength="250" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label">Why 3<span style="color: red">*</span></label>
                <input type ="text" class="form-control why3" id="why3_1" name="why3_1" maxlength="250" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label">Why 4</label>
                <input type ="text" class="form-control why4" id="why4_1" name="why4_1" maxlength="250" />
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label">Why 5</label>
                <input type ="text" class="form-control why5" name="why5_1" id="why5_1" maxlength="250" />
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-md-3 col-lg-12" >
            <div class="controls">
                <label class="control-label" style="font-weight: bold;font-size: large">Root Causes and Countermeasures</label>
            </div></div></div>
    <br/>

    <div class="row">
        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label">Root Cause (a)<span style="color: red">*</span></label>
                <input type ="text" class="form-control rootCauseA" name="rootCauseA_1" id="rootCauseA_1" maxlength="250" />
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label">Root Cause (b)</label>
                <input type ="text" class="form-control rootCauseB" name="rootCauseB_1" id="rootCauseB_1" maxlength="250" />
            </div>
        </div>
        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label">Root Cause (c)</label>
                <input type ="text" class="form-control rootCauseC" id="rootCauseC_1" name="rootCauseC_1" maxlength="250" />
            </div>
        </div>
    </div>
    <div class="row">
        <div id ="whyBlockValidation_1" class="showError alert alert-danger whyBlockValidation" style="display: none"><strong>Please enter all fields marked required above.</Strong></div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row" >

        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label counterMeasureAIdLabel" id="counterMeasureAIdLabel_1" style="font-weight: bold">Action ID:</label>
                <input type="hidden" value="0" id="counterMeasureAId_1" class ="counterMeasureAId"/>
            </div>
            <div class="controls">
                <label class="control-label">Countermeasure (a)<span style="color: red">*</span></label>
                <textarea rows ="3" class="form-control counterMeasureA" id="counterMeasureA_1" maxlength="250" ></textarea>
            </div>
        </div>
        <div class="col-md-3 col-lg-8" >
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Type<span style="color: red">*</span></label>
                    <select class="custom-select counterMeasureTypeA" id="counterMeasureTypeA_1">
                        <option value="-1"></option>
                        <option value="Behaviour">Behaviour</option>
                        <option value="Condition">Condition</option>
                        <option value="Equipment">Equipment</option>
                        <option value="Training">Training</option>
                        <option value="Procedure">Procedure</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Deadline<span style="color: red">*</span></label>
                    <input type="date" id="counterMeasureDeadlineA_1" name="counterMeasureDeadlineA_1" class="form-control counterMeasureDeadlineA"/>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Status</label>
                    <input type="text" id="counterMeasureStatusA_1" name="counterMeasureStatusA_1" class="form-control counterMeasureStatusA" disabled="true"/>

                </div>

            </div>
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Site<span style="color: red">*</span></label>
                    <select class="custom-select counterMeasureSiteA" id="counterMeasureSiteA_1" onchange="populateOwnersForCounterMeasure($(this))">
                        <?php
                        include ('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                        echo "<option value='-1'></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                        }
                        echo '';
                        mysqli_close($con);
                        ?>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Owner<span style="color: red">*</span></label>
                    <select class="custom-select counterMeasureOwnerA" id="counterMeasureOwnerA_1" onchange="populateCounterMeasureManager(this)">
                        <option value='-1'></option>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Manager<span style="color: red">*</span></label>
                    <input type="text" id='counterMeasureManagerA_1' class="form-control counterMeasureManagerA" readonly="true"/>

                </div>

            </div>
        </div>

    </div>
    <div class="row">
        <div id ="counterMeasureValidationA_1" class="counterMeasureValidationA showError alert alert-danger" style="display: none"><strong>Please enter all fields in Countermeasure A.</Strong></div>
    </div>
    <br/>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">

        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label counterMeasureBIdLabel" id="counterMeasureBIdLabel_1" style="font-weight: bold">Action ID:</label>
                <input type="hidden" value="0" id="counterMeasureBId_1" class ="counterMeasureBId"/>
            </div>
            <div class="controls">
                <label class="control-label">Countermeasure (b)</label>
                <textarea rows ="3" class="form-control counterMeasureB" id="counterMeasureB_1" maxlength="250" ></textarea>
            </div>
        </div>
        <div class="col-md-3 col-lg-8" >
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Type</label>
                    <select class="custom-select counterMeasureTypeB" id="counterMeasureTypeB_1">
                        <option value="-1"></option>
                        <option value="Behaviour">Behaviour</option>
                        <option value="Condition">Condition</option>
                        <option value="Equipment">Equipment</option>
                        <option value="Training">Training</option>
                        <option value="Procedure">Procedure</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Deadline</label>
                    <input type="date" id='counterMeasureDeadlineB_1' name="counterMeasureDeadlineB_1" class="form-control counterMeasureDeadlineB"/>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Status</label>
                    <input type="text" id="counterMeasureStatusB_1" name="counterMeasureStatusB_1" class="form-control counterMeasureStatusB" disabled="true"/>

                </div>

            </div>
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Site</label>
                    <select class="custom-select counterMeasureSiteB" id="counterMeasureSiteB_1" onchange="populateOwnersForCounterMeasure($(this))">
                        <?php
                        include ('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                        echo "<option value='-1'></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                        }
                        echo '';
                        mysqli_close($con);
                        ?>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Owner</label>
                    <select class="custom-select counterMeasureOwnerB" id="counterMeasureOwnerB_1" onchange="populateCounterMeasureManager(this)">
                        <option value="-1"></option>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Manager</label>
                    <input type="text" id='counterMeasureManagerB_1' class="form-control counterMeasureManagerB" readonly="true"/>

                </div>

            </div>
        </div>

    </div>
    <div class="row">
        <div id ="counterMeasureValidationB_1" class="counterMeasureValidationB showError alert alert-danger" style="display: none"><strong>Please enter/clear all fields in Countermeasure B.</Strong></div>
    </div>

    <br/>
    <hr style="border-top: dotted;color: gainsboro" />
    <div class="row">
        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label counterMeasureCIdLabel" id="counterMeasureCIdLabel_1" style="font-weight: bold">Action ID:</label>
                <input type="hidden" value="0" id="counterMeasureCId_1" class ="counterMeasureCId"/>
            </div>
            <div class="controls">
                <label class="control-label">Countermeasure (c)</label>
                <textarea rows ="3" class="form-control counterMeasureC" id="counterMeasureC_1" maxlength="250" ></textarea>
            </div>
        </div>
        <div class="col-md-3 col-lg-8" >
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Type</label>
                    <select class="custom-select counterMeasureTypeC" id="counterMeasureTypeC_1">
                        <option value="-1"></option>
                        <option value="Behaviour">Behaviour</option>
                        <option value="Condition">Condition</option>
                        <option value="Equipment">Equipment</option>
                        <option value="Training">Training</option>
                        <option value="Procedure">Procedure</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Deadline</label>
                    <input type="date" id='counterMeasureDeadlineC_1' name="counterMeasureDeadlineC_1" class="form-control counterMeasureDeadlineC"/>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Status</label>
                    <input type="text" id="counterMeasureStatusC_1" name="counterMeasureStatusC_1" class="form-control counterMeasureStatusC" disabled="true"/>

                </div>

            </div>

            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Site</label>
                    <select class="custom-select counterMeasureSiteC" id="counterMeasureSiteC_1" onchange="populateOwnersForCounterMeasure($(this))">
                        <?php
                        include ('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                        echo "<option value='-1'></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                        }
                        echo '';
                        mysqli_close($con);
                        ?>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Owner</label>
                    <select class="custom-select counterMeasureOwnerC" id="counterMeasureOwnerC_1" onchange="populateCounterMeasureManager(this)">
                        <option value="-1"></option>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Manager</label>
                    <input type="text" id='counterMeasureManagerC_1' class="form-control counterMeasureManagerC" readonly="true"/>

                </div>

            </div>


        </div>


    </div>

    <div class="row">
        <div id ="counterMeasureValidationC_1" class="counterMeasureValidationC showError alert alert-danger" style="display: none"><strong>Please enter/clear all fields in Countermeasure C.</Strong></div>
    </div>
    <div class="row">
        <div id ="counterMeasureDateValidation_1" class="counterMeasureDateValidation showError alert alert-danger" style="display: none"><strong>Please enter correct dates.</Strong></div>
    </div>

</div>
<br/>
<div class="row" id="whyButtons" >
    <div class="col-md-12">
        <label class="control-label" style="font-size: large;font-weight: bold">Add another block of 5 WHYs</label>
        <div>
            <input type="button" class="btn btn-blue" id="btnAddWhy" value="+">
            <input type="button" class="btn btn-red" id="btnDelWhy" value="-" disabled="true">
        </div>

    </div> 
</div>

<hr style="height:2px;border:none;color:#333;background-color:#333;" />

<div class="row">
    <div class="col-md-3 col-lg-12" >
        <div class="controls">
            <h4 class="control-label">Other Countermeasures</h4>
        </div></div></div>
<br/>
<div class="clonedCounterMeasure" id="otherCounterMeasureDiv1" >
    <div class="row" >

        <div class="col-md-3 col-lg-4" >
            <div class="controls">
                <label class="control-label otherCounterMeasureIdLabel" id="otherCounterMeasureIdLabel1" style="font-weight: bold">Action ID:</label>
                <input type="hidden" value="0" id = "otherCounterMeasureId1" class = "otherCounterMeasureId"/>
            </div>
            <div class="controls">
                <label class="control-label">Other Countermeasures </label>
                <textarea rows ="3" class="form-control otherCounterMeasure" id="otherCounterMeasure1" maxlength="250" ></textarea>
            </div>
        </div>
        <div class="col-md-3 col-lg-8" >
            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Type</label>
                    <select class="custom-select otherCounterMeasureType" id="otherCounterMeasureType1">
                        <option value="-1"></option>
                        <option value="Behaviour">Behaviour</option>
                        <option value="Condition">Condition</option>
                        <option value="Equipment">Equipment</option>
                        <option value="Training">Training</option>
                        <option value="Procedure">Procedure</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Deadline</label>
                    <input type="date" id="otherCounterMeasureDeadline1" name="otherCounterMeasureDeadline1" class="form-control otherCounterMeasureDeadline"/>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Countermeasure Status</label>
                    <input type="text" id="otherCounterMeasureStatus1" name="otherCounterMeasureStatus" class="form-control otherCounterMeasureStatus" disabled="true"/>

                </div>

            </div>

            <div  class="row">
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Site</label>
                    <select class="custom-select otherCounterMeasureSite" id="otherCounterMeasureSite1" onchange="populateOwnersForCounterMeasure($(this))">
                        <?php
                        include ('../config/phpConfig.php');
                        if (mysqli_connect_errno()) {
                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                        }
                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                        echo "<option value='-1'></option>";
                        while ($row = mysqli_fetch_array($result)) {
                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                        }
                        echo '';
                        mysqli_close($con);
                        ?>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Select Owner</label>
                    <select class="custom-select otherCounterMeasureOwner" id="otherCounterMeasureOwner1" onchange="populateCounterMeasureManager(this)">
                        <option value="-1"></option>
                    </select>

                </div>
                <div class="col-md-3 col-lg-4">
                    <label class="control-label">Manager</label>
                    <input type="text" id='otherCounterMeasureManager1' class="form-control otherCounterMeasureManager" readonly="true"/>

                </div>
            </div>
        </div>
        <div class="row">
            <div id ="otherCounterMeasureValidation1" class="otherCounterMeasureValidation showError alert alert-danger" style="display: none"><strong>Please enter/clear all fields in Other Countermeasure added.</Strong></div>
        </div>
    </div>
    <hr style="border-top: dotted;color: gainsboro" />
    <br/>
</div>
<div class="row">
    <div id ="otherCounterMeasureDateValidation" class="otherCounterMeasureDateValidation showError alert alert-danger" style="display: none"><strong>Please enter correct dates.</Strong></div>
</div>

<br/>
<div class="row" id="counterButtons" >
    <div class="col-md-12">

        <div>
            <input type="button" class="btn btn-blue" id="btnAddCounter" value="+">
            <input type="button" class="btn btn-red pull-right" id="btnDelCounter" value="-" disabled="true">
        </div>

    </div> 
</div>
<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<div class="row">
    <div class="col-md-3 col-lg-4">

        <label class="control-label">SSoW Suitable and Sufficient <span style="color: red">*</span></label>
        <select class="custom-select" id="ssowSuitable">
            <option value="-1"></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
    <div class="col-md-3 col-lg-8">
        <label class="control-label">Comments on SSoW</label>
        <textarea rows="2" id='commentsSSOW' class="form-control" maxlength="1000"></textarea>
    </div>

</div>
<div class="row">
    <div class="col-md-3 col-lg-4">

        <label class="control-label">RA Suitable and Sufficient<span style="color: red">*</span></label>
        <select class="custom-select" id="raSuitable" >
            <option value="-1"></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
    <div class="col-md-3 col-lg-8">
        <label class="control-label">Comments on RA</label>
        <textarea rows="2" id='commentsRA' class="form-control" maxlength="1000"></textarea>
    </div>
</div>
<div class="row">
    <div id ="commentsValidation" class="showError alert alert-danger" style="display: none"><strong>Please select/enter all the required fields.</Strong></div>
</div>
<div class="row">
    <div id ="commentsRequiredValidation" class="showError alert alert-danger" style="display: none"><strong>Please add Comments on SSoW / RA as to why they are insufficient.</Strong></div>
</div>

<div class="row">
    <div class="col-md-3 col-lg-4">
        <br/><br/>
        <label class="control-label">Refresher MHE training required?</label>
        <select class="custom-select" id="mheTrainingReq">
            <option value="-1"></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>

    </div>

</div>
<div class="row">
    <div id ="mheReqValidation" class="showError alert alert-danger" style="display: none"><strong>Please select whether a refresher MHE training is required, since a vehicle was involved.</Strong></div>
</div>

<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<h4>Person</h4>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Concentration<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="personConcentrationVal1" name="personConcentration" value="Prime Cause">
            <label class="custom-control-label" for="personConcentrationVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personConcentrationVal2" name="personConcentration" value="Major Cause"  >
            <label class="custom-control-label" for="personConcentrationVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personConcentrationVal3" name="personConcentration" value="Minor Cause"  >
            <label class="custom-control-label" for="personConcentrationVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personConcentrationVal4" name="personConcentration" value="No Effect"  >
            <label class="custom-control-label" for="personConcentrationVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Experience<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="personExperienceVal1" name="personExperience" value="Prime Cause">
            <label class="custom-control-label" for="personExperienceVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personExperienceVal2" name="personExperience" value="Major Cause"  >
            <label class="custom-control-label" for="personExperienceVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personExperienceVal3" name="personExperience" value="Minor Cause"  >
            <label class="custom-control-label" for="personExperienceVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personExperienceVal4" name="personExperience" value="No Effect"  >
            <label class="custom-control-label" for="personExperienceVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Training<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="personTrainingVal1" name="personTraining" value="Prime Cause">
            <label class="custom-control-label" for="personTrainingVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personTrainingVal2" name="personTraining" value="Major Cause"  >
            <label class="custom-control-label" for="personTrainingVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personTrainingVal3" name="personTraining" value="Minor Cause"  >
            <label class="custom-control-label" for="personTrainingVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="personTrainingVal4" name="personTraining" value="No Effect"  >
            <label class="custom-control-label" for="personTrainingVal4">No Effect</label>
        </div> 
    </div>


</div>
<h4>Machine</h4>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Condition<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="machineConditionVal1" name="machineCondition" value="Prime Cause">
            <label class="custom-control-label" for="machineConditionVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineConditionVal2" name="machineCondition" value="Major Cause"  >
            <label class="custom-control-label" for="machineConditionVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineConditionVal3" name="machineCondition" value="Minor Cause"  >
            <label class="custom-control-label" for="machineConditionVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineConditionVal4" name="machineCondition" value="No Effect"  >
            <label class="custom-control-label" for="machineConditionVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Maintenance<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="machineMaintenanceVal1" name="machineMaintenance" value="Prime Cause">
            <label class="custom-control-label" for="machineMaintenanceVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineMaintenanceVal2" name="machineMaintenance" value="Major Cause"  >
            <label class="custom-control-label" for="machineMaintenanceVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineMaintenanceVal3" name="machineMaintenance" value="Minor Cause"  >
            <label class="custom-control-label" for="machineMaintenanceVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineMaintenanceVal4" name="machineMaintenance" value="No Effect"  >
            <label class="custom-control-label" for="machineMaintenanceVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Facility<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="machineFacilityVal1" name="machineFacility" value="Prime Cause">
            <label class="custom-control-label" for="machineFacilityVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineFacilityVal2" name="machineFacility" value="Major Cause"  >
            <label class="custom-control-label" for="machineFacilityVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineFacilityVal3" name="machineFacility" value="Minor Cause"  >
            <label class="custom-control-label" for="machineFacilityVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="machineFacilityVal4" name="machineFacility" value="No Effect"  >
            <label class="custom-control-label" for="machineFacilityVal4">No Effect</label>
        </div> 
    </div>


</div>
<h4>Method</h4>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">SSoW<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="methodSSOWVal1" name="methodSSOW" value="Prime Cause">
            <label class="custom-control-label" for="methodSSOWVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodSSOWVal2" name="methodSSOW" value="Major Cause"  >
            <label class="custom-control-label" for="methodSSOWVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodSSOWVal3" name="methodSSOW" value="Minor Cause"  >
            <label class="custom-control-label" for="methodSSOWVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodSSOWVal4" name="methodSSOW" value="No Effect"  >
            <label class="custom-control-label" for="methodSSOWVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Ruling<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="methodRulingVal1" name="methodRuling" value="Prime Cause">
            <label class="custom-control-label" for="methodRulingVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRulingVal2" name="methodRuling" value="Major Cause"  >
            <label class="custom-control-label" for="methodRulingVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRulingVal3" name="methodRuling" value="Minor Cause"  >
            <label class="custom-control-label" for="methodRulingVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRulingVal4" name="methodRuling" value="No Effect"  >
            <label class="custom-control-label" for="methodRulingVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Risk Assessment<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="methodRiskAssessmentVal1" name="methodRiskAssessment" value="Prime Cause">
            <label class="custom-control-label" for="methodRiskAssessmentVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRiskAssessmentVal2" name="methodRiskAssessment" value="Major Cause"  >
            <label class="custom-control-label" for="methodRiskAssessmentVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRiskAssessmentVal3" name="methodRiskAssessment" value="Minor Cause"  >
            <label class="custom-control-label" for="methodRiskAssessmentVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="methodRiskAssessmentVal4" name="methodRiskAssessment" value="No Effect"  >
            <label class="custom-control-label" for="methodRiskAssessmentVal4">No Effect</label>
        </div> 
    </div>


</div>

<h4>Environment</h4>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Floor<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="environmentFloorVal1" name="environmentFloor" value="Prime Cause">
            <label class="custom-control-label" for="environmentFloorVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentFloorVal2" name="environmentFloor" value="Major Cause"  >
            <label class="custom-control-label" for="environmentFloorVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentFloorVal3" name="environmentFloor" value="Minor Cause"  >
            <label class="custom-control-label" for="environmentFloorVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentFloorVal4" name="environmentFloor" value="No Effect"  >
            <label class="custom-control-label" for="environmentFloorVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Lighting<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="environmentLightingVal1" name="environmentLighting" value="Prime Cause">
            <label class="custom-control-label" for="environmentLightingVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentLightingVal2" name="environmentLighting" value="Major Cause"  >
            <label class="custom-control-label" for="environmentLightingVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentLightingVal3" name="environmentLighting" value="Minor Cause"  >
            <label class="custom-control-label" for="environmentLightingVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentLightingVal4" name="environmentLighting" value="No Effect"  >
            <label class="custom-control-label" for="environmentLightingVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div class="col-md-3 form-group"><label class="control-label">Condition<span style="color: red">*</span></label></div>

    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id ="environmentConditionVal1" name="environmentCondition" value="Prime Cause">
            <label class="custom-control-label" for="environmentConditionVal1">Prime Cause</label>
        </div>
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentConditionVal2" name="environmentCondition" value="Major Cause"  >
            <label class="custom-control-label" for="environmentConditionVal2">Major Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentConditionVal3" name="environmentCondition" value="Minor Cause"  >
            <label class="custom-control-label" for="environmentConditionVal3">Minor Cause</label>
        </div> 
    </div>
    <div class="col-md-2 form-group">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" class="custom-control-input" id="environmentConditionVal4" name="environmentCondition" value="No Effect"  >
            <label class="custom-control-label" for="environmentConditionVal4">No Effect</label>
        </div> 
    </div>


</div>
<div class="row">
    <div id ="radioButtonsValidation" class="showError alert alert-danger" style="display: none"><strong>Please select at least one value for all above fields</Strong></div>
</div>
<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<h4>Vantec Way - Decision Tree</h4>
<div class="row">
    <div class="col-md-3 col-lg-3">
                <br/>
                <label class="btn btn-secondary">Is it Ill Health? 
                    <input type="checkbox" id="isIllHealth" class="badgeboxneutral" onclick="enableVantecWay(this)"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
</div>
<div class="row" id="vantecWayDiv">
     <div class="col-md-3 col-lg-3">
        <label class="control-label">Vantec Way - Decision Tree<span style="color: red">*</span></label>
        <input type="text" id='vantecWayDecision' class="form-control" readonly="true" onclick=" ask(question);"/>
       
    </div>
        <div class="col-md-3 col-lg-3">
        <label class="control-label">Comments<span style="color: red">*</span></label>
        <input type="text" id='vantecWayComments' class="form-control"/>
       
    </div>

    <div class="col-md-3 col-lg-6"> <span id="decisionTree" style="white-space: pre-wrap"></span></div>

</div>
<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<h4>Investigation Team</h4>
<div class="row">
    <div class="col-md-3 col-lg-6">
        <label class="control-label">Names & Positions<span style="color: red">*</span></label>
        <input type="text" id='investigationTeamNames' class="form-control" maxlength="1500"/>
       
    </div>
     
</div>
<div class="row">
    <div id ="vantecWayValidation" class="showError alert alert-danger" style="display: none"><strong>Please select a value for Vantec Way Decision and/or add comments.</Strong></div>
</div>
<div class="row">
    <div id ="investigationTeamValidation" class="showError alert alert-danger" style="display: none"><strong>Please enter the Investigation team details.</Strong></div>
</div>

<hr style="height:2px;border:none;color:#333;background-color:#333;" />


