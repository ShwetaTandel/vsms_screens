<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$incidentNumber = $_GET['id'];
$action = $_GET['action'];
?>
<html>
    <head>
        <title>VSMS - Report Incident</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <script src="../js/popper.min.js"></script>
        

    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Report Incident</h1>      
            </div>
            <br/>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L1_CREATED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Incident Report <a href="reportIncident.php?action=IR&iid=<?php echo $incidentNumber;?>">(IR<?php echo $incidentNumber;?>)</a> has been created.</h4>
                <p style="color: red;font-weight: bolder; font-size: large" class="text-center">REMINDER: You have 24 hrs from the incident time to submit the Initial Report.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_SUBMITTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Initial Report (IR<?php echo $incidentNumber;?>) has been submitted for Site Management approval.</h4>
                <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">The status of this Incident Report has now changed to Pending Site Approval.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
              <div class="alert alert-success" role="alert" <?php if ($action !== "L1_SAVED") { ?>style="display:none;"<?php } ?>>
                  <h4 class="alert-heading text-center">Incident Report <a href="reportIncident.php?action=IR&iid=<?php echo $incidentNumber;?>">(IR<?php echo $incidentNumber;?>)</a> has been saved.</h4>
                <p style="color: red;font-weight: bolder; font-size: large" class="text-center">REMINDER: You have 24 hrs from the incident time to submit the Initial Report for Site Management Approval.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_EDITED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Initial Report for this Incident (<a href="reportIncident.php?action=AIR&iid=<?php echo $incidentNumber;?>">IR<?php echo $incidentNumber;?>)</a> has been saved.</h4>
                <p style="color: red;font-weight: bolder;font-size: large" class="text-center">REMINDER: You have 24 hrs from the incident time to approve the Initial Report and 72 hrs to submit the Full Investigation.</p>
                <hr>
                
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L2_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Initial Report for this Incident (<a href="reportIncident.php?action=FIR&iid=<?php echo $incidentNumber;?>">IR<?php echo $incidentNumber;?>)</a> has been approved. The status of this Incident Report has now changed to Pending Full Investigation.</h4>
                <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">REMINDER: You have 72 hrs from the incident time to submit the Full Investigation.</p>
                <hr>
                
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "REPORT_DELETE") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Incident Report (IR<?php echo $incidentNumber;?>) has been deleted.</h4>
                <hr>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Initial Report for this Incident (IR<?php echo $incidentNumber;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the originator for editing and re-submission. The status of this Incident Report has now changed to Rejected by Site Management.</p>
                
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L3_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Full Investigation for this Incident (IR<?php echo $incidentNumber;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the Site Management for editing and re-submission. The status of this Incident Report has now changed to Rejected by GM.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L3_EDITED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Full Investigation for this Incident (<a href="reportIncident.php?action=AFIR&iid=<?php echo $incidentNumber;?>">IR<?php echo $incidentNumber;?>)</a>  has been saved. </h4>
                     <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">Please ensure the Full Investigation is either approved or rejected in a timely manner.</p>
                <hr>
            </div>
           <div class="alert alert-success" role="alert" <?php if ($action !== "L3_SAVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Full Investigation for this Incident (<a href="reportIncident.php?action=FIR&iid=<?php echo $incidentNumber;?>">IR<?php echo $incidentNumber;?>)</a>  has been saved. </h4>
                     <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">REMINDER: You have 72 hrs from the incident time to submit the Full Investigation.</p>
                <hr>
            </div>

            <div class="alert alert-success" role="alert" <?php if ($action !== "L3_SUBMITTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Full Investigation for this Incident (IR<?php echo $incidentNumber;?>) has been submitted for GM approval. </h4>
                     <p class="mb-0 text-center">The status of this Incident Report has now changed to Pending GM Approval.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
                
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L3_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Full Investigation for this Incident (IR<?php echo $incidentNumber;?>) has been approved. </h4>
                     <p class="mb-0 text-center">The status of this Incident Report has now changed to Pending H&S Sign Off.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_SAVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Incident Report (<a href="reportIncident.php?action=HSSIGNOFF&iid=<?php echo $incidentNumber;?>">IR<?php echo $incidentNumber;?></a>) has been saved. </h4>
                     <p class="mb-0 text-center">Press Submit and Close Off to close off this Incident Report.</p>
                     <p class="mb-0 text-center">All new Countermeasures have now been sent to the selected owners.</p>
                <hr>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Incident Report (IR<?php echo $incidentNumber;?>) has been closed off. </h4>
                     <p class="mb-0 text-center">If there are still outstanding actions left associated with this Incident report, its status will change to Outstanding Actions . Otherwise, it will change to Closed.</p>
                      <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
                <hr>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Incident (IR<?php echo $incidentNumber;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the Full Investigation Approver for editing and re-submission. The status of this Incident Report has now changed to Rejected by H&S.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Your Site Incidents </b> tab on Home screen.</p>
            </div>


            <div class="pull-right">
                   <a class="btn btn-dark" href="home.php" id="btnBack"><i class="fa fa-arrow-left"></i> Back To Home</a>
            </div>
        </div>
    </body>
</html>
