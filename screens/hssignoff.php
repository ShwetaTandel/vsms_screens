<div class="row">
    <div class="col-md-3">
        <label class="control-label">Category  <span style="color: red">*</span></label>
        <select class="custom-select" id="category" >
            <option value=""></option>
            <option value="Burn, Scalds">Burn, Scalds</option>
            <option value="Cuts and Abrasions">Cuts and Abrasions</option>
            <option value="Dropped Product">Dropped Product</option>
            <option value="Handling and Lifting">Handling and Lifting</option>
            <option value="Nips, Traps and Strains">Nips, Traps and Strains</option>
            <option value="Slips, Trips and Falls">Slips, Trips and Falls</option>
            <option value="Struck Against">Struck Against</option>
            <option value="Struck By">Struck By</option>
            <option value="Other">Other</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Category Level  <span style="color: red">*</span></label>
        <select class="custom-select" id="categoryLevel" >
            <option value=""></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>

        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Type  <span style="color: red">*</span></label>
        <select class="custom-select" id="type" >
            <option value=""></option>
            <option value="LTI">LTI</option>
            <option value="NLTI">NLTI</option>
        </select>
    </div>
    <div class="col-md-3">
        <label class="control-label">Dangerous Occurrence<span style="color: red">*</span></label>
        <select class="custom-select" id="isDangerousOccurence" >
            <option value="-1"></option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
    <div class="col-md-2">
        <label class="control-label">Act or Condition<span style="color: red">*</span></label>
        <select class="custom-select" id="actOrConditon" >
            <option value=""></option>
            <option value="Act">Act</option>
            <option value="Condition">Condition</option>
            <option value="Act/Condition">Act/Condition</option>
        </select>
    </div>
</div>
<br/>
<div class="row">
     <div class="col-md-3 col-lg-6">
        <label class="control-label">Classification<span style="color: red">*</span></label>
        <select class="custom-select" id="classification" >
            <option value=""></option>
            <option value="Fatal Incident">Fatal Incident</option>
            <option value="RIDOR (Immediate)">RIDDOR (Immediate)</option>
            <option value="RIDDOR (> 7 days)">RIDDOR (> 7 days)</option>
            <option value="Lost Time Injury (LTI) (< 7 days)">Lost Time Injury (LTI) (< 7 days)</option>
            <option value="Medical Treatment Case(No LTI)">Medical Treatment Case(No LTI)</option>
            <option value="First Aid Case">First Aid Case</option>
            <option value="Advice Only">Advice Only</option>
            <option value="Property Damage Incident (Customer Property)">Property Damage Incident (Customer Property)</option>
            <option value="Property Damage Incident (VEU Only)">Property Damage Incident (VEU Only)</option>
            <option value="Near Miss">Near Miss</option>
            <option value="Dangerous Occurrence">Dangerous Occurrence</option>
            <option value="Ill Health">Ill Health</option>
            <option value="Occupational Health">Occupational Health</option>
            <option value="Contractor Incident">Contractor Incident</option>
        </select>
    </div>
     <div class="col-md-3 col-lg-6">
            <div class="form-group">
                <label class="control-label">H&S Summary</label>
                <br/>
                <textarea rows="3" class="form-control" id='hsSummary'  name = "hsSummary" maxlength="500"></textarea>
            </div>
        </div>
   

</div>
<br/>

<div class="row">
        <div id ="hsRequiredValidatiionError" class="showError alert alert-danger hsRequiredValidatiionError" style="display: none"><strong>Please select all the required fields.</Strong></div>
    </div>
<br/>
<hr style="border-top: dotted;color: gainsboro" />
<h4>Person(s) Involved</h4>
<br/>
<div class="container-fluid clonedInjuredDetails" id="injuredPerson_1">  
    
    <h4 id="injuredHeading_1" name="injuredHeading_1" class="heading-reference"></h4>
    <input type="hidden" class="form-control injured-person-id" id="injuredPersonId_1" name="injuredPersonId_1[]" value = "0"/>
    <div class="row">
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Name <span style="color: red">*</span></label>
                  <select class="custom-select injured-person-name" name="injuredPersonName_1" id = "injuredPersonName_1"> 
                      <option value="-1"></option> 
                  </select>
            </div>
        </div>
         <div class="col-md-2 col-lg-6">
            <div class="form-group">
                <label class="control-label">Further Details of Treatment <span style="color: red">*</span></label>
                 <textarea rows="3" class="form-control injured-person-treatment-details" id='injuredPersonTreatmentDetails_1'  name = "injuredPersonTreatmentDetails_1[]" maxlength="1000"></textarea>
            </div>
        </div>
        <div class="col-md-2 col-lg-3">
            <div class="form-group">
                <label class="control-label">Days Lost <span style="color: red">*</span></label>
                <input type="number" class="form-control injured-person-days-lost" id="dayLost_1" name="dayLost_1[]" maxlength="10" min="0"/>
            </div>
        </div>
    </div>
<div class="row">
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <br/>
                <label class="btn btn-danger">RIDDOR 
                    <input type="checkbox" id="isRIDDORToggle_1" class="injured-person-riddor-toggle badgebox" onchange="enableRIDDOR(this)"/>
                    <span class="badge">&cross;</span>
                </label>
                <span style="color: red">*</span>
            </div>
        </div>
        <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <label class="control-label">RIDDOR Reference  <span style="color: red">*</span></label>
                <input type="text" class="form-control injured-person-riddor-ref" id="injuredPersonRiddorRef_1" name="injuredPersonRiddorRef_1[]" maxlength="250" disabled="true"/>
            </div>

        </div>
              <div class='col-md-2 col-lg-3'>
            <div class="form-group">
                <label class="control-label">Date reported to HSE (DD/MM/YYYY)<span style="color: red">*</span></label>
                <div class="input-group date person-hse-date-1"  id="injuredPersonHSEReportedDate_1" name="injuredPersonHSEReportedDate_1[]" data-target-input="nearest">
                    <input class="form-control datetimepicker-input person-hse-date-2" type="text" name="injuredPersonHSEReportedDate_1[]" id="injuredPersonHSEReportedDate_1"  data-target="#injuredPersonHSEReportedDate_1"  autocomplete="off" disabled="true"/>
                    <div class="input-group-append person-hse-date-3" data-target="#injuredPersonHSEReportedDate_1" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div> 
                </div>
            </div>
        </div>
         <div class="col-md-3 col-lg-3">
            <div class="form-group">
                <label class="control-label">Attach RIDDOR Form  <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span><span style="color: red">*</span></label>
                <input type="file" class="form-control injured-person-riddor-form" id="injuredPersonRIDDORForm_1" name="injuredPersonRIDDORForm_1[]" multiple disabled="true"/>
            </div>
        </div>
    </div>
   
  
    <div class="row injured-person-riddor-files" id="showRIDDORFiles_1"  >
        <div class="col-md-2"><label class="control-label">Attached RIDDOR Forms</label></div>
    </div>
    <br/>
     <div class="row">
        <div id ="injuredPersonDaysLostError_1" class="showError alert alert-danger injuredPersonDaysLostError" style="display: none"><strong>Days lost cannot be less than 0.</Strong></div>
        <div id ="injuredPersonDaysLostLTIError_1" class="showError alert alert-danger injuredPersonDaysLostLTIError" style="display: none"><strong>Days lost cannot be 0 or less if LTI is selected.</Strong></div>
        <div id ="injuredPersonDaysLostMoreThan7Error_1" class="showError alert alert-danger injuredPersonDaysLostMoreThan7Error" style="display: none"><strong>If days lost is greater than 7, please add all the required details for RIDDOR.</Strong></div>
        <div id ="injuredPersonValidationError_1" class="showError alert alert-danger injuredPersonValidationError" style="display: none"><strong>Please add all the required details for the selected person.</Strong></div>
        <div id ="injuredPersonValidationRiddorError_1" class="showError alert alert-danger injuredPersonValidationRiddorError" style="display: none"><strong>Please add all the required details for the RIDDOR.</Strong></div>
    </div>
    <br/>
    <hr style="border-top: dotted;color: gainsboro" />
</div>
<div class="row" id="injuredPersonButtons">
    <div class="col-md-12">
        <div >
            <input type="button" class="btn btn-blue" id="btnAddInjuredPerson" value="+">
            <input type="button" class="btn btn-red pull-right" id="btnDelInjuredPerson" value="-">
        </div>
      </div>
</div>
