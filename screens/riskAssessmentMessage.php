<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$graId = $_GET['id'];
$action = $_GET['action'];
?>
<html>
    <head>
        <title>VSMS - Risk Assessment</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <script src="../js/popper.min.js"></script>
        

    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Risk Assessment</h1>      
            </div>
            <br/>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_SUBMITTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>) has been submitted for Operations Manager's approval.</h4>
                <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">The status of this Risk assessment has now changed to Pending Operations Manager Approval.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Risk Assessment </b> tab on Home screen.</p>
            </div>
              <div class="alert alert-success" role="alert" <?php if ($action !== "L1_SAVED") { ?>style="display:none;"<?php } ?>>
                  <h4 class="alert-heading text-center">Risk Assessment <a href="genericRiskAssessment.php?action=L1SUBMIT&graid=<?php echo $graId;?>">(RA<?php echo $graId;?>)</a> has been saved.</h4>
                <hr>
                <p class="mb-0 text-center">Click the above link to edit and submit the Risk Assessment</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_EDITED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (<a href="genericRiskAssessment.php?action=L2APPROVE&graid=<?php echo $graId;?>">RA<?php echo $graId;?>)</a> has been saved.</h4>
                <p class="mb-0 text-center">Click the above link to edit and approve the Risk Assessment</p>
                <hr>
                
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L2_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>)</a> has been approved and now pending for GM approval.</h4>
                <hr>
                
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L3_EDITED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (<a href="genericRiskAssessment.php?action=L3APPROVE&graid=<?php echo $graId;?>">RA<?php echo $graId;?>)</a> has been saved.</h4>
                <p class="mb-0 text-center">Click the above link to edit and approve the Risk Assessment</p>
                <hr>
                
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "REPORT_DELETE") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>) has been deleted.</h4>
                <hr>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the originator for editing and re-submission. The status of this RA has now changed to Rejected by Operation's Manager.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Generic Risk Assessment </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L3_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the Operations Management for editing and re-submission. The status of this RA has now changed to Rejected by GM.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Generic Risk Assessment </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L3_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>)</a> has been approved and now pending for H&S approval.</h4>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Risk Assessment </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>)</a> has been approved at the final stage.</h4>
                 <p class="mb-0 text-center">There are still outstanding actions left associated with this Risk Assessment report.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Risk Assessment </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "_CLOSED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>)</a> has been approved at the final stage with no pending actions.</h4>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Risk Assessment </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_EDITED") { ?>style="display:none;"<?php } ?>>
                  <h4 class="alert-heading text-center">Risk Assessment (<a href="genericRiskAssessment.php?action=L4APPROVE&graid=<?php echo $graId;?>">RA<?php echo $graId;?>)</a> has been saved.</h4>
                       <p class="mb-0 text-center">Click the above link to edit and approve the Risk Assessment</p>
                     
                <hr>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "L4_REJECTED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Risk Assessment (RA<?php echo $graId;?>) has been rejected. </h4>
                     <p class="mb-0 text-center">It has now been sent back to the GM for editing and re-submission. The status of this RA has now changed to Rejected by H&S.</p>
                <hr>
                 <p class="mb-0 text-center">You can track its progress on <b> Risk Assessment </b> tab on Home screen.</p>
            </div>


            <div class="pull-right">
                   <a class="btn btn-dark" href="home.php" id="btnBack"><i class="fa fa-arrow-left"></i> Back To Home</a>
            </div>
        </div>
    </body>
</html>
