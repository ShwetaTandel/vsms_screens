<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
include '../masterData/generalMasterData.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}

$show = "I";
if (isset($_GET['show'])) {
    $show = $_GET['show'];
}
$sites = $_SESSION['vsmsUserData']['sites'];
$pendingIncidentCnt = getPendingIncidentsCount($_SESSION['vsmsUserData']['id']);
$lastIncidentDays = getLastIncidentDays();
$monthWiseIncidents = getIncidentOccurredThisMonth();
$lastMonthIncidents = $monthWiseIncidents[1];
$thisMonthIncidents = $monthWiseIncidents[0];
$monthWiseHazards = getHaradsReportedThisMonth();
$lastMonthHazards = $monthWiseHazards[1];
$thisMonthHazards = $monthWiseHazards[0];
$monthWiseInjuries = getInjuriestOccurredThisMonth();
$lastMonthInjuries = $monthWiseInjuries[1];
$thisMonthInjuries = $monthWiseInjuries[0];
$thisMonthRevisedRA = getGRAThisMonth();
$lastMonthGRA = $thisMonthRevisedRA[1];
$thisMonthGRA = $thisMonthRevisedRA[0];
$outstandingactionsCnt = getOutstandingActions($_SESSION['vsmsUserData']['id']);
$actionOwners = getActionOwners();
$userSiteLevels = getUserLevels($_SESSION['vsmsUserData']['id']);
$maxLevel = getMaxUserLevel($_SESSION['vsmsUserData']['id']);
if($_SESSION['vsmsUserData']['is_super_user'] == 1){
    $maxLevel = 'L4';
}
$isActionOWner = $_SESSION['vsmsUserData']['is_action_owner'];
ChromePhp::log($maxLevel );

?>
<html>
    <head>
        <title>Vantec Safety Management System - Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>-->

        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>

    </head>
    <body>
        <header>
            <div class="row">
                <div class="col-4" class="row pull-left">
                    <img STYLE="position:absolute;width: 650px;height: 200px" src="../images/logo.png" alt=""/>
                </div>
                <div class="col-8">
                    <div class="row pull-right">

                        <?php
                        include './commonHeader.php';
                        ?>
                    </div>
                </div>
            </div>

        </header> 
     <div class="container-fluid"style="margin-top: 0px">
        <br/>
        <div class="row">
           
            <div class="col-12">
               
                <br/><br/><br/>
                <div class="row">
                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #ef292d"><?php echo $lastIncidentDays != null ? $lastIncidentDays[0] : 0 ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969"> Days since last Incident</span>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $monthWiseIncidents[0] ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Incidents this month</span>
                            <?php if ($lastMonthIncidents > $thisMonthIncidents) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthIncidents - $lastMonthIncidents ); ?></span>
                            <?php } elseif ($thisMonthIncidents > $lastMonthIncidents) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d">+<?php echo ($thisMonthIncidents - $lastMonthIncidents); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    
                     <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $monthWiseHazards[0] ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Hazards this month</span>
                            <?php if ($lastMonthHazards > $thisMonthHazards) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthHazards - $lastMonthHazards ); ?></span>
                            <?php } elseif ($thisMonthHazards > $lastMonthHazards) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d">+<?php echo ($thisMonthHazards - $lastMonthHazards); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $thisMonthInjuries ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Injuries this month</span>
                            <?php if ($lastMonthInjuries > $thisMonthInjuries) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthInjuries - $lastMonthInjuries ); ?></span>
                            <?php } elseif ($thisMonthInjuries > $lastMonthInjuries) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d"> +<?php echo ($thisMonthInjuries - $lastMonthInjuries); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $thisMonthGRA ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Risk Assessments this month</span>
                            <?php if ($lastMonthGRA > $thisMonthGRA) { ?>
                                <i class="fa fa-fw fa-caret-down text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d"> <?php echo($thisMonthGRA - $lastMonthGRA ); ?></span>
                            <?php } elseif ($thisMonthGRA > $lastMonthGRA) { ?>
                                <i class="fa fa-fw fa-caret-up text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> +<?php echo ($thisMonthGRA - $lastMonthGRA); ?></span>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <br/>
        <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-3">
                    <button  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn3d btn-blue btn-lg btn-block btn-huge" onclick="window.open('reportIncident.php', '_self')">Report Incident</button>
                </div>
              
                <div class="col-md-3">
                    <button href="#"  <?php if ($isActionOWner === '0') { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn btn3d btn-green btn-lg btn-block btn-huge" onclick="window.open('action.php', '_self')">New Action</button>
                </div>
                <div class="col-md-3">
                    <button  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn btn3d btn-orange btn-lg btn-block btn-huge" onclick="window.open('genericRiskAssessment.php', '_self')">Risk Assessment</button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn3d btn-danger btn-lg btn-block btn-huge"   <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >Audit, Tours and Inspections (Coming Soon)</button>
                </div>
            </div>
            <br/>
            <div class="row">
                  <div class="col-md-3">
                    <button href="#" style="width: 100%" class="btn btn3d btn-blue btn-lg btn-block btn-huge" onclick="window.open('reportHazard.php', '_self')">Report Hazard </button>
                </div>
                <div class="col-md-3"  <?php if ($isActionOWner === '0') { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >
                    <button href="#" style="width: 100%" onclick="window.open('showOutstandingActions.php', '_self')" class="btn btn3d btn-green btn-lg btn-block btn-huge">Outstanding Actions</button>
                    <p class="d-flex justify-content-center" style="color: #ef292d;font-weight: bold;font-size: large">Outstanding Actions [<?php echo $outstandingactionsCnt[0] ?>]</p>
                </div>
                 <div class="col-md-3"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >
                    <button onclick="window.open('showPendingApprovals.php', '_self')" style="width: 100%" class="btn btn3d btn-orange btn-lg btn-block btn-huge">Pending Approvals</button>
                    <p class="d-flex justify-content-center" style="color: #ef292d;font-weight: bold;font-size: large">Incidents [<?php echo $pendingIncidentCnt[0] ?>]/Hazards [<?php echo $pendingIncidentCnt[1] ?>]/RAs [<?php echo $pendingIncidentCnt[2] ?>] </p>
                </div>
                
                <div class="col-md-3">
                    <button class="btn btn3d btn-danger btn-lg btn-block btn-huge"  onclick="window.open('reportAndForms.php', '_self')"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >Reports & Forms</button>
                </div>
            </div>
            <br/>
            <br/>
            <div class="container-fluid">

                <?php
                include './showSiteIncidentsAndHazards.php';
                ?>

            </div>

        </div>
</div>
    </body>
    <script>
        /*   $(document).ready(function () {
            var lastUpdated = '<?php echo $_SESSION['vsmsUserData']['updated_at']?>';
            var userName = '<?php echo $_SESSION['vsmsUserData']['first_name']?>';
            if(lastUpdated === ''){
                alert("Please consider changing your system set password. Follow Menu Welcome "+ userName + " -> Change Password.");
            }
           });*/
        
    </script>
</html>
