<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$action = '';
$graId = 0;
$userLevel  = $_SESSION['vsmsUserData']['level'];
if (isset($_GET['action'])) {
    $action = $_GET['action'];
}
if (isset($_GET['graid'])) {
    $graId = $_GET['graid'];
}
if ($action === '') {
    $action = "L1SUBMIT";
}
include '../masterData/generalMasterData.php';
$data = null;
$actionOwners = getActionOwners();
$approverLevel = 'L2';
$userLevel  = $_SESSION['vsmsUserData']['level'];
if ($graId !== 0) {
    $serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/graData.php?filter=GRA&graid=" . $graId;
    $data = json_decode(file_get_contents($serviceUrl));
    ChromePhp::log($data);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
    $approverLevel = $data[0]->approver_level;
   if($action!='VIEW' && ($data[0]->status  === 'L2_SUBMITTED' ||$data[0]->status  === 'L2_EDITED' 
       || $data[0]->status  === 'L2_APPROVED' ||$data[0]->status  === 'L3_EDITED' 
       || $data[0]->status  === 'L3_APPROVED' ||$data[0]->status  === 'L4_EDITED' 
       ||  $data[0]->status  === 'L2_REJECTED' ||$data[0]->status  === 'L3_REJECTED' ||$data[0]->status  === 'L4_REJECTED') 
       && $data[0]->curr_approver_id !=$_SESSION['vsmsUserData']['id'] ){
        echo "<h1>You are not authorised to access this report. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
   }
   
}

$approvers = getSiteApproversForGRA();
$siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
ChromePhp::log($action);
?>
<html>
    <head>
        <title>VSMS - Risk Assessment</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>
        <script src="../js/tempusdominus-bootstrap-4.min.js"></script>
        <link href="../css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/bootstrap-select.min.js"></script>
        <script src="../js/graJS.js?random=<?php echo filemtime('../js/reportIncidentJS.js'); ?>"></script>
        <script src="../js/IEFixes.js"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <style>
            .one{
                background-color: #63b37b;
            }
            .two{
                background-color: #63c26c
            }
            .three{
                background-color: #6DC664
            }
            .four{
                background-color: #7FCB65
            }
            .five{
                background-color: #94CF66
            }
            .six{
                background-color: #A9D466
            }

            .eight{
                background-color:  #E9E36F
                }
            .nine{
                    background-color: #EFC669
            }
            .ten{
                background-color: #E9CF6F
            }
            .twelve{
                background-color: #E6BF68
            }
            .fifteen{
                background-color: #EAAB68
            }
            .sixteen{
                background-color: #EF9668
            }
            .twenty{
                background-color: #F38068
            }
            .twentyfive{
                background-color: #F86969
            }
            #newRiskRatingL, #newRiskRatingS, #riskRatingL, #riskRatingS {
                background: linear-gradient(to right, #63b37b 0%, #6DC664 50%, #6DC664 50%, #94CF66 100%);
                border: solid 1px #82CFD0;
                border-radius: 8px;
                height: 7px;
                width: 356px;
                outline: none;
                transition: background 450ms ease-in;
                -webkit-appearance: none;
            }
            #newRiskRatingR, #riskRatingR {
                background: linear-gradient(to right, #63b37b 0%, #E6BF68 50%, #E6BF68 50%, #F86969 100%);
                border: solid 1px #82CFD0;
                border-radius: 8px;
                height: 7px;
                width: 356px;
                outline: none;
                transition: background 450ms ease-in;
                -webkit-appearance: none;
            }
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
            .card-header .fa {
                transition: .3s transform ease-in-out;
            }
            .card-header .collapsed .fa {
                transform: rotate(-90deg);

            }
            #hazardOverlayDiv >img{

                position:absolute;
                top:38%;
                left: 38%;
                z-index:10;


            }
            #hazardOverlayDiv{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: absolute; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0;
                left:0;
                bottom:0;
                right:0;
                z-index: 100;

            }
            #hazardOverlay {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px;
                right:20px;
                bottom:20px;
                left:20px;
            }
             #riskOverlayDiv >img{

                position:absolute;
                top:38%;
                left: 38%;
                z-index:10;


            }
            #riskOverlayDiv{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0;
                left:0;
                bottom:0;
                right:0;
                z-index: 100;

            }
            #riskOverlay {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px;
                right:20px;
                bottom:20px;
                left:20px;
            }
            #container{
                position:relative;
                display:none;
            }

            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }


        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">VSMS-Risk Assessment</h1>      
            </div>
        </div>
        <hr/>

        <div class="alert alert-info container-fluid">
            <span >Data Protection: Please ensure all personal data is handled with care and not shared with any unauthorised person.  </span>
        </div>
         <?php if ($action !== "L1SUBMIT") { ?>
            <div class="container-fluid"><span style="font-size: x-large;font-weight: bolder;color: brown"> Risk Assessment ID: RA<?php echo($data[0]->gra_id ); ?></span></div>
        <?php } ?>
        <br/>

        <form action="" method="post">
            <div id="riskOverlayDiv">
                <img src="../images/loading.gif" alt="Wait" alt="Loading"/>
                <div id="riskOverlay">
                    Uploading Risk Assessment data and attached files. Please refrain from clicking anywhere......
                </div>
            </div>
            <div class="container-fluid ">
                <div id="gaSummData" class="container-fluid" style="background-color: whitesmoke">
                    <h4>Risk Assessment Summary</h4>
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site <span style="color: red">*</span></label>
                                <br/>
                                <input type="hidden" id="graId" value="0"/>
                                <input type="hidden" id="graNumber" value="0"/>
                                <select class="custom-select mandatory" id="site" required onchange="populateApprovers(this)">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users_sites,' . $mDbName . '.site where users_sites.site_id = site.id and users_sites.user_id = ' . $_SESSION['vsmsUserData']['id']);
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['site_id'] . '">' . $row['code'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-1 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Process/Area Assessed <span style="color: red">*</span></label>
                                <br/>
                                <input type="text" class="form-control mandatory"  list="areaList" id="areaAssessed">
                                <datalist  id="areaList">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.LOCATION;');
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['name'] . '">' . $row['name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </datalist>

                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Revision Date (DD/MM/YYYY)<span style="color: red">*</span></label>
                                <input type="date" id="gaDate"   class="form-control mandatory"  max="<?= date('Y-m-d'); ?>" onchange="changeMax()"/>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Next Review Date (DD/MM/YYYY)<span style="color: red">*</span></label>
                                <input type="date" id="gaRevisionDate"   class="form-control mandatory" max="<?= date('Y-m-d', strtotime('+1 year', strtotime($data[0]->gra_date)))?>"/>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Assessor(s)<span style="color: red">*</span></label>
                                <br/>
                                <input type="text" class="form-control mandatory"  list="assessorList" id="gaAssessor">
                                <datalist  id="assessorList">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT distinct concat(first_name, " ", last_name) as name FROM ' . $mDbName . '.approvers, ' . $mDbName . '.users where approvers.user_id = users.id and  site_id in (' . $siteIds . ')');
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['name'] . '">' . $row['name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </datalist>

                            </div>
                        </div>
                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Owner (Site/Department Management)<span style="color: red">*</span></label>
                                <div class="input-group">
                                    <input class="form-control mandatory" type="text" id="gaOwner" maxlength="255"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include './riskHazard.php'; ?>
                <div class="row">
                    <div id="hazards" class="container-fluid ">
                        <br>
                        <table  class="table table-hover small-text table-bordered" id="hazardTb">
                            <tr class="tr-header">
                                <th  rowspan="2">Hazard</th>
                                <th  rowspan="2">Persons at risk</th>
                                <th  rowspan="2">Existing Controls</th>
                                <th colspan="3">Risk Rating</th>
                                <th  rowspan="2">Further Action Required</th>
                                <th  rowspan="2">Open Actions</th>
                                <th colspan="3">New Risk Rating</th>
                                <th  rowspan="2"><a href="javascript:void(0);" style="font-size:18px;" id="addHazard" title="Add New">
                                        <span class="fa fa-plus"></span>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                    <th scope="col">S</th>
                                    <th scope="col">L</th>
                                    <th scope="col">R</th>
                                    <th scope="col">S</th>
                                    <th scope="col">L</th>
                                    <th scope="col">R</th>
                            </tr>
                            <tr id="row0">
                                <td><span name="riskHazard[]" class="riskHazard" style="white-space: pre-line"></span><input type="hidden" name="riskHazardId[]" class="riskHazardId" value="0"/></td>
                                <td><span name="personAtRisk[]" class="personAtRisk" style="white-space: pre-line"></span></td>
                                <td><span name="existingControls[]" class="existingControls" style="white-space: pre-line"></span></td>
                                <td class=" riskColor"><span name="riskRatingS[]" class="riskRatingS"></span></td>
                                <td class="riskColor"><span name="riskRatingL[]" class="riskRatingL"></span></td>
                                <td class="riskColor"><span name="riskRatingR[]" class="riskRatingR"></span></td>
                                <td><span name="furtherActions[]" class="furtherActions"></span></td>
                                <td><span name="openActions[]" class="openActions"></span></td>
                                <td class=" riskColor"><span name="newRiskRatingS[]" class="newRiskRatingS"></span></td>
                                 <td class=" riskColor"><span name="newRiskRatingL[]" class="newRiskRatingL"></span></td>
                                  <td class=" riskColor"><span name="newRiskRatingR[]" class="newRiskRatingR"></span></td>
                                <td><a href='javascript:void(0);'  class='remove'><span class='fa fa-remove'></span></a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
           <hr/>
           <div class="container-fluid">
               <button data-toggle="collapse" data-target="#matrix" type="button" class="btn btn-block btn-info">Show Severity X Likelihood Calculator </button>
               <br/>
               <div id="matrix" class="collapse">
                   <img src="../images/matrix.JPG"  style="margin-left: 35%;margin-right: auto";>
               </div>
           </div>
           <hr/>
            <div class="container-fluid" id="allActionsRow"  <?php if ($action !== "VIEW") { ?> style="display: none" <?php } ?>>
                 <h4>Risk Hazard Actions</h4>
                <table id="actionsDataTable" class="table">
                 </table>
            </div>
            <div class="container-fluid" style="background-color: whitesmoke">
                <div class = "row"  <?php if ($action === "L4APPROVE") { ?> style="display: none" <?php } ?>>
                    <div class = "col-md-4">
                        <label class = "control-label">Select an approver<span style = "color: red">*</span></label>
                        
                        <select class = "custom-select mandatory" id = "approver">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id ="approverValidationError" class="showError alert alert-danger approverValidationError" style="display: none"><strong>Please select an approver.</Strong></div>
                </div>

                <?php if ($action !== "VIEW") { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE" onclick="submitGRAData('SAVE')"></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                     <?php if ($action === "L1SUBMIT" ) { ?>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validateGRA('SUBMIT', '<?php echo $action ?>')"></input>
                                    <?php } else { ?>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="APPROVE" onclick="validateGRA('APPROVE', '<?php echo $action ?>')"></input>
                                    <input href="#"  class="btn btn-red" id="btnReject" type="button" value="REJECT" onclick="rejectGRA()"></input>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
            
                <?php } elseif($action === 'VIEW') {  ?> 
                <div class="row" > 
                <div class="col-lg-12">
                    <div style="margin-top: 2rem">
                        <div class="pull-right">
                            <input class="btn btn-orange" id="btnDiscard" type="button" value="BACK" onclick="javascript:history.go(-1)"></input>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?> 
            </div>
            <br />
            <div class="modal fade" id="promptModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                        </div>
                        <br>
                        <div class="modal-body">
                            <div class="col-md-3 col-lg-12">
                                <div class="form-group">
                                    <label id="promptMessage">Please add comments:</label>
                                    </br>
                                    <textarea class="form-control" rows="2" maxlength="500" id="promptComments"></textarea>
                                    <input type="hidden" id="promptAction"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" >
                            <button type="button" class="btn btn-green" id="bYes"  data-dismiss="modal" onclick="submitGRAData('REJECT')">OK</button>
                            <button type="button" class="btn btn-gray" id="bNo"  data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
      
    </body>
    <script>
        $(document).ready(function () {
            var action = '<?php echo $action ?>';
            var graId = <?php echo $graId ?>;
           
            var data;
            if (graId !== 0) {
                var data = <?php echo json_encode($data) ?>;
                populateGRA(data[0]);
            }
            if (action === 'VIEW') {
                buildActionsTable(data[0].allActions);
                    $('form *').prop('disabled', true);
                    $('#btnDiscard').prop('disabled', false);;
                     
                }
            $('#addHazard').on('click', function () {
               var checkEmptyInput = $("#gaSummData").find(".mandatory").filter(function () {
                    if ($(this).val() === "")
                        return $(this);
                }).length;
                  if (checkEmptyInput > 0) {
                        alert("Please enter all Risk Assessment Summary mandatory fields before adding any Risk Hazards");
                  } else {
                        showHazard(null, null, graId);
                  }
            });
            $('#addAction').on('click', function () {
                var data = $("#tab_action tr:eq(1)").clone(true).appendTo("#tab_action");
                var rowCount = $('#tab_action tr').length - 1;
                $('#tab_action tr:last').attr("id", "actionRow" + rowCount);
                data.find("input,textarea,select").val('');
                data.find(".actionId").val('0');
            });
            //Delete actions
            $(document).on('click', '.removeAction', function () {
                var trIndex = $(this).closest("tr").index();
                if (trIndex > 0) {
                    $(this).closest("tr").remove();
                } else {
                    alert("Please uncheck the Further Action Required, if no actions required");
                }
            });
            $('.riskHazard').on('click', function () {
                showHazard($(this), data,graId);
            });
            //Delete Harzards
            $(document).on('click', '.remove', function () {
                var trIndex = $(this).closest("tr").index();
                if (trIndex > 1) {
                      if (confirm("Are you sure you want to delete this Risk Hazard? All the data and associated actions will be deleted and the changes cannot be undone. Only proceed if you are sure.")){
                          deleteRiskHazard($(this).closest("tr"));
                      }
                } else {
                    alert("Atleast one hazard is mandatory.");
                }
            });
            $("#bYes").on('click', function () {
                
            });

        });
        function changeMax(){
            var date = new Date($("#gaDate").val()); 
            date.setFullYear(date.getFullYear() + 1);
            document.getElementById('gaRevisionDate').setAttribute('max', date.toISOString().split('T')[0]);
        }
        function showActionTable() {
            if ($("#isFurtherActionRequired").is(':checked')) {
                $("#tab_action").show();

            } else {
                $("#tab_action").hide();
                $("#newRiskRatingS").val($("#riskRatingS").val());
                $("#newRiskRatingR").val($("#riskRatingR").val());
                $("#newRiskRatingL").val($("#riskRatingL").val());
                $('#valnewRiskRatingS').val(parseInt($("#riskRatingS").val()));
                $('#valnewRiskRatingL').val(parseInt($("#riskRatingL").val()));
                calcNewRisk($("#riskRatingL"));
            }

        }
         function populateApprovers(elem) {

            var approversMasterList = <?php echo json_encode($approvers) ?>;
            var userId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
            var level =  '<?php echo $approverLevel ?>';
            var siteId = $(elem).val();
            var approvers = '';
            var approverList = $("#approver");
            
            approverList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
              option.setAttribute("selected", "selected");
            approverList[0].add(option);
            for (var i = 0; i < approversMasterList.length; i++) {
                var site = approversMasterList[i];
                if (site.id === siteId ) {
                    approvers = site.approvers;
                    break;
                }
            }
            
            for (var i = 0; i < approvers.length; i++) {
                if(approvers[i].level === level && approvers[i].user_id !== userId){
                    var newOption = document.createElement('option');
                    newOption.setAttribute("value", approvers[i].user_id);
                    newOption.innerHTML = approvers[i].first_name + ' ' + approvers[i].last_name;
                    approverList[0].add(newOption);
                }
            }
        }
        function populateOwners(elem) {
            var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
            var actionSite = elem.value;
            var owners = '';
            var ownerList = $(elem).closest("td").next().find("select");
            //$(elem).closest("td").next().next().find("input")[0].value = '';
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === actionSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateManager(elem) {
            $(elem).closest("td").next().find("input")[0].value = $(elem).find('option:selected').attr('data-divid') === undefined ? '' : $(elem).find('option:selected').attr('data-divid');
        }
        function getRiskAssessmentData(filter){
           var graId = $('#graId').val();
           var graNumber = $('#graNumber').val();
           var site = $('#site').val();
           var areaAssessed = $('#areaAssessed').val();
           var gaAssessor = $('#gaAssessor').val();
           var gaRevisionDate = $('#gaRevisionDate').val();
           var gaDate = $('#gaDate').val();
           var gaOwner = $('#gaOwner').val();
           var currApproverId = $('#approver').val();
           var userId = <?php echo $_SESSION['vsmsUserData']['id'] ?>;
           var status =status;
           var rejectComments = "";
           if(filter === 'REJECT'){
                rejectComments = document.getElementById("promptComments").value ;
            }
            var graModel = {"graId": graId, "graNumber":graNumber,"siteId":site,"areaAssessed":areaAssessed, "assessor":gaAssessor,
                            "assessmentDate":gaDate,  "revisionDate":gaRevisionDate, "owner":gaOwner, "currApproverId": currApproverId, "userId": userId,
                            "filter":filter, "rejectComments":rejectComments};
            return graModel;
        }
        function submitGRAData(filter) {
            var gra = getRiskAssessmentData(filter);
            console.log(gra);
            
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(gra));
            form_data.append('function', 'saveRiskAssessment');
            form_data.append('connection', vsmsservice);
            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        //uploadFiles(res[1]);
                        sendGRAEmails(res[2],res[1]);
                        sendActionEmails(res[2],res[1]);
                        $("#riskOverlayDiv").fadeIn("slow");
                        var delay = 5000;
                        setTimeout(function () {
                            $("#riskOverlayDiv").fadeOut("fast");
                            $('#container').fadeIn();
                            alert("Risk saved successfully");
                            window.location.href = 'riskAssessmentMessage.php?action='+res[2]+'&id=' +res[1];
                        }, delay);
                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }
            });
            
        }
        function discardChanges() {
             if (confirm("Are you sure you want to discard all changes made to this form? This Risk Assessment Report will not be saved.")) {
                      window.open("home.php", "_self");
            }
        }
        function deleteRiskHazard(elem){
             var filter = "?hazardId=" + elem.find(".riskHazardId").val() + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "deleteRiskHazard", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response === 'OK') {
                            elem.remove();
                        }
                    }
                });
        }
        function submitHazardData() {
            var hazardDetails = $('#hazardDetails').val();
            var hazardId = $('#hazardId').val();
            var graId = $('#graId').val();
           
           
            var personsAtRisk = $('#personsAtRisk').val();
            var existingControls = $('#existingControls').val();
            var riskRatingS = $('#riskRatingS').val();
            var riskRatingL = $('#riskRatingL').val();
            var newRiskRatingS = $('#newRiskRatingS').val();
            var newRiskRatingL = $('#newRiskRatingL').val();
            var isFurtherActionsRequired =  $("#isFurtherActionRequired").is(':checked') ? true: false;
            var userId = <?php echo $_SESSION['vsmsUserData']['id'] ?>;
            
            var actions = new Array();
            if($("#isFurtherActionRequired").is(':checked')){
                $('#tab_action tbody tr').each(function (i, tr) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var actionId = $(this).find('.actionId').val();
                        if(actionId === '' || actionId === undefined){
                            actionId = '0';
                        }
                        actions[i] = {
                            "id": actionId === '0' ? '0' : actionId.substring(2, actionId.length),
                            "action": $(this).find('.action').val(),
                            "actionSite": $(this).find('.actionSite').val(),
                            "actionOwner": $(this).find('.actionOwner').val(),
                            "actionSubType": $(this).find('.actionSubType').val(),
                            "deadline": $(this).find('.deadline').val()

                        };
                    }
                });
            }
            var gra = getRiskAssessmentData('SAVE');
            var riskHazard = {"hazardId": hazardId, "graId": graId, "hazardDetails": hazardDetails, 
                "personsAtRisk": personsAtRisk, "existingControls": existingControls,
                "riskRatingS": riskRatingS, "riskRatingL": riskRatingL, "newRiskRatingS": newRiskRatingS, "newRiskRatingL": newRiskRatingL,
                "userId": userId, "isFurtherActionsRequired": isFurtherActionsRequired, "furtherActions": actions, "raModel":gra};


            console.log(riskHazard);
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(riskHazard));
            form_data.append('function', 'saveRiskHazard');
            form_data.append('connection', vsmsservice);


            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    if (response.startsWith("OK")) {
                        var res = response.split("-");

                        uploadFiles(res[1]);
                        
                        $("#hazardOverlayDiv").fadeIn("slow");
                        var delay = 3000;
                        setTimeout(function () {
                            $("#hazardOverlayDiv").fadeOut("fast");
                            $('#hazardModal').fadeIn();
                            alert("Hazard saved successfully");
                            window.location.href = 'genericRiskAssessment.php?action=L1SUBMIT&graid=' + res[2];
                        }, delay);
                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }
            });

        }
    function sendActionEmails(action, graid) {
        
            if(action === 'L2_SUBMITTED' || action === 'L2_APPROVED' || action === 'L3_APPROVED' || action ==='L4_EDITED' || action === 'L4_APPROVED'){
                 $.ajax({
                url: "../masterData/actionsData.php?filter=NEWACTIONSGRA&graid=" + graid,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    var actionIds = JSON.parse(response);
                    for(var i=0;i<actionIds.length;i++){
                         $.ajax({
                          type: "GET",
                          url: "sendEmails.php?actionId="+actionIds[i].id+"&function=ActionAssigned&type=Workflow&source=GRA",
                                success: function (data) {
                                    console.log(data);
                            }
                        });
                    }
                        
                   
                }});
            }
          
    }
    function sendGRAEmails(status, graid) {
            var func, type =  "";
            
            if(status === 'L2_SUBMITTED'){
                func = "GRASubmitted";
                type= "Workflow";
            } else if(status === 'L2_APPROVED' || status === 'L3_APPROVED'){
                func = "GRAApproved";
                type= "Workflow";
            }else if(status === 'L4_APPROVED'){
                func = "GRASignedOff";
                type= "Workflow";
            }else if(status === 'L2_REJECTED' || status === 'L3_REJECTED' || status === 'L4_REJECTED'){
                func = "GRARejected";
                type= "Workflow";
            }
            $.ajax({
                type: "GET",
                url: "sendEmails.php?graid=" + graid + "&function="+func+"&type="+type,
                success: function (data) {
                    console.log(data);
                   // window.location.href = 'reportIncidentMessage.php?action=' + action + '&id=' + iid;
                }
            });

        }
         function rejectGRA() {
            $('#promptMessage').html('Are you sure you want to reject this Risk Assessment Report? It will be sent back to the submitter for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>');
            $('#promptModal').modal('show');
        }

    </script>
</html>
