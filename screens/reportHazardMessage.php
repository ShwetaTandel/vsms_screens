<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$hazardNumber = $_GET['id'];
$action = $_GET['action'];
?>
<html>
    <head>
        <title>VSMS - Report Hazard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js" type="text/javascript"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <script src="../js/popper.min.js"></script>
        

    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Report Hazard</h1>      
            </div>
            <br/>
            <div class="alert alert-success" role="alert" <?php if ($action !== "NEW") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Thank You! Your Hazard Report <a href="reportHazard.php?action=VIEW&hid=<?php echo $hazardNumber;?>">(HZ<?php echo $hazardNumber;?>)</a> will be addressed as soon as possible.</h4>
               
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Hazards </b> tab on Home screen.</p>
            </div>
            <div class="alert alert-success" role="alert" <?php if ($action !== "L2_APPROVED") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Thank You. This Hazard Report (HZ<?php echo $hazardNumber;?>) has now been closed off.</h4>
                <p style="color: red;font-weight: bolder ;font-size: large" class="text-center">If there are still outstanding actions left associated with this Hazard Report, its status will change to Outstanding Actions. Otherwise, it will change to Closed</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Hazards </b> tab on Home screen.</p>
            </div>
              <div class="alert alert-success" role="alert" <?php if ($action !== "L2_SAVED") { ?>style="display:none;"<?php } ?>>
                  <h4 class="alert-heading text-center">Hazard Report <a href="reportHazard.php?action=HI&hid=<?php echo $hazardNumber;?>">(HZ<?php echo $hazardNumber;?>)</a> has been saved.</h4>
                <p style="color: red;font-weight: bolder; font-size: large" class="text-center">To close off the Hazard Report, you can press the Submit & Close off button.</p>
                <hr>
                <p class="mb-0 text-center">You can track its progress on <b> Your Site Hazards </b> tab on Home screen.</p>
            </div>
             <div class="alert alert-success" role="alert" <?php if ($action !== "REPORT_DELETE") { ?>style="display:none;"<?php } ?>>
                <h4 class="alert-heading text-center">Hazard Report (HZ<?php echo $hazardNumber;?>) has been deleted.</h4>
                <hr>
            </div>


            <div class="pull-right">
                   <a class="btn btn-dark" href="home.php" id="btnBack"><i class="fa fa-arrow-left"></i> Back To Home</a>
            </div>
        </div>
    </body>
</html>
