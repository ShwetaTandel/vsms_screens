<?php

require('../pdf/fpdf.php');
define('POUND', chr(163) );

class PDF_Invoice extends FPDF
{
// private variables
var $colonnes;
var $format;
var $angle=0;
var $logoFile = "../images/vel_logo_large.jpg";
var $logoXPos = 50;
var $logoYPos = 108;
var $logoWidth = 110;


function addLogo(){
    global $logoFile,$logoXPos,$logoYPos,$logoWidth;
    $this->Image( $logoFile, $logoXPos, $logoYPos, $logoWidth );
}
function addSeperator(){
    $this->Line(0, 19  , 220, 19);
}
function addLineSeperator(){
	$this->SetDash();
    $this->Line(10, $this->GetY()+5  , 200, $this->GetY()+5);
}
function addDashedSeperator(){
	$this->SetLineWidth(0.1);
		$this->SetDash(1,1); //1mm on, 1mm off
		$this->Line(10,$this->GetY()+5,200,$this->GetY()+5);
}
// private functions
function RoundedRect($x, $y, $w, $h, $r, $style = '')
{
    $k = $this->k;
    $hp = $this->h;
    if($style=='F')
        $op='f';
    elseif($style=='FD' || $style=='DF')
        $op='B';
    else
        $op='S';
    $MyArc = 4/3 * (sqrt(2) - 1);
    $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
    $xc = $x+$w-$r ;
    $yc = $y+$r;
    $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

    $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
    $xc = $x+$w-$r ;
    $yc = $y+$h-$r;
    $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
    $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
    $xc = $x+$r ;
    $yc = $y+$h-$r;
    $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
    $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
    $xc = $x+$r ;
    $yc = $y+$r;
    $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
    $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
    $this->_out($op);
}

function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
{
    $h = $this->h;
    $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
                        $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
}

function Rotate($angle, $x=-1, $y=-1)
{
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
    }
}

function _endpage()
{
    if($this->angle!=0)
    {
        $this->angle=0;
        $this->_out('Q');
    }
    parent::_endpage();
}

// public functions
function sizeOfText( $texte, $largeur )
{
    $index    = 0;
    $nb_lines = 0;
    $loop     = TRUE;
    while ( $loop )
    {
        $pos = strpos($texte, "\n");
        if (!$pos)
        {
            $loop  = FALSE;
            $ligne = $texte;
        }
        else
        {
            $ligne  = substr( $texte, $index, $pos);
            $texte = substr( $texte, $pos+1 );
        }
        $length = floor( $this->GetStringWidth( $ligne ) );
        $res = 1 + floor( $length / $largeur) ;
        $nb_lines += $res;
    }
    return $nb_lines;
}

// Company
function addCompanyName( $name, $address , $vat)
{
    $x1 = 10;
    $y1 = 5;
    $this->SetXY( $x1, $y1 );
    $this->SetFont('Arial','B',12);
    $length = $this->GetStringWidth( $name );
    $this->Cell( $length, 2, $name);
    $this->SetXY( $x1, $y1 + 5 );
    $this->SetFont('Arial','',10);
    $length = $this->GetStringWidth( $address );
    $this->Cell($length, 2, $address);
    $this->SetXY( $x1, $y1 + 10 );
    $this->SetFont('Arial','',9);
    $length = $this->GetStringWidth( $vat );
    $this->Cell($length, 2, $vat);

}

function addIncidentHeaderTitle($iid)
{
    $x     = 40;
    $y     = 25;
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 20);
    $this->Cell( 40, 2, "DETAILED INCIDENT REPORT #IR".$iid);
    
}

function addSubHeader($subHeader, $x, $y)
{
    //$x     = 50;
    //$y     = 25;
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 15);
    $this->Cell( 40, 2,$subHeader);
    
}
function addSubSubHeader($subHeader, $x, $y)
{
    //$x     = 50;
    //$y     = 25;
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 12);
    $this->Cell( 40, 2,$subHeader);
    
}
function addTitle($title, $x, $y)
{
    //$x     = 50;
    //$y     = 25;
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2,$title);
    
}
function addPaymentTermsAlt($terms){
    $r1     = 160;
    $y1     = 55;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Payment Terms:");
    $this->SetXY( $r1, $y1+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 60, 4, $terms);
}

function addInvoiceNumber( $invoiceId )
{
    $r1     = 160;
    $y1     = 70;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Invoice Number:");
    $this->SetXY( $r1, $y1+3);
    $this->SetFont( "Arial", "B", 10);
    $this->MultiCell( 60, 4, $invoiceId);
}

// Client address
function addDeliveryAddress( $adresse )
{
    $r1     = 10;
    $y1     = 55;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Delivery Location:");
    $this->SetXY( $r1, $y1+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 105, 4, $adresse);
}

// Client address
function addSupplier( $adresse )
{
    $r1     = 10;
    $y1     = 35;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Order to:");
    $this->SetXY( $r1, $y1+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 60, 4, $adresse);
}

function addSingleColumnLabelValue( $label, $value, $x, $y )
{
    
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, $label);
    $this->SetXY( $x, $y+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 50, 4, $value);
}
function addTripleColumnLabelValue( $label, $value, $x, $y )
{
    
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 80, 2, $label);
    $this->SetXY( $x, $y+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 140, 4, $value);
}

function addQuadrapleColumnLabelValue( $label, $value, $x, $y )
{
    
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 10);
    $lineNos = $this->GetStringWidth($value)/110;
    $this->Cell( 80, 2, $label);
    $this->SetXY( $x, $y+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 190, 4, $value);
}
function SetDash($black=null, $white=null)
    {
        if($black!==null)
            $s=sprintf('[%.3F %.3F] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }
function addDoubleColumnLabelValue( $label, $value, $x, $y )
{
    
    $this->SetXY( $x, $y);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 50, 2, $label);
    $this->SetXY( $x, $y+3);
    $this->SetFont( "Arial", "", 10);
    $this->MultiCell( 100, 4, $value);
}

// Better table
function CreateLicenseTable($header, $tableData)
{
    // Column widths
    $w = array(70, 70, 40);
    // Header
   $this->SetFont('Arial','B', 10);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'LR');
    $this->Ln();
    // Data
    $this->SetFont('Arial','', 10);
    foreach($tableData as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR');
        $this->Cell($w[1],6,$row[1],'LR');
        $this->Cell($w[2],6,$row[2],'LR');
        //$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
        $this->Ln();
    }
    // Closing line
    $this->Cell(array_sum($w),0,'','T');
}
function addOrderReference( $order )
{
    $r1     = 160;
    $y1     = 35;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Order Reference:");
    $this->SetXY( $r1, $y1+4);
    $this->SetFont( "Arial", "B", 15);
    $this->MultiCell( 60, 4, "VEPO".$order);
}
function addOrderBy($dept,$email,$phone )
{
    $r1     = 155;
    $y1     = 55;
    $this->SetXY( $r1, $y1);
    $this->SetFont( "Arial", "B", 10);
    $this->Cell( 40, 2, "Ordered By:");
    $this->SetXY( $r1, $y1+4);
    $this->SetFont( "Arial", "", 10);
    $this->Cell( 40, 4, $dept);
    $this->SetXY( $r1, $y1+8);
    $this->SetFont( "Arial", "", 10);
    $this->Cell(50,4 ,$email,'','','C',false, "mailto:".$email);
    $this->SetXY( $r1, $y1+12);
    $this->SetFont( "Arial", "", 10);
    $this->Cell( 40, 4, $phone);

}


// Label and number of invoice/estimate
function fact_dev( $label, $num )
{
    $r1  = $this->w - 200;
    $r2  = $r1 + 68;
    $y1  = 17;
    $y2  = $y1 + 2;
    $mid = ($r1 + $r2 ) / 2;
    
    $texte  = $label . " EN " . EURO . " N° : " . $num;    
    $szfont = 12;
    $loop   = 0;
    
    while ( $loop == 0 )
    {
       $this->SetFont( "Arial", "B", $szfont );
       $sz = $this->GetStringWidth( $texte );
       if ( ($r1+$sz) > $r2 )
          $szfont --;
       else
          $loop ++;
    }

    $this->SetLineWidth(0.1);
    $this->SetFillColor(192);
    $this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 2.5, 'DF');
    $this->SetXY( $r1+1, $y1+2);
    $this->Cell($r2-$r1 -1,5, $texte, 0, 0, "C" );
}

// Estimate
function addEstimate( $numdev )
{
    $string = sprintf("DEV%04d",$numdev);
    $this->fact_dev( "Devis", $string );
}

// Invoice
function addFacture( $numfact )
{
    $string = sprintf("FA%04d",$numfact);
    $this->fact_dev( "Facture", $string );
}



// Mode of payment
function addDeliveryDate( $date )
{
    $r1  = 10;
    $r2  = $r1 + 40;
    $y1  = 80;
    $y2  = $y1+10;
    $mid = $y1 + (($y2-$y1) / 2);
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1, $mid, $r2, $mid);
    $this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1+1 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(10,4, "Delivery Date", 0, 0, "C");
    $this->SetXY( $r1 + ($r2-$r1)/2 -5 , $y1 + 5 );
    $this->SetFont( "Arial", "", 10);
    $this->Cell(10,5,$date, 0,0, "C");
}

// Expiry date
function addPaymentTerms( $terms )
{
    $r1  = 55;
    $r2  = $r1 + 40;
    $y1  = 80;
    $y2  = $y1+10;
    $mid = $y1 + (($y2-$y1) / 2);
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1, $mid, $r2, $mid);
    $this->SetXY( $r1 + ($r2 - $r1)/2 - 5 , $y1+1 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(10,4, "Payment Terms", 0, 0, "C");
    $this->SetXY( $r1 + ($r2-$r1)/2 - 5 , $y1 + 5 );
    $this->SetFont( "Arial", "", 10);
    $this->Cell(10,5,$terms, 0,0, "C");
}


function addOrderDate($date)
{
     $r1  = 100;
    $r2  = $r1 + 40;
    $y1  = 80;
    $y2  = $y1+10;
    $mid = $y1 + (($y2-$y1) / 2);
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1, $mid, $r2, $mid);
    $this->SetXY( $r1 + ($r2 - $r1)/2 - 5 , $y1+1 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(10,4, "Order Date", 0, 0, "C");
    $this->SetXY( $r1 + ($r2-$r1)/2 - 5 , $y1 + 5 );
    $this->SetFont( "Arial", "", 10);
    $this->Cell(10,5 ,$date,0,0,"C");

}

// Invoic emails address
function addInvoiceEmail()
{
     $r1  = 145;
    $r2  = $r1 + 55;
    $y1  = 80;
    $y2  = $y1+10;
    $mid = $y1 + (($y2-$y1) / 2);
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1, $mid, $r2, $mid);
    $this->SetXY( $r1 + ($r2 - $r1)/2 - 5 , $y1+1 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(10,4, "Send invoices to", 0, 0, "C");
    $this->SetXY( $r1 + ($r2-$r1)/2 - 5 , $y1 + 5 );
    $this->SetFont( "Arial", "", 10);
    $this->Cell(10,5 ,"veu-accounts@vantec-gl.com",'','','C',false, "mailto:veu-accounts@vantec-gl.com");

}


function addDepartmentToCharge($ref)
{
    $this->SetFont( "Arial", "B", 10);
    $length = $this->GetStringWidth( "Departments To Charge : ");
    $r1  = 10;
    $r2  = $r1 + $length;
    $y1  = 80;
    
    $this->SetXY( $r1 , $y1 );
    
    $this->Cell($length,4, "Departments To Charge : " );
    $this->SetFont( "Arial", "", 10);
    $this->SetXY( $r2 , $y1 );
    $this->MultiCell(145,4,  $ref);
}

function addRecharge($recharge)
{
    $this->SetFont( "Arial", "B", 10);
    $length = $this->GetStringWidth( "Recharge : ");
    $r1  = 10;
    $r2  = $r1 + $length;
    $y1  = 70;
    
    $this->SetXY( $r1 , $y1 );
    
    $this->Cell($length,4, "Recharge : " );
    $this->SetFont( "Arial", "", 10);
    $this->SetXY( $r2 , $y1 );
    $this->MultiCell(145,4,  $recharge);
}
function addExpenseType($expense)
{
    $this->SetFont( "Arial", "B", 10);
    $length = $this->GetStringWidth( "Expense Type/Code : ");
    $r1  = 10;
    $r2  = $r1 + $length;
    $y1  = 75;
    
    $this->SetXY( $r1 , $y1 );
    
    $this->Cell($length,4, "Expense Type/Code : " );
    $this->SetFont( "Arial", "", 10);
    $this->SetXY( $r2 , $y1 );
    $this->MultiCell(170,4,  $expense);
}



function addItemsTable( $tab ,$y1, $y2)
{
    global $colonnes;
    $r1  = 10;
    $r2  = $this->w - ($r1 * 2) ;
    //$y1  = 90;
   // $y2  = 175;//$this->h - 120 - $y1;
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1, $y1 );
    $this->Rect( $r1, $y1, $r2, $y2, "D");
    $this->Line( $r1, $y1+6, $r1+$r2, $y1+6);
    $colX = $r1;
    $colonnes = $tab;
    while ( list( $lib, $pos ) = each ($tab) )
    {
        $this->SetXY( $colX, $y1+2 );
        $this->Cell( $pos, 1, $lib, 0, 0, "C");
        $colX += $pos;
        $this->Line( $colX, $y1, $colX, $y1+$y2);
    }
}
function addCurrenyFrameAlt($sub, $disc,$shipp,$tot, $y1){
    $r1  = 10;
    $r2  = $this->w - ($r1 * 2) ;
    //$y1  = 265;
    $y2  = 7;//$this->h - 120 - $y1;
    $this->SetXY( $r1+55, $y1 );
    $this->Rect( $r1, $y1, $r2, $y2, "D");
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(140,8,  'SUB-TOTAL: '. POUND.$sub.'  SHIPPING:'.POUND.$shipp.'    TOTAL COST Exc VAT: '.POUND.$tot);
    
}

function addInvoiceCols( $tab, $y1, $y2 )
{
    global $colonnes;
    ChromePhp::log('Dimensions'.$y1.'---'.$y2);
    $r1  = 10;
    $r2  = $this->w - ($r1 * 2) ;
    //$y1  = 170;
    //$y2  = 67;//$this->h - 120 - $y1;
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1, $y1 );
    $this->Rect( $r1, $y1, $r2, $y2, "D");
    $this->Line( $r1, $y1+6, $r1+$r2, $y1+6);
    $colX = $r1;
    $colonnes = $tab;
    while ( list( $lib, $pos ) = each ($tab) )
    {
        $this->SetXY( $colX, $y1+2 );
        $this->Cell( $pos, 1, $lib, 0, 0, "C");
        $colX += $pos;
        $this->Line( $colX, $y1, $colX, $y1+$y2);
    }
}
function addInvoiceTotal($invoiceTot, $outstanding, $y1){
    $r1  = 10;
    $r2  = $this->w - ($r1 * 2) ;
  //  $y1  = 238;
    $y2  = 8;//$this->h - 120 - $y1;
    
    $this->Rect( $r1, $y1, $r2, $y2, "D");
    $this->SetXY( $r1+78, $y1 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(140,8,  'INVOICE TOTAL: '. POUND.$invoiceTot.'    OUTSTANDING AMOUNT:'.POUND.$outstanding);
    
}

function addLineFormat( $tab )
{
    global $format, $colonnes;
    
    while ( list( $lib, $pos ) = each ($colonnes) )
    {
        if ( isset( $tab["$lib"] ) )
            $format[ $lib ] = $tab["$lib"];
    }
}

function lineVert( $tab )
{
    global $colonnes;

    reset( $colonnes );
    $maxSize=0;
    while ( list( $lib, $pos ) = each ($colonnes) )
    {
        $texte = $tab[ $lib ];
        $longCell  = $pos -2;
        $size = $this->sizeOfText( $texte, $longCell );
        if ($size > $maxSize)
            $maxSize = $size;
    }
    return $maxSize;
}

// add a line to the invoice/estimate
/*    $ligne = array( "REFERENCE"    => $prod["ref"],
                      "DESIGNATION"  => $libelle,
                      "QUANTITE"     => sprintf( "%.2F", $prod["qte"]) ,
                      "P.U. HT"      => sprintf( "%.2F", $prod["px_unit"]),
                      "MONTANT H.T." => sprintf ( "%.2F", $prod["qte"] * $prod["px_unit"]) ,
                      "TVA"          => $prod["tva"] );
*/
function addLine( $ligne, $tab )
{
    global $colonnes, $format;

    $ordonnee     = 10;
    $maxSize      = $ligne;
    

    reset( $colonnes );
    while ( list( $lib, $pos ) = each ($colonnes) )
    {
        $longCell  = $pos -2;
        $texte     = $tab[ $lib ];
        $length    = $this->GetStringWidth( $texte );
        $tailleTexte = $this->sizeOfText( $texte, $length );
        $formText  = $format[ $lib ];
        $this->SetXY( $ordonnee, $ligne-1);
        $this->SetFont( "Arial", "", 8);
        $this->MultiCell( $longCell, 4 , $texte, 0, $formText);
        if ( $maxSize < ($this->GetY()  ) )
            $maxSize = $this->GetY() ;
        $ordonnee += $pos;
    }
    return ( $maxSize - $ligne );
}

function addFinanceNotes($remarks, $z)
{
    $this->SetFont( "Arial", "", 10);
    $length = $this->GetStringWidth( "Notes for Finance Department : " . $remarks );
    $r1  = 10;
    $r2  = $r1 + 190;
    $y1  = $this->h - $z;
    $y2  = $y1+30;
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1, $y1+6, $r2, $y1+6); 
    $this->SetXY( $r1 , $y1 +2 );
    $this->SetFont( "Arial", "B", 10);
    $this->Cell(40,4, "Notes for Finance Department");
    $this->SetXY( $r1 , $y1 +7 );
    $this->SetFont( "Arial", "", 8);
    $this->MultiCell(180,4,  $remarks);
}

function addDisclaimer()
{
    $this->SetFont( "Arial", "", 10);
    $r1  = 10;
    $y1  = $this->h - 30;
    $this->SetFont( "Arial", "B", 10);
    $this->SetXY( $r1 , $y1 );
    $this->MultiCell(190,4, "Disclaimer: Order Reference number is to be quoted on ALL documents and packages pertaining to this order. Failure to quote order number, provide completion of works or delivery notes may result in delay of payment." );
}

function addCurrencyFrame($sub, $disc,$shipp,$tot)
{
    $r1  = $this->w - 70;
    $r2  = $r1 + 60;
    $y1  = $this->h - 115;
    $y2  = $y1+30;
    $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2-$y1), 2.5, 'D');
    $this->Line( $r1+40,  $y1, $r1+40, $y2); 
    $this->Line( $r1+40, $y1+6, $r2, $y1+6); 
    $this->Line( $r1, $y1+24, $r2, $y1+24);
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1+22, $y1 );
    $this->Cell(55,5, "POUNDS (".POUND.')', 0, 0, "C");
    
    //Sub total title - value
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1, $y1+10 );
    $this->Cell(20,4, "SUB-TOTAL", 0, 0, "C");
    $this->SetFont( "Arial", "B", 10);
    $this->SetXY( $r1+35, $y1+10 );
    $this->Cell(20,4, $sub, 0, 0, "R");
    
    //Discount title value
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1-1, $y1+15 );
    $this->Cell(20,4, "DISCOUNT", 0, 0, "C");
    $this->SetFont( "Arial", "B", 10);
    $this->SetXY( $r1+35, $y1+15 );
    $this->Cell(20,4, $disc, 0, 0, "R");
    
    
    //shiiping title value
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1-1.5, $y1+20 );
    $this->Cell(20,4, "SHIPPING", 0, 0, "C");
    $this->SetFont( "Arial", "B", 10);
    $this->SetXY( $r1+35, $y1+20 );
    $this->Cell(20,4, $shipp, 0, 0, "R");
    
    
    //Total codst title value
    $this->SetFont( "Arial", "B", 8);
    $this->SetXY( $r1+7.5, $y1+25 );
    $this->Cell(20,4, "TOTAL COST Exc VAT", 0, 0, "C");
    $this->SetFont( "Arial", "B", 10);
    $this->SetXY( $r1+35, $y1+25 );
    $this->Cell(20,4, $tot, 0, 0, "R");
    
}

// remplit les cadres TVA / Totaux et la remarque
// params  = array( "RemiseGlobale" => [0|1],
//                      "remise_tva"     => [1|2...],  // {la remise s'applique sur ce code TVA}
//                      "remise"         => value,     // {montant de la remise}
//                      "remise_percent" => percent,   // {pourcentage de remise sur ce montant de TVA}
//                  "FraisPort"     => [0|1],
//                      "portTTC"        => value,     // montant des frais de ports TTC
//                                                     // par defaut la TVA = 19.6 %
//                      "portHT"         => value,     // montant des frais de ports HT
//                      "portTVA"        => tva_value, // valeur de la TVA a appliquer sur le montant HT
//                  "AccompteExige" => [0|1],
//                      "accompte"         => value    // montant de l'acompte (TTC)
//                      "accompte_percent" => percent  // pourcentage d'acompte (TTC)
//                  "Remarque" => "texte"              // texte
// tab_tva = array( "1"       => 19.6,
//                  "2"       => 5.5, ... );
// invoice = array( "px_unit" => value,
//                  "qte"     => qte,
//                  "tva"     => code_tva );
function addTVAs( $params, $tab_tva, $invoice )
{
    $this->SetFont('Arial','',8);
    
    reset ($invoice);
    $px = array();
    while ( list( $k, $prod) = each( $invoice ) )
    {
        $tva = $prod["tva"];
        @ $px[$tva] += $prod["qte"] * $prod["px_unit"];
    }

    $prix     = array();
    $totalHT  = 0;
    $totalTTC = 0;
    $totalTVA = 0;
    $y = 261;
    reset ($px);
    natsort( $px );
    while ( list($code_tva, $articleHT) = each( $px ) )
    {
        $tva = $tab_tva[$code_tva];
        $this->SetXY(17, $y);
        $this->Cell( 19,4, sprintf("%0.2F", $articleHT),'', '','R' );
        if ( $params["RemiseGlobale"]==1 )
        {
            if ( $params["remise_tva"] == $code_tva )
            {
                $this->SetXY( 37.5, $y );
                if ($params["remise"] > 0 )
                {
                    if ( is_int( $params["remise"] ) )
                        $l_remise = $param["remise"];
                    else
                        $l_remise = sprintf ("%0.2F", $params["remise"]);
                    $this->Cell( 14.5,4, $l_remise, '', '', 'R' );
                    $articleHT -= $params["remise"];
                }
                else if ( $params["remise_percent"] > 0 )
                {
                    $rp = $params["remise_percent"];
                    if ( $rp > 1 )
                        $rp /= 100;
                    $rabais = $articleHT * $rp;
                    $articleHT -= $rabais;
                    if ( is_int($rabais) )
                        $l_remise = $rabais;
                    else
                        $l_remise = sprintf ("%0.2F", $rabais);
                    $this->Cell( 14.5,4, $l_remise, '', '', 'R' );
                }
                else
                    $this->Cell( 14.5,4, "ErrorRem", '', '', 'R' );
            }
        }
        $totalHT += $articleHT;
        $totalTTC += $articleHT * ( 1 + $tva/100 );
        $tmp_tva = $articleHT * $tva/100;
        $a_tva[ $code_tva ] = $tmp_tva;
        $totalTVA += $tmp_tva;
        $this->SetXY(11, $y);
        $this->Cell( 5,4, $code_tva);
        $this->SetXY(53, $y);
        $this->Cell( 19,4, sprintf("%0.2F",$tmp_tva),'', '' ,'R');
        $this->SetXY(74, $y);
        $this->Cell( 10,4, sprintf("%0.2F",$tva) ,'', '', 'R');
        $y+=4;
    }

    if ( $params["FraisPort"] == 1 )
    {
        if ( $params["portTTC"] > 0 )
        {
            $pTTC = sprintf("%0.2F", $params["portTTC"]);
            $pHT  = sprintf("%0.2F", $pTTC / 1.196);
            $pTVA = sprintf("%0.2F", $pHT * 0.196);
            $this->SetFont('Arial','',6);
            $this->SetXY(85, 261 );
            $this->Cell( 6 ,4, "HT : ", '', '', '');
            $this->SetXY(92, 261 );
            $this->Cell( 9 ,4, $pHT, '', '', 'R');
            $this->SetXY(85, 265 );
            $this->Cell( 6 ,4, "TVA : ", '', '', '');
            $this->SetXY(92, 265 );
            $this->Cell( 9 ,4, $pTVA, '', '', 'R');
            $this->SetXY(85, 269 );
            $this->Cell( 6 ,4, "TTC : ", '', '', '');
            $this->SetXY(92, 269 );
            $this->Cell( 9 ,4, $pTTC, '', '', 'R');
            $this->SetFont('Arial','',8);
            $totalHT += $pHT;
            $totalTVA += $pTVA;
            $totalTTC += $pTTC;
        }
        else if ( $params["portHT"] > 0 )
        {
            $pHT  = sprintf("%0.2F", $params["portHT"]);
            $pTVA = sprintf("%0.2F", $params["portTVA"] * $pHT / 100 );
            $pTTC = sprintf("%0.2F", $pHT + $pTVA);
            $this->SetFont('Arial','',6);
            $this->SetXY(85, 261 );
            $this->Cell( 6 ,4, "HT : ", '', '', '');
            $this->SetXY(92, 261 );
            $this->Cell( 9 ,4, $pHT, '', '', 'R');
            $this->SetXY(85, 265 );
            $this->Cell( 6 ,4, "TVA : ", '', '', '');
            $this->SetXY(92, 265 );
            $this->Cell( 9 ,4, $pTVA, '', '', 'R');
            $this->SetXY(85, 269 );
            $this->Cell( 6 ,4, "TTC : ", '', '', '');
            $this->SetXY(92, 269 );
            $this->Cell( 9 ,4, $pTTC, '', '', 'R');
            $this->SetFont('Arial','',8);
            $totalHT += $pHT;
            $totalTVA += $pTVA;
            $totalTTC += $pTTC;
        }
    }

    $this->SetXY(114,266.4);
    $this->Cell(15,4, sprintf("%0.2F", $totalHT), '', '', 'R' );
    $this->SetXY(114,271.4);
    $this->Cell(15,4, sprintf("%0.2F", $totalTVA), '', '', 'R' );

    $params["totalHT"] = $totalHT;
    $params["TVA"] = $totalTVA;
    $accompteTTC=0;
    if ( $params["AccompteExige"] == 1 )
    {
        if ( $params["accompte"] > 0 )
        {
            $accompteTTC=sprintf ("%.2F", $params["accompte"]);
            if ( strlen ($params["Remarque"]) == 0 )
                $this->addRemarks( "Accompte de $accompteTTC Euros exigé à la commande.");
            else
                $this->addRemarks( $params["Remarque"] );
        }
        else if ( $params["accompte_percent"] > 0 )
        {
            $percent = $params["accompte_percent"];
            if ( $percent > 1 )
                $percent /= 100;
            $accompteTTC=sprintf("%.2F", $totalTTC * $percent);
            $percent100 = $percent * 100;
            if ( strlen ($params["Remarque"]) == 0 )
                $this->addRemarks( "Accompte de $percent100 % (soit $accompteTTC Euros) exigé à la commande." );
            else
                $this->addRemarks( $params["Remarque"] );
        }
        else
            $this->addRemarks( "Drôle d'acompte !!! " . $params["Remarque"]);
    }
    else
    {
        if ( strlen ($params["Remarque"]) > 0 )
            $this->addRemarks( $params["Remarque"] );
    }
    $re  = $this->w - 50;
    $rf  = $this->w - 29;
    $y1  = $this->h - 40;
    $this->SetFont( "Arial", "", 8);
    $this->SetXY( $re, $y1+5 );
    $this->Cell( 17,4, sprintf("%0.2F", $totalTTC), '', '', 'R');
    $this->SetXY( $re, $y1+10 );
    $this->Cell( 17,4, sprintf("%0.2F", $accompteTTC), '', '', 'R');
    $this->SetXY( $re, $y1+14.8 );
    $this->Cell( 17,4, sprintf("%0.2F", $totalTTC - $accompteTTC), '', '', 'R');
    $this->SetXY( $rf, $y1+5 );
    $this->Cell( 17,4, sprintf("%0.2F", $totalTTC * EURO_VAL), '', '', 'R');
    $this->SetXY( $rf, $y1+10 );
    $this->Cell( 17,4, sprintf("%0.2F", $accompteTTC * EURO_VAL), '', '', 'R');
    $this->SetXY( $rf, $y1+14.8 );
    $this->Cell( 17,4, sprintf("%0.2F", ($totalTTC - $accompteTTC) * EURO_VAL), '', '', 'R');
}

// add a watermark (temporary estimate, DUPLICATA...)
// call this method first
function addWatermark( $texte )
{
    $this->SetFont('Arial','B',50);
    $this->SetTextColor(203,203,203);
    $this->Rotate(45,55,190);
    $this->Text(55,190,$texte);
    $this->Rotate(0);
    $this->SetTextColor(0,0,0);
}
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

}
?>