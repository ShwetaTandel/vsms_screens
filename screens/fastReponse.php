<?php
//header("Content-type: application/vnd.ms-word");
//header("Content-Disposition: attachment; Filename=VSMS-Fast Response.doc");
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
$iid = $_GET['iid'];
$serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/incidentsData.php?filter=INCIDENT&iid=" . $iid;
$data = json_decode(file_get_contents($serviceUrl));
$incidentDate = new DateTime($data[0]->incident_date);
$immdAction = '';
ChromePhp::log($data);
$itemCnt = sizeof($data[0]->allActions);
$cnt=1;
for ($i = 0; $i < $itemCnt; $i++) {
    if ($data[0]->allActions[$i]->category === 'Incident-Containment') {
        $immdAction = $immdAction . ($cnt ) . ') ' . $data[0]->allActions[$i]->description . "\r\n";
        $cnt++;
    }
}
$path = $dir . $data[0]->incident_number . "/Photos";
//echo $dir;
$files = glob($path . '/*.*');
$i = 0;
$link1 = '';
$link2 = '';
foreach ($files as $jpg) {
    //echo basename($jpg), "\n";
    if ($i === 0) {
        $link1 = "http://$_SERVER[HTTP_HOST]" . "/VSMS/VSMSUploads/" . $data[0]->incident_number . "/Photos/" . basename($jpg);
    } else if ($i === 1) {
        $link2 = "http://$_SERVER[HTTP_HOST]" . "/VSMS/VSMSUploads/" . $data[0]->incident_number . "/Photos/" . basename($jpg);
    }
    $i++;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
            <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
            <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
            <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
            <style>
                @page Section2 {size:841.7pt 595.45pt;mso-page-orientation:landscape;}
                div.Section2 {page:Section2;}

                #div1 {width:900px;height:900px;padding:10px;border:1px solid #aaaaaa;}

            </style>
    </head>
    <body>

        <div class="container">

            <header>
                <div class="row">
                    <div class="col-6 pull-left">
                        <h1 >Fast Response Brief</h1>
                    </div>
                    <div class="col-3"></div>
                    <div class="col-3 pull-right"> 
                        <img  src="../images/logoTest.jpg" alt=""/>
                    </div>

                </div>
                <!--STYLE="position:absolute; top:10px; right:10px; height: 60px; width: 200px" -->
            </header>


            <hr style="border-top:solid;color: dark;width: 100%;height: 5px" /> 

            <div class="row border border-dark">
                <div class="col-4">
                    <label style="font-family: calibri;font-size: x-large;font-weight: bolder">Incident Date: </label>
                    <span style="font-family: calibri;font-size: larger;font-weight: bold">  <?php echo $incidentDate->format('d-m-Y') ?>  </span>
                </div>
                <div class="col-4">
                    <label style="font-family: calibri;font-size: x-large;font-weight: bolder"> Incident Time: </label>
                    <span style="font-family: calibri;font-size: larger;font-weight: bold"> <?php echo $incidentDate->format('H:i:s') ?> </span>
                </div>

                <div class="col-4">
                    <label style="font-family: calibri;font-size: x-large;font-weight: bolder"> Location: </label>
                    <span style="font-family: calibri;font-size: larger;font-weight: bold"> <?php echo $data[0]->code ?></span>
                </div>

            </div>
            <div class="row">
                <div class="col-8">
 
                    <div class="row">

                        <label style="font-family: calibri;font-size: x-large;font-weight: bolder">Detail of Concerns: </label>
                        <br/>
                        <textarea style="width:700px;height: 300px" maxlength="600"> <?php echo $data[0]->incident_detail ?> </textarea>


                    </div>
                    <div class="row">

                        <label style="font-family: calibri;font-size: x-large;font-weight: bolder">Key Points:</label>
                        <br/>
                        <textarea style="width: 700px;height: 300px" maxlength="600"> <?php echo $data[0]->key_points ?> </textarea>


                    </div>
                    <div class="row">

                        <label style="font-family: calibri;font-size: x-large;font-weight: bolder">Immediate Actions: </label>
                        <br/>
                        <textarea style="width: 700px;height: 300px"><?php echo $immdAction ?></textarea>


                    </div>
                </div>
                <div class="col-4">
                    <div class="row">

                        <img  src="<?php echo$link1 ?>" style="position:absolute;width: 400px; top:45px;height: 450px;border: solid 2px" ></img>


                    </div>
                    <div class="row">

                        <img  src="<?php echo$link2 ?>" style="position:absolute;width: 400px; top:580px;height: 450px;border: solid 2px"></img>


                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center" ><span style="text-decoration:underline;color:red;font-size: x-large;font-weight: bold"> <?php echo $data[0]->main_message ?> </span></div>

            </div>
            <footer>
                <div class="row">
                 <div class="col-8 pull-left">
                       <span>Hitachi Transport System Group</span>
                    </div>
                    
                    <div class="col-4 pull-right"> 
                       <span class="text-right">Vantec Corporation.2020.All Rights Reserved.</span>
                    </div>
                </div>
               
            </footer>
        </div>
    </body>
    <script>
        /* function showButton() {
         
         document.getElementById('xButton').style.display = 'block';
         }
         
         document.getElementById('xButton').addEventListener("click", function () {
         document.getElementById('banner').style.display = 'none';
         document.getElementById('xButton').style.display = 'none';
         });
         
         $("#drag1").draggable();
         $("#div1").droppable({
         drop: function (ev, ui) {
         // console.log(ui);
         ev.preventDefault();
         
         var data = ui.draggable.attr("id");
         
         ev.target.appendChild(document.getElementById(data));
         //$("#" + data).css({top: ui.offset.top, left: ui.offset.left, position: 'absolute'});
         }
         });*/
    </script>