<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$reportId = $_GET['id'];
$actionId = $_GET['actionId'];
$action = $_GET['action'];
$source = $_GET['source'];
?>
<html>
    <head>
        <title>VSMS - Action</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>

    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div  <?php if ($action !== "CHANGE") { ?>style="display:none;"<?php } ?>>
                <div class="page-header">
                    <h1 class="text-center">Re-schedule Action</h1>      
                </div>
                <br/>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Incident') { ?>style="display:none;"<?php } ?>>The estimated completion date for this Action (AC<?php echo $actionId; ?>) associated with report (IR<?php echo $reportId; ?>) has now been changed.</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Other') { ?>style="display:none;"<?php } ?>>The estimated completion date for this Action (AC<?php echo $actionId; ?>) has now been changed.</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Hazard') { ?>style="display:none;"<?php } ?>>The estimated completion date for this Action (AC<?php echo $actionId; ?>) associated with report (HZ<?php echo $reportId; ?>) has now been changed.</h4>
                    <hr>
                </div>
            </div>
            <div  <?php if ($action !== "COMMENTS") { ?>style="display:none;"<?php } ?>>
                <div class="page-header">
                    <h1 class="text-center">Save Action</h1>      
                </div>
                <br/>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Incident') { ?>style="display:none;"<?php } ?>>The comments have been saved for Action (AC<?php echo $actionId; ?>) associated with report (IR<?php echo $reportId; ?>).</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Other') { ?>style="display:none;"<?php } ?>>The comments have been saved for Action (AC<?php echo $actionId; ?>).</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Hazard') { ?>style="display:none;"<?php } ?>>The comments have been saved for Action (AC<?php echo $actionId; ?>) associated with report (HZ<?php echo $reportId; ?>).</h4>
                    <hr>
                </div>
            </div>
            <div <?php if ($action !== "CANCEL") { ?>style="display:none;"<?php } ?>>
                <div class="page-header">
                    <h1 class="text-center">Cancel Action</h1>      
                </div>
                <br/>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Incident') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (IR<?php echo $reportId; ?>) has now been cancelled.</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Other') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) has now been cancelled.</h4>
                     <h4 class="alert-heading text-center" <?php if ($source !== 'Hazard') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (HZ<?php echo $reportId; ?>) has now been cancelled.</h4>
                    <hr>
                </div>

            </div>
            <div <?php if ($action !== "COMPLETE") { ?>style="display:none;"<?php } ?>>
                <div class="page-header">
                    <h1 class="text-center">Close Action</h1>      
                </div>
                <br/>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Incident') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (IR<?php echo $reportId; ?>) has now been closed off.</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Other') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) has now been closed off.</h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Hazard') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (HZ<?php echo $reportId; ?>) has now been closed off.</h4>
                    <hr>
                </div>
            </div>
            <div <?php if ($action !== "NEW") { ?>style="display:none;"<?php } ?>>
                <div class="page-header">
                    <h1 class="text-center">New Action</h1>      
                </div>
                <br/>
                <div class="alert alert-success" role="alert">
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Incident') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (IR<?php echo $reportId; ?>) has now been created. </h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Other') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) has now been created. </h4>
                    <h4 class="alert-heading text-center" <?php if ($source !== 'Hazard') { ?>style="display:none;"<?php } ?>>This Action (AC<?php echo $actionId; ?>) associated with report (HZ<?php echo $reportId; ?>) has now been created. </h4>
                    <p class="text-center">The email has been sent to the selected owner and their manager.</p>
                    <hr>
                </div>
            </div>
            <div class="pull-right">
                <button class="btn btn-dark" onclick = "goBack()" id="btnBack">< Back</button>
            </div>
        </div>
    </body>
    <script>
        function goBack(){
            var action = '<?php echo $action?>';
            if(action === 'NEW'){
               window.open("home.php", "_self");
            }else{
                window.open("showOutstandingActions.php", "_self");
            }
            
        }
    </script>
</html>
