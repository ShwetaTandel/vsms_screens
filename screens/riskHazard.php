<div class="modal fade" id="hazardModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 80%;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Hazard Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="panel-group" id="accordionGroupClosed" role="tablist" aria-multiselectable="true">
                    <div id="hazardOverlayDiv">
                          <img src="../images/loading3.gif" alt="Wait" alt="Loading"/>
                          <div id="hazardOverlay">
                                   Uploading data and attached files. Please refrain from clicking anywhere......
                         </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">

                            </h4>
                        </div>
                        <div id="collapseCloseOne" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="clonedCounterMeasure" id="otherCounterMeasureDiv1" >
                                    <div class="row" >
                                        <input type="hidden" value="0" id = "hazardId" class = "hazardId"/>
                                        <input type="hidden" value="0" id = "modalGraId" class = "modalGraId"/>

                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Hazard(Associated Risk) </label>
                                                <textarea rows ="2" class="form-control hazardDetails mandatory" id="hazardDetails" maxlength="1500" ></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Persons at risk</label>
                                                <textarea rows ="2" class="form-control personsAtRisk mandatory" id="personsAtRisk" maxlength="1500" ></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Existing controls </label>
                                                <textarea rows ="2" class="form-control existingControls mandatory" id="existingControls" maxlength="1500" ></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Severity</label><br/>
                                                <input type="range" min="1" max="5"  class="slider mandatory" id="riskRatingS" onchange="calcRisk(this)"/>
                                                <output id="valriskRatingS" class="sliderval"></output>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Likelihood</label><br/>
                                                <input type="range" min="1" max="5"  class="slider mandatory" id="riskRatingL" onchange="calcRisk(this)"/>
                                                <output id="valriskRatingL" class="sliderval"></output>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">Risk rating </label><br/>
                                                <input type="range" min="1" max="25" class="slider" id="riskRatingR" disabled="true"/>
                                                <output id="valriskRatingR" class="sliderval"></output>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <hr style="border-top: dotted;color: gainsboro" />
                                    <div class="row" id="actionTableRow"> 
                                        <div class="col-md-12">
                                            <label class="control-label">Further Actions required<span style="color: red">*</span></label>
                                            <input type="checkbox" id="isFurtherActionRequired" checked="true" onchange="showActionTable()"/>
                                            <table class="table table-bordered table-hover" id="tab_action">
                                                <thead>
                                                <th style="display:none" id="actionCol1">Action ID</th>
                                                <th >Actions</th>
                                                <th >Action Type</th>
                                                <th >Site</th>
                                                <th >Owner</th>
                                                <th >Deadline</th>
                                                <th style="display:none" id="actionCol2">Status</th>
                                                <th><a href="javascript:void(0);" style="font-size:18px;" id="addAction" title="Add New">
                                                        <span class="fa fa-plus"></span>
                                                    </a>
                                                </th>
                                                </thead>
                                                <tbody>
                                                    <tr id='actionRow0'>
                                                        <td class='actionIdCol' style="display:none"><input type="text" value="0" name = "actionId[]" class="form-control actionId"/></td>
                                                        <td ><textarea rows = "1" name='action[]'  class="form-control action" maxlength="250"></textarea></td>
                                                        <td> <select class="custom-select actionSubType" name="actionSubType[]">
                                                                <option value=""></option>
                                                                <option value="Behaviour">Behaviour</option>
                                                                <option value="Condition">Condition</option>
                                                                <option value="Equipment">Equipment</option>
                                                                <option value="Training">Training</option>
                                                                <option value="Procedure">Procedure</option>
                                                                <option value="Other">Other</option>
                                                            </select></td>
                                                        <td > <select class="custom-select actionSite" name="actionSite[]" onchange="populateOwners(this)">
                                                                <?php
                                                                include ('../config/phpConfig.php');
                                                                if (mysqli_connect_errno()) {
                                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                                }
                                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.SITE;');
                                                                echo "<option value=''></option>";
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                                                }
                                                                echo '';
                                                                mysqli_close($con);
                                                                ?>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <select class="custom-select actionOwner" name="actionOwner[]">
                                                                <option value=""></option>
                                                            </select>
                                                        </td>
                                                        <td><input type="date" name="deadline[]"  class="form-control deadline"/></td>
                                                        <td class='actionStatusCol' style="display:none"><input type="text" value="" name = "actionStatus[]" class="form-control actionStatus"  readonly="true"/></td>
                                                        <td><a href='javascript:void(0);'  class='removeAction'><span class='fa fa-remove'></span></a></td>

                                                    </tr>

                                                </tbody>
                                            </table>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                    <hr style="border-top: dotted;color: gainsboro" />
                                    
                                    <br/>
                                    <div class="row">
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">New Severity</label><br/>
                                                <input type="range" min="1" max="5" value="1" class="slider mandatory" id="newRiskRatingS" onchange="calcNewRisk(this)"/>
                                                <output id="valnewRiskRatingS" class="sliderval"></output>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">New Likelihood</label><br/>
                                                <input type="range" min="1" max="5" value="1" class="slider mandatory" id="newRiskRatingL"  onchange="calcNewRisk(this)"/>
                                                <output id="valnewRiskRatingL" class="sliderval"></output>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-4" >
                                            <div class="controls">
                                                <label class="control-label">New Risk rating</label><br/>
                                                <input type="range" min="1" max="25" value="1" class="slider" id="newRiskRatingR" disabled="true"/>
                                                <output id="valnewRiskRatingR" class="sliderval"></output>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style="border-top: dotted;color: gainsboro" />

                                    <br/>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label">Attach File(s) <span style="font-size: small;font-style: italic">(Maximum file size is 10MB)</span></label>
                                            <br/>
                                            <input class="form-control" type="file" multiple="true" id="attachFiles">
                                        </div>
                                       
                                    </div>
                                     <div class="row" id="showFiles"  >
                                            <div class="col-md-2"><label class="control-label">Attached Files(s)</label></div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="validateHazard()">Save changes</button>
            </div>
        </div>
    </div>
</div>
