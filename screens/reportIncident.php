<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$action = '';
$iid = 0;
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $iid = $_GET['iid'];
}
if ($action === '') {
    $action = "NEW";
}
include '../masterData/generalMasterData.php';
$siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
$data = array();
$actionOwners = array();
$approvers =array();
$prevApproverId = 0;
$userLevel  = $_SESSION['vsmsUserData']['level'];
if ($action !== 'NEW') {
    $serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/incidentsData.php?filter=INCIDENT&iid=" . $iid;
    $data = json_decode(file_get_contents($serviceUrl));
    ChromePhp::log($data);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
    ChromePhp::log($data);
    $actionOwners = getActionOwners();
    $level = ($action === 'IR' || $action === 'AIR') ? 'L2' : (($action === 'FIR' ) ? 'L3' : 'L4');
    $prevApproverId = getPreviousApprover($iid);
    $siteId = $data[0]->site_id;
    $approvers = getSiteApprovers($siteId, $level,  $_SESSION['vsmsUserData']['id']);
    if ($action === 'AIR' && $data[0]->curr_approver_id !== $_SESSION['vsmsUserData']['id']) {
        echo "<h1>You are not authorized to access this incident. Reason: You are not the current approver of the seleted incident. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }else if ($action === 'AIR' && ($data[0]->status!=='L2_SUBMITTED' && $data[0]->status!=='L2_EDITED' && $data[0]->status!=='L2_REJECTED')) {
        echo "<h1>Looks like this incident is no longer pending for your action. Please go back to the  <a href='home.php'>Home</a> Page</h1>";
        die();
    }else if ($action === 'AFIR' && $data[0]->curr_approver_id !== $_SESSION['vsmsUserData']['id']) {
        echo "<h1>You are not authorized to access this incident. Reason: You are not the current approver of the seleted incident. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }else if ($action === 'AFIR' && ($data[0]->status!=='L3_SUBMITTED' && $data[0]->status!=='L3_EDITED'  && $data[0]->status!=='L4_REJECTED')) {
        echo "<h1>Looks like this incident is no longer pending for your action. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }else if ($action === 'FIR' && ($data[0]->status!=='L2_APPROVED' && $data[0]->status!=='L3_SAVED' && $data[0]->status!=='L3_REJECTED')) {
        echo "<h1>Looks like this incident is no longer pending for your action. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }else if ($action === 'HSSIGNOFF' && $data[0]->curr_approver_id !== $_SESSION['vsmsUserData']['id']) {
        echo "<h1>You are not authorized to access this incident. Reason: You are not the current approver of the seleted incident. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    } else if (!in_array($data[0]->site_id,$_SESSION['vsmsUserData']['sites'])) {
        echo "<h1>You are not authorized to access this incident. Reason: You are not mapped to the site of the incident. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
}
?>
<html>
    <head>
        <title>VSMS - Report Incident</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>
        <script src="../js/tempusdominus-bootstrap-4.min.js"></script>
        <link href="../css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/bootstrap-select.min.js"></script>
        <script src="../js/reportIncidentJS.js?random=<?php echo filemtime('../js/reportIncidentJS.js'); ?>"></script>
        <script src="../js/decision.js?random=<?php echo filemtime('../js/decision.js'); ?>"></script>
        <script src="../js/IEFixes.js"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
            .card-header .fa {
                transition: .3s transform ease-in-out;
            }
            .card-header .collapsed .fa {
                transform: rotate(-90deg);

            } 
            #overlay >img{

                position:absolute;
                top:38%;
                left: 38%;
                z-index:10;


            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            #modal {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px; right:20px;
                bottom:20px; left:20px;
            }
            #container{
                position:relative;
                display:none;
            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }


        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Report Incident</h1>      
            </div>
        </div>
        <hr/>
        <div class="alert alert-warning container">
            <?php if ($action === "NEW") { ?> 
                <strong>Please fill out the form below to create an Incident Report. All fields marked * are required. Once the Incident Report is created, you can find it
                    on Your Site Incidents tab on Home screen to complete the Initial Report.</strong>
            <?php } elseif ($action === "IR") { ?> 

                <strong>Please fill out the form below to submit the Initial Report. All fields marked * are required. You can save the changes before submitting the report and return to this form later. 
                    <span style="font-weight: bold;color: red">REMINDER: You have 24 hrs from the incident time to submit the Initial Report.</span></strong>
            <?php } elseif ($action === "AIR") { ?> 
                <strong>Please see the details of this Incident Report in the form below. 
                    <br/>
                    <span style="font-weight: bold;color: red">REMINDER: You have 72 hrs from the incident time to submit the Full Investigation.</span>
                </strong>
            <?php } elseif ($action === "VIEW") { ?> 
                <strong>Please see the details of this Incident Report in the form below.</strong>
            <?php } elseif ($action === "AFIR") { ?> 
                <strong>Please see the details of this Incident Report in the form below.</strong>
            <?php } elseif ($action === "FIR") { ?> 
                <strong>Please fill out the form below to submit the Full Investigation. All fields marked * are required. You can save the changes before submitting the report and return to this form later.
                    <br/>
                    <span style="font-weight: bold;color: red">REMINDER: You have 72 hrs from the incident time to submit the Full Investigation.</span>
                </strong>
            <?php } elseif ($action === "HSSIGNOFF") { ?> 
                <strong>Fill out the fields relevant to H&S Sign Off in a form below to close the incident report. All fields marked * are required. You can save the changes before submitting the report and return to this form later.</strong>
                <br/>
                <span style="color: red"><strong>You can add more countermeasure actions on Full Investigation tab. NOTE: The new actions will be submitted to the selected action owners once you click on "Save" or "Submit and Close Off".</strong></span>
            <?php } ?> 
                <br/>
                 
        </div>
        <div class="alert alert-info container">
            <span >Data Protection: Please ensure all personal data is handled with care and not shared with any unauthorised person.  </span>
        </div>
        <?php if ($action !== "NEW") { ?>
            <div class="container"><span style="font-size: x-large;font-weight: bolder;color: brown"> Incident ID: IR<?php echo($data[0]->incident_number ); ?></span></div>
        <?php } ?>
        <br/>
        <?php include './tabList.php'; ?>
        <form action="" method="post">
            <div id="overlay">
                <img src="../images/loading.gif" alt="Wait" alt="Loading"/>
                <div id="modal">
                    Uploading Incident data and attached files. Please refrain from clicking anywhere......
                </div>
            </div>

            <div class="container tab-content">
                <div id="incidentReport" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="incidentSite" required>
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users_sites,' . $mDbName . '.site where users_sites.site_id = site.id and users_sites.user_id = ' . $_SESSION['vsmsUserData']['id']);
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['site_id'] . '">' . $row['code'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-1 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="incidentLocation" required>
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.LOCATION;');
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Date & Time (DD/MM/YYYY)<span style="color: red">*</span></label>
                                <div class="input-group date" id="incidentDateTime" data-target-input="nearest">
                                    <input class="form-control datetimepicker-input" type="text" id="incidentDateTime"  data-target="#incidentDateTime" required autocomplete="off"/>
                                    <div class="input-group-append" data-target="#incidentDateTime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Day of the week</label>
                                <input type="text" class="form-control" id="incidentDay" maxlength="255" readonly/>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                         <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Brief Details of Incident<span style="color: red">*</span></label>
                                <div class="input-group">
                                    
                                    <textarea rows="2" class="form-control" id="incidentBrief" maxlength="250" ></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <label class="control-label">Event Severity <span style="color: red">*</span></label>
                            <select class="custom-select" id="eventSeverity">
                                <option value="-1"></option>
                                <option value="Minimal">Minimal</option>
                                <option value="Low">Low</option>
                                <option value="Medium">Medium</option>
                                <option value="High">High</option>
                            </select>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Please specify exact location</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="exactLocation" maxlength="255"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr/>

                    <div class="row">
                        <div class="pull-left col-md-6"><span>Please answer the questions below for the system to identify the incident type.</span></div>
                        <div class="pull-right col-md-6"><span>Incident Type will be assigned automatically once you answer the questions on the left.</span></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="pull-left col-md-6">
                            <div class="row" id="question1Div" style="display:none">
                                <div class="col-9" >
                                    <div class="form-group" >
                                        <label class="control-label"><b>Question 1: </b></label>
                                        <label class="control-label" id="question1Label">Was someone hurt or injured? </label>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div  class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer1Yes" name="answer1" value="Yes" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer1Yes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer1No" name="answer1" value="No" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer1No">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="question2Div" style="display:none">
                                <div class="col-10">
                                    <div class="form-group">
                                        <label class="control-label"><b>Question 2: </b></label>
                                        <label class="control-label" id="question2Label">Dependent on Answer 1 </label>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer2Yes" name="answer2" value="Yes" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer2Yes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer2No" name="answer2" value="No" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer2No">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="question3Div" style="display:none">
                                <div class="col-9">
                                    <div class="form-group" >
                                        <label class="control-label"><b>Question 3: </b></label>
                                        <label class="control-label" id="question3Label">Dependent on Answer 2 </label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer3Yes" name="answer3" value="Yes" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer3Yes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer3No" name="answer3" value="No" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer3No">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="question4Div" style="display:none">
                                <div class="col-9">
                                    <div class="form-group">
                                        <label class="control-label"><b>Question 4: </b></label>
                                        <label class="control-label" id="question4Label">Dependent on Answer 3 </label>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer4Yes" name="answer4" value="Yes" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer4Yes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="answer4No" name="answer4" value="No" onchange="populateIncident()">
                                            <label class="custom-control-label" for="answer4No">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pull-right col-md-6">
                            <div class="row " >

                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="seriousAccident" name="incidentType" value="Serious Accident"  onclick="return false;">
                                        <label class="custom-control-label" for="seriousAccident">Serious Accident</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="nearMiss" name="incidentType" value="Near Miss"  onclick="return false;">
                                        <label class="custom-control-label" for="nearMiss">Near Miss</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="row " >
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="accident" name="incidentType" value="Accident"  onclick="return false;">
                                        <label class="custom-control-label" for="accident">Accident</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="nearMissStar" name="incidentType" value="Near Miss*"  onclick="return false;">
                                        <label class="custom-control-label" for="nearMissStar">Near Miss*</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="row " >
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="accidentStar" name="incidentType" value="Accident*"  onclick="return false;">
                                        <label class="custom-control-label" for="accidentStar">Accident*</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="illHealth" name="incidentType" value="Ill Health"  onclick="return false;">
                                        <label class="custom-control-label" for="illHealth">Ill Health</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="row " >
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="incident" name="incidentType" value="Incident"  onclick="return false;">
                                        <label class="custom-control-label" for="incident">Incident</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="illHealthStar" name="incidentType" value="Ill Health*"  onclick="return false;">
                                        <label class="custom-control-label" for="illHealthStar">Ill Health*</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="row " >
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="incidentStar" name="incidentType" value="Incident*"  onclick="return false;">
                                        <label class="custom-control-label" for="incidentStar">Incident*</label>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" class="custom-control-input" id="occupationalDiesease" name="incidentType" value="Occupational Disease" onclick="return false;" >
                                        <label class="custom-control-label" for="occupationalDiesease">Occupational Disease</label>
                                    </div> 
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <span id="incidentTypeDesc" style="display: none;font-style: italic"></span>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>

                <div id="initialReport" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './initialReport.php'; ?>
                </div>
                <div id="fullInvestigation" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './fullInvestigation.php'; ?>
                </div>
                <div id="hsSignOff" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './hssignoff.php'; ?>
                </div>
                <div id="allActions" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './allActions.php'; ?>
                </div>
            </div>
            <div class="container" style="background-color: whitesmoke">
                <div class = "row" <?php if ($action !== "IR" && $action !== "FIR" && $action !== "AFIR") { ?>style="display:none"<?php } ?>>
                    <div class = "col-md-4">
                        <label class = "control-label" <?php if ($action !== "IR" && $action !== "FIR") { ?>style="display:none"<?php } ?>>Select an approver<span style = "color: red">*</span></label>
                        <label class = "control-label" <?php if ($action !== "AFIR") { ?>style="display:none"<?php } ?>>Submit to H&S Team Member<span style = "color: red">*</span></label>
                        <select class = "custom-select " id = "approver">
                            <?php
                            echo "<option value='-1'></option>";
                            foreach ($approvers as $value) {
                                echo '<option value=' . $value['user_id'] . '>' . $value['first_name'] . ' ' . $value['last_name'] . '</option>';
                            }
                            echo '';
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id ="approverValidationError" class="showError alert alert-danger approverValidationError" style="display: none"><strong>Please select an approver.</Strong></div>
                </div>

                <?php if ($action === "NEW" || $action === "IR") { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-red" id="btnDelete" type="button" value="DELETE" onclick="deleteChanges()"<?php if ($action === "NEW" || $userLevel !== 'L4') { ?>style="display:none"<?php } ?>></input>
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE" onclick="submitIncidentData('L1_SAVED', '')"<?php if ($action === "NEW") { ?>style="display:none"<?php } ?>></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate('<?php echo $action ?>')"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($action === 'AIR') { ?>
                    <div class="row" >
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE INITIAL REPORT" onclick="validate('L2_EDITED')"></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="REJECT INITIAL REPORT" onclick="rejectIncident('Initial Report')"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="APPROVE INITIAL REPORT" onclick="validate('L2_APPROVED')"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($action === 'AFIR') { ?>
                    <div class="row" >
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE FULL INVESTIGATION" onclick="validate('L3_EDITED')"></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="REJECT FULL INVESTIGATION" onclick="rejectIncident('Full Investigation')"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="APPROVE FULL INVESTIGATION" onclick="validate('L3_APPROVED')"></input>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } elseif ($action === "FIR") { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-red" id="btnDelete" type="button" value="DELETE" onclick="deleteChanges()" <?php if ($userLevel !== 'L4') { ?>style="display:none"<?php } ?>></input>
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE" onclick="validate('L3_SAVED')"></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate('L3_SUBMITTED')"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($action === 'HSSIGNOFF') { ?>
                    <div class="row" >
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-red" id="btnDelete" type="button" value="DELETE" onclick="deleteChanges()"  <?php if ($userLevel !== 'L4') { ?>style="display:none"<?php } ?>></input>
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE" onclick="validate('L4_SAVED')"></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="REJECT" onclick="rejectIncident('HS Signoff')"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT AND CLOSE OFF" onclick="validate('L4_APPROVED')"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?> 
            </div>
            <br />
             <div class="modal fade" id="promptModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                    </div>
                    <br>
                      <div class="modal-body">
                  <div class="col-md-3 col-lg-12">
                        <div class="form-group">
                         <label id="promptMessage">Please add comments:</label>
                        </br>
                        <textarea class="form-control" rows="2" maxlength="500" id="promptComments"></textarea>
                        <input type="hidden" id="promptAction"/>
                    </div>
                  </div>
                      </div>
                    <div class="modal-footer" >
                        <button type="button" class="btn btn-green" id="bYes"  data-dismiss="modal">OK</button>
                        <button type="button" class="btn btn-gray" id="bNo"  data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <?php if ($action === 'VIEW') { ?>
            <div class="container" >
                <div class="col-md-3 col-lg-3 "> <a class="btn btn-blue" href="javascript:history.go(-1)" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a></div>
            </div>
        <?php } ?> 
    </body>
    <script>
        var deletedLicenseIds = [];

        $(document).ready(function () {


            var action = '<?php echo $action ?>';
            var actionOwners = '';
            if (action === 'NEW') {
                $('#incidentTab').trigger('click');
                document.getElementById('question1Div').style.display = "block";
                //Initialize datetime picker
                $("#incidentDateTime").datetimepicker({
                    format: 'DD-MM-YYYY HH:mm:ss',
                    locale: moment.locale('en', {week: {dow: 1}}),
                    useCurrent: false,
                    maxDate: moment()
                });
                $("#incidentDateTime").on("change.datetimepicker", function (e) {
                    if (e.date !== undefined) {
                        document.getElementById('incidentDay').value = getDay(e.date);
                    } else {
                        document.getElementById('incidentDay').value = '';
                    }
                });
            }
            else if (action !== "NEW") {
                /*<ark field disabled*/
                var data = <?php echo json_encode($data) ?>;

                if (action === 'IR' || action === 'AIR') {
                    $('#initialReportTab').trigger('click');
                } else if (action === 'FIR' || action === 'AFIR') {
                    $('#fullInvestigationTab').trigger('click');
                } else if (action === 'VIEW') {
                    var level = data[0].status.split('_')[0];
                    if (data[0].status === '_CLOSED') {
                        $('#hsTab').trigger('click');
                    }
                    if (level === 'L1') {
                        $('#incidentTab').trigger('click');
                    }
                    if (level === 'L2') {
                        $('#initialReportTab').trigger('click');
                    }
                    if (level === 'L3') {
                        $('#fullInvestigationTab').trigger('click');
                    }
                    if (level === 'L4') {
                        $('#hsTab').trigger('click');
                    }

                } else if (action === 'HSSIGNOFF') {
                    $('#hsTab').trigger('click');
                }
                buildActionsTable(data[0].allActions);
                //Set all Datepickers
                
                
                 
                $("#personDOB_1").datetimepicker({
                    format: 'DD/MM/YYYY',
                    maxDate: new Date(data[0].incident_date),
                    viewMode: 'years',
                    locale: moment.locale('en', {week: {dow: 1}})
                });
                $("#personDOB_1").find('.person-dob-2').val('');
                $("#personDOB_1").on("change.datetimepicker", function (e) {
                    populateTimeDiff(this, e.date, "DOB");
                });
                $("#personESD_1").datetimepicker({
                    format: 'DD/MM/YYYY',
                    maxDate: new Date(data[0].incident_date),
                    locale: moment.locale('en', {week: {dow: 1}})
                });

                $("#personESD_1").on("change.datetimepicker", function (e) {
                    populateTimeDiff(this, e.date, "ESD");
                });
                $("#personESD_1").find('.person-esd-2').val('');
                $("#personTOJSD_1").datetimepicker({
                    format: 'DD/MM/YYYY',
                    maxDate: new Date(data[0].incident_date), locale: moment.locale('en', {week: {dow: 1}})
                });
                $("#personTOJSD_1").on("change.datetimepicker", function (e) {
                    populateTimeDiff(this, e.date, "TOJ");
                });
                $("#personTOJSD_1").find('.person-jsd-2').val('');
                $('#btnDelPerson').attr('disabled', true);
                var incidentType = data[0].incident_type;
                if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident') {
                    $("input[name=isLocationOfInjury_1").prop('disabled', false);
                    // $("#personLocationInjury_1").selectpicker('refresh');
                }
                if (incidentType === 'Serious Accident' || incidentType === 'Accident*' || incidentType === 'Accident' || incidentType === 'Occupational Disease' || incidentType === 'Ill Health*' || incidentType === 'Ill Health') {
                    $("#personTreatmentDetails_1").prop('disabled', false);
                    $("#personTreatment_1").prop('disabled', false);
                    $("#personTreatment_1").selectpicker('refresh');
                }
                if (action === 'HSSIGNOFF') {
                    $("#injuredPersonHSEReportedDate_1").datetimepicker({
                        format: 'DD/MM/YYYY',
                        maxDate: moment(), locale: moment.locale('en', {week: {dow: 1}})
                    });
                    $("#injuredPersonHSEReportedDate_1").find('.person-hse-date-2').val('');
                }
                populatePageFromData(data[0]);
                var deadline = getDeadline();
                $('#containmentDeadline').html("Deadline for all containment actions is : " + deadline.toLocaleDateString() + " " + deadline.toLocaleTimeString());
                var k = $('#tab_action tbody tr').length - 1;
                $("#add_action").click(function () {
                    d = k - 1
                    $('#actionRow' + k).html($('#actionRow' + d).html());
                    $('#tab_action').append('<tr id="actionRow' + (k + 1) + '"></tr>');
                    $('#actionRow' + k).find('.actionOwner').empty().append('<option value="-1"></option>');
                    k++;
                });
                $("#delete_action").click(function () {
                    if (k > 1) {
                        $("#actionRow" + (k - 1)).html('');
                        k--;
                    }
                });

                //Add/delete license
                var a = $('#tab_license tbody tr').length - 1;
                $("#add_license").click(function () {
                    b = a - 1
                    $('#licenseRow' + a).html($('#licenseRow' + b).html());
                    $('#tab_license').append('<tr id="licenseRow' + (a + 1) + '"></tr>');
                    $('#licenseRow' + a).find('.licenseId').val('0');
                    $('#licenseRow' + a).find('.fileLinks').empty();
                    $('#licenseRow' + a).find('.fileLinks').css('display', 'none');
                    $('#licenseRow' + a).find('.licensePersonName').val('');
                    a++;
                });
                $("#delete_license").click(function () {
                    if (a > 1) {
                        deletedLicenseIds.push($("#licenseRow" + (a - 1)).find('.licenseId').val());
                        $("#licenseRow" + (a - 1)).html('');

                        a--;
                    }

                });

                //Dynamic person Details
                $("#btnAddPerson").click(function () {
                    addPerson('NEW');
                });
                $('#btnDelPerson').click(function () {
                    // confirmation
                    if (confirm("Are you sure you wish to remove this section of the form? Any information it contains will be lost!")) {
                        var num = $('.clonedInput').length;
                        // how many "duplicatable" input fields we currently have
                        $('#personDetails_' + num).slideUp('slow', function () {
                            $(this).remove();
                            // if only one element remains, disable the "remove" button
                            if (num - 1 === 1)
                                $('#btnDelPerson').attr('disabled', true);
                            // enable the "add" button
                            $('#btnAddPerson').attr('disabled', false).prop('value', "Add person");
                        });
                        $('.licensePersonName option[value="' + 'Person ' + num + '"]').remove();
                    }
                    return false;
                    // remove the last element

                    // enable the "add" button
                    $('#btnAddPerson').attr('disabled', false);
                });

                //Dynamic WHY Details
                $("#btnAddWhy").click(function () {
                    addWhy();
                });
                $('#btnDelWhy').click(function () {
                    // confirmation
                    if (confirm("Are you sure you wish to remove this section of the form? Any information it contains will be lost!")) {
                        var num = $('.clonedWhy').length;
                        // how many "duplicatable" input fields we currently have
                        $('#block5Why' + num).slideUp('slow', function () {
                            $(this).remove();
                            // if only one element remains, disable the "remove" button
                            if (num - 1 === 1)
                                $('#btnDelWhy').attr('disabled', true);
                            // enable the "add" button
                            $('#btnAddWhy').attr('disabled', false).prop('value', "+");
                        });
                    }
                    return false;
                    // remove the last element

                    // enable the "add" button
                    $('#btnAddWhy').attr('disabled', false);
                });

                //Dynamic WHY Details
                $("#btnAddCounter").click(function () {
                    addCounterMeasure();
                });
                $('#btnDelCounter').click(function () {
                    // confirmation
                    if (confirm("Are you sure you wish to remove this section of the form? Any information it contains will be lost!")) {
                        var num = $('.clonedCounterMeasure').length;
                        // how many "duplicatable" input fields we currently have
                        $('#otherCounterMeasureDiv' + num).slideUp('slow', function () {
                            $(this).remove();
                            // if only one element remains, disable the "remove" button
                            if (num - 1 === 1)
                                $('#btnDelCounter').attr('disabled', true);
                            // enable the "add" button
                            $('#btnAddCounter').attr('disabled', false).prop('value', "+");
                        });
                    }
                    return false;
                    // remove the last element

                    // enable the "add" button
                    $('#btnAddCounter').attr('disabled', false);
                });



                //Dynamic Injured Person Details
                $("#btnAddInjuredPerson").click(function () {
                    addInjuredPerson();
                });
                $('#btnDelInjuredPerson').click(function () {
                    // confirmation
                    if (confirm("Are you sure you wish to remove this section of the form? Any information it contains will be lost!")) {
                        var num = $('.clonedInjuredDetails').length;
                        // how many "duplicatable" input fields we currently have
                        $('#injuredPerson_' + num).slideUp('slow', function () {
                            $(this).remove();
                            // if only one element remains, disable the "remove" button
                            if (num - 1 === 1)
                                $('#btnDelInjuredPerson').attr('disabled', true);
                            // enable the "add" button
                            $('#btnAddInjuredPerson').attr('disabled', false).prop('value', "+");
                        });
                    }
                    return false;
                    // remove the last element

                    // enable the "add" button
                    $('#btnAddInjuredPerson').attr('disabled', false);
                });



                //$('input[type=checkbox]').click(function () {
              
                
             
                if (action === 'VIEW') {
                    $('form *').prop('disabled', true);
                }
            }
              $('.badgebox').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).next().html("&check;");
                        $(this).parent().removeClass('btn-danger');
                        $(this).parent().addClass('btn-primary');
                    } else {
                        $(this).next().html("&cross;");
                        $(this).parent().removeClass('btn-primary');
                        $(this).parent().addClass('btn-danger');
                    }
                });
             $('.badgeboxReverse').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).next().html("&check;");
                        $(this).parent().removeClass('btn-primary');
                        $(this).parent().addClass('btn-danger');
                    } else {
                        $(this).next().html("&cross;");
                           $(this).parent().removeClass('btn-danger');
                        $(this).parent().addClass('btn-primary');

               }
                  });
                  $('.badgeboxneutral').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).next().html("&check;");
                           $(this).parent().removeClass('btn-secondary');
                        $(this).parent().addClass('btn-dark');
                    } else {
                        $(this).next().html("&cross;");
                           $(this).parent().removeClass('btn-dark');
                        $(this).parent().addClass('btn-secondary');
                    }
                });
        });
        /*Dont move this function from this file*/
        function populateActions(data) {
            var actions = data.actions;
            var k = $('#tab_action tbody tr').length - 1;
            var lower = "";
            //Disable the buttons
             if (actions.length !== 0) {
                lower = actions[0].status.toLowerCase();
            }
            if ((data.status !== 'L1_CREATED' && data.status !== 'L1_SAVED')|| (data.status === 'L1_SAVED' && lower!=='created'))  {
                $('#add_action').prop('disabled', true);
                $('#delete_action').prop('disabled', true);
                $('#actionCol1').css('display', 'block');
                $('#actionCol2').css('display', 'block');
            }
            if (actions.length !== 0) {
                $('#actionRow0').find('.containmentAction').val(actions[0].action);
                $('#actionRow0').find('.containmentSite').val(actions[0].action_site);
                $('#actionRow0').find('.containmentId').val("AC" + actions[0].id);

                populateOwners($('#actionRow0').find('.containmentSite')[0]);
                $('#actionRow0').find('.actionOwner').val(actions[0].action_owner);
                populateManager($('#actionRow0').find('.actionOwner')[0]);
                $('#actionRow0').find('.containmentStatus').val(lower.charAt(0).toUpperCase() + lower.slice(1));
                if ((data.status !== 'L1_CREATED' && data.status !== 'L1_SAVED')|| (data.status === 'L1_SAVED' && lower!=='created'))  {
                    $('#actionRow0').find('.containmentIdCol').css("display", "block");
                    $('#actionRow0').find('.containmentStatusCol').css("display", "block");

                    $('#actionRow0').find('input').prop('disabled', true);
                    $('#actionRow0').find('select').prop('disabled', true);
                    $('#actionRow0').find('textarea').prop('disabled', true);
                }
            }
            for (j = 1; j < actions.length; j++) {
                d = k - 1;
                var lower = actions[j].status.toLowerCase();
                $('#actionRow' + k).html($('#actionRow' + d).html());
                $('#actionRow' + k).find('.containmentAction').val(actions[j].action);
                $('#actionRow' + k).find('.containmentSite').val(actions[j].action_site);
                $('#actionRow' + k).find('.containmentId').val("AC" + actions[j].id);
                $('#actionRow' + k).find('.containmentId').css("display", "block");
                populateOwners($('#actionRow' + k).find('.containmentSite')[0]);
                $('#actionRow' + k).find('.actionOwner').val(actions[j].action_owner);
                populateManager($('#actionRow' + k).find('.actionOwner')[0]);
                $('#actionRow' + k).find('.containmentSite').val(actions[j].action_site);
                $('#actionRow' + k).find('.containmentStatus').val(lower.charAt(0).toUpperCase() + lower.slice(1));
                if ((data.status !== 'L1_CREATED' && data.status !== 'L1_SAVED')|| (data.status === 'L1_SAVED' && lower!=='created'))  {
                    $('#actionRow' + k).find('input').prop('disabled', true);
                    $('#actionRow' + k).find('select').prop('disabled', true);
                    $('#actionRow' + k).find('textarea').prop('disabled', true);
                    $('#actionRow' + k).find('.containmentIdCol').css("display", "block");
                    $('#actionRow' + k).find('.containmentStatusCol').css("display", "block");


                }
                $('#tab_action').append('<tr id="actionRow' + (k + 1) + '"></tr>');

                k++;
            }
        }
        /*Dont move this function from this file*/
        function populateLicense(data) {
            /*Populates License*/
            var licenses = data.licenses;
            var a = $('#tab_license tbody tr').length - 1;
            if (licenses.length !== 0) {
                $('#licenseRow0').find('.licensePersonName').find('option').each(function () {
                    if ($(this).prop('text') === licenses[0].person_name) {
                        $(this).attr('selected', true);
                    }
                });
                if (licenses[0].expiry_date !== '') {
                    $('#licenseRow0').find('.licenseExpiryDate').val(new Date(licenses[0].expiry_date).toISOString().split('T')[0]);
                }
                var validVal = licenses[0].is_valid === '1' ? 'Yes' : 'No';
                $('#licenseRow0').find('.licenseValid').val(validVal);
                $('#licenseRow0').find('.licenseId').val(licenses[0].licenseId);
                $('#licenseRow0').find('.fileLinks').css('display', 'block');
                if (licenses[0].file_name !== '') {
                    var href = "../action/downloadFile.php?iid=" + <?php echo $iid ?> + "&fileName=" + encodeURIComponent(licenses[0].file_name) + "&type=license";
                    var link = '<a href="' + href + '">' + licenses[0].file_name + '</a>';
                    $('#licenseRow0').find('.fileLinks').append(link);
                }


            }
            for (var j = 1; j < licenses.length; j++) {
                b = a - 1
                $('#licenseRow' + a).html($('#licenseRow' + b).html());
                $('#licenseRow' + a).find('.licensePersonName').find('option').each(function () {
                    if ($(this).prop('text') === licenses[j].person_name) {
                        $(this).attr('selected', true);
                    }
                });
                if (licenses[j].expiry_date !== '') {
                    $('#licenseRow' + a).find('.licenseExpiryDate').val(new Date(licenses[j].expiry_date).toISOString().split('T')[0]);
                }
                var validVal = licenses[j].is_valid === '1' ? 'Yes' : 'No';
                $('#licenseRow' + a).find('.licenseValid').val(validVal);
                $('#licenseRow' + a).find('.licenseId').val(licenses[j].licenseId);
                $('#licenseRow' + a).find('.fileLinks').css('display', 'block');
                $('#licenseRow' + a).find('.fileLinks').empty();
                if (licenses[j].file_name !== '') {
                    var href = "../action/downloadFile.php?iid=" + <?php echo $iid ?> + "&fileName=" + encodeURIComponent(licenses[j].file_name) + "&type=license";
                    var link = '<a href="' + href + '">' + licenses[j].file_name + '</a>';
                    $('#licenseRow' + a).find('.fileLinks').append(link);
                }

                $('#tab_license').append('<tr id="licenseRow' + (a + 1) + '"></tr>');
                a++;
            }
        }
        function populateOwners(elem) {

            var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
            var containmentSite = elem.value;
            var owners = '';
            var ownerList = $(elem).closest("td").next().find("select");
           $(elem).closest("td").next().next().find("input")[0].value = '';
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === containmentSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateOwnersForCounterMeasure(elem) {

            var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
            var containmentSite = elem.val();
            var owners = '';
            var ownerList = $(elem).closest("div").next().find("select");
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === containmentSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateCounterMeasureManager(elem) {

            $(elem).closest("div").next().find("input")[0].value = $(elem).find('option:selected').attr('data-divid') === undefined ? '' : $(elem).find('option:selected').attr('data-divid');
        }
        function populateManager(elem) {

            $(elem).closest("td").next().find("input")[0].value = $(elem).find('option:selected').attr('data-divid') === undefined ? '' : $(elem).find('option:selected').attr('data-divid');
        }
        function submitIncidentData(submitAction, approverComments) {

            var siteId = document.getElementById("incidentSite").value;
            var filter = 'NEW';

            var locationElement = document.getElementById("incidentLocation");
            var location = locationElement.options [locationElement.selectedIndex].text;

            var incidentDateTime = moment($('#incidentDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss').toJSON();
            var locationOther = document.getElementById("exactLocation").value;
            var incidentBrief = document.getElementById("incidentBrief").value;
            var eventSeverity = document.getElementById("eventSeverity").value;
            var question1Text = document.getElementById("question1Label").innerText;
            var question2Text = document.getElementById("question2Label").innerText;
            var question1Ans = $("input:radio[name=answer1]:checked").val();
            var question2Ans = $("input:radio[name=answer2]:checked").val();
            var question3Text, question3Ans, question4Text, question4Ans = "";
            if (document.getElementById('question3Div').style.display === 'block') {
                question3Text = document.getElementById("question3Label").innerText;
                question3Ans = $("input:radio[name=answer3]:checked").val();
            }
            if (document.getElementById('question4Div').style.display === 'block') {
                question4Text = document.getElementById("question4Label").innerText;
                question4Ans = $("input:radio[name=answer4]:checked").val();
            }
            var category, categoryLevel, type, actOrCondition, isDangerousOccurence,classification,hsSummary = "";


            var incidentType = $("input:radio[name=incidentType]:checked").val();
            var createdBy, updatedBy = <?php echo json_encode($_SESSION['vsmsUserData']['first_name'] . " " . $_SESSION['vsmsUserData']['last_name']); ?>;
            var requestorId = <?php echo json_encode($_SESSION['vsmsUserData']['id']) ?>;



            var action = '<?php echo $action ?>';
            var incidentRequest = '';
            if (action === 'NEW') {
                //ONLY incident report fields
                incidentRequest = {"siteId": siteId, "location": location, "incidentDateTime": incidentDateTime, "locationOther": locationOther, "question1Text": question1Text, "question2Text": question2Text,
                    "question3Text": question3Text, "incidentBrief":incidentBrief,"eventSeverity":eventSeverity,"question4Text": question4Text, "question1Answer": question1Ans, "question2Answer": question2Ans, "question3Answer": question3Ans, "question4Answer": question4Ans,
                    "incidentType": incidentType, "requestorId": requestorId, "filter": filter};
            } else {
                //Common fields
                if (submitAction === 'L1_SAVED') {
                    filter = 'L1_SAVED';
                } else if (submitAction === 'L2_SUBMITTED') {
                    filter = 'L2_SUBMITTED';
                } else if (submitAction === 'L2_EDITED') {
                    filter = 'L2_EDITED';
                } else if (submitAction === 'L3_SAVED') {
                    filter = 'L3_SAVED';
                } else if (submitAction === 'L2_APPROVED') {
                    filter = 'L2_APPROVED';
                } else if (submitAction === 'L3_SUBMITTED') {
                    filter = 'L3_SUBMITTED';
                } else if (submitAction === 'L3_EDITED') {
                    filter = 'L3_EDITED';
                } else if (submitAction === 'L3_APPROVED') {
                    filter = 'L3_APPROVED';
                } else if (submitAction === 'L4_SAVED') {
                    filter = 'L4_SAVED';
                } else if (submitAction === 'L4_APPROVED') {
                    filter = 'L4_APPROVED';
                }
                var incidentDetail = document.getElementById("incidentDetail").value;
                var actionType = document.getElementById("actionType").value;
                var actionTypeOther = document.getElementById("otherActionType").value;
                var area = document.getElementById("incidentArea").value;
                var lighting = document.getElementById("lighting").value;
                var temperature = document.getElementById("temperature").value;
                var noise = document.getElementById("noise").value;
                var surfaceType = document.getElementById("surfaceType").value;
                var surfaceCondition = document.getElementById("surfaceCondition").value;
                var incidentNumber = <?php echo json_encode($iid) ?>;
                var isPersonInvolved = $("#personYes").is(':checked') ? true : ($("#personNo").is(':checked') ? false : null);
                var currApproverId = document.getElementById("approver").value;
                var graId = document.getElementById("graId").value;
                //Person Details
                var personInvolved = new Array();
                var totalPersons = $('.clonedInput').length;
                for (var i = 0; i < totalPersons; i++) {
                    var num = i + 1;
                    console.log($('#personDOB' + num).find('.person-dob-2').val());
                    //var val = $("input:checkbox[name=personLocationInjury_"+num+"]:checked");
                    var injuryVal = [];
                    if ($('#injuryYes_' + num).is(':checked')) {
                        $("input:checkbox[name=personLocationInjury_" + num + "]:checked").each(function () {
                            injuryVal.push($(this).val());
                        });
                    } else if ($('#injuryNo_' + num).is(':checked')) {
                        injuryVal.push('None');
                    }
                    personInvolved[i] = {
                        "personName": $('#personName_' + num).val()
                        , "personDOB": moment($('#personDOB_' + num).find('.person-dob-2').val(), 'DD/MM/YYYY').toJSON()
                        , "employmentstartDate": moment($('#personESD_' + num).find('.person-esd-2').val(), 'DD/MM/YYYY').toJSON()
                        , "timeOnJobStartDate": moment($('#personTOJSD_' + num).find('.person-jsd-2').val(), 'DD/MM/YYYY').toJSON()
                        , "employmentStatus": $('#personEmpStatus_' + num).val()
                        , "anyPrevIncidents": $('#personPrevIncidentsToggle_' + num).is(':checked')
                        , "prevIncidentDateReference": $('#personDateAndRef_' + num).val()
                        , "isVehicleInvolved": $('#personVehicleInvolvedToggle_' + num).is(':checked')
                        , "vehicleType": $('#personVehicleType_' + num).val()
                        , "vehicleRegNo": $('#personVehicleReg_' + num).val()
                        , "vehicleTypeOther": $('#personVehicleTypeComments_' + num).val()
                        , "wasPersonremovedFromTruck": $('#personRemovedToggle_' + num).is(':checked')
                        , "isInjuryInvolved": $("input:radio[name=isLocationOfInjury_" + num + "]:checked").val() === 'Yes' ? true : $("input:radio[name=isLocationOfInjury_" + num + "]:checked").val() === 'No' ? false : null
                        , "locationOfInjury": injuryVal.join()
                        , "treatmentAdministered": $('#personTreatment_' + num).val().join()
                        , "ppeWorn": $('#personPPEWorn_' + num).val().join()
                        , "wasPPEOfReqLevel": $('#personPPEOfReqLevelToggle_' + num).is(':checked')
                        , "treatementDetails": $('#personTreatmentDetails_' + num).val()
                        , "daTestRequired": $('#personDAFormReqToggle_' + num).is(':checked')
                        , "daTestNotReqReason": $('#personDAFormNotReqComments_' + num).val()
                        , "daPassed": $('#personDAPassToggle_' + num).is(':checked')
                        , "daTestAgreed": $('#personDATestRefusedToggle_' + num).is(':checked')
                        , "daTestedBy": $('#personDATestedBy_' + num).val()
                    }
                    console.log(personInvolved[i]);
                }
                //License Details
                var licenseDetails = new Array();
                $('#tab_license tbody tr').each(function (i, tr) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var person = $(this).find('.licensePersonName').val();// Values will be Person 1 or Person 2 etc
                        if (person !== null && person !== '') {
                            var num = person.substring(person.indexOf(" ") + 1, person.length);
                            var personName = $('#personName_' + num).val();
                            if (personName !== '') {
                                var ele = $(this).find('.licenseFile')[0];
                                var fileName = ele.files[0] !== undefined ? ele.files[0].name : '';
                                licenseDetails[i] = {
                                    "personName": personName
                                    , "licenseExpiryDate": $(this).find('.licenseExpiryDate').val()
                                    , "isLicenseValid": $(this).find('.licenseValid').val() === 'Yes' ? true : false
                                    , "licenseId": $(this).find('.licenseId').val()
                                    , "fileName": fileName
                                }
                            }
                        }
                    }
                });

                ////Process fields
                var isActivityStandardProcess = document.getElementById('activityStandard').checked;
                var stillageType = document.getElementById("typeOfStillage").value;
                var estimatedCost = document.getElementById('estimatedCost').value;
                var estimatedCostOther = document.getElementById('estimatedCostOther').value;
                var immediateCause = document.getElementById('immdCause').value;
                //Fast reponse
                var keyPoints = document.getElementById('keyPoints').value;
                var mainMessage = document.getElementById('mainMessage').value;
                //Action Details
                var actions = new Array();
                $('#tab_action tbody tr').each(function (i, tr) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var actionId = $(this).find('.containmentId').val();
                        actions[i] = {
                            "id": actionId === '0' ? '0' : actionId.substring(2, actionId.length),
                            "action": $(this).find('.containmentAction').val()
                            , "actionSite": $(this).find('.containmentSite').val()
                            , "actionOwner": $(this).find('.actionOwner').val()
                            , "deadline": getDeadline()
                        }
                    }
                });
                var fiDetails = {};
                //populate all Full Investigation fields
                if (submitAction === 'L3_EDITED' || submitAction === 'L3_SUBMITTED' || submitAction === 'L3_APPROVED' || submitAction === 'L3_SAVED' || submitAction === 'L4_APPROVED' || submitAction === 'L4_SAVED') {
                    var whyModels = new Array();

                    var totalWhys = $('.clonedWhy').length;
                    for (var i = 0; i < totalWhys; i++) {
                        var num = i + 1;
                        var counterMeasures = new Array();
                        for (var j = 0; j < 3; j++) {
                            var cnt = j + 1;
                            var tag = (cnt === 1) ? 'A' : (cnt === 2) ? 'B' : 'C';
                            counterMeasures[j] = {
                                "id": document.getElementById('counterMeasure' + tag + 'Id_' + num).value,
                                "action": document.getElementById('counterMeasure' + tag + '_' + num).value,
                                "actionSite": document.getElementById('counterMeasureSite' + tag + '_' + num).value,
                                "actionSubType": document.getElementById('counterMeasureType' + tag + '_' + num).value,
                                "actionOwner": document.getElementById('counterMeasureOwner' + tag + '_' + num).value,
                                "deadline": document.getElementById('counterMeasureDeadline' + tag + '_' + num).value,
                                "incidentWhyId": document.getElementById("whyid_" + num).value,
                                "counterMeasureNumber": tag,
                                "addHistory": $('#counterMeasure' + tag + '_' + num).is(":disabled") ? false : true
                            }
                        }

                        whyModels[i] = {
                            "id": document.getElementById("whyid_" + num).value,
                            "why1": document.getElementById("why1_" + num).value,
                            "why2": document.getElementById("why2_" + num).value,
                            "why3": document.getElementById("why3_" + num).value,
                            "why4": document.getElementById("why4_" + num).value,
                            "why5": document.getElementById("why5_" + num).value,
                            "rootCauseA": document.getElementById("rootCauseA_" + num).value,
                            "rootCauseB": document.getElementById("rootCauseB_" + num).value,
                            "rootCauseC": document.getElementById("rootCauseC_" + num).value,
                            "counterMeasures": counterMeasures
                        }

                    }
                    var otherCounterMeasures = new Array();

                    var totalCntMeasures = $('.clonedCounterMeasure').length;
                    for (var k = 0; k < totalCntMeasures; k++) {
                        var tmp = k + 1;
                        otherCounterMeasures [k] = {
                            "id": document.getElementById('otherCounterMeasureId' + tmp).value,
                            "action": document.getElementById('otherCounterMeasure' + tmp).value,
                            "actionSite": document.getElementById('otherCounterMeasureSite' + tmp).value,
                            "actionSubType": document.getElementById('otherCounterMeasureType' + tmp).value,
                            "actionOwner": document.getElementById('otherCounterMeasureOwner' + tmp).value,
                            "deadline": document.getElementById('otherCounterMeasureDeadline' + tmp).value,
                            "incidentWhyId": '-1',
                            "counterMeasureNumber": '',
                            "addHistory": $('#otherCounterMeasure' + tmp).is(":disabled") ? false : true

                        }

                    }
                    fiDetails = {
                        "id": document.getElementById("fiId").value,
                        "eventSummary": document.getElementById("eventSummary").value,
                        "isSSOWSuitable": $("#ssowSuitable").val() === 'Yes' ? true : ($("#ssowSuitable").val() === 'No' ? false : null),
                        "ssowComments": document.getElementById("commentsSSOW").value,
                        "isRASuitable": $("#raSuitable").val() === 'Yes' ? true : ($("#raSuitable").val() === 'No' ? false : null),
                        "raComments": document.getElementById("commentsRA").value,
                        "isMHETrainingRequired": $("#mheTrainingReq").val() === 'Yes' ? true : ($("#mheTrainingReq").val() === 'No' ? false : null),
                        "personConcentraion": $("input:radio[name=personConcentration]:checked").val(),
                        "personExperience": $("input:radio[name=personExperience]:checked").val(),
                        "personTraining": $("input:radio[name=personTraining]:checked").val(),
                        "machineCondition": $("input:radio[name=machineCondition]:checked").val(),
                        "machineMaintenance": $("input:radio[name=machineMaintenance]:checked").val(),
                        "machineFacility": $("input:radio[name=machineFacility]:checked").val(),
                        "methodSSOW": $("input:radio[name=methodSSOW]:checked").val(),
                        "methodRuling": $("input:radio[name=methodRuling]:checked").val(),
                        "methodRiskAssessment": $("input:radio[name=methodRiskAssessment]:checked").val(),
                        "environmentFloor": $("input:radio[name=environmentFloor]:checked").val(),
                        "environmentLighting": $("input:radio[name=environmentLighting]:checked").val(),
                        "environmentCondition": $("input:radio[name=environmentCondition]:checked").val(),
                        "whyModels": whyModels,
                        "otherCounterMeasures": otherCounterMeasures,
                        "vantecWayDecision":document.getElementById('vantecWayDecision').value,
                        "vantecWayComments":document.getElementById('vantecWayComments').value,
                        "vantecWayDecisionTree":document.getElementById('decisionTree').innerHTML,
                        "investigationTeamNames":document.getElementById('investigationTeamNames').value
                        
                    };

                }
                var injuredPersons = new Array();
                if (submitAction === 'L4_SAVED' || submitAction === 'L4_APPROVED') {
                    // Fill up H&S Details
                    category = $('#category').val();
                    categoryLevel = $('#categoryLevel').val();
                    type = $('#type').val();
                    actOrCondition = $('#actOrConditon').val();
                    isDangerousOccurence = $('#isDangerousOccurence').val() === 'Yes' ? true : $('#isDangerousOccurence').val() === 'No' ? false : '';
                    hsSummary = $('#hsSummary').val();
                    classification = $('#classification').val();

                    var injuredPersonsCnt = $('.clonedInjuredDetails').length;
                    for (var m = 0; m < injuredPersonsCnt; m++) {
                        var numCnt = m + 1;
                        injuredPersons[m] = {
                            "injuredPersonId": $('#injuredPersonId_' + numCnt).val(),
                            "injuredPersonName": $('#injuredPersonName_' + numCnt).val(),
                            "hseReportedDate": moment($('#injuredPersonHSEReportedDate_' + numCnt).find('.person-hse-date-2').val(), 'DD/MM/YYYY').toJSON(),
                            "treatmentAndDetails": $('#injuredPersonTreatmentDetails_' + numCnt).val(),
                            "daysLost": $('#dayLost_' + numCnt).val(),
                            "isRIDDOR": $('#isRIDDORToggle_' + numCnt).is(':checked'),
                            "riddorReference": $('#injuredPersonRiddorRef_' + numCnt).val()
                        }
                    }

                }

                //incident report + INITIAL REPORT fields
                incidentRequest = {"siteId": siteId, "location": location, "incidentDateTime": incidentDateTime, "locationOther": locationOther, "question1Text": question1Text, "question2Text": question2Text,
                    "question3Text": question3Text, "question4Text": question4Text, "question1Answer": question1Ans, "question2Answer": question2Ans, "question3Answer": question3Ans, "question4Answer": question4Ans,
                    "incidentType": incidentType, "incidentBrief": incidentBrief, "eventSeverity": eventSeverity,"requestorId": requestorId, "incidentDetail": incidentDetail, "actionType": actionType, "actionTypeOther": actionTypeOther,
                    "area": area, "lighting": lighting, "temperature": temperature, "noise": noise, "surfaceType": surfaceType, "surfaceCondition": surfaceCondition, "personInvolved": personInvolved, "licenseDetails": licenseDetails,
                    "isActivityStandardProcess": isActivityStandardProcess, "stillageType": stillageType, "estimatedCost": estimatedCost, "estimatedCostOther": estimatedCostOther,"immediateCause": immediateCause, "keyPoints": keyPoints, "mainMessage": mainMessage,
                    "actions": actions, "filter": filter, "incidentNumber": incidentNumber, "deletedLicenseIds": deletedLicenseIds, "currApproverId": currApproverId, "isPersonInvolved": isPersonInvolved,
                    "approverComments": approverComments, "fiDetails": fiDetails, "injuredPersons": injuredPersons, "category": category, "categoryLevel": categoryLevel, "type": type, "actOrCondition": actOrCondition, 
                    "classification":classification,"isDangerousOccurence": isDangerousOccurence, "hsSummary":hsSummary, "graId":graId};
            }

            //var jsonReq = encodeURIComponent(JSON.stringify(incidentRequest));
            var jsonReq = encodeURIComponent(incidentRequest);
            console.log(incidentRequest);
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(incidentRequest));
            form_data.append('function', 'saveIncident');
            form_data.append('connection', vsmsservice);


            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    console.log(response);
                    if (response.startsWith("OK")) {
                        var res = response.split("-");

                        var windowLocation = 'reportIncidentMessage.php?action=' + submitAction + '&id=' + res[1];
                        if (submitAction === 'L4_APPROVED') {
                            markClosed(res[1]);
                        }
                        sendIncidentEmails(submitAction, res[1]);
                        sendActionEmails(submitAction, res[1]);
                        if (submitAction !== 'L1_CREATED') {
                            uploadFiles(res[1]);
                            $("#overlay").fadeIn("slow");
                                    var delay = 5000;

                                   setTimeout(function ()
                                    {
                                        $("#overlay").fadeOut("fast");
                                        $('#container').fadeIn();
                                        alert("Incident Data saved");
                                        window.location.href = windowLocation;
                                    },
                                    delay
                                    );
                        } else {
                            window.location.href = windowLocation;
                        }

                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }});
        }
        function sendActionEmails(action, iid) {
        
            if(action === 'L2_SUBMITTED' || action === 'L2_APPROVED' || action === 'L3_SUBMITTED'  || action === 'L3_APPROVED' || action ==='L4_SAVED' || action === 'L4_APPROVED'){
                 $.ajax({
                url: "../masterData/actionsData.php?filter=NEWACTIONS&iid=" + iid,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    var actionIds = JSON.parse(response);
                    for(var i=0;i<actionIds.length;i++){
                         $.ajax({
                          type: "GET",
                          url: "sendEmails.php?incidentId=" + iid + "&actionId="+actionIds[i].id+"&function=ActionAssigned&type=Workflow&source=Incident",
                                success: function (data) {
                                    console.log(data);
                            }
                        });
                    }
                        
                   
                }});
            }
          
        }
        function sendIncidentEmails(action, iid) {
            var func, type =  "";
            
            if(action === 'L1_CREATED'){
                func = "ReportIncident";
                type= "Notification";
            } else if(action === 'L2_SUBMITTED'){
                func = "InitialReportSubmitted";
                type= "Workflow";
            }else if(action === 'L2_APPROVED'){
                func = "InitialReportApproved";
                type= "Notification";
            }else if(action === 'L2_REJECTED'){
                func = "InitialReportRejected";
                type= "Workflow";
            }else if(action === 'L3_SUBMITTED'){
                func = "FullInvestigationSubmitted";
                type= "Workflow";
            }else if(action === 'L3_REJECTED'){
                func = "FullInvestigationRejected";
                type= "Workflow";
            }else if(action === 'L3_APPROVED'){
                func = "FullInvestigationApproved";
                type= "Both";
            }else if(action === 'L4_APPROVED'){
                func = "IncidentSignoffed";
                type= "Notification";
            }else if(action === 'DELETED'){
                func = "IncidentDeleted";
                type= "Notification";
            }else if(action === 'L4_REJECTED'){
                func = "HSRejected";
                type= "Workflow";
            }
            $.ajax({
                type: "GET",
                url: "sendEmails.php?incidentId=" + iid + "&function="+func+"&type="+type,
                success: function (data) {
                    console.log(data);
                    window.location.href = 'reportIncidentMessage.php?action=' + action + '&id=' + iid;
                }
            });

        }
        function markClosed(id) {
            $.ajax({
                url: "../masterData/incidentsData.php?filter=MARKCLOSED&iid=" + id,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    console.log(response);
                }});
        }
        function deleteChanges() {
            var comments = prompt("Are you sure you want to delete this Incident Report? This action cannot be undone. Please add the reason for deletion.");
            var incidentId = '<?php echo $iid ?>';
            if (comments !== null && comments !== '') {
                var filter = "?incidentNumber=" +incidentId + "&comments=" + encodeURIComponent(comments) + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "deleteIncident", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response === 'OK') {
                             sendIncidentEmails("DELETED", incidentId);
                            window.location.href = 'reportIncidentMessage.php?action=REPORT_DELETE&id=' + incidentId;
                        }

                    }
                });
            }
        }
      $("#bYes").on('click', function () {
            var comments = $('#promptComments').val();
            var incidentId = '<?php echo $iid ?>';
            var prevApproverId = '<?php echo $prevApproverId ?>';
            var action = $('#promptAction').val();
            var flow = action.split('_')[1];
         
                if(flow === 'APPROVED'){
                      submitIncidentData(action, comments);
                }else{
                       if (comments !== null && comments !== '') {
                var filter = "?incidentNumber=" +incidentId + "&comments=" + encodeURIComponent(comments) + "&approverId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>'
                               + "&prevApproverId=" + prevApproverId;
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "rejectIncident", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response === 'OK') {
                             sendIncidentEmails(action, incidentId);
                            //window.location.href = 'reportIncidentMessage.php?action=' + action + '&id=' + incidentId;
                        }

                    }
                });
            }
            }
        });
        function rejectIncident(elem) {
            var action = (elem === "Initial Report") ? 'L2_REJECTED' :(elem === "HS Signoff") ?'L4_REJECTED': 'L3_REJECTED';
            if(action === 'L2_REJECTED'){
		$('#promptMessage').html('Are you sure you want to reject Initial Report? It will be sent back to the Reporter for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>');
            }else if (action === 'L4_REJECTED'){
                $('#promptMessage').html('Are you sure you want to reject the Incident Report? It will be sent back to the Full Investigation Approver for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>');
            }else{
		$('#promptMessage').html('Are you sure you want to reject Full Investigation? It will be sent back to the Site Management for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>');
            }
            $('#promptAction').val(action);
            $('#promptModal').modal('show');
        }

    </script>
</html>
