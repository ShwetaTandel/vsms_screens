<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
?>
<html>
    <head>
        <title>Vantec Safety Management System-Notification Groups</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>

        <style>
            .step {
                list-style: none;
                margin: .2rem 0;
                width: 100%;
            }

            .step .step-item {
                -ms-flex: 1 1 0;
                flex: 1 1 0;
                margin-top: 0;
                min-height: 1rem;
                position: relative; 
                text-align: center;
            }

            .step .step-item:not(:first-child)::before {
                background: #0069d9;
                content: "";
                height: 2px;
                left: -50%;
                position: absolute;
                top: 9px;
                width: 100%;
            }

            .step .step-item a {
                color: #acb3c2;
                display: inline-block;
                padding: 20px 10px 0;
                text-decoration: none;
            }

            .step .step-item a::before {
                background: #0069d9;
                border: .1rem solid #fff;
                border-radius: 50%;
                content: "";
                display: block;
                height: .9rem;
                left: 50%;
                position: absolute;
                top: .2rem;
                transform: translateX(-50%);
                width: .9rem;
                z-index: 1;
            }

            .step .step-item.active a::before {
                background: #fff;
                border: .1rem solid #0069d9;
            }

            .step .step-item.active ~ .step-item::before {
                background: #e7e9ed;
            }

            .step .step-item.active ~ .step-item a::before {
                background: #e7e9ed;
            }
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }
        </style>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/>
        <br/>
        <div class="container-fluid">
            <div class="row">

                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label">Site</label>
                        <select class="custom-select" id="site" onchange="showStepper()">
                            <?php
                            include ('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site;');
                            echo "<option value></option>";
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class='row'>

                <div class="col-12">
                    <h2 class="text-center">Notification Groups</h2>      
                    <br/><br/>
                    <ul class="step d-flex flex-wrap" id="stepper">
                        <li class="step-item" id="A">
                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group A </a>
                            <br/>
                            <span style="color: gray">Site Management (supervisors, managers, operations manager)</span>
                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">A</span>
                            </label>
                            <br/>

                        </li>
                        <li class="step-item" id="B">

                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group B </a>
                            <br/>
                            <span style="color: gray">Site H&S Officer</span>

                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">B</span>
                            </label>
                            <br/>
                        </li>
                        <li class="step-item" id="D">

                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group D </a>
                            <br/>
                            <span style="color: gray">Senior Management (GM and Divisional Lead)</span>

                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">D</span>
                            </label>
                            <br/>
                        </li>
                        <li class="step-item" id="F">

                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group F </a>
                            <br/>
                            <span style="color: gray">FCE</span>

                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">F</span>
                            </label>
                            <br/>
                        </li>
                        <li class="step-item" id="H">

                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group H </a>
                            <br/>
                            <span style="color: gray">H&S Management (managers and GM)</span>

                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox" >
                                <span class="glyphicon glyphicon-ok">H</span>
                            </label>
                            <br/>
                        </li>
                         <li class="step-item" id="Z">

                            <a href="#!" class="" style="font-size: large;font-weight: bolder;color: black">Group Z </a>
                            <br/>
                            <span style="color: gray">Senior Management</span>

                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox" >
                                <span class="glyphicon glyphicon-ok">Z</span>
                            </label>
                            <br/>
                        </li>
                    </ul> 
                </div>

            </div>
        </div>
        <br/>
        <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
        <div class="container">
            <table id="sites" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Site Name</th>
                        <th>Site Code</th>
                        <th></th>
                    </tr>
                </thead>

            </table>
            <div class="row">
                <div class="pull-right">
                    <a class="btn btn-orange" href="home.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                </div>
            </div>
        </div>

        <div id="mAddRecipient" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Recipient</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Site name</label>
                            <div class="controls">
                                <input type="hidden" id="recipientSiteId" class="form-control" value=""></input>
                                <input type="text" name="recipientSiteName" id="recipientSiteName" class="form-control " required maxlength="50" value="" readonly></input>

                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 column">
                                <table class="table table-bordered table-hover" id="tab_recipients">
                                    <thead>
                                        <tr >
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th class="text-center">
                                                Group
                                            </th>
                                            <th class="text-center">
                                                User Name
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id='recipient0'>
                                            <td>
                                                1
                                                <input type="hidden" value="0" name= "id[]" class="id"/>
                                            </td>
                                            <td>

                                                <select class="custom-select group" id="group" name="group[]">
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.groups;');
                                                    echo "<option value></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['group_name'] . '">' . $row['group_name'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="custom-select user" id="user" name="user[]">
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users where active=true order by concat(first_name, last_name) asc ');
                                                    echo "<option value></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr id='recipient1'></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button id="add_recipient" class="btn btn-blue pull-left"  type="button">+</button>
                        <button id='delete_recipient' class="pull-right btn btn-red"  type="button">-</button>
                        <br/>
                        <div id="recipientShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                        <div id="recipientShowDataError" class="showError alert alert-danger" style="display: none"><strong>Please don't duplicate values.</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mSaveDivision" onclick="addRecipients()">SAVE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>
        
        
        <div id="mRemoveRecipient" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Remove Recipients</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Site name</label>
                            <div class="controls">
                                <input type="hidden" id="remRecipientSiteId" class="form-control" value=""></input>
                                <input type="text" name="remRecipientSiteName" id="remRecipientSiteName" class="form-control " required maxlength="50" value="" readonly></input>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="input-group-text">Select Group</label>
                            <select class="custom-select" id="recGroups" name="recGroups" onchange="populateRecipientsForSite(this)">
                                 <?php
                                                    include ('../config/phpConfig.php');
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.groups;');
                                                    echo "<option value></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['group_name'] . '">' . $row['group_name'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                            </select>
                        </div>
                        <div class="control-group">
                            <select class="custom-select" id="recipients" name="recipients" multiple="true">
                            </select>
                        </div>
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mRemove" onclick="removeRecipients()">REMOVE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>

        <br/>
        <script>
            $(document).ready(function () {
                var k = 1;
                var deptTable;
                $("#add_dept").click(function () {
                    d = k - 1
                    $('#deptRow' + k).html($('#deptRow' + d).html()).find('td:first-child').html(k + 1);
                    $('#tab_depts').append('<tr id="deptRow' + (k + 1) + '"></tr>');
                    k++;
                });
                $("#delete_dept").click(function () {
                    if (k > 1) {
                        $("#deptRow" + (k - 1)).html('');
                        k--;
                    }
                });


                var j = 1;
                $("#add_recipient").click(function () {
                    c = j - 1
                    $('#recipient' + j).html($('#recipient' + c).html()).find('td:first-child').html(j + 1);
                    $('#tab_recipients').append('<tr id="recipient' + (j + 1) + '"></tr>');
                    j++;
                });
                $("#delete_recipient").click(function () {
                    if (j > 1) {
                        $("#recipient" + (j - 1)).html('');
                        j--;
                    }
                });
                function format(d) {
                    // `d` is the original data object for the row
                    return '<table id="deptDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                            '<thead>' +
                            '<th>Department</th>' +
                            '<th></th>' +
                            '</thead>' +
                            '</table>';
                }
                var siteTable = $('#sites').DataTable({
                    retrieve: true,
                    ajax: {"url": "../masterData/sitesData.php", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-green' href='#' id ='bAddRecipients'><i class='fa fa-edit'></i> Add Recipients</a><span> </span><a class='btn btn-red' href='#' id ='bDelRecipients'><i class='fa fa-remove'></i> Remove Recipients</a>"
                        }],
                    buttons: [{extend: 'excel', filename: 'users', title: 'Sites Master'}],
                    columns: [{
                            data: "index",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "name"},
                        {data: "code"},
                        {data: ""}
                    ],
                    order: [[0, 'asc']]
                });
                $('#sites tbody').on('click', '#bAddRecipients', function () {
                    var data = siteTable.row($(this).parents('tr')).data();
                    $('#mAddRecipient').modal('show');
                    $('#tab_recipients tbody tr').each(function (i, tr) {
                        $("#delete_recipient").click();
                    });
                    $('#tab_recipients').find('select').val('');
                    document.getElementById("recipientSiteName").value = data.name;
                    document.getElementById("recipientSiteId").value = data.id;
                });
                 $('#sites tbody').on('click', '#bDelRecipients', function () {
                    var data = siteTable.row($(this).parents('tr')).data();
                    $('#mRemoveRecipient').modal('show');
                    document.getElementById("remRecipientSiteName").value = data.name;
                    document.getElementById("remRecipientSiteId").value = data.id;
                });

            });

            function populateRecipientsForSite(elem) {
                var group = $(elem).val();
                var selectList = document.getElementById("recipients");
                var siteId = document.getElementById("remRecipientSiteId").value;
                selectList.options.length = 0;
                $.ajax({
                    url: "../masterData/usersData.php?data=siterecipients&siteId=" + siteId+"&group="+group,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        var jsonData = JSON.parse(response);
                        var users = jsonData;
                        for (var i = 0; i < users.length; i++) {
                            var newOption = document.createElement('option');
                            newOption.setAttribute("value", users[i].id);
                            newOption.innerHTML = users[i].first_name + ' ' + users[i].last_name;
                            selectList.add(newOption);
                        }
                    }
                });
            }
            function removeRecipients(){
                var siteId = document.getElementById("remRecipientSiteId").value;
                var userIds = $('#recipients').val();
                if(userIds.length === 0){
                    alert('Please select at least one Recipient name');
                    return;
                }
                 $.ajax({
                    url: "../masterData/usersData.php?data=removerecipients&userIds="+userIds,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        alert("Selected recipients are removed from the notifications list.");
                         $('#mRemoveRecipient').modal('hide');
                         location.reload(true);
                      
                    }
                });
                
            }
            function addRecipients() {
                var recipients = new Array();
                var tempArr = new Array();
                var valid = true;
                var requestor = '<?php echo $_SESSION['vsmsUserData']['first_name']." ".$_SESSION['vsmsUserData']['last_name'] ?>';
                var siteId = document.getElementById("recipientSiteId").value;
                document.getElementById('recipientShowDataError').style.display = 'none';
                document.getElementById('recipientShowRequiredError').style.display = 'none';
                $('#tab_recipients tbody tr').each(function (i, tr) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var group = $(this).find('.group').val();
                        var userId = $(this).find('.user').val();
                        if (group !== '' && userId !== '') {
                            if (tempArr.includes(group + userId) === false) {
                                tempArr.push(group + userId);
                                recipients[i] = (group + "-" + userId);
                            } else {
                                valid = false;
                                document.getElementById('recipientShowDataError').style.display = 'block';
                            }
                        }
                    }
                });
                if (tempArr.length === 0) {
                    valid = false;
                    document.getElementById('recipientShowRequiredError').style.display = 'block';
                }
                if (valid) {
                    var notificationReq = {"siteId": siteId, "requestor": requestor, "approvers": recipients};
                    var jsonReq = JSON.stringify(notificationReq);
                    console.log(jsonReq);
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=addRecipients" + "&connection=" + vsmsservice,
                        type: 'GET',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            if (response.trim() === "OK") {
                                location.reload(true);
                            } else {
                                alert("Something went wrong. Please check with the administrator.");
                            }
                        }
                    });

                }
            }
            function showStepper() {
                var siteId = document.getElementById("site").value;
                $('#stepper').find('input:checkbox').prop('checked', false);
                $('#stepper').find('div').remove();

                $.ajax({
                    url: '../masterData/recipientsData.php',
                    data: {siteId: siteId},
                    type: 'GET',
                    success: function (response) {
                        var jsonData = JSON.parse(response);
                        for (var i = 0; i < jsonData.length; i++) {
                            var group = jsonData[i];
                            var ele = document.getElementById(group.group);
                            var div = $(document.getElementById(group.group)).find('div');
                            if (div !== null) {
                                div.remove();
                            }
                            if (ele === null) {
                            } else {
                                var recipients = group.users;
                                ele.children[4].firstElementChild.checked = true;
                                var dynaDiv = document.createElement('div');
                                ele.appendChild(dynaDiv);
                                for (var j = 0; j < recipients.length; j++) {
                                    dynaDiv.appendChild(document.createElement('br'));
                                    var span = document.createElement('span')
                                    span.innerHTML = recipients[j].first_name + " " + recipients[j].last_name;
                                    dynaDiv.appendChild(span);
                                }
                            }
                        }
                    }
                });
            }
        </script>
    </body>
</html>