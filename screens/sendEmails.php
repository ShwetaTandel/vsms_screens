<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$incidentId = $_GET['incidentId'];
$function = $_GET['function'];
$type = $_GET['type'];
$data = array();
$actionData = array();
$hazardData = array();
$graData = array();
if($incidentId !== 0){
    $serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/incidentsData.php?filter=INCIDENT&iid=" . $incidentId;
    $data = json_decode(file_get_contents($serviceUrl));
    ChromePhp::log($data);
}
$actionId = 0;
if(isset($_GET['actionId'])){
    $actionId = $_GET['actionId'];
    $source = $_GET['source'];
    
    if($source == 'Incident'){
        $serviceActionUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/actionsData.php?filter=ACTIONDATA&rowID=" . $actionId;
    }else if($source == 'Hazard'){
        $serviceActionUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/actionsData.php?filter=ACTIONDATAHAZARD&rowID=" . $actionId;
    }else if($source == 'Other'){
        $serviceActionUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/actionsData.php?filter=ACTIONDATAOTHER&rowID=" . $actionId;
    }else if($source == 'GRA'){
        $serviceActionUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/actionsData.php?filter=ACTIONDATAGRA&rowID=" . $actionId;
    }
    $actionData = json_decode(file_get_contents($serviceActionUrl));
    ChromePhp::log($serviceActionUrl); 
}
$hazardId = 0;
if(isset($_GET['hazardId'])){
    $hazardId = $_GET['hazardId'];
    $serviceHazardUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/hazardsData.php?filter=HAZARD&hid=" . $hazardId;
    $hazardData = json_decode(file_get_contents($serviceHazardUrl));
    ChromePhp::log($hazardData);
    ChromePhp::log($serviceHazardUrl);
}
$graId=0;
if(isset($_GET['graid'])){
      $graId = $_GET['graid'];
      $serviceGRAUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/graData.php?filter=GRA&graid=" . $graId;
      $graData = json_decode(file_get_contents($serviceGRAUrl));
      ChromePhp::log($graData);
      ChromePhp::log($serviceGRAUrl);
}

if ($data == null && $incidentId!=0) {
    echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the Home Page</h1>";
    die();
}
$groups = array();

if ($function == 'ReportIncident') {
    array_push($groups, 'A', 'D', 'B', 'H');
    if ($type === 'Notification') {
        toGroups('E01', $groups);
    }
    unset($groups);
    $groups = array();
    array_push($groups, 'Z');
    if ($data[0]->incident_type==='Serious Accident') {
            toGroups('E01_1', $groups);
        }
} else if ($function == 'InitialReportSubmitted') {
    if ($type === 'Workflow') {
        toApprover('E02');
    }
} else if ($function == 'InitialReportApproved') {
    if ($type === 'Notification') {
        array_push($groups, 'A', 'D', 'B', 'H');
        toGroups('E03', $groups);
    }
} else if ($function == 'InitialReportRejected') {
    if ($type === 'Workflow') {
        toApprover('E05');
    }
} else if ($function == 'FullInvestigationSubmitted') {
    if ($type === 'Workflow') {
        toApprover('E06');
    }
} else if ($function == 'FullInvestigationRejected') {
    if ($type === 'Workflow') {
        toApprover('E09');
    }
}  else if ($function == 'HSRejected') {
    if ($type === 'Workflow') {
        toApprover('E42');
    }
} else if ($function == 'FullInvestigationApproved') {
    if ($type === 'Both') {
        array_push($groups, 'A', 'H');
        toGroups('E07', $groups);
        toApprover('E08');
        unset($groups);
        $groups = array();
        array_push($groups, 'F');
        if ($data[0]->fullInvestigation->is_mhe_training_required==='1') {
            toGroups('E04', $groups);
        }
         $array = ["SABOTAGE, MALICIOUS DAMAGE", "SYSTEM INDUCED VIOLATION", "RECKLESS VIOLATION", "POSSIBLE NEGLIGENT ERROR"];
        if (stripos(json_encode($array),$data[0]->fullInvestigation->vantec_way_decision) !== false) {
            toHR();
        }
    }
} else if ($function == 'IncidentDeleted') {
    if ($type === 'Notification') {
        array_push($groups, 'A', 'D', 'B', 'H');
        toGroups('E11', $groups);
    }
} else if ($function == 'IncidentSignoffed') {
    if ($type === 'Notification') {
        array_push($groups, 'D', 'B', 'H');
        toGroups('E10', $groups);
    }
} else if ($function == 'IncidentApproverChanged') {
    
    if ($type === 'Both') {
        toApprover('E34'); /// Old approver
        toApprover('E41'); /// New approver
    }
    
}else if ($function == 'ActionCancelled'){
    if($type == 'Notification'){
        array_push($groups, 'A','B');
        toActionGroups('E19', $groups);
    }
    
}else if ($function == 'ActionAssigned'){
    
    if($type == 'Workflow'){
        $sent = toActionOwner('E16',$actionId, $source);
        if($sent == 'SENT'){
            markActionNotified($actionId);
        }
    }
    
}else if ($function == 'NewActionAssigned'){
    
    if($type == 'Both'){
        $sent = toActionOwner('E41',$actionId, $source);
        if($sent == 'SENT'){
            markActionNotified($actionId);
        }
        array_push($groups, 'A','B');
        toActionGroups('E40', $groups);

    }
    
}else if ($function == 'ActionComplete'){
    if($type == 'Notification'){
        array_push($groups, 'A','B');
        toActionGroups('E18', $groups);
    }
}else if ($function == 'ActionReopened'){
    if($type == 'Both'){
        array_push($groups, 'A','B');
        toActionGroups('E20', $groups);
        $sent = toActionOwner('E39',$actionId, $source);
        if($sent == 'SENT'){
            markActionNotified($actionId);
        }
    }
}else if ($function == 'ActionRescheduled'){
    if($type == 'Notification'){
        array_push($groups, 'A');
        toActionGroups('E38', $groups);
    }
}else if ($function == 'ActionReassigned'){
    if($type == 'Both'){
        array_push($groups, 'A');
        toActionGroups('E17', $groups);
        toActionOwner('E37', $actionId, $source);
    }
}else if ($function == 'HazardSubmitted'){
    if($type == 'Both'){
        array_push($groups, 'A');
        toHazardGroups('E13', $groups);
        toHazardApprover('E12');
    }
}else if ($function == 'HazardClosed'){
    if($type == 'Notification'){
        array_push($groups, 'A');
        toHazardGroups('E14', $groups);
        toHazardCreator('E14');
    }
}else if ($function == 'HazardDeleted'){
    if($type == 'Notification'){
        array_push($groups, 'A', 'B', 'H');
        toHazardGroups('E15', $groups);
        toHazardCreator('E15');
    }
} else if ($function == 'HazardApproverChanged') {
    
    if ($type === 'Both') {
        toHazardApprover('E36'); /// Old approver
        toHazardApprover('E12'); /// New approver
    }
    
}else if ($function == 'GRASubmitted') {
    if ($type === 'Workflow') {
        toGRAApprover('E43'); 
    }
    
}else if ($function == 'GRAApproved') {
    if ($type === 'Workflow') {
        toGRAApprover('E43');
        toGRASubmitter('E44'); 
    }
}else if ($function == 'GRASignedOff') {
    if ($type === 'Workflow') {
       array_push($groups, 'A','H');
       toGRAGroups('E45', $groups);
    }
}else if ($function == 'GRAApproverChanged') {
    if ($type === 'Workflow') {
        toGRAApprover('E43'); /// Old approver
        toGRAApprover('E47'); /// New approver
    }
}else if ($function == 'GRARejected') {
    if ($type === 'Workflow') {
        toGRASubmitter('E46'); 
    }
}
mysqli_close($connection);
//Send emails to the Groups 
function toGroups($emailType, $groups) {
    global $data;
    $comments = '';
    $ccTo = array();
    $mailTo = getEmailForGroupsBySite($groups, $data[0]->site_id);
    $email = new EmailHelper();
    $link = '';
    if ($emailType === 'E07' || $emailType === 'E03'){
        $tmpComments = getComments('APPROVED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if($emailType === 'E11'){
        $tmpComments = getComments('DELETED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E19'){
        $tmpComments = getActionComments('CANCELLED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E18'){
        $tmpComments = getActionComments('CLOSED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E20'){
        $tmpComments = getActionComments('RE-OPENED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E38'){
        $tmpComments = getActionComments('RE-SCHEDULED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }
    $sent = $email->sendIncidentEmails($data, $link, $mailTo, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}
function toActionGroups($emailType, $groups) {
    global $data, $actionData;
    $comments = '';
    $ccTo = array();
    $mailTo = getEmailForGroupsBySite($groups,  $actionData[0]->action_site);
    $email = new EmailHelper();
    $link = '';
    $prevOwner='';
    if ($emailType === 'E19'){
        $tmpComments = getActionComments('CANCELLED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E18'){
        $tmpComments = getActionComments('CLOSED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E20'){
        $tmpComments = getActionComments('RE-OPENED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if ($emailType === 'E38'){
        $tmpComments = getActionComments('RE-SCHEDULED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else  if ($emailType === 'E17') {
        $prevOwnerId = $_GET['prevOwnerId'];
        $prevOwner = getUserNameById($prevOwnerId);
        $ccTo = array_merge(getManagerEmailId($prevOwnerId), getEmailByUserId($prevOwnerId));
        $tmpComments = getActionComments('RE-ASSIGNED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);

    }
   $sent = $email->sendActionEmails($actionData, $link, $mailTo, $ccTo, $emailType, $comments,$prevOwner);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}

function toHazardGroups($emailType, $groups){
    global $hazardData;
    $comments='';
    if($emailType === 'E15'){
        $tmpComments = getHazardComments('DELETED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }
    $mailTos = getEmailForGroupsBySite($groups, $hazardData[0]->site_id);
    $email = new EmailHelper();
    $link = '';
    $ccTo = array();
    $sent = $email->sendHazardEmails($hazardData, $link, $mailTos, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
}
function toGRAGroups($emailType, $groups){
    global $graData;
    $comments='';
    $mailTos = getEmailForGroupsBySite($groups, $graData[0]->site_id);
    $email = new EmailHelper();
    $link = '';
    $ccTo = array();
    $sent = $email->sendRiskAssessmentEmails($graData, $link, $mailTos, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
}
function toHR(){
    global $data,$serverName;
      $mailTos = 'veu-hr-general-enquiry@vantec-gl.com1';
    if ($serverName === "vantecapps") {
        $mailTos = 'veu-hr-general-enquiry@vantec-gl.com';
    }
    $email = new EmailHelper();
    $sent = $email->sendVantecWayHRNotification($data, $mailTos);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
}
function toActionOwner($emailType, $actionId, $source) {
    global $data, $connection, $actionData;
    $link ='';
    $comments = '';
    $prevOwner = '';
    $ccTo = getManagerEmailId($actionData[0]->action_owner);
    $mailTo = getEmailByUserId($actionData[0]->action_owner);
    $email = new EmailHelper();
    if ($emailType === 'E16') {
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=".$source;
    }else  if ($emailType === 'E39') {
       
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=".$source;
        $tmpComments = getActionComments('RE-OPENED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else  if ($emailType === 'E37') {
        $prevOwnerId = $_GET['prevOwnerId'];
        $prevOwner = getUserNameById($prevOwnerId);
        $tmpComments = getActionComments('RE-ASSIGNED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=".$source;
    
    }else  if ($emailType === 'E41') {
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/action.php?filter=OPEN&id=" . $actionId."&source=".$source;
    }
    $sent = $email->sendActionEmails($actionData, $link, $mailTo, $ccTo, $emailType, $comments,$prevOwner);
     if($sent === 'FAILED'){
        logEmailActivity();
    }
    return $sent;
    
}

//send email to approver
function toApprover($emailType) {
    global $data, $connection;
    $link ='';
    $comments = '';
    $ccTo = array();
    $mailTo = getEmailForApprover();
    $email = new EmailHelper();
    if ($emailType === 'E02') {
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AIR&iid=" . $data[0]->incident_number;
    }else if ($emailType === 'E06') {
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $data[0]->incident_number;
    }else if($emailType === 'E05'){
        $tmpComments = getComments('REJECTED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=IR&iid=" . $data[0]->incident_number;
        $mailTo = getSubmitterEmailId();
    }else if($emailType === 'E09'){
        $tmpComments = getComments('REJECTED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=FIR&iid=" . $data[0]->incident_number;
        $mailTo = getSubmitterEmailId();
    }else if($emailType === 'E34'){
        $prevApproverId = $_GET['prevApprover'];
        $mailTo = getEmailByUserId($prevApproverId);
        $tmpComments = getComments('RE-ASSIGNED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    } else if($emailType === 'E41'){
        $tmpComments = getComments('RE-ASSIGNED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }  else if ($emailType === 'E08') {
         $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=HSSIGNOFF&iid=" . $data[0]->incident_number;
          $tmpComments = getComments('APPROVED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }else if($emailType === 'E42'){
        $tmpComments = getComments('REJECTED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportIncident.php?action=AFIR&iid=" . $data[0]->incident_number;
        //$mailTo = getSubmitterEmailId();
    }
    $sent = $email->sendIncidentEmails($data, $link, $mailTo, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}
function toGRAApprover($emailType) {
    global $graData;
    $link ='';
    $comments = '';
    $ccTo = array();
    $mailTo = getEmailByUserId($graData[0]->curr_approver_id);
    $email = new EmailHelper();
    if ($emailType === 'E43') {
        $status = $graData[0]->display_status;
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/genericRiskassessment.php?action=".$status."APPROVE&graid=" . $graData[0]->gra_id;
    }
	ChromePhp::log("here");
    $sent = $email->sendRiskAssessmentEmails($graData, $link, $mailTo, $ccTo, $emailType, $comments);
	ChromePhp::log("here");
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}
function toGRASubmitter($emailType) {
    global $graData;
    $link ='';
    $comments = '';
    $ccTo = array();
	$statusArr = explode("_",$graData[0]->status);
	
    $mailTo = getGRASubmitterEmailId($statusArr[0]);
    $email = new EmailHelper();
   if($emailType === 'E46'){
        $tmpComments = getGRAComments('REJECTED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
        $status = $graData[0]->status;
        $action = "";
        if($status === "L2_REJECTED") {
            $action = "L1SUBMIT";
        }else if($status === "L3_REJECTED") {
            $action = "L2APPROVE";
        }else if($status === "L4_REJECTED") {
            $action = "L3APPROVE";
        }
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/genericRiskAssessment.php?action=".$action."&graid=" . $graData[0]->gra_id;
        
    }
    $sent = $email->sendRiskAssessmentEmails($graData, $link, $mailTo, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}
function toHazardApprover($emailType) {
    global $hazardData, $connection;
    $link ='';
    $comments = '';
    $ccTo = array();
    $mailTo = getEmailByUserId($hazardData[0]->curr_approver_id);
    $email = new EmailHelper();
    if ($emailType === 'E12') {
        $link = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/reportHazard.php?action=HI&hid=" . $hazardData[0]->hazard_number;
    }else if ($emailType === 'E36'){
         $prevApproverId = $_GET['prevApprover'];
        $mailTo = getEmailByUserId($prevApproverId);
        $tmpComments = getHazardComments('RE-ASSIGNED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }
    $sent = $email->sendHazardEmails($hazardData, $link, $mailTo, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}
function toHazardCreator($emailType) {
    global $hazardData, $connection;
    $comments = '';
    $link ='';
    $ccTo = array();
    $mailTo = getEmailByUserId($hazardData[0]->creator_user_id);
    $email = new EmailHelper();
     if($emailType === 'E15'){
        $tmpComments = getHazardComments('DELETED');
        $comments = substr($tmpComments[0], strpos($tmpComments[0], ":") + 1);
    }
    $sent = $email->sendHazardEmails($hazardData, $link, $mailTo, $ccTo, $emailType, $comments);
    if($sent === 'FAILED'){
        logEmailActivity();
    }
    echo $sent;
    
}

function getEmailForApprover() {
    global $mDbName, $connection, $data, $serverName;
    if ($serverName === "vantecapps" ) {
        $sql = "select email_id as email from " . $mDbName . ".incident, " . $mDbName . ".users where curr_approver_id = users.id and incident_number =" . $data[0]->incident_number;
    } else {
        $sql = "SELECT concat('test',email_id ) as email from " . $mDbName . ".incident, " . $mDbName . ".users where curr_approver_id = users.id and incident_number =" . $data[0]->incident_number;
    }
 
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
 
    return $record;
}


function getEmailByUserId($userId) {
    global $mDbName, $connection, $data, $serverName, $actionData;
    if ($serverName === "vantecapps" ) {
            $sql = "SELECT email_id as email FROM " . $mDbName . ".users where active=true and users.id =" . $userId;
    } else {
            $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".users where active=true and users.id=" . $userId;
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
    
    return $record;
}
function getEmailForGroupsBySite($groups, $siteId) {
    global $mDbName, $connection,  $serverName;
   
    if ($serverName === "vantecapps") {
       $sql = "SELECT email_id as email FROM " . $mDbName . ".users, " . $mDbName . ".recipients where users.id = recipients.user_id and active=true and group_name in ('" . implode("','", $groups) . "') and site_id =" . $siteId ;
   } else {
       $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".users, " . $mDbName . ".recipients where users.id = recipients.user_id and active=true  and group_name in ('" . implode("','", $groups) . "') and site_id =" . $siteId ;
   }
   
    ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
    ChromePhp::log($record);
    return $record;
}

function getComments($status) {
    global $mDbName, $connection, $data, $serverName;
    $sql = "SELECT comments FROM " . $mDbName . ".incident_history where incident_number = ".$data[0]->incident_number." and status = '".$status. "' order by id desc Limit 1";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['comments'];
    }
    
    return $record;
}
function getGRAComments($status) {
    global $mDbName, $connection, $graData;
    $sql = "SELECT comments FROM " . $mDbName . ".gra_history where gra_id = ".$graData[0]->gra_id." and status = '".$status. "' order by id desc Limit 1";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['comments'];
    }
    
    return $record;
}
function getHazardComments($status) {
    global $mDbName, $connection, $hazardData, $serverName;
    $sql = "SELECT comments FROM " . $mDbName . ".hazard_history where hazard_id = ".$hazardData[0]->hazard_number." and status = '".$status. "' order by id desc Limit 1";
     ChromePhp::log($sql);
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['comments'];
    }
    
    return $record;
}

function getActionComments($status) {
    global $mDbName, $connection, $data, $serverName,$actionData;
    $sql = "SELECT comments FROM " . $mDbName . ".action_history where action_number = ".$actionData[0]->id." and status = '".$status. "' order by id desc Limit 1";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['comments'];
    }
    
    return $record;
}
function getGRASubmitterEmailId($status) {
    global $mDbName, $connection, $graData, $serverName;
    if ($serverName === "vantecapps") {
        $sql = "SELECT email_id as email FROM " . $mDbName . ".gra_history, " . $mDbName . ".users where gra_id = ".$graData[0]->gra_id. " and comments like 'Pending%'  and users.id = gra_history.updated_by_user_id and level = '".$status."' order by gra_history.id desc limit 1";
    }else{
         $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".gra_history, " . $mDbName . ".users where gra_id = ".$graData[0]->gra_id. " and comments like 'Pending%'  and users.id = gra_history.updated_by_user_id and level = '".$status."' order by gra_history.id desc limit 1";
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
	ChromePhp::log($sql);
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
    return $record;
}

function getSubmitterEmailId() {
    global $mDbName, $connection, $data, $serverName;
    if ($serverName === "vantecapps") {
        $sql = "SELECT email_id as email FROM " . $mDbName . ".incident_history, " . $mDbName . ".users where incident_number = ".$data[0]->incident_number. " and status = 'SUBMITTED'  and users.id = incident_history.updated_by_user_id order by incident_history.id desc limit 1";
    }else{
        $sql = "SELECT concat('test',email_id ) as email FROM vsms.incident_history, users where incident_number = ".$data[0]->incident_number. "  and status = 'SUBMITTED'  and users.id = incident_history.updated_by_user_id order by incident_history.id desc limit 1";
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
    return $record;
}
function getManagerEmailId($userId) {
    global $mDbName, $connection, $data, $serverName, $actionData;
    if ($serverName === "vantecapps") {
            $sql = "SELECT email_id  as email FROM " . $mDbName . ".users where concat(first_name, ' ', last_name) = (select manager_name from " . $mDbName . ".users where id = ".$userId.") ;";
        } else {
            $sql = "SELECT concat('test',email_id ) as email FROM " . $mDbName . ".users where concat(first_name, ' ', last_name) = (select manager_name from " . $mDbName . ".users where id = ".$userId.") ;";
    }
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $record[] = $row['email'];
    }
    
    return $record;
}

function markActionNotified($actionId){
    global $mDbName, $connection, $data, $serverName;
    $sql= "UPDATE ".$mDbName. ".actions set notified=true where id=".$actionId.";";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if($result == TRUE){
        echo "OK";
    }
}
function logEmailActivity(){
    global $mDbName, $connection, $data, $serverName;
   
    $sql= "INSERT INTO  ".$mDbName. ".email_log(success, url) VALUES ('FAIL','".$_SERVER['REQUEST_URI']."');";
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    if($result == TRUE){
        echo "OK";
    }
}
function getUserNameById($userId) {
    global $mDbName, $connection;
    $sql = "SELECT concat(first_name,' ',last_name ) as name FROM " . $mDbName . ".users where users.id=" . $userId;
    
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = '';
    while ($row = mysqli_fetch_assoc($result)) {
        $record = $row['name'];
    }
    
    return $record;
}

?>
