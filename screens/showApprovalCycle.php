<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
?>
<html>
    <head>
        <title>Vantec Safety Management System-Approvers</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>

        <style>
            .step {
                list-style: none;
                margin: .2rem 0;
                width: 100%;
            }

            .step .step-item {
                -ms-flex: 1 1 0;
                flex: 1 1 0;
                margin-top: 0;
                min-height: 1rem;
                position: relative; 
                text-align: center;
            }

            .step .step-item:not(:first-child)::before {
                background: #0069d9;
                content: "";
                height: 2px;
                left: -50%;
                position: absolute;
                top: 9px;
                width: 100%;
            }

            .step .step-item a {
                color: #acb3c2;
                display: inline-block;
                padding: 20px 10px 0;
                text-decoration: none;
            }

            .step .step-item a::before {
                background: #0069d9;
                border: .1rem solid #fff;
                border-radius: 50%;
                content: "";
                display: block;
                height: .9rem;
                left: 50%;
                position: absolute;
                top: .2rem;
                transform: translateX(-50%);
                width: .9rem;
                z-index: 1;
            }

            .step .step-item.active a::before {
                background: #fff;
                border: .1rem solid #0069d9;
            }

            .step .step-item.active ~ .step-item::before {
                background: #e7e9ed;
            }

            .step .step-item.active ~ .step-item a::before {
                background: #e7e9ed;
            }
             .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
    background-color: #469bc0 !important;
    color: white !important;
}
.btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
    background-color: #4db117 !important;
    color: white !important;
}
.btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
    background-color: #ef8929 !important;
    color: white !important;
}
.btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
    background-color: #ef292d !important;
    color: white !important;
}
.btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
    background-color: #696969 !important;
    color: white !important;
}
        </style>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/>
        <br/>
        <div class="container-fluid">
            <div class="row">

                <div class="col-6">
                    <div class="form-group">
                        <label class="control-label">Site</label>
                        <select class="custom-select" id="site" onchange="showStepper()">
                            <?php
                            include ('../config/phpConfig.php');
                            if (mysqli_connect_errno()) {
                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                            }
                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site;');
                            echo "<option value></option>";
                            while ($row = mysqli_fetch_array($result)) {
                                echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                            }
                            echo '';
                            mysqli_close($con);
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class='row'>

                <div class="col-12">
                    <h2 class="text-center">Approval Levels</h2>      
                    <br/><br/>
                    <ul class="step d-flex flex-wrap" id="stepper">
                        <li class="step-item" id="L1">
                            <a href="#!" class="">Level 1 - Reporter</a>
                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">L1</span>
                            </label>
                            <br/>

                        </li>
                        <li class="step-item" id="L2">

                            <a href="#!" class="">Level 2 - Investigator</a>
                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">L2</span>
                            </label>
                            <br/>
                        </li>
                        <li class="step-item" id="L3">

                            <a href="#!" class="">Level 3 - Approver </a>
                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox">
                                <span class="glyphicon glyphicon-ok">L3</span>
                            </label>
                            <br/>
                        </li>
                        <li class="step-item" id="L4">

                            <a href="#!" class="">Level 4 - Sign Off Actor</a>
                            <br/>
                            <label class="btn btn-info">
                                <input type="checkbox" id="l4">
                                <span class="glyphicon glyphicon-ok">L4</span>
                            </label>
                            <br/>
                        </li>
                    </ul> 
                </div>

            </div>
        </div>
        <br/>
        <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
        <div class="container">
            <table id="sites" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>Site Name</th>
                        <th>Site Code</th>
                        <th></th>
                    </tr>
                </thead>

            </table>
            <div class="row">
                <div class="pull-right">
                    <a class="btn btn-orange" href="home.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                    <a class="btn btn-green" href="#" id="btnAddSite"><i class="fa fa-plus"></i> Add New Site</a>
                </div>
            </div>
        </div>
        <div id="mCreateSite" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add a new Site</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Site name<span style="color: red">*</span></label>
                            <div class="controls">
                                <input type="text" name="nSiteName" id="nSiteName" class="form-control " required maxlength="50" value=""></input>

                            </div>
                        </div>
                        <br/>
                        <div class="control-group">
                            <label class="input-group-text">Site Code <span style="color: red">*</span></label>
                            <div class="controls">
                                <input type="text" name="nSiteCode" id="nSiteCode" class="form-control" required maxlength="50"></input>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <div id="nShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields and at least one department.</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mSaveDivision" onclick="saveSite('n')">SAVE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="mEditSite" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Site</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Site name<span style="color: red">*</span></label>
                            <div class="controls">
                                <input type="hidden" id="siteId" class="form-control" value=""></input>
                                <input type="text" name="eSiteName" id="eSiteName" class="form-control " required maxlength="50" value=""></input>

                            </div>
                        </div>
                        <br/>
                        <div class="control-group">
                            <label class="input-group-text">Site Code <span style="color: red">*</span></label>
                            <div class="controls">
                                <input type="text" name="eSiteCode" id="eSiteCode" class="form-control" required maxlength="50"></input>
                            </div>
                        </div>
                        <br/>
                        <div id="eShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mSaveDivision" onclick="saveSite('e')">SAVE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="mAddAppovers" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Approvers</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="control-group">
                            <label class="input-group-text">Site name</label>
                            <div class="controls">
                                <input type="hidden" id="approverSiteId" class="form-control" value=""></input>
                                <input type="text" name="approverSiteName" id="approverSiteName" class="form-control " required maxlength="50" value="" readonly></input>

                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12 column">
                                <table class="table table-bordered table-hover" id="tab_approver">
                                    <thead>
                                        <tr >
                                            <th class="text-center">
                                                #
                                            </th>
                                            <th class="text-center">
                                                Level
                                            </th>
                                            <th class="text-center">
                                                User Name
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id='approver0'>
                                            <td>
                                                1
                                                <input type="hidden" value="0" name= "id[]" class="id"/>
                                            </td>
                                            <td>

                                                <select class="custom-select level" id="level" name="level[]">
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.levels;');
                                                    echo "<option value></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['level'] . '">' . $row['level'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="custom-select user" id="user" name="user[]">
                                                    <?php
                                                    include ('../config/phpConfig.php');
                                                    if (mysqli_connect_errno()) {
                                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                    }
                                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users where is_approver=true and active=true order by concat(first_name, last_name) asc ');
                                                    echo "<option value></option>";
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        echo '<option value="' . $row['id'] . '">' . $row['first_name'] . ' ' . $row['last_name'] . '</option>';
                                                    }
                                                    echo '';
                                                    mysqli_close($con);
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr id='approver1'></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button id="add_approver" class="btn btn-blue pull-left"  type="button">+</button>
                        <button id='delete_approver' class="pull-right btn btn-red"  type="button">-</button>
                        <br/>
                        <div id="approverShowRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields.</strong></div>
                        <div id="approverShowDataError" class="showError alert alert-danger" style="display: none"><strong>Please don't duplicate values.</strong></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="mSaveDivision" onclick="addApprovers()">SAVE</button>
                        <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>

        <br/>
        <script>
            $(document).ready(function () {
                var k = 1;
                var deptTable;
                $("#add_dept").click(function () {
                    d = k - 1
                    $('#deptRow' + k).html($('#deptRow' + d).html()).find('td:first-child').html(k + 1);
                    $('#tab_depts').append('<tr id="deptRow' + (k + 1) + '"></tr>');
                    k++;
                });
                $("#delete_dept").click(function () {
                    if (k > 1) {
                        $("#deptRow" + (k - 1)).html('');
                        k--;
                    }
                });


                var j = 1;
                $("#add_approver").click(function () {
                    c = j - 1
                    $('#approver' + j).html($('#approver' + c).html()).find('td:first-child').html(j + 1);
                    $('#tab_approver').append('<tr id="approver' + (j + 1) + '"></tr>');
                    j++;
                });
                $("#delete_approver").click(function () {
                    if (j > 1) {
                        $("#approver" + (j - 1)).html('');
                        j--;
                    }
                });
                function format(d) {
                    // `d` is the original data object for the row
                    return '<table id="deptDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                            '<thead>' +
                            '<th>Department</th>' +
                            '<th></th>' +
                            '</thead>' +
                            '</table>';
                }
                var siteTable = $('#sites').DataTable({
                    retrieve: true,
                    ajax: {"url": "../masterData/sitesData.php", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-green' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-blue' href='#' id ='bAddApprovers'><i class='fa fa-edit'></i> Add Approvers</a>"
                        }],
                    buttons: [{extend: 'excel', filename: 'users', title: 'Sites Master'}],
                    columns: [{
                            data: "index",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "name"},
                        {data: "code"},
                        {data: ""}
                    ],
                    order: [[0, 'asc']]
                });
                $("#btnAddSite").on("click", function () {
                    $('#mCreateSite').modal('show');
                });

                $('#sites tbody').on('click', '#bEdit', function () {
                    var data = siteTable.row($(this).parents('tr')).data();
                    $('#mEditSite').modal('show');
                    document.getElementById("eSiteName").value = data.name;
                    document.getElementById("eSiteCode").value = data.code;
                    document.getElementById("siteId").value = data.id;
                });
                $('#sites tbody').on('click', '#bAddApprovers', function () {
                    var data = siteTable.row($(this).parents('tr')).data();
                    $('#mAddAppovers').modal('show');
                     $('#tab_approver tbody tr').each(function (i, tr) {
                         $("#delete_approver").click();
                     });
                     $('#tab_approver').find('select').val('');
                    document.getElementById("approverSiteName").value = data.name;
                    document.getElementById("approverSiteId").value = data.id;
                });

            });

            function populateApproversForSite(siteId) {
                var selectList = document.getElementById("user");
                selectList.options.length = 0;
                $.ajax({
                    url: "../masterData/usersData.php?data=siteapprovers&siteId=" + siteId,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        alert(response);
                        var jsonData = JSON.parse(response);
                        var users = jsonData;
                        for (var i = 0; i < users.length; i++) {
                                var newOption = document.createElement('option');
                                newOption.setAttribute("value", users[i].user_id);
                                newOption.innerHTML = users[i].first_name + ' ' + users[i].last_name;
                                selectList.add(newOption);
                        }
                    }
                });
            }
            function addApprovers() {
                var approvers = new Array();
                var tempArr = new Array();
                var valid = true;
                var requestor = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                var siteId = document.getElementById("approverSiteId").value;
                document.getElementById('approverShowDataError').style.display = 'none';
                document.getElementById('approverShowRequiredError').style.display = 'none';
                $('#tab_approver tbody tr').each(function (i, tr) {
                    var html = $(this).html();
                    if (html !== '')
                    {
                        var level = $(this).find('.level').val();
                        var userId = $(this).find('.user').val();
                        if (level !== '' && userId !== '') {
                            if (tempArr.includes(level + userId) === false) {
                                tempArr.push(level + userId);
                                approvers[i] = (level + "-" + userId);
                            } else {
                                valid = false;
                                document.getElementById('approverShowDataError').style.display = 'block';
                            }
                        }
                    }
                });
                if (tempArr.length === 0) {
                    valid = false;
                    document.getElementById('approverShowRequiredError').style.display = 'block';
                }
                if (valid) {
                    var approverReq = {"siteId": siteId, "requestor": requestor, "approvers": approvers};
                    var jsonReq = JSON.stringify(approverReq);
                    console.log(jsonReq);
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=addApprovers" + "&connection=" + vsmsservice,
                        type: 'GET',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (response) {
                            if (response.trim() === "OK") {
                                location.reload(true);
                            } else {
                                alert("Something went wrong. Please check with the administrator.");
                            }
                        }
                    });

                }
            }
            function saveSite(id) {
                //Create division data
                document.getElementById(id + 'ShowRequiredError').style.display = 'none';
                var requestorId = '<?php echo$_SESSION['vsmsUserData']['id']; ?>';
                var siteName = document.getElementById(id + 'SiteName').value;
                var siteCode = document.getElementById(id + 'SiteCode').value;
                var siteId = document.getElementById('siteId').value;

                if (siteName === '' || siteCode === '') {
                    document.getElementById(id + 'ShowRequiredError').style.display = 'block';
                    return;
                }

                var siteReq = {"siteId": siteId, "siteName": siteName, "siteCode": siteCode, "requestorId": requestorId};
                var jsonReq = encodeURIComponent(JSON.stringify(siteReq));
                console.log(jsonReq);
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=saveSite" + "&connection=" + vsmsservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response) {
                        if (response.trim() === "OK") {
                            if (id === 'n') {
                                alert("New Site created");
                                $('#mCreateSite').modal('hide');
                            }
                            else {
                                alert("Site information edited");
                                $('#mEditSite').modal('hide');
                            }
                            location.reload(true);

                        } else {
                            alert("Something went wrong. Please check with the administrator.");
                        }
                    }
                });
            }

            function showStepper() {
                var siteId = document.getElementById("site").value;
                $('#stepper').find('input:checkbox').prop('checked', false);
                $('#stepper').find('div').remove();
                $('#stepper1').find('input:checkbox').prop('checked', false);
                $('#stepper1').find('div').remove();

                $.ajax({
                    url: '../masterData/approversData.php',
                    data: {siteId: siteId},
                    type: 'GET',
                    success: function (response) {
                        var jsonData = JSON.parse(response);
                        for (var i = 0; i < jsonData.length; i++) {
                            var level = jsonData[i];
                            var ele = document.getElementById(level.level);
                            var div = $(document.getElementById(level.level)).find('div');
                            if (div !== null) {
                                div.remove();
                            }
                            if (ele === null) {
                            } else {
                                var approvers = level.users;
                                ele.children[2].firstElementChild.checked = true;
                                var dynaDiv = document.createElement('div');
                                ele.appendChild(dynaDiv);
                                for (var j = 0; j < approvers.length; j++) {
                                    dynaDiv.appendChild(document.createElement('br'));
                                    var span = document.createElement('span')
                                    span.innerHTML = approvers[j].first_name + " " + approvers[j].last_name;
                                    dynaDiv.appendChild(span);
                                }
                            }
                        }
                    }
                });
            }
        </script>
    </body>
</html>