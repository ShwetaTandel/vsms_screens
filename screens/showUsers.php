<!--This page renders all the USERS -->
<?php
session_start();
include ('../config/phpConfig.php');
include ('../config/ChromePhp.php');
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$siteData = array();
$sql = 'SELECT code FROM ' . $mDbName . '.site;';
$result = mysqli_query($con, $sql);
while ($row = mysqli_fetch_array($result)) {
    $siteData[] = $row;
}
?>
<html>
    <head>
        <title>Vantec Safety Management System - Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js" ></script>
        
        <!--Data tables -->
      <!--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>-->
      <script src="../js/datatables.min.js"></script>
      <script src="../js/bootstrap.bundle.min.js"></script>
      <script src="../js/bootstrap-select.min.js"></script>
      
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
    background-color: #469bc0 !important;
    color: white !important;
}
.btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
    background-color: #4db117 !important;
    color: white !important;
}
.btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
    background-color: #ef8929 !important;
    color: white !important;
}
.btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
    background-color: #ef292d !important;
    color: white !important;
}
.btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
    background-color: #696969 !important;
    color: white !important;
}


        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Users Master Data</h1>      
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <table id="users" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email Id</th>
                        <th>Approver?</th>
                        <th>Action Owner?</th>
                          <th>Is active?</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
            <div class="row" style="margin-left: 5px">
                <div class="pull-right">
                    <a class="btn btn-orange" href="home.php" id="btnToTop"><i class="fa fa-arrow-left"></i> Back</a>
                    <a class="btn btn-green" href="#" id="bCreateNew"><i class="fa fa-plus"></i> Add User</a>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mCreateNew">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalPreviewLabel">User Master</h5>
                    </div>
                    <div class="modal-body">
                        <div  class=" col-md-12">
                            <div style="display: none;">
                                <input type="text" id="PreventChromeAutocomplete" name="PreventChromeAutocomplete" autocomplete="address-level4" />
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">First Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="hidden" name="nUserId" id="nUserId" value = "0"/>
                                            <input type="text" name="nFirstName" id="nFirstName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Last Name<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="nLastName" id="nLastName" class="form-control" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Email<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="email" name="nEmail" id="nEmail" class="form-control" autocomplete="false">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Re-enter email<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <input type="password" name="nEmailTwo" id="nEmailTwo" class="form-control" autocomplete="new-password">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="control-group">
                                        <label class="control-label">Select Site<span style="color: red">*</span></label>
                                        <div class="controls">
                                            <select id ="nSites" class="selectpicker" multiple="multiple" data-live-search="true">
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site;');

                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="control-group">
                                        <label class="control-label"> Is Approver at any level?</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nApproverToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="control-group">
                                        <label class="control-label"> Is Action owner?</label>
                                        <div class="controls">
                                            <input type="checkbox" data-toggle="toggle" id='nActionOwnerToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Manager</label>
                                            <br/>
                                            <select class="custom-select" id="nManager" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.USERS;');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['first_name'].' '.$row['last_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                            </div>
                            <br/>
                        </div>
                        <br/>
                    </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-gray" data-dismiss="modal">Close</button>
                    <button type="button" id="saveNew" class="btn btn-green">Create</button>
                </div>
                    </div>
            </div>
        </div>
   
    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id ="mEditUser">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalPreviewLabel">User Master</h5>
                </div>
                <div class="modal-body">
                    <div  class=" col-md-12">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label">First Name<span style="color: red">*</span></label>
                                    <div class="controls">
                                        <input type="hidden" name="eUserId" id="eUserId" value = "0"/>
                                        <input type="text" name="eFirstName" id="eFirstName" class="form-control" >
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label">Last Name<span style="color: red">*</span></label>
                                    <div class="controls">
                                        <input type="text" name="eLastName" id="eLastName" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <input type="hidden" id="origEmail"/>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label">Email<span style="color: red">*</span></label>
                                    <div class="controls">
                                        <input type="email" name="eEmail" id="eEmail" class="form-control" >
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label">Re-enter email <span style="color: red">[if you change your email id ]</span></label>
                                    <div class="controls">
                                        <input type="password" name="eEmailTwo" id="eEmailTwo" class="form-control" autocomplete="new-password">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br/>


                        <div class="row" >
                            <div class="col-md-6">
                                <div class="control-group">
                                    <label class="control-label">Select Site<span style="color: red">*</span></label>
                                    <div class="controls">
                                        <select id ="eSites" class="selectpicker" multiple="multiple" data-live-search="true">
                                            <?php
                                            include ('../config/phpConfig.php');
                                            if (mysqli_connect_errno()) {
                                                echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                            }
                                            $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site;');

                                            echo "<option value></option>";
                                            while ($row = mysqli_fetch_array($result)) {
                                                echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                            }
                                            echo '';
                                            mysqli_close($con);
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="control-group">
                                    <label class="control-label"> Is Approver at any level?</label>
                                    <div class="controls">
                                        <input type="checkbox" data-toggle="toggle" id='eApproverToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="control-group">
                                    <label class="control-label"> Is Action owner?</label>
                                    <div class="controls">
                                        <input type="checkbox" data-toggle="toggle" id='eActionOwnerToggle'  data-on="Yes" data-off="No" data-onstyle="primary" data-offstyle="danger"/>
                                    </div>
                                </div>
                            </div>

                            <br/>
                        </div>
                        <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Manager</label>
                                            <br/>
                                            <select class="custom-select" id="eManager" required>
                                                <?php
                                                include ('../config/phpConfig.php');
                                                if (mysqli_connect_errno()) {
                                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                                }
                                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.USERS;');
                                                echo "<option value></option>";
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo '<option value="' . $row['id'] . '">' . $row['first_name'].' '.$row['last_name'] . '</option>';
                                                }
                                                echo '';
                                                mysqli_close($con);
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-red" data-dismiss="modal">Close</button>
                        <button type="button" id="saveEdit" class="btn btn-green" onclick="submitUserData('e')">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="modal fade bd-example-modal-sm" id="confDelete" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" >
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete: <strong><span id="delMessage2">*</span></strong></h5>
                    </div>
                    <br>
                    <div align="center">
                        <h4>Are you sure you want to prevent this user from logging in? <strong><span id="delMessage">*</span></strong></h4>
                    </div>

                    <div class="modal-footer" >
                        <input type="hidden" value="0" id = "delUserId"/>
                        <button type="button" class="btn btn-green" id="conDel">Yes</button>
                        <button type="button" class="btn btn-red"  data-dismiss="modal" >No</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('eSites').selectpicker();
                $('nSites').selectpicker();
                var usersTable = $('#users').DataTable({
                    ajax: {"url": "../masterData/usersData.php?data=users", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bEdit'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-red' href='#' id='bDelete'><i class='fa fa-remove'></i> Mark Inactive</a> "
                        }
                    ],
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            data: "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {data: "first_name"},
                        {data: "last_name"},
                        {data: "email_id"},
                        {data: "is_approver",
                            render: function (data, type, row) {

                                return (data === '1') ? '<span style="color:green" class="fa fa-check"></span>' : '<span style="color: red;" class="fa fa-remove"></span>';
                            }},
                        {data: "is_action_owner",
                            render: function (data, type, row) {

                                return (data === '1') ? '<span style="color:green" class="fa fa-check"></span>' : '<span style="color: red;" class="fa fa-remove"></span>';
                            }},
                       {data: "active",
                            render: function (data, type, row) {

                                return (data === '1') ? '<span style="color:green" class="fa fa-check"></span>' : '<span style="color: red;" class="fa fa-remove"></span>';
                            }},
                        {data: ""}

                    ],
                    order: [[0, 'asc']]
                });
                $("#bCreateNew").on("click", function () {
                    $('#mCreateNew').modal('show');

                });
                $('#users tbody').on('click', '#bDelete', function () {
                    var data = usersTable.row($(this).parents('tr')).data();
                    $('#confDelete').modal('show');
                    document.getElementById('delUserId').value = data.id;
                });
                $("#conDel").on("click", function () {
                    var rowID = document.getElementById('delUserId').value;
                    $.ajax({
                        type: "GET",
                        url: "../masterData/usersData.php?data=markinactive&userId=" + rowID,
                        success: function (data) {
                            if (data === 'OK') {
                                alert('The login is now disabled for this user.');
                                window.location.href = 'showUsers.php';
                            }
                        }
                    });
                });
                $('#users tbody').on('click', '#bEdit', function () {
                    var data = usersTable.row($(this).parents('tr')).data();
                    $('#mEditUser').modal('show');
                    document.getElementById("eUserId").value = data.id;
                    document.getElementById("eFirstName").value = data.first_name;
                    document.getElementById("eLastName").value = data.last_name;

                    document.getElementById("eEmail").value = data.email_id;
                    document.getElementById("origEmail").value = data.email_id;
                    $("#eManager option").prop('disabled', false);
                    if(data.manager_name === null || data.manager_name === ''){
                        $("#eManager").val(-1);
                    }else{
                       $("#eManager option:contains(" + data.manager_name + ")").attr('selected', 'selected');
                       
                    }
                    $("#eManager option:contains(" + (data.first_name + ' '+data.last_name) + ")").prop('disabled', true);
                    if (data.is_approver === '1') {
                        $("#eApproverToggle").prop('checked', true).change();

                    } else {
                        $("#eApproverToggle").prop('checked', false).change();

                    }
                    if (data.is_action_owner === '1') {
                        $("#eActionOwnerToggle").prop('checked', true).change();

                    } else {
                        $("#eActionOwnerToggle").prop('checked', false).change();

                    }
                    $.ajax({
                        type: "GET",
                        url: "../masterData/usersData.php?data=site&userId=" + data.id,
                        success: function (data) {
                            var jsonData = JSON.parse(data);
                            var site = [];


                            for (var i = 0; i < jsonData.length; i++) {
                                site.push(jsonData[i].site_id);
                            }
                            $('#eSites').selectpicker('val', site);
                            $("#eSites").selectpicker('refresh');
                        }
                    });
                    if (data.is_approver === '1') {
                        $("#eApproverToggle").prop('checked', true).change();

                    } else {
                        $("#eApproverToggle").prop('checked', false).change();

                    }
                });
                $("#saveNew").on("click", function () {

                    var requestorId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                    var firstName = document.getElementById("nFirstName").value;
                    var lastName = document.getElementById("nLastName").value;

                    var email = document.getElementById("nEmail").value;
                    var email2 = document.getElementById("nEmailTwo").value;
                    var managerEle = document.getElementById("nManager");
                    
                    var managerName = managerEle.options [managerEle.selectedIndex]=== undefined ? '':managerEle.options [managerEle.selectedIndex].text;
                    var sitesList = $('#nSites').val();
                    var isApprover = document.getElementById("nApproverToggle").checked;
                    var isActionOwner = document.getElementById("nActionOwnerToggle").checked;
                    if (firstName === '' || lastName === '' || email === '' || email2 === '') {
                        alert("Please enter all the fields. All fields are mandatory");
                        return;
                    }
                    if(isActionOwner && managerName=== '' ){
                       alert("Please select a Manager, if the user is an Action Owner.");
                        return;
                    }
                    var re = /\S+@\S+\.\S+/;
                    if (!re.test(email)) {
                        alert("Please enter valid email id");
                        return;
                    }

                    if (email !== email2) {
                        alert("Both the email addresses should match. Please correct them. ")
                        return;
                    }

                    if (sitesList.length === 0) {
                        alert("Please select at least one Site");
                        return;
                    }
                    var userReq = {"firstName": firstName, "lastName": lastName, "emailId": email,
                        "isApprover": isApprover, "siteList": sitesList, "requestorId": requestorId, "isActionOwner": isActionOwner, "managerName":managerName};

                    var jsonReq = encodeURIComponent(JSON.stringify(userReq));
                    console.log(jsonReq)
                    $.ajax({
                        url: "../action/callService.php?filter=" + jsonReq + "&function=saveUser" + "&connection=" + vsmsservice,
                        type: 'GET',
                        crossDomain: true,
                        contentType: "application/json; charset=utf-8",
                        success: function (response, textstatus) {
                            if (response.startsWith("OK")) {
                                alert("New user created. Please use your PC Login Id/Password to logon.");
                                window.location.href = 'showUsers.php';
                            } else {
                                if(response === "DUPLICATE_EMAIL"){
                                    alert("Error: Duplicate email address.");
                                }else{
                                    alert("Something went wrong.Please submit again");
                                }
                            }

                        }
                    });

                });
            });

            function submitUserData(id) {
                var requestorId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                var userId = document.getElementById(id + "UserId").value;
                var firstName = document.getElementById(id + "FirstName").value;
                var lastName = document.getElementById(id + "LastName").value;
                var managerEle = document.getElementById(id+"Manager");
               var managerName = managerEle.options [managerEle.selectedIndex]=== undefined ? '':managerEle.options [managerEle.selectedIndex].text;
                var email = document.getElementById(id + "Email").value;
                var email2 = document.getElementById("nEmailTwo").value;
                var siteList = $('#' + id + 'Sites').val();

                var isApprover = document.getElementById(id + "ApproverToggle").checked;
                var isActionOwner = document.getElementById(id + "ActionOwnerToggle").checked;
                if (firstName === '' || lastName === '' || email === '') {
                    alert("Please enter all the fields. All fields are mandatory");
                    return;
                }
                 if(isActionOwner && managerName=== '' ){
                         alert("Please select a Manager, if the user is an Action Owner.");
                        return;
                    }
                var re = /\S+@\S+\.\S+/;
                if (!re.test(email)) {
                    alert("Please Enter valid email id");
                    return;
                }
                if (id === 'e') {
                    var origEmail = document.getElementById("origEmail").value;
                    var email2 = document.getElementById("eEmailTwo").value;
                    if (email !== origEmail) {
                        if (email !== email2) {
                            alert("If changing the email id,please re-enter the email id and both should match. Please correct them.")
                            return;
                        }
                    }

                }else{
                     if (email !== email2) {
                        alert("Both the email addresses should match. Please correct them. ")
                        return;
                    }
                }

                if (siteList.length === 0) {
                    alert("Please select at least one site");
                    return;
                }

                var userReq = {"userId": userId, "firstName": firstName, "lastName": lastName, "emailId": email,
                    "isApprover": isApprover, "siteList": siteList, "requestorId": requestorId, "isActionOwner": isActionOwner, "managerName":managerName};

                var jsonReq = encodeURIComponent(JSON.stringify(userReq));
                console.log(jsonReq)
                $.ajax({
                    url: "../action/callService.php?filter=" + jsonReq + "&function=saveUser" + "&connection=" + vsmsservice,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        //Lets upload the quotation files if everything is good
                        if (id === 'n') {
                            if (response.startsWith("OK")) {
                                alert("New user created. Please use entered email id and password as 'test' to logon");
                                window.location.href = 'showUsers.php';
                            } else {
                                if(response === "DUPLICATE_EMAIL"){
                                    alert("Error: Duplicate email address.");
                                }else{
                                    alert("Something went wrong.Please submit again");
                                }
                            }
                        } else {
                            if (response.startsWith("OK")) {
                                alert("User information edited successfully.");
                                $("#mEditUser").modal('hide');
                                window.location.href = 'showUsers.php';
                            } else {
                                 if(response === "DUPLICATE_EMAIL"){
                                    alert("Error: Duplicate email address.");
                                }else{
                                    alert("Something went wrong.Please submit again");
                                }
                            }
                        }
                    }
                });

            }

        </script>
</body>
</html>