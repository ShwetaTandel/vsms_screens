 <?php if ($action === "NEW") { ?>
<ul class="nav nav-tabs container" role="tablist" id="myTabs">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="incidentTab" href="#incidentReport"><span style="font-size: large;font-weight: bold">Incident Details</span></a>
                </li>
</ul>

 <?php } elseif ($action === 'IR' || $action ==='AIR') { ?>
<ul class="nav nav-tabs container" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" id="incidentTab" href="#incidentReport"><span style="font-size: large;font-weight: bold">Incident Details</span></a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link" data-toggle="tab" id="initialReportTab" href="#initialReport"><span style="font-size: large;font-weight: bold">Initial Report</span></a>
                </li>
                 <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
                </li>
</ul>
 <?php } elseif ($action === 'FIR' || $action === 'AFIR' ) { ?>
<ul class="nav nav-tabs container" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab"  id="incidentTab" href="#incidentReport"><span style="font-size: large;font-weight: bold">Incident Details</span></a>
                </li>
                <li class="nav-item active" >
                    <a class="nav-link" data-toggle="tab" id="initialReportTab" href="#initialReport"><span style="font-size: large;font-weight: bold">Initial Report</span></a>
                </li>
                <li class="nav-item " >
                    <a class="nav-link" data-toggle="tab" id="fullInvestigationTab" href="#fullInvestigation"><span style="font-size: large;font-weight: bold">Full Investigation</span></a>
                </li>
                 <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
                </li>
</ul>
 <?php } elseif ($action === 'HSSIGNOFF') { ?>
<ul class="nav nav-tabs container" role="tablist"  id="myTabs">
                <li class="nav-item">
                    <a class="nav-link " data-toggle="tab"  id="incidentTab" href="#incidentReport"><span style="font-size: large;font-weight: bold">Incident Details</span></a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link" data-toggle="tab" id="initialReportTab" href="#initialReport"><span style="font-size: large;font-weight: bold">Initial Report</span></a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link" data-toggle="tab" id="fullInvestigationTab" href="#fullInvestigation"><span style="font-size: large;font-weight: bold">Full Investigation</span></a>
                </li>
                <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="hsTab" href="#hsSignOff"><span style="font-size: large;font-weight: bold">H&S Sign Off</span></a>
                </li>
                 <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
                </li>
</ul>
<?php } elseif ($action === 'VIEW') { ?>
<ul class="nav nav-tabs container" role="tablist"  id="myTabs">
            <?php if (intval(substr($data[0]->status, 1, 2)) >= 1 || $data[0]->status === '_CLOSED') { ?>
                <li class="nav-item" >
                    <a class="nav-link " data-toggle="tab"  id="incidentTab" href="#incidentReport"><span style="font-size: large;font-weight: bold">Incident Details</span></a>
                </li>
            <?php } if (intval(substr($data[0]->status, 1, 2)) >=2 || $data[0]->status === '_CLOSED') { ?>
                <li class="nav-item" >
                    <a class="nav-link" data-toggle="tab" id="initialReportTab" href="#initialReport"><span style="font-size: large;font-weight: bold">Initial Report</span></a>
                </li>
                <?php } if (intval(substr($data[0]->status, 1, 2)) >=3 || $data[0]->status === '_CLOSED') { ?>
                <li class="nav-item" >
                    <a class="nav-link" data-toggle="tab" id="fullInvestigationTab" href="#fullInvestigation"><span style="font-size: large;font-weight: bold">Full Investigation</span></a>
                </li>
                <?php } if (intval(substr($data[0]->status, 1, 2)) >=4 || $data[0]->status === '_CLOSED') { ?>
                <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="hsTab" href="#hsSignOff"><span style="font-size: large;font-weight: bold">H&S Sign Off</span></a>
                </li>
                 <?php } if (intval(substr($data[0]->status, 1, 2)) >=2 || $data[0]->status === '_CLOSED') { ?>
                <li class="nav-item active"  >
                    <a class="nav-link " data-toggle="tab" id="allActionsTab" href="#allActions"><span style="font-size: large;font-weight: bold">View All Actions</span></a>
                </li>
                <?php } ?>
</ul>

 <?php } ?>

