<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    ?>
    <head>
        <title>VSMS - Pending Approvals</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <!--      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
       <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>-->
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Incidents Pending Approval</h1>      
            </div>
        </div>
        <br/>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#incidents">Pending Incidents</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#hazards">Pending Hazards</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#riskAssessments">Pending Risk Assessments</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="incidents" class="container-fluid tab-pane active">
                <br>
                <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>Incident Number</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Incident Date</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>


            </div>
            <div id="hazards" class="container-fluid tab-pane">
                <br>
                <table id="hazardsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>Hazard Number</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Hazard Date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </thead>
                </table>


            </div>
              <div id="riskAssessments" class="container-fluid tab-pane">
                <br>
                <table id="riskAssessmentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>RA Number</th>
                            <th>Site</th>
                            
                            <th>RA Revision Date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </thead>
                </table>


            </div>
        </div>
        <!--Modal Dialogs --->


        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="progressTitle">Audit Trail IR</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>

        <script>
            $(document).ready(function () {
                function formatHistory() {

                    return '<table id="incidentHistory" class="compact stripe hover row-border" style="width:100%">' +
                            '<thead>' +
                            '<th>Comments</th>' +
                            '<th>Status</th>' +
                            '<th>Updated By</th>' +
                            '<th>Updated On</th>' +
                            '</thead>' +
                            '</table>';
                }
                 function formatHazardHistory() {

                    return '<table id="hazardHistory" class="compact stripe hover row-border" style="width:100%">' +
                            '<thead>' +
                            '<th>Comments</th>' +
                            '<th>Status</th>' +
                            '<th>Updated By</th>' +
                            '<th>Updated On</th>' +
                            '</thead>' +
                            '</table>';
                }
                 function formatGRAHistory() {

                    return '<table id="graHistory" class="compact stripe hover row-border" style="width:100%">' +
                            '<thead>' +
                            '<th>Comments</th>' +
                            '<th>Status</th>' +
                            '<th>Updated By</th>' +
                            '<th>Updated On</th>' +
                            '</thead>' +
                            '</table>';
                }
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var initIncidents = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 5 || index === 4 || index === 2) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };
                var initHazards = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 2 || index === 4) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#hazardsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                var res = d;

                                if (d !== "") {
                                    select.append('<option value="' + res + '">' + res + '</option>');
                                }


                            });
                        }
                    });
                };
                var incidentsTable = $('#incidentsTable').DataTable({
                    ajax: {"url": "../masterData/incidentsData.php?filter=APPROVER&approverId=" + userId, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bOpenIncidents'><i class='fa fa-edit'></i> Open</a><span>  </span><a class='btn btn-green' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                        }
                    ],
                    initComplete: initIncidents,
                    orderCellsTop: true,
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "incident_number",
                            render: function (data, type, row) {
                                return "IR" + data;
                            }
                        },
                        {data: "code"},
                        {data: "location"},
                        {data: "incident_date"},
                        {data: "incident_type"},
                        {data: "statusDesc"},
                        {data: ""}

                    ],
                    order: []
                });

                var hazardsTable = $('#hazardsTable').DataTable({
                    ajax: {"url": "../masterData/hazardsData.php?filter=APPROVER&approverId=" + userId, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bOpenHazards'><i class='fa fa-edit'></i> Open</a><span>  </span><a class='btn btn-green' href='#' id='bViewHistHazards'><i class='fa fa-file'></i> View Progress</a><span>  </span>"
                        }
                    ],
                    initComplete: initHazards,
                    orderCellsTop: true,
                    autoWidth: false,
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "hazard_number", 
                            render: function (data, type, row) {
                                return "HZ" + data;
                            }},
                        {data: "code"},
                        {data: "location"},
                        {data: "hazard_date"},
                        {data: "description"},
                        {data: ""}

                    ],
                    order: [[0, 'desc']]
                });

                var riskAssessmentsTable = $('#riskAssessmentsTable').DataTable({
                    ajax: {"url": "../masterData/graData.php?filter=APPROVER&approverId=" + userId, "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bOpenGRA'><i class='fa fa-edit'></i> Open</a><span>  </span><a class='btn btn-green' href='#' id='bViewGRAHistory'><i class='fa fa-file'></i> View Progress</a><span>  </span>"
                        }
                    ],
                  //  initComplete: initHazards,
                    orderCellsTop: true,
                    autoWidth: false,
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "gra_id", 
                            render: function (data, type, row) {
                                return "RA" + data;
                            }},
                        {data: "code"},
                        
                        {data: "gra_date"},
                        {data: "area_assessed"},
                        {data: ""}

                    ],
                    order: [[0, 'desc']]
                });

                $('#incidentsTable tbody').on('click', '#bOpenIncidents', function () {
                    var status = incidentsTable.row($(this).parents('tr')).data().status;
                    var level = incidentsTable.row($(this).parents('tr')).data().status.split('_')[0];
                    var incidentId = incidentsTable.row($(this).parents('tr')).data().incident_number;
                    if (level === 'L2') {
                        window.open("reportIncident.php?action=AIR&iid=" + incidentId, '_self');
                    } else if (status === 'L3_APPROVED' || status === 'L4_SAVED') {
                        window.open("reportIncident.php?action=HSSIGNOFF&iid=" + incidentId, '_self');
                    } else if (level === 'L3' || status === 'L4_REJECTED' ) {
                        window.open("reportIncident.php?action=AFIR&iid=" + incidentId, '_self');
                    }
                });

                //VIEW Progress CLICK
                $('#incidentsTable tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = incidentsTable.row(tr).data();
                    var rowID = data.incident_number;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#progressTitle').html("Audit Trail IR" + rowID);
                    $('#mProgressModel').modal('show');
                });
             
                $('#hazardsTable tbody').on('click', '#bOpenHazards', function () {
                    var hid = hazardsTable.row($(this).parents('tr')).data().hazard_number;
                    window.open("reportHazard.php?action=HI&hid=" + hid, '_self');
                    
                });
                
                 //VIEW Progress CLICK
                $('#hazardsTable tbody').on('click', '#bViewHistHazards', function () {
                    var tr = $(this).closest('tr');
                    var data = hazardsTable.row(tr).data();
                    var rowID = data.hazard_number;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHazardHistory());
                    hazardHistoryTable(rowID);
                    $('#progressTitle').html("Audit Trail HZ" + rowID);
                    $('#mProgressModel').modal('show');
                });
                function historyTable(rowID) {
                    var bodyTable = $('#incidentHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/incidentHistoryData.php", "data": {incidentId: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'incidentHistory', title: 'Incident History'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "updated_by"},
                            {data: "updated_at"}
                        ],
                        order: []
                    });

                }
                
                function hazardHistoryTable(rowID) {
                    var bodyTable = $('#hazardHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/hazardHistoryData.php", "data": {hid: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'hazardHistory', title: 'Hazard History'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "updated_by"},
                            {data: "updated_at"}
                        ],
                        order: []
                    });

                }
                
                
                ///Risk Assesments 
                  $('#riskAssessmentsTable tbody').on('click', '#bOpenGRA', function () {
                    var status = riskAssessmentsTable.row($(this).parents('tr')).data().status;
                    var level = riskAssessmentsTable.row($(this).parents('tr')).data().status.split('_')[0];
                    var graId = riskAssessmentsTable.row($(this).parents('tr')).data().gra_id;
                    if (status === 'L2_SUBMITTED' || status === 'L2_EDITED') {
                        window.open("genericRiskAssessment.php?action=L2APPROVE&graid=" + graId, '_self');
                    } else if (status === 'L2_APPROVED' || status === 'L3_EDITED') {
                        window.open("genericRiskAssessment.php?action=L3APPROVE&graid=" + graId, '_self');
                    } else if(status === 'L3_APPROVED' || status === 'L4_EDITED'){
                        window.open("genericRiskAssessment.php?action=L4APPROVE&graid=" + graId, '_self');
                    }
                });
                
                 //VIEW Progress CLICK
                $('#riskAssessmentsTable tbody').on('click', '#bViewGRAHistory', function () {
                    var tr = $(this).closest('tr');
                    var data = riskAssessmentsTable.row(tr).data();
                    var rowID = data.gra_id;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatGRAHistory());
                    graHistoryTable(rowID);
                    $('#progressTitle').html("Audit Trail RA" + rowID);
                    $('#mProgressModel').modal('show');
                });
              
                
                function graHistoryTable(rowID) {
                    var bodyTable = $('#graHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/graHistoryData.php", "data": {id: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'GRAHistory', title: 'GRA History'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "updated_by"},
                            {data: "updated_at"}
                        ],
                        order: []
                    });

                }
            });

        </script>
    </body>

</html>