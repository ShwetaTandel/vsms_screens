<?php
$isSuperUser = $_SESSION['vsmsUserData']['is_super_user'] == 1 ? true : false;
$isCommonLogin = $_SESSION['vsmsUserData']['is_common_user'] == 1? true : false;
ChromePhp::log($isCommonLogin);

?>

<nav class="navbar navbar-expand-sm bg-light navbar-light">
    <!-- Brand -->
    <a class="navbar-brand" href="home.php"><i class="fa fa-fw fa-home"></i> Home</a>

    <!-- Links -->
    <ul class="navbar-nav"  <?php if (!$isSuperUser) { ?>style="display:none"<?php } ?>>
        <li class="nav-item dropdown"  >
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <i class="fa fa-fw fa-briefcase"></i>Admin</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="showUsers.php">Users</a>
                <a class="dropdown-item" href="showApprovalCycle.php">Approvers</a>
                <a class="dropdown-item" href="showNotificationGroups.php">Notification Groups</a>
            </div>
        </li>

    </ul>
    <ul class = "navbar-nav ml-auto" >
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                <i class="fa fa-fw fa-user"></i>
                <Span>Welcome <?php echo $_SESSION['vsmsUserData']['first_name']; ?></span>

                <span class="caret"></span>
            </a>
            <div class="dropdown-menu" <?php if ($isCommonLogin) { ?>style="display:none"<?php } ?>>
                <a class="dropdown-item" href="accountSettings.php">Account Settings</a>
                <a class="dropdown-item" onclick="$('#mPasswordModal').modal('show');">Change Password</a>

            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#" onclick="logOut()"> <i class="fa fa-fw fa-sign-out"></i>Log Out</a>
        </li>
    </ul>
</nav>
<div id="mPasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Change password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="control-group">
                                <label class="input-group-text">Current Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="text" style="display:none;">
                                    <input type="password" name="mCurrPassword" id="mCurrPassword" class="form-control " required maxlength="50" autocomplete="new-password" value=""></input>

                                </div>
                            </div>
                            <br/>
                            <div class="control-group">
                                <label class="input-group-text">New Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="password" name="mNewPassword" id="mNewPassword" class="form-control" required maxlength="50" autocomplete="off"></input>
                                </div>
                            </div>
                            <br/>

                            <div class="control-group">
                                <label class="input-group-text">Confirm Password <span style="color: red">*</span></label>
                                <div class="controls pass_show">
                                    <input type="password" name="mConfPassword" id="mConfPassword" class="form-control" required maxlength="50" autocomplete="off" onpaste="return false;"></input>
                                </div>
                            </div>

                            <div id="showCurrPasswordError" class="showError alert alert-danger" style="display: none"><strong>Invalid current password</strong></div>
                            <div id="showNewPasswordError" class="showError alert alert-danger" style="display: none"><strong>The new password and confirm password do not match</strong></div>
                            <div id="showRequiredError" class="showError alert alert-danger" style="display: none"><strong>Please enter all the fields</strong></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="mSaveButton" >SAVE</button>
                            <button type="button" class="btn btn-secondary" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>

<script>
    function logOut() {
        var userID = '<?php isset($_SESSION['vsmsUserData']) ? $_SESSION['vsmsUserData']['email_id'] : 'No User'; ?>';
        $.ajax({
            url: '../action/userlogout.php',
            type: 'GET',
            data: {userID: userID},
            success: function (response, textstatus) {
                alert("You have been logged out")
                window.open('../login.php', '_self')
            }
        });
    }
      $('.pass_show').append('<span class="ptxt">Show</span>');
          $("#mSaveButton").on("click", function () {
                        document.getElementById('showRequiredError').style.display = 'none';
                        document.getElementById('showNewPasswordError').style.display = 'none';
                        document.getElementById('showCurrPasswordError').style.display = 'none';
                        var currPass = document.getElementById("mCurrPassword").value;
                        var newPass = document.getElementById("mNewPassword").value;
                        var confPass = document.getElementById("mConfPassword").value;
                        var emailId = '<?php echo ($_SESSION['vsmsUserData']['email_id']); ?>';
                    var check = 'yes';
                    if (currPass === '' || newPass === '' || confPass === '') {
                        document.getElementById('showRequiredError').style.display = 'block';
                    }
                    else if (newPass !== confPass) {
                        document.getElementById('showNewPasswordError').style.display = 'block';

                    } else {
                        $.ajax({
                            url: '../action/userlogin.php',
                            type: 'GET',
                            data: {userID: emailId,
                                pass: currPass,
                                check: check},
                            success: function (response, textstatus) {
                                if (response.substring(0, 4) === 'OKUS') {
                                    $.ajax({
                                        type: "GET",
                                        url: "../masterData/usersData.php?data=changepwd&pwd=" + newPass + "&username=" + emailId,
                                        success: function (data) {
                                            if (data.trim() === "OK") {
                                                $('#mPasswordModal').modal('hide');
                                                alert("Password has changed successfully. Please re-login for changes to reflect");
                                            } else {
                                                alert("Something went wrong. Please check with the administrator.");
                                            }
                                        }
                                    });
                                } else {
                                    document.getElementById('showCurrPasswordError').style.display = 'block';
                                }
                            }
                        });
                    }

                });
                 $(document).on('click', '.pass_show .ptxt', function () {

                $(this).text($(this).text() == "Show" ? "Hide" : "Show");

                $(this).prev().attr('type', function (index, attr) {
                    return attr == 'password' ? 'text' : 'password';
                });
            });

</script>

