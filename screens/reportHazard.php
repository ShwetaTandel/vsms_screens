<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$action = '';
$hid = 0;
if (isset($_GET['action'])) {
    $action = $_GET['action'];
    $hid = $_GET['hid'];
}
if ($action === '') {
    $action = "NEW";
}
include '../masterData/generalMasterData.php';
$siteIds = join(",", $_SESSION['vsmsUserData']['sites']);
$data = array();
$userLevel = $_SESSION['vsmsUserData']['level'];
$approvers = getSiteApproversForHazard();
$actionOwners =array();


if ($action !== 'NEW') {
    $serviceUrl = "http://$_SERVER[HTTP_HOST]" . "/VSMS/masterData/hazardsData.php?filter=HAZARD&hid=" . $hid;
    ChromePhp::log($serviceUrl);
    $data = json_decode(file_get_contents($serviceUrl));
    ChromePhp::log($data);
    if ($data == null) {
        echo "<h1>Looks like something is wrong with data you are accessing.Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
    $actionOwners = getActionOwners();
    $siteId = $data[0]->site_id;
    if ($action === 'HI' && $data[0]->curr_approver_id !== $_SESSION['vsmsUserData']['id']) {
        echo "<h1>You are not authorized to access this hazard. Reason: You are not the current approver of the selected hazard. Please go back to the <a href='home.php'>Home</a> Page</h1>";
        die();
    }
}
?>
<html>
    <head>
        <title>VSMS - Report Hazard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>
        <script src="../js/tempusdominus-bootstrap-4.min.js"></script>
        <link href="../css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/bootstrap-select.min.js"></script>
        <script src="../js/reportHazardJS.js?random=<?php echo filemtime('../js/reportHazardJS.js'); ?>"></script>
        <script src="../js/IEFixes.js"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
            .card-header .fa {
                transition: .3s transform ease-in-out;
            }
            .card-header .collapsed .fa {
                transform: rotate(-90deg);

            } 
            #overlay >img{

                position:absolute;
                top:38%;
                left: 38%;
                z-index:10;


            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            #modal {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px; right:20px;
                bottom:20px; left:20px;
            }
            #container{
                position:relative;
                display:none;
            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }


        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Report Hazard</h1>      
            </div>
        </div>
        <hr/>
        <div class="alert alert-warning container">
            <?php if ($action === "NEW") { ?> 
                <strong>Please fill out the form below to submit your Hazard Report. All fields marked * are required. You can find all Hazard Reports for your site(s) on
                    Your Site Hazards tab on Home screen.</strong>
            <?php } elseif ($action === "HI") { ?> 

                <strong>Please fill out the form below to action this Hazard Report. All fields marked * are required. You can save the changes before closing off this
                    Hazard Report and return to this form later. You can find all Hazard Reports for your site(s) on Your Site Hazards tab on Home screen.
                </strong>

            <?php } ?> 
            <br/>

        </div>
        <div class="alert alert-info container">
            <span >Data Protection: Please ensure all personal data is handled with care and not shared with any unauthorised person.  </span>
        </div>
        <?php if ($action !== "NEW") { ?>
            <div class="container"><span style="font-size: x-large;font-weight: bolder;color: brown"> Hazard ID: HZ<?php echo($data[0]->hazard_number ); ?></span></div>
        <?php } ?>
        <br/>
        <?php include './hazardTabList.php'; ?>
        <form action="" method="post">
            <div id="overlay">
                <img src="../images/loading.gif" alt="Wait" alt="Loading"/>
                <div id="modal">
                    Uploading Hazard data and attached files. Please refrain from clicking anywhere......
                </div>
            </div>

            <div class="container tab-content">
                <div id="hazardReport" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="hazardSite" required onchange="populateApprovers(this)">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.users_sites,' . $mDbName . '.site where users_sites.site_id = site.id and users_sites.user_id = ' . $_SESSION['vsmsUserData']['id']);
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['site_id'] . '">' . $row['code'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-1 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="hazardLocation" required>
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.LOCATION;');
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Date & Time (DD/MM/YYYY)<span style="color: red">*</span></label>
                                <div class="input-group date" id="hazardDateTime" data-target-input="nearest">
                                    <input class="form-control datetimepicker-input" type="text" id="hazardDateTime"  data-target="#hazardDateTime" required autocomplete="off"/>
                                    <div class="input-group-append" data-target="#hazardDateTime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Day of the week</label>
                                <input type="text" class="form-control" id="hazardDay" maxlength="255" readonly/>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Please specify exact location</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="exactLocation" maxlength="255"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr/>
                    <div class="row">
                        <div class="col-md-2 col-lg-12">
                            <div class="form-group">
                                <label class="control-label">Hazard Description<span style="color: red">*</span></label>
                                <div class="input-group">

                                    <textarea rows="3" class="form-control" id="hazardDescription" maxlength="1000" ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Attach Photographs/Sketches <span style="font-size: small;font-style: italic">(You can attach multiple files at the same time. Total of the files size should not exceed 10MB)</span></label>
                            <br/>
                            <input class="form-control" type="file" multiple="true" id="attachPhotos">

                        </div>

                        

                    </div>
                    <br/>
                    <div class="row"  id="showPhotos"  <?php if ($action === "NEW") { ?>style="display:none"<?php } ?>>
                    <div class="col-md-2" >
                            <label class="control-label">Attached Photographs/Sketches</label>
                        </div>
                    </div>

                    <hr/>
                    <div class="row">
                        <div class="col-md-3 col-lg-5">

                            <label class="control-label">Have you done anything to make it safe? (Challenged an unsafe act or safely rectified an unsafe condition)<span style="color: red">*</span></label>
                            <select class="custom-select" id="makeItSafe" onchange="changeWhatWhyLabel()">
                                <option value="-1"></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-7">
                            <label class="control-label" id="lblWhatWhy">What?<span style="color: red">*</span></label>
                            <textarea rows="2" id='makeItSafeWhyWhat' class="form-control" maxlength="500"></textarea>
                        </div>

                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-5">

                            <label class="control-label">Were there any witnesses?<span style="color: red">*</span></label>
                            <select class="custom-select" id="anyWitness">
                                <option value="-1"></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-7">
                            <label class="control-label">Who?</label>
                            <textarea rows="2" id='anyWitnessWho' class="form-control" maxlength="255"></textarea>
                        </div>

                    </div>
                    <br/>
                    <div class="row">

                        <div class="col-md-3 col-lg-6">
                            <label class="control-label">Do you have improvement suggestion?</label>
                            <textarea rows="5" id='improvementSuggestion' class="form-control" maxlength="1000"></textarea>
                        </div>
                        <div class="col-md-3 col-lg-6">
                            <label class="control-label">Your Details</label>
                            <br/>
                            <label class="control-label">Your Name (Optional)</label>
                            <input class="form-control" type="text" id="reporterName" maxlength="255"/>
                            <label class="control-label">Your Department (Optional)</label>
                            <input class="form-control" type="text" id="reporterDepartment" maxlength="255"/>


                        </div>

                    </div>
                </div>

                <div id="hazardInvestigation" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './hazardInvestigation.php'; ?>
                </div>
                
                <div id="allActions" class="container tab-pane" style="background-color: whitesmoke">
                    <br/>
                    <?php include './allActions.php'; ?>
                </div>

            </div>
            <br/>
            <div class="container" style="background-color: whitesmoke">
                <div class = "row" <?php if ($action !== "NEW") { ?>style="display:none"<?php } ?>>
                    <div class = "col-md-4">
                        <label class = "control-label">Submit to... <span style = "color: red">*</span></label>

                        <select class = "custom-select " id = "approver">
                            <?php
                            echo "<option value='-1'></option>";
                            foreach ($approvers as $value) {
                                echo '<option value=' . $value['user_id'] . '>' . $value['first_name'] . ' ' . $value['last_name'] . '</option>';
                            }
                            echo '';
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id ="approverValidationError" class="showError alert alert-danger approverValidationError" style="display: none"><strong>Please select an approver.</Strong></div>
                </div>

                <?php if ($action === "HI") { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    
                                    <input href="#"  class="btn btn-gray" id="btnSave" type="button" value="SAVE" onclick="submitHazard('L2_SAVED')"<?php if ($action === "NEW") { ?>style="display:none"<?php } ?>></input>
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate('L2_APPROVED')"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } elseif ($action === 'NEW') { ?>
                    <div class="row" >
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate('NEW')"></input>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?> 
            </div>
            <br />
            <div class="modal fade" id="promptModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                        </div>
                        <br>
                        <div class="modal-body">
                            <div class="col-md-3 col-lg-12">
                                <div class="form-group">
                                    <label id="promptMessage">Please add comments:</label>
                                    </br>
                                    <textarea class="form-control" rows="2" maxlength="500" id="promptComments"></textarea>
                                    <input type="hidden" id="promptAction"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" >
                            <button type="button" class="btn btn-green" id="bYes"  data-dismiss="modal">OK</button>
                            <button type="button" class="btn btn-gray" id="bNo"  data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php if ($action === 'VIEW') { ?>
            <div class="container" >
                <div class="col-md-3 col-lg-3 "> <a class="btn btn-blue" href="home.php?show=H" id="btnBack"><i class="fa fa-arrow-left"></i> BACK</a></div>
            </div>
        <?php } ?> 
    </body>
    <script>
        var deletedLicenseIds = [];

        $(document).ready(function () {


            var action = '<?php echo $action ?>';
            var actionOwners = '';
            if (action === 'NEW') {
                $('#hazardTab').trigger('click');

                //Initialize datetime picker
                
                $("#hazardDateTime").datetimepicker({
                    format: 'DD-MM-YYYY HH:mm:ss',
                    locale: moment.locale('en', {week: {dow: 1}}),
                    maxDate: moment()
                });
                
                $("#hazardDateTime").on("change.datetimepicker", function (e) {
                    if (e.date !== undefined) {
                        document.getElementById('hazardDay').value = getDay(e.date);
                    } else {
                        document.getElementById('hazardDay').value = '';
                    }
                });
                document.getElementById('hazardDay').value = getDay(new Date());
            } else if (action !== "NEW") {
                /*<ark field disabled*/
                var data = <?php echo json_encode($data) ?>;

                if (action === 'HI') {
                    $('#hazardInvestigationTab').trigger('click');
                }else  if (action === 'VIEW') {
                     $('#hazardTab').trigger('click');
                }
                // 
                //Set all Datepickers
                populatePageFromHazardData(data[0]);
                buildActionsTable(data[0].allActions);
                //Dynamic WHY Details
                $("#btnAddCounter").click(function () {
                    addCounterMeasure();
                });
                $('#btnDelCounter').click(function () {
                    // confirmation
                    if (confirm("Are you sure you wish to remove this section of the form? Any information it contains will be lost!")) {
                        var num = $('.clonedCounterMeasure').length;
                        // how many "duplicatable" input fields we currently have
                        $('#counterMeasureDiv' + num).slideUp('slow', function () {
                            $(this).remove();
                            // if only one element remains, disable the "remove" button
                            if (num - 1 === 1)
                                $('#btnDelCounter').attr('disabled', true);
                            // enable the "add" button
                            $('#btnAddCounter').attr('disabled', false).prop('value', "+");
                        });
                    }
                    return false;
                    // remove the last element

                    // enable the "add" button
                    $('#btnAddCounter').attr('disabled', false);
                });





                //$('input[type=checkbox]').click(function () {
                $('.badgebox').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).next().html("&check;");
                        $(this).parent().removeClass('btn-danger');
                        $(this).parent().addClass('btn-primary');
                    } else {
                        $(this).next().html("&cross;");
                        $(this).parent().removeClass('btn-primary');
                        $(this).parent().addClass('btn-danger');
                    }
                });
                if (action === 'VIEW') {
                    $('form *').prop('disabled', true);
                }
            }
        });

        function populateApprovers(elem) {

            var approverMasterList = <?php echo json_encode($approvers) ?>;
            var containmentSite = elem.value;
            var approvers = '';
            var approverSelect = document.getElementById('approver');
            approverSelect.options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            approverSelect.add(option);
            for (var i = 0; i < approverMasterList.length; i++) {
                var site = approverMasterList[i];
                if (site.id === containmentSite) {
                    approvers = site.approvers;
                    break;
                }
            }
            for (var i = 0; i < approvers.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", approvers[i].user_id);
                //  newOption.setAttribute("data-divid", approvers[i].manager_name);
                newOption.innerHTML = approvers[i].first_name + ' ' + approvers[i].last_name;
                approverSelect.add(newOption);
            }
        }
function populateOwnersForCounterMeasure(elem) {

            var ownersMasterList = '<?php echo json_encode($actionOwners)?>';
            var containmentSite = elem.val();
            var owners = '';
            var ownerList = $(elem).closest("div").next().find("select");
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === containmentSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateCounterMeasureManager(elem) {

            $(elem).closest("div").next().find("input")[0].value = $(elem).find('option:selected').attr('data-divid') === undefined ? '' : $(elem).find('option:selected').attr('data-divid');
        }
        function submitHazard(submitAction) {

            var siteId = document.getElementById("hazardSite").value;
            var filter = submitAction;

            var locationElement = document.getElementById("hazardLocation");
            var location = locationElement.options [locationElement.selectedIndex].text;

            var hazardDateTime = moment($('#hazardDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss').toJSON();
            var locationOther = document.getElementById("exactLocation").value;
            var hazardDescription = document.getElementById("hazardDescription").value;


            var makeItSafe = document.getElementById("makeItSafe").value;
            var makeItSafeWhyWhat = document.getElementById("makeItSafeWhyWhat").value;
            var anyWitness = document.getElementById("anyWitness").value;
            var anyWitnessWho = document.getElementById("anyWitnessWho").value;
            var improvementSuggestion = document.getElementById("improvementSuggestion").value;
            var reporterName = document.getElementById("reporterName").value;
            var reporterDepartment = document.getElementById("reporterDepartment").value;

          

            var createdBy, updatedBy = <?php echo json_encode($_SESSION['vsmsUserData']['first_name'] . " " . $_SESSION['vsmsUserData']['last_name']); ?>;
            var requestorId = <?php echo json_encode($_SESSION['vsmsUserData']['id']) ?>;



            var action = '<?php echo $action ?>';
            var hazardRequest = '';
            if (submitAction === 'NEW') {
                var approver = document.getElementById("approver").value;
                //ONLY hazard report fields
                hazardRequest = {"siteId": siteId, "location": location, "hazardDateTime": hazardDateTime, "locationOther": locationOther,
                    "hazardDescription": hazardDescription, "makeItSafe": makeItSafe, "makeItSafeWhyWhat": makeItSafeWhyWhat,
                    "anyWitness": anyWitness, "witnessName": anyWitnessWho, "improvementSuggestions": improvementSuggestion,
                    "reporterName": reporterName, "reporterDepartment": reporterDepartment, "currApproverId": approver, "userId": requestorId, "filter": filter};
            }else{
                 var hazardNumber = <?php echo json_encode($hid) ?>;
                var actOrCondition = document.getElementById('actOrCondition').value;
                var riskCategory = document.getElementById('riskCategory').value;
                var hazardComments = document.getElementById('hazardComments').value;
                var hazardActions = document.getElementById('hazardActions').value;
                var actionsNeeded = document.getElementById('actionsNeeded').value;
             
                
                 var counterMeasures = new Array();
                 if(actionsNeeded === 'Yes'){

                    var totalCntMeasures = $('.clonedCounterMeasure').length;
                    for (var k = 0; k < totalCntMeasures; k++) {
                        var tmp = k + 1;
                        counterMeasures [k] = {
                            "id": document.getElementById('counterMeasureId' + tmp).value,
                            "action": document.getElementById('counterMeasure' + tmp).value,
                            "actionSite": document.getElementById('counterMeasureSite' + tmp).value,
                            "actionSubType": document.getElementById('counterMeasureType' + tmp).value,
                            "actionOwner": document.getElementById('counterMeasureOwner' + tmp).value,
                            "deadline": document.getElementById('counterMeasureDeadline' + tmp).value,
                            "incidentWhyId": '-1',
                            "counterMeasureNumber": '',
                            "addHistory": $('#counterMeasure' + tmp).is(":disabled") ? false : true

                        }

                    }
                }
                 hazardRequest = {"hazardNumber":hazardNumber,"siteId": siteId, "location": location, "hazardDateTime": hazardDateTime, "locationOther": locationOther,
                    "hazardDescription": hazardDescription, "makeItSafe": makeItSafe, "makeItSafeWhyWhat": makeItSafeWhyWhat,
                    "anyWitness": anyWitness, "witnessName": anyWitnessWho, "improvementSuggestions": improvementSuggestion,
                    "actOrCondition":actOrCondition, "riskCategory":riskCategory,"hazardComments":hazardComments, "hazardActions":hazardActions,
                    "counterMeasures":counterMeasures,"furtherActionsNeeded":actionsNeeded,"reporterName": reporterName, "reporterDepartment": reporterDepartment, 
                    "currApproverId": approver, "userId": requestorId, "filter": filter};
            }

            //var jsonReq = encodeURIComponent(hazardRequest);
            console.log(hazardRequest);
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(hazardRequest));
            form_data.append('function', 'saveHazard');
            form_data.append('connection', vsmsservice);


            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    console.log(response);
                    if (response.startsWith("OK")) {
                        var res = response.split("-");

                        var windowLocation = 'reportHazardMessage.php?action=' + submitAction + '&id=' + res[1];
                        sendHazardEmails(submitAction, res[1]);
                        sendActionEmails(submitAction, res[1]);
                        uploadHazardFiles(res[1]);
                        $("#overlay").fadeIn("slow");
                        var delay = 5000;
                        setTimeout(function ()
                        {
                            $("#overlay").fadeOut("fast");
                            $('#container').fadeIn();
                            alert('Hazard Data Saved');
                            window.location.href = windowLocation;
                        }, delay);


                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }});
        }
        function sendActionEmails(action, hid) {

            if (action === 'L2_APPROVED') {
                $.ajax({
                    url: "../masterData/actionsData.php?filter=NEWACTIONSHAZARD&hid=" + hid,
                    type: 'GET',
                    crossDomain: true,
                    contentType: "application/json; charset=utf-8",
                    success: function (response, textstatus) {
                        var actionIds = JSON.parse(response);
                        for (var i = 0; i < actionIds.length; i++) {
                            $.ajax({
                                type: "GET",
                                url: "sendEmails.php?hazardId=" + hid + "&actionId=" + actionIds[i].id + "&function=ActionAssigned&type=Workflow&source=Hazard",
                                success: function (data) {
                                    console.log(data);
                                }
                            });
                        }


                    }});
            }

        }
        function sendHazardEmails(action, hid) {
            var func, type = "";

            if (action === 'NEW') {
                func = "HazardSubmitted";
                type = "Both";
            } else if (action === 'L2_APPROVED') {
                func = "HazardClosed";
                type = "Notification";
            } else if (action === 'DELETED') {
                func = "HazardDeleted";
                type = "Notification";
            } 
            $.ajax({
                type: "GET",
                url: "sendEmails.php?hazardId=" + hid + "&function=" + func + "&type=" + type,
                success: function (data) {
                    console.log(data);
                }
            });

        }
        function markClosed(id) {
            $.ajax({
                url: "../masterData/incidentsData.php?filter=MARKCLOSED&iid=" + id,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    console.log(response);
                }});
        }
        function deleteChanges() {
            var comments = prompt("Are you sure you want to delete this Hazard Report? This action cannot be undone. Please add the reason for deletion.");
            var hid = '<?php echo $hid ?>';
            if (comments !== null && comments !== '') {
                var filter = "?hazardNumber=" + hid + "&comments=" + encodeURIComponent(comments) + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "deleteHazard", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response === 'OK') {
                            sendHazardEmails("DELETED", hid);
                            window.location.href = 'reportHazardMessage.php?action=REPORT_DELETE&id=' + hid;
                        }

                    }
                });
            }
        }
        $("#bYes").on('click', function () {
            var comments = $('#promptComments').val();
            var incidentId = '<?php echo $hid ?>';
            var action = $('#promptAction').val();
            var flow = action.split('_')[1];

            if (flow === 'APPROVED') {
                submitIncidentData(action, comments);
            } else {
                if (comments !== null && comments !== '') {
                    var filter = "?incidentNumber=" + incidentId + "&comments=" + encodeURIComponent(comments) + "&approverId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                    $.ajax({
                        url: "../action/callGetService.php",
                        data: {functionName: "rejectIncident", filter: filter},
                        type: 'GET',
                        success: function (response) {
                            if (response === 'OK') {
                                sendIncidentEmails(action, incidentId);
                                window.location.href = 'reportIncidentMessage.php?action=' + action + '&id=' + incidentId;
                            }

                        }
                    });
                }
            }
        });
        function rejectIncident(elem) {
            var action = (elem === "Initial Report") ? 'L2_REJECTED' : 'L3_REJECTED';
            if (action === 'L2_REJECTED') {
                $('#promptMessage').html('Are you sure you want to reject Initial Report? It will be sent back to the Reporter for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>')
            } else {
                $('#promptMessage').html('Are you sure you want to reject Full Investigation? It will be sent back to the Site Management for editing. This action cannot be undone. Please add your comments in the box below (Maximum allowed 500 characters) <span style="color: red">*</span>')
            }
            $('#promptAction').val(action);
            $('#promptModal').modal('show');
        }

    </script>
</html>
