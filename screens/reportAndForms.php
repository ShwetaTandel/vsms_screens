<?php
session_start();
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
include '../masterData/generalMasterData.php';
if (!isset($_SESSION['vsmsUserData'])) {
    echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
    die();
}
$show = "I";
if (isset($_GET['show'])) {
    $show = $_GET['show'];
}
$sites = $_SESSION['vsmsUserData']['sites'];
$pendingIncidentCnt = getPendingIncidentsCount($_SESSION['vsmsUserData']['id']);
$lastIncidentDays = getLastIncidentDays();
$monthWiseIncidents = getIncidentOccurredThisMonth();
$lastMonthIncidents = $monthWiseIncidents[1];
$thisMonthIncidents = $monthWiseIncidents[0];
$monthWiseHazards = getHaradsReportedThisMonth();
$lastMonthHazards = $monthWiseHazards[1];
$thisMonthHazards = $monthWiseHazards[0];
$monthWiseInjuries = getInjuriestOccurredThisMonth();
$lastMonthInjuries = $monthWiseInjuries[1];
$thisMonthInjuries = $monthWiseInjuries[0];
$thisMonthRevisedRA = getGRAThisMonth();
$lastMonthGRA = $thisMonthRevisedRA[1];
$thisMonthGRA = $thisMonthRevisedRA[0];

$outstandingactionsCnt = getOutstandingActions($_SESSION['vsmsUserData']['id']);
$actionOwners = getActionOwners();
$userSiteLevels = getUserLevels($_SESSION['vsmsUserData']['id']);
$maxLevel = getMaxUserLevel($_SESSION['vsmsUserData']['id']);
$isReportUser = $_SESSION['vsmsUserData']['is_report_user'];
$isActionOWner = $_SESSION['vsmsUserData']['is_action_owner'];

?>
<html>
    <head>
        <title>Vantec Safety Management System - Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/moment.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-select.min.js"></script>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>

    </head>
    <body>
          <header>
            <div class="row">
                <div class="col-4" class="row pull-left">
                    <img STYLE="position:absolute;width: 650px;height: 200px" src="../images/logo.png" alt=""/>
                </div>
                <div class="col-8">
                    <div class="row pull-right">

                        <?php
                        include './commonHeader.php';
                        ?>
                    </div>
                </div>
            </div>

        </header> 
     <div class="container-fluid"style="margin-top: 0px">
        <br/>
        <div class="row">
           
            <div class="col-12">
                <br/><br/><br/>
                <div class="row">

                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #ef292d"><?php echo $lastIncidentDays != null ? $lastIncidentDays[0] : 0 ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969"> Days since last Incident</span>
                        </div>
                    </div>
                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $monthWiseIncidents[0] ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Incidents this month</span>
                            <?php if ($lastMonthIncidents > $thisMonthIncidents) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthIncidents - $lastMonthIncidents ); ?></span>
                            <?php } elseif ($thisMonthIncidents > $lastMonthIncidents) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d">+<?php echo ($thisMonthIncidents - $lastMonthIncidents); ?></span>
                            <?php } ?>
                        </div>
                    </div>

                         <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $monthWiseHazards[0] ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Hazards this month</span>
                            <?php if ($lastMonthHazards > $thisMonthHazards) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthHazards - $lastMonthHazards ); ?></span>
                            <?php } elseif ($thisMonthHazards > $lastMonthHazards) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d">+<?php echo ($thisMonthHazards - $lastMonthHazards); ?></span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-2">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $thisMonthInjuries ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Injuries this month</span>
                            <?php if ($lastMonthInjuries > $thisMonthInjuries) { ?>
                                <i class="fa fa-fw fa-caret-down text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> <?php echo($thisMonthInjuries - $lastMonthInjuries ); ?></span>
                            <?php } elseif ($thisMonthInjuries > $lastMonthInjuries) { ?>
                                <i class="fa fa-fw fa-caret-up text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d"> +<?php echo ($thisMonthInjuries - $lastMonthInjuries); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                      <div class="col-3">
                        <div class="text-center mt-3">
                            <span style="font-size: 56px;font-weight: bold;color: #465ec0"><?php echo $thisMonthGRA ?></span>
                            <span style="font-size: 16px;font-weight: bold;color: #696969">Risk Assessments this month</span>
                             <?php if ($lastMonthGRA > $thisMonthGRA) { ?>
                                <i class="fa fa-fw fa-caret-down text-danger"></i>
                                <span style="font-weight: bolder;color: #ef292d"> <?php echo($thisMonthGRA - $lastMonthGRA ); ?></span>
                            <?php } elseif ($thisMonthGRA > $lastMonthGRA) { ?>
                                <i class="fa fa-fw fa-caret-up text-success"></i>
                                <span style="font-weight: bolder;color: #17b12e"> +<?php echo ($thisMonthGRA - $lastMonthGRA); ?></span>
                            <?php } ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <br/>
        <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
        <div class="container-fluid">

                <div class="row">
                <div class="col-md-3">
                    <button  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn3d btn-blue btn-lg btn-block btn-huge" onclick="window.open('reportIncident.php', '_self')">Report Incident</button>
                </div>
              
                <div class="col-md-3">
                    <button href="#"  <?php if ($isActionOWner === '0') { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn btn3d btn-green btn-lg btn-block btn-huge" onclick="window.open('action.php', '_self')">New Action</button>
                </div>
                <div class="col-md-3">
                    <button  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?>  class="btn btn3d btn-orange btn-lg btn-block btn-huge" onclick="window.open('genericRiskAssessment.php', '_self')">Risk Assessment</button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn3d btn-danger btn-lg btn-block btn-huge"   <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >Audit, Tours and Inspections (Coming Soon)</button>
                </div>
            </div>
            <br/>
            <div class="row">
                  <div class="col-md-3">
                    <button href="#" style="width: 100%" class="btn btn3d btn-blue btn-lg btn-block btn-huge" onclick="window.open('reportHazard.php', '_self')">Report Hazard </button>
                </div>
                <div class="col-md-3"  <?php if ($isActionOWner === '0') { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >
                    <button href="#" style="width: 100%" onclick="window.open('showOutstandingActions.php', '_self')" class="btn btn3d btn-green btn-lg btn-block btn-huge">Outstanding Actions</button>
                    <p class="d-flex justify-content-center" style="color: #ef292d;font-weight: bold;font-size: large">Outstanding Actions [<?php echo $outstandingactionsCnt[0] ?>]</p>
                </div>
                 <div class="col-md-3"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >
                    <button onclick="window.open('showPendingApprovals.php', '_self')" style="width: 100%" class="btn btn3d btn-orange btn-lg btn-block btn-huge">Pending Approvals</button>
                    <p class="d-flex justify-content-center" style="color: #ef292d;font-weight: bold;font-size: large">Incidents [<?php echo $pendingIncidentCnt[0] ?>]/Hazards [<?php echo $pendingIncidentCnt[1] ?>]/RAs [<?php echo $pendingIncidentCnt[2] ?>] </p>
                </div>
                
                <div class="col-md-3">
                    <button class="btn btn3d btn-danger btn-lg btn-block btn-huge"  onclick="window.open('reportAndForms.php', '_self')"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } else { ?> style="width: 100%" <?php } ?> >Reports & Forms</button>
                </div>
            </div>
            <br/>
            <br/>
            <div class="container">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" id= "incidentsReportTab" href="#incidentsReport">Incidents Reports</a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link" data-toggle="tab" id= "hazardsReportTab" href="#hazardsReport">Hazards Reports</a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link" data-toggle="tab" id="actionReportTab" href="#actionsReport">Actions Reports</a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link" data-toggle="tab" id="generalReportTab" href="#generalReport" <?php if ($maxLevel === "L1") { ?>style="display:none"<?php } ?>>General Reports</a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link" data-toggle="tab" id="downloadFormsTab" href="#downloadReport" <?php if ($maxLevel === "L1") { ?>style="display:none"<?php } ?>>Download Forms</a>
                    </li>


                </ul>
                <div class="tab-content">
                    <div id="incidentsReport" class="container tab-pane active">
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" onclick="window.open('../reports/reportIncidentPendingApproval.php', '_self')">Incident Reports Pending Approval</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" onclick="window.open('../reports/reportIncidentNotClosed.php', '_self')">Incident Reports Not Closed</button>
                            </div>

                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id = "AllIncidentReports" onclick="openModal(this);">All Incident Reports</button>
                            </div>
                        </div>
                        <br/>
                        <br/>    
                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="DeletedIncidentReports" onclick="openModal(this);"  <?php if ($maxLevel === 'L1') { ?>style="display:none"<?php } else { ?> style="height: 100%" <?php } ?>>Deleted Incident Reports</button>
                            </div>

                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="IncidentDataByName" onclick="openModal(this);" <?php if ($maxLevel === 'L1') { ?>style="display:none"<?php } else { ?> style="height: 100%" <?php } ?>>Previous Incidents By Person Name</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="DAReport" onclick="openModal(this);" <?php if ($maxLevel === 'L1') { ?>style="display:none"<?php } else { ?> style="height: 100%" <?php } ?>>D&A Testing Reports</button>
                            </div>

                        </div>
                        <br/><br/>
                        <div class="row">

                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="ExportIncidentData" onclick="openModal(this);" <?php if ($maxLevel === "L2" || $maxLevel === "L1" ) { ?>style="display:none"<?php } ?>>Export All Incident Data</button>
                            </div>
                            
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="ExportHistoricIncidentData" onclick="openModal(this);" <?php if ($maxLevel === "L2" || $maxLevel === "L1" ){ ?>style="display:none"<?php } ?>>Export All Historic Data</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="ExportCreateDateReport" onclick="openModal(this);" <?php if ($maxLevel === "L2" || $maxLevel === "L1" ){ ?>style="display:none"<?php } ?>>Export Incidents by Creation Date</button>
                            </div>
                        </div>
                         <br/><br/>
                          <div class="row">
                               <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="ExportIncidentTimeTaken" onclick="openModal(this);" <?php if ($maxLevel === "L2" || $maxLevel === "L1" ){ ?>style="display:none"<?php } ?>>Incident Time Taken</button>
                            </div>
                                <div class="col-md-4">
                                <button href="#" class="btn btn-blue btn-lg btn-block btn-huge" id="ExportDamageCost" onclick="openModal(this);" <?php if ($maxLevel === "L2" || $maxLevel === "L1" ){ ?>style="display:none"<?php } ?>>Damage Cost Report</button>
                            </div>
                            <div class="col-md-4">
                                <div class="btn-group" style="width: 340px;">
                                    <button type="button"   class="btn btn-gray btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" <?php if ($maxLevel === "L1") { ?>style="display:none"<?php } ?>>
                                        Summary Reports
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryInjuries" onclick="openModal(this);">Incidents: Injuries</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryIncidentActType" onclick="openModal(this);">Incidents: Action Types</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryLocation" onclick="openModal(this);">Incidents: Location</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryLOS" onclick="openModal(this);">Incidents: Length of Service</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryDOW" onclick="openModal(this);">Incidents: Day of Week</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryTOD" onclick="openModal(this);">Incidents: Time of Day</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryRIDDOR" onclick="openModal(this);">Incidents: RIDDOR</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryVehicleType" onclick="openModal(this);">Incidents: Vehicle Type</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryAvgProTime" onclick="openModal(this);">Incidents: Average Processing Time</button>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <div id="hazardsReport" class="container tab-pane" >
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" onclick="window.open('../reports/reportHazardPendingApproval.php', '_self')">Outstanding Hazard Reports</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" id = "ClosedHazards"  onclick="openModal(this);">Closed Hazard Reports</button>
                            </div>

                            <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" id = "AllHazardReport" onclick="openModal(this);">All Hazard Reports </button>
                            </div>
                        </div>
                        <br/>
                        <br/>                          
                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" id ="DeletedHazards" onclick="openModal(this);">Deleted Hazard Reports</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" id ="AllHazardsData" onclick="openModal(this);" >Export All Hazard Data</button>
                            </div>
                        <div class="col-md-4">
                                <button href="#" class="btn btn-green btn-lg btn-block btn-huge" id ="AllHazardsDataFull" onclick="openModal(this);" >Export Full Hazard Data</button>
                            </div>
                        </div>
                          <br/>
                        <br/>   
                         <div class="row"> 
                                   <div class="col-md-4">
                                <button href="#" class="btn btn-gray btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true">Summary Reports</button>
                                <div class="dropdown-menu" style="width: 340px;">
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryHazardLocation" onclick="openModal(this);">Hazards: Location</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryHazardRiskCategory" onclick="openModal(this);">Hazards: Risk Category</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryHazardActCondition" onclick="openModal(this);">Hazards: Act Or Condition</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryHazardCountermeasures" onclick="openModal(this);">Hazards: Further Countermeasures</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryHazardAvgProTime" onclick="openModal(this);">Hazards: Average Processing Time</button>
                                    </div>
                            </div>
                         </div>
                    </div>


                    <div id="actionsReport" class="container-fluid tab-pane">
                        <br>

                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" class="btn btn-orange btn-lg btn-block btn-huge" onclick="window.open('../reports/reportOutstandingActions.php', '_self')">Outstanding Actions</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-orange btn-lg btn-block btn-huge" id="ClosedActions"  onclick="openModal(this);">Closed Actions</button>
                            </div>

                            <div class="col-md-4">
                                <button href="#" class="btn btn-orange btn-lg btn-block btn-huge" id="CancelledActions" onclick="openModal(this);">Cancelled Actions</button>
                            </div>
                        </div>
                        <br/>
                        <br/>                          
                        <div class="row">
                            <div class="col-md-4">
                                <button href="#" style="height: 100%"class="btn btn-orange btn-lg btn-block btn-huge" id="RescheduledActions" onclick="openModal(this);">Re-scheduled Actions</button>
                            </div>
                            <div class="col-md-4">
                                <button href="#" class="btn btn-orange   btn-lg btn-block btn-huge" id="AllActions" onclick="openModal(this);">All Actions</button>
                            </div>

                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button"   class="btn btn-gray btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" <?php if ($maxLevel === "L1") { ?>style="display:none"<?php } else { ?> style="width: 340px" <?php } ?>>
                                        Summary Reports
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryTypeActions" onclick="openModal(this);">Actions: Type</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryActionUsers" onclick="openModal(this);">Actions: User</button>
                                        <button class="dropdown-item btn-lg btn-success" id="SummaryActionSites" onclick="openModal(this);">Actions: Site</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <br/>
                        <br/>      
                        <div class="row">

                        </div>


                    </div>

                    <div id="generalReport" class="container tab-pane" <?php if ($maxLevel === "L1") { ?>style="display:none"<?php } ?>>
                        <br/>
                        <br/>                          
                        <div class="row">
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button"  style="width: 340px;" class="btn btn-red btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                        Users
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.approvers,' . $mDbName . '.site where approvers.site_id = site.id and level in ("L2", "L3", "L4") and is_active = true and approvers.user_id = ' . $_SESSION['vsmsUserData']['id']);
                                        
                                        while ($row = mysqli_fetch_array($result)) {

                                            echo '<button class="dropdown-item btn-lg btn-white"  onclick="generateUserReport(this);" id="Users_' . $row['site_id'] . '_' . $row['code'] . '"  >' . $row['code'] . '</button>';
                                        }
                                        mysqli_close($con);
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button"  style="width: 340px;" class="btn btn-red btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                        Approvers
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                       $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.approvers,' . $mDbName . '.site where approvers.site_id = site.id and level in ("L2", "L3", "L4") and is_active = true and approvers.user_id = ' . $_SESSION['vsmsUserData']['id']);

                                        while ($row = mysqli_fetch_array($result)) {

                                            echo '<button class="dropdown-item btn-lg btn-white"  onclick="generateUserReport(this);" id="Approvers_' . $row['id'] . '_' . $row['code'] . '"  >' . $row['code'] . '</button>';
                                        }
                                        mysqli_close($con);
                                        ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button"  style="width: 340px;" class="btn btn-red btn-lg btn-block btn-huge dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                        Action Owners
                                    </button>
                                    <div class="dropdown-menu" style="width: 340px;">
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.approvers,' . $mDbName . '.site where approvers.site_id = site.id and level in ("L2", "L3", "L4") and is_active = true and approvers.user_id = ' . $_SESSION['vsmsUserData']['id']);

                                        while ($row = mysqli_fetch_array($result)) {

                                            echo '<button class="dropdown-item btn-lg btn-white"  onclick="generateUserReport(this);" id="Action Owners_' . $row['id'] . '_' . $row['code'] . '"  >' . $row['code'] . '</button>';
                                        }
                                        mysqli_close($con);
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div id="downloadReport" class="container tab-pane">
                        <br/>
                        <br/>                          
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Site</label>
                                    <br/>
                                    <select class="custom-select" id="filterSite" onchange="populateReportIds()">
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.approvers,' . $mDbName . '.site where approvers.site_id = site.id and level in ("L2","L3","L4") and is_active = true and approvers.user_id = ' . $_SESSION['vsmsUserData']['id']. ' group by site_id');
                                        echo "<option value=-1></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['site_id'] . '">' . $row['code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Incident ID</label>

                                    <div class="input-group">
                                        <select class="form-control" id="incidentId">
                                            <option value="-1"></option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Form</label>

                                    <div class="input-group">
                                        <select class="form-control" id="formId">
                                            <option value="-1"></option>
                                            <option value="Fast Response">Fast Response</option>
                                            <option value="Detail Report">Detail Report</option>
                                           
                                            <option value="Short Report">Streamlined Report</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <br/>
                                <div class="form-group"><button href="#" class="btn btn-green btn-lg btn-block btn-huge" onclick="downloadReportForm()">Download Form</button></div>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <input href="#"  class="btn btn-white" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/home.php', '_self')"></input>

                </div>
            </div>


            <!---MODAL BOX------>
            <div id="mFilterModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="ownerModalTitle">Select the criteria</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Select Period</label>
                                        <select class="form-control" id="mPeriod" onchange="showDates()">
                                            <option value="-1"></option>
                                            <option value="Last 7 Days">Last 7 Days</option>
                                            <option value="Last 30 Days">Last 30 Days</option>
                                            <option value="Current Month">Current Month</option>
                                            <option value="Custom Period">Custom Period</option>
                                        </select>
                                        <input id="reportName" type="hidden" value=""/>
                                    </div>

                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" id="label1">Incident Date From</label>
                                        <input class="form-control"  type="date" id="mDateFrom" disabled="true"/>
                                    </div>

                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" id="label2">Incident Date To</label>
                                        <input class="form-control"  type="date" id="mDateTo" disabled="true"/>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label">Select Site</label>
                                        <select class="selectpicker" multiple id="mSite">

                                        </select>
                                    </div>

                                </div>

                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-2 col-lg-4" id="compareRow">
                                    <div class="form-group">
                                        <br/>
                                        <label class="btn btn-danger">Compare to previous
                                            <input type="checkbox" id="compareToPrev" class="compareToPrev badgebox" onchange="showPrevPeriod(this)"/>
                                            <span class="badge">&cross;</span>
                                        </label>
                                    </div>

                                </div>
                                <div class="col-md-2 col-lg-4">
                                    <div class="form-group" id="mCompareTo" style="display: block">
                                        <label class="control-label">Compare To</label>
                                        <select class="custom-select" id="mCompareToSelect" >
                                            <option value="Previous Period">Previous Period</option>
                                            <option value="Previous Year">Previous Year</option>
                                        </select>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-gray" id="mChangeButton" onclick="generateReport()">OK</button>
                            <button type="button" class="btn btn-gray" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>
    </body>
    <script>
        function showDates() {
            if (document.getElementById('mPeriod').value === 'Custom Period') {
                $('#mDateFrom').attr('disabled', false);
                $('#mDateTo').attr('disabled', false);
            } else {
                $('#mDateFrom').val('');
                $('#mDateTo').val('');
                $('#mDateFrom').attr('disabled', true);
                $('#mDateTo').attr('disabled', true);


            }
        }
        function showPrevPeriod(elem) {
            if ($(elem).is(':checked')) {
                $('#mCompareTo').css('display', 'block');
            } else {
                $('#mCompareTo').css('display', 'none');
            }
        }
        function openModal(elem) {
            var id = $(elem).attr('id');

            $('#reportName').val(id);
            $('#mPeriod').val('-1');
            $('#mDateFrom').val('');
            $('#mDateTo').val('');
           if(id.indexOf('ClosedAction')!== -1){
                
                $('#label1').text('Closed Date From');
                $('#label2').text('Closed From To');
                
            }else if(id.indexOf('CancelledAction')!== -1){
               $('#label1').text('Cancelled Date From');
                $('#label2').text('Cancelled From To');

                
            }else if(id.indexOf('RescheduledAction')!== -1){
                $('#label1').text('Re-scheduled Date From');
                $('#label2').text('Re-scheduled From To');
            } else if(id.indexOf('Action')!== -1){
                $('#label1').text('Action Date From');
                $('#label2').text('Action From To');
            }else {
                 $('#label1').text('Incident Date From');
                $('#label2').text('Incident From To');
            }

            
            if (id.startsWith("Summary")) {
                $('#compareRow').css('display', 'block');
                $('#compareToPrev').prop('checked', false);
                $('#compareToPrev').next().html("&cross;");
                $('#compareToPrev').parent().removeClass('btn-primary');
                $('#compareToPrev').parent().addClass('btn-danger');
            } else {
                $('#compareRow').css('display', 'none');
            }
            $('#mCompareTo').css('display', 'none');
            if(id === 'ExportHistoricIncidentData'){
                $("#mPeriod option[value*='Last 7 Days']").prop('disabled',true);
                $("#mPeriod option[value*='Last 30 Days']").prop('disabled',true);
                $("#mPeriod option[value*='Current Month']").prop('disabled',true);
            }else{
                $("#mPeriod option").prop('disabled', false);
            }
            loadSites(id);
        }
        function loadSites(id) {
            var levels = "'L1','L2', 'L3', 'L4'";
            var isReportUser = '<?php echo $isReportUser?>';
            if (id === 'DeletedIncidentReports' || id === 'IncidentDataByName' || id === 'DAReport' || id.startsWith("Summary")) {
                levels = "'L2', 'L3', 'L4'";
            }else if (id === 'ExportIncidentData' || id === 'ExportHistoricIncidentData'){
                levels = "'L3','L4'";
            }
           
            $.ajax({
                url: "../masterData/siteLevelsData.php?levels=" + levels+ '&allSites='+isReportUser,
                type: 'GET',
                success: function (response) {
                    $('#mSite').empty();
                    var jsonData = JSON.parse(response);
                    var selectList = document.getElementById("mSite");
                    var sites = jsonData;
                    for (var i = 0; i < sites.length; i++) {
                        var newOption = document.createElement('option');
                        newOption.setAttribute("value", sites[i].id);
                        newOption.setAttribute("selected", "true");
                        newOption.innerHTML = sites[i].code;
                        selectList.add(newOption);
                    }
                    $("#mSite").selectpicker("refresh");
                    $('#mFilterModal').modal('show');
                }
            });



        }

        function generateUserReport(elem) {
            var id = $(elem).attr('id');
            var siteId = id.split('_')[1];
            var filter = id.split('_')[0];
            var siteName = id.split('_')[2];
            window.open('../reports/reportUsersReport.php?siteId=' + siteId + '&filter=' + filter + '&siteName=' + siteName, '_self');
        }
        function generateReport() {
            var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
            var reportName = $('#reportName').val();
            var period = $('#mPeriod').val();
            var toDate = $('#mDateTo').val();
            var fromDate = $('#mDateFrom').val();
            var siteIds = $('#mSite').val();
            var siteNames = $('#mSite option:selected').toArray().map(item => item.text).join();
            
            var compareTo = '', prevToDate = new Date(), prevFromDate = new Date();

            if (period === '-1') {
                alert('Please select the period for the report.');
                return;
            } else if (siteIds.length === 0) {
                alert('Please select at least one site.');
                return;
            } else if (period === 'Custom Period' && toDate === '' && fromDate === '') {
                alert('Please enter From & To dates.');
                return;
            } else if (period === 'Custom Period' && ((moment(toDate, dateFormats, true).isValid() === false) || (moment(fromDate, dateFormats, true).isValid() === false))) {
                alert('Please enter valid From & To dates.');
                return;
            } else if (period === 'Custom Period' && new Date(fromDate) > new Date(toDate)) {
                alert('Please enter a From date which is less than of equal to To Date');
                return;
            }
            if (period === 'Last 7 Days') {
                toDate = new Date();
                fromDate = moment().subtract(6, "days");

            } else if (period === 'Last 30 Days') {
                toDate = new Date();
                fromDate = moment().subtract(29, "days");

            } else if (period === 'Current Month') {
                toDate = new Date();
                fromDate = new Date(toDate.getFullYear(), toDate.getMonth(), 1);
            } else {
                fromDate = new Date(fromDate);
                toDate = new Date(toDate);
            }
            if (reportName.startsWith("Summary")) {

                if ($("#compareToPrev").is(':checked')) {
                    compareTo = $('#mCompareToSelect').val();
                    if (period === 'Last 7 Days') {
                        if (compareTo === 'Previous Period') {
                            prevToDate = moment(fromDate).subtract(1, "days");
                            prevFromDate = moment(prevToDate).subtract(6, "days");
                        } else if (compareTo === 'Previous Year') {
                            prevToDate = new Date(toDate);
                            prevToDate.setFullYear(prevToDate.getFullYear() - 1);
                            prevFromDate = new Date(fromDate);
                            prevFromDate.setFullYear(prevFromDate.getFullYear() - 1);
                        }
                    } else if (period === 'Last 30 Days') {
                        if (compareTo === 'Previous Period') {
                            prevToDate = moment(fromDate).subtract(1, "days");
                            prevFromDate = moment(prevToDate).subtract(29, "days");

                        } else if (compareTo === 'Previous Year') {
                            prevToDate = new Date(toDate);
                            prevToDate.setFullYear(prevToDate.getFullYear() - 1);
                            prevFromDate = new Date(fromDate);
                            prevFromDate.setFullYear(prevFromDate.getFullYear() - 1);
                        }


                    } else if (period === 'Current Month') {

                        if (compareTo === 'Previous Period') {
                            prevToDate.setDate(fromDate.getDate() - 1);
                            prevFromDate = new Date(prevToDate.getFullYear(), prevToDate.getMonth(), 1);

                        } else if (compareTo === 'Previous Year') {
                            prevToDate = new Date(toDate);
                            prevToDate.setFullYear(prevToDate.getFullYear() - 1);
                            prevFromDate = new Date(fromDate);
                            prevFromDate.setFullYear(prevFromDate.getFullYear() - 1);

                        }


                    } else {
                        if (compareTo === 'Previous Period') {
                            var dayDiff = (toDate - fromDate) / (1000 * 60 * 60 * 24);
                            prevToDate = new Date(fromDate);
                            prevToDate = moment(prevToDate).subtract(1, "days");
                            prevFromDate = new Date(prevToDate);
                            prevFromDate = moment(prevToDate).subtract(dayDiff, "days");
                        } else if (compareTo === 'Previous Year') {
                            prevToDate = new Date(toDate);
                            prevToDate.setFullYear(prevToDate.getFullYear() - 1);
                            prevFromDate = new Date(fromDate);
                            prevFromDate.setFullYear(prevFromDate.getFullYear() - 1);
                        }
                    }
                }
            }
            fromDate = fromDate.toISOString().slice(0, 10);
            toDate = toDate.toISOString().slice(0, 10);
            if (compareTo !== '') {
                prevToDate = prevToDate.toISOString().slice(0, 10);
                prevFromDate = prevFromDate.toISOString().slice(0, 10);
            } else {
                prevToDate = 'None';
                prevFromDate = 'None';
            }
            


            var url = '';
            if (reportName === "AllIncidentReports") {
                url = '../reports/reportAllIncidents.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "ExportIncidentData") {
                url = '../reports/reportExportAllIncidentData.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "ExportHistoricIncidentData") {
                url = '../reports/reportExportAllHistoricIncidentData.php?siteNames=' + siteNames + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "ExportCreateDateReport") {
                url = '../reports/reportAllIncidentsByCreateDate.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "ExportIncidentTimeTaken") {
                url = '../reports/reportAllIncidentsByTimeTaken.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "ExportDamageCost") {
                url = '../reports/reportDamageCosts.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "DeletedIncidentReports") {
                url = '../reports/reportDeletedIncidents.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "IncidentDataByName") {
                url = '../reports/reportIncidentsByPersonName.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "DAReport") {
                url = '../reports/reportIncidentsDATesting.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            } else if (reportName === "SummaryInjuries") {
                url = '../reports/reportInjuriesSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryIncidentActType") {
                url = '../reports/reportIncidentActionTypesSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryLocation") {
                url = '../reports/reportLocationSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryVehicleType") {
                url = '../reports/reportVehicleTypesSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryLOS") {
                url = '../reports/reportLOSSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryDOW") {
                url = '../reports/reportDOWSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryTOD") {
                url = '../reports/reportTODSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryAvgProTime") {
                url = '../reports/reportAvgProvTimeSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryRIDDOR") {
                url = '../reports/reportRiddorSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "ClosedActions") {
                url = '../reports/reportClosedOrCancelledActions.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&filter=Closed';
            } else if (reportName === "CancelledActions") {
                url = '../reports/reportClosedOrCancelledActions.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&filter=Cancelled';
            } else if (reportName === "RescheduledActions") {
                url = '../reports/reportClosedOrCancelledActions.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&filter=Re-scheduled';
            } else if (reportName === "AllActions") {
                url = '../reports/reportAllActions.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&filter=All';
            } else if (reportName === "SummaryTypeActions") {
                url = '../reports/reportActionTypesSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryActionUsers") {
                url = '../reports/reportActionsByUserSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            } else if (reportName === "SummaryActionSites") {
                url = '../reports/reportActionsBySiteSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }else if (reportName === "AllHazardReport") {
                url = '../reports/reportAllHazards.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            }else if (reportName === "ClosedHazards") {
                url = '../reports/reportClosedHazards.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            }else if (reportName === "DeletedHazards") {
                url = '../reports/reportDeletedHazards.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            }else if (reportName === "AllHazardsData") {
                url = '../reports/reportAllHazardsData.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            }else if (reportName === "AllHazardsDataFull") {
                url = '../reports/reportAllHazardsFull.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate;
            }else if (reportName === "SummaryHazardLocation") {
                url = '../reports/reportHazardsLocationSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }else if (reportName === "SummaryHazardRiskCategory") {
                url = '../reports/reportHazardRiskCategorySummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }else if (reportName === "SummaryHazardActCondition") {
                url = '../reports/reportHazardActConditionSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }else if (reportName === "SummaryHazardCountermeasures") {
                url = '../reports/reportHazardActionNeededSummary.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }else if (reportName === "SummaryHazardAvgProTime") {
                url = '../reports/reportHazardAvgProcessingTime.php?siteIds=' + siteIds + '&toDate=' + toDate + '&fromDate=' + fromDate + '&prevToDate=' + prevToDate + '&prevFromDate=' + prevFromDate;
            }
            window.open(url, '_self');


        }
        function populateReportIds() {
            var site = $('#filterSite').val();
            var selectList = document.getElementById("incidentId");

            selectList.options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            option.innerHTML = "";
            selectList.add(option);

            $.ajax({
                url: "../masterData/actionsData.php?filter=SITEINCIDENTSFORFR&siteId=" + site,
                type: 'GET',
                crossDomain: true,
                contentType: "application/json; charset=utf-8",
                success: function (response, textstatus) {
                    var incidents = JSON.parse(response);
                    for (var i = 0; i < incidents.length; i++) {

                        var newOption = document.createElement('option');
                        newOption.setAttribute("value", incidents[i].incident_number);
                        newOption.innerHTML = 'IR' + incidents[i].incident_number;
                        selectList.add(newOption);

                    }
                }});

        }
        
        function downloadReportForm(){
             var iid = $('#incidentId').val();
             var formType = $('#formId').val();
             iid = iid.substring(0, iid.length)
             if(iid !== '-1'){
             if(formType === 'Fast Response'){
                window.open("../screens/fastReponse.php?iid="+iid,'_blank');
              }else if(formType === 'Detail Report'){
                  window.open("../screens/exportIncident.php?iid="+iid,'_blank');
              }else if(formType === 'Short Report'){
                  window.open("../screens/exportBriefIncident.php?iid="+iid,'_blank');
              }
          }
            
        }
        $('.badgebox').click(function () {
            if ($(this).is(':checked')) {
                $(this).next().html("&check;");
                $(this).parent().removeClass('btn-danger');
                $(this).parent().addClass('btn-primary');

            } else {
                $(this).next().html("&cross;");
                $(this).parent().removeClass('btn-primary');
                $(this).parent().addClass('btn-danger');

            }
        });
    </script>
</html>
