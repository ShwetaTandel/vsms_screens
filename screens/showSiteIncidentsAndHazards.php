<!-- This class renders all pending orders of the logged in user-->
<?php ?>

<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" id= "incidentsTab" href="#incidents"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } ?>  >Your Site Incidents</a>
    </li>
    <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" id= "hazardsTabs" href="#hazards"  <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } ?> >Your Site Hazards</a>
    </li>
    <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" id="actionTabs" href="#actions" <?php if ($isActionOWner === '0') { ?>style="display:none"<?php } ?> >Your Site Actions</a>
    </li>
     <li class="nav-item" >
        <a class="nav-link" data-toggle="tab" id="graTabs" href="#riskAssesment" <?php  if ($maxLevel === NULL) { ?>style="display:none"<?php } ?> >Risk Assessment</a>
    </li>


</ul>
<div class="tab-content" <?php if ($maxLevel === NULL) { ?>style="display:none"<?php } ?>>
    <div id="incidents" class="container-fluid tab-pane active">
        <br>
        <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>
                    <th>Incident Number</th>
                    <th>Incident Site</th>
                    <th>Location</th>
                    <th>Incident Date</th>
                    <th>Type</th>
                    <th>Current Level</th>
                    <th>Status</th>
                    <th>Current Approver</th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </thead>
        </table>


    </div>


    <div id="actions" class="container-fluid tab-pane">
        <br>
        <table id="actionsDataTable" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>

                    <th>Action ID</th>
                    <th>Action Site</th>
                    <th>Action Source</th>
                    <th>Action Description</th>
                    <th>Report ID</th>
                    <th>Report Status</th>
                    <th>Deadline</th>
                    <th>Owner</th>
                    <th>Estimated Comp. Date</th>
                    <th>Status</th>
                    <th>Date Closed</th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
            </thead>
        </table>


    </div>

    <div id="hazards" class="container-fluid tab-pane">
        <br>
        <table id="hazardsTable" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>
                    <th>Hazard Number</th>
                    <th>Site</th>
                    <th>Location</th>
                    <th>Hazard Date</th>

                    <th>Status</th>
                    <th>Current Approver</th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
            </thead>
        </table>


    </div>
    <div id="riskAssesment" class="container-fluid tab-pane">
        <br>
        <table id="riskAssessmentTable" class="compact stripe hover row-border" style="width:100%">
            <thead>
                <tr>
                    <th>RA Number</th>
                    <th>Site/Department</th>
                    <th>Area/Process Assessed</th>
                    <th>Revision Date</th>
                    <th>Next Review Date</th>
                    <th>Status</th>
                    <th>Current Approver</th>
                    <th>Live Document</th>
                    <th></th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                     <td></td>

                </tr>
            </thead>
        </table>


    </div>


</div>

<!---Modal dialogs-->
<div id="mOwnerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ownerModalTitle">Change the Owner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Current Site</label>
                            <input class="form-control"  type="text" id="currSite" disabled="true"/>
                            <input type="hidden" id = "actionId"/>
                            <input type="hidden" id = "reportId"/>
                            <input type="hidden" id = "reportSource"/>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Current Owner</label>
                            <input class="form-control"  type="text" id="currOwner" disabled="true"/>
                            <input type="hidden" id="prevOwnerId" />
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Current Manager</label>
                            <input class="form-control"  type="text" id="currManager" disabled="true"/>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Select Site<span style="color: red">*</span></label>
                            <select class="form-control" id="newSite" onchange="populateOwnersForCounterMeasure($(this), '#newOwner')" >
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site');
                                echo "<option value='-1'></option>";
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Select Owner<span style="color: red">*</span></label>
                            <select class="custom-select" id="newOwner" onchange="populateCounterMeasureManager($(this), 'newManager', '#newOwner')">

                            </select>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Manager</label>
                            <input class="form-control"  type="text" id="newManager" disabled="true"/>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-12">
                        <div class="form-group">
                            <label class="control-label">Comments / Reason for re-assigning<span style="color: red">*</span></label>
                            <textarea type="text" name="mComments" id="mComments" class="form-control" required maxlength="500"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" id="mChangeButton" onclick="changeOwner()">CHANGE</button>
                <button type="button" class="btn btn-gray" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
            </div>
        </div>

    </div>
</div>
<div id="mApproverModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="approverModalTitle">Change the Approver</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Current Level</label>
                            <input class="form-control"  type="text" id="currLevel" disabled="true"/>
                            <input type="hidden" id = "approverReportId"/>
                              <input type="hidden" id = "approverReportType"/>
                            <input type="hidden" id = "currAprroverSite"/>
                            <input type="hidden" id = "currApproverId"/>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Current Approver</label>
                            <input class="form-control"  type="text" id="currApprover" disabled="true"/>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Select Level<span style="color: red">*</span></label>
                            <select class="form-control" id="newLevel" onchange="populateApprovers($(this))" >
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Select Approver<span style="color: red">*</span></label>
                            <select class="custom-select" id="newApprover" >
                            </select>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-12">
                        <div class="form-group">
                            <label class="control-label">Comments / Reason for changing<span style="color: red">*</span></label>
                            <textarea type="text" name="mChangeApproverComments" id="mChangeApproverComments" class="form-control" required maxlength="500"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" id="mChangeApproverButton" onclick="changeApprover()">CHANGE</button>
                <button type="button" class="btn btn-gray" id="mCancelApproverButton" data-dismiss="modal" >CANCEL</button>
            </div>
        </div>

    </div>
</div>
<div id="mReOpenModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reopenModalTitle">Re-Open Action </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Select Site<span style="color: red">*</span></label>
                            <input type="hidden" id = "reopenActionId"/>
                            <input type="hidden" id = "reopenReportId"/>
                              <input type="hidden" id = "reopenReportType"/>

                            <select class="form-control" id="reopenSite" onchange="populateOwnersForCounterMeasure($(this), '#reopenOwner')" >
                                <?php
                                include ('../config/phpConfig.php');
                                if (mysqli_connect_errno()) {
                                    echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                }
                                $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site');
                                echo "<option value='-1'></option>";
                                while ($row = mysqli_fetch_array($result)) {
                                    echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                }
                                echo '';
                                mysqli_close($con);
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Select Owner<span style="color: red">*</span></label>
                            <select class="custom-select" id="reopenOwner" onchange="populateCounterMeasureManager($(this), 'reopenManager', '#reopenOwner')">

                            </select>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Manager</label>
                            <input class="form-control"  type="text" id="reopenManager" disabled="true"/>
                        </div>

                    </div>
                    <div class="col-md-2 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Deadline<span style="color: red">*</span></label>
                            <input class="form-control"  type="date" id="reopenDeadline"/>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2 col-lg-12">
                        <div class="form-group">
                            <label class="control-label">Comments / Reason for re-opening<span style="color: red">*</span></label>
                            <textarea type="text" id="reopenComments" class="form-control" required maxlength="500"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" id="mReOpenButton" onclick="reOpenAction()">SUBMIT</button>
                <button type="button" class="btn btn-gray" id="mCancel" data-dismiss="modal" >CANCEL</button>
            </div>
        </div>

    </div>
</div>


<div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="progressTitle">Audit Trail IR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tableDiv">
            </div>
        </div>

    </div>
</div>

<!---END Modal---------->
<script>
    var actionsTable;
    var hazardsTable;
    var incidentsTable;
    $(document).ready(function () {

        var show = '<?php echo$show; ?>';
        var maxLevel = '<?php echo$maxLevel; ?>';
       
        if (show === 'A') {

            $('#actionTabs').trigger('click');
        }else if(show === 'I'){
            $('#incidentsTab').trigger('click');
        }else if(show === 'H'){
            $('#hazardsTabs').trigger('click');
        }
        function formatHistory() {

            return '<table id="incidentHistory" class="compact stripe hover row-border" style="width:100%">' +
                    '<thead>' +
                    '<th>Comments</th>' +
                    '<th>Status</th>' +
                    '<th>Updated By</th>' +
                    '<th>Updated On</th>' +
                    '</thead>' +
                    '</table>';
        }
        function formatHazardHistory() {

            return '<table id="hazardHistory" class="compact stripe hover row-border" style="width:100%">' +
                    '<thead>' +
                    '<th>Comments</th>' +
                    '<th>Status</th>' +
                    '<th>Updated By</th>' +
                    '<th>Updated On</th>' +
                    '</thead>' +
                    '</table>';
        }
        function formatActionHistory() {

            return '<table id="actionHistory" class="compact stripe hover row-border" style="width:100%">' +
                    '<thead>' +
                    '<th>Comments</th>' +
                    '<th>Status</th>' +
                    '<th>Updated By</th>' +
                    '<th>Updated On</th>' +
                    '</thead>' +
                    '</table>';
        }
       
        var initIncidents = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
                if (index === 4 || index === 2 || index === 6 || index === 1 || index === 5) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var res = d.split('_');
                        if ($("#" + id + " option[value='" + d + "']").length === 0) {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        }
                    });
                }
            });
        };

        incidentsTable = $('#incidentsTable').DataTable({
            ajax: {"url": "../masterData/incidentsData.php?filter=SITE", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-blue' href='#' id ='bOpenIncidents'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-green' href='#' id ='bViewIncidents'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-orange' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a><span> </span><a class='btn btn-red' href='#' id='bChangeApprover'><i class='fa fa-group'></i> Change Approver</a> "
                }
            ],
            initComplete: initIncidents,
            orderCellsTop: true,
            dom: 'Bfrltip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {data: "incident_number",
                    render: function (data, type, row) {
                        return "IR" + data;
                    }
                },
                {data: "code"},
                {data: "location"},
                {data: "incident_date"},
                {data: "incident_type"},
                {data: "display_status",
                    render: function (data, type, row) {

                        if (row.status === 'L4_APPROVED' || row.status === '_CLOSED') {
                            return '';
                        } else {
                            return data;
                        }
                    }
                },
                {data: "statusDesc"},
                {data: "approver_name",
                    render: function (data, type, row) {

                        if (row.status === 'L2_SUBMITTED' || row.status === 'L2_EDITED' || row.status === 'L3_SUBMITTED' || row.status === 'L3_EDITED' || row.status === 'L3_APPROVED' || row.status === 'L4_SAVED') {
                            return data;
                        } else {
                            return '';
                        }
                    }
                },
                {data: ""}

            ],
            order: []
        });
        $('#incidentsTable tbody').on('click', '#bOpenIncidents', function () {
            var incidentId = incidentsTable.row($(this).parents('tr')).data().incident_number;
            var site = incidentsTable.row($(this).parents('tr')).data().site_id;
            var status = incidentsTable.row($(this).parents('tr')).data().status;
            var approverName = incidentsTable.row($(this).parents('tr')).data().approver_name;
            var user = '<?php echo $_SESSION['vsmsUserData']['first_name'] . ' ' . $_SESSION['vsmsUserData']['last_name'] ?>';
            if (status === 'L1_CREATED' || status === 'L1_SAVED' || status === 'L2_REJECTED') {
                if (isUserAtLevel('L1', site)) {
                    window.open("reportIncident.php?action=IR&iid=" + incidentId, '_self');
                } else {
                    alert("You are not authorised to make changes to this Incident Report. You can choose to view it instead.")
                }
            } else if (status === 'L2_SUBMITTED' || status === 'L2_EDITED') {
                if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to approve or reject this Incident Report.");
                } else if (isUserAtLevel('L2', site)) {
                    //Other users are L2 stage
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    //L1 or other normal users
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L3_REJECTED') {
                if (isUserAtLevel('L2', site)) {
                    window.open("reportIncident.php?action=FIR&iid=" + incidentId, '_self');
                } else {
                    alert("You are no longer authorised to make changes to this Incident Report. You can choose to view it instead.");
                }
            } else if (status === 'L2_APPROVED' || status === 'L3_SAVED') {
                if (isUserAtLevel('L2', site)) {
                    window.open("reportIncident.php?action=FIR&iid=" + incidentId, '_self');
                } else {
                    alert("You are no longer authorised to make changes to this Incident Report. You can choose to view it instead.");
                }
            } else if (status === 'L3_SUBMITTED' || status === 'L3_EDITED') {
                if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to approve or reject this Incident Report.");
                } else if (isUserAtLevel('L3', site)) {
                    //Other users are L3 stage
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    //L1 or other normal users
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L3_APPROVED' || status === 'L4_SAVED') {
                if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to sign off this Incident Report.");
                } else if (isUserAtLevel('L4', site)) {
                    //Other users are L4 stage
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    alert("Only current approver can edit this Incident Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L4_APPROVED' || status === '_CLOSED') {
                alert("This incident report has been closed and can no longer be edited. You can choose to view it instead.");

            } else {
                alert("You cannot edit an Incident Report when it is Pending approval.");
            }

        });
        $('#incidentsTable tbody').on('click', '#bViewIncidents', function () {
            var incidentId = incidentsTable.row($(this).parents('tr')).data().incident_number;
            window.open("reportIncident.php?action=VIEW&iid=" + incidentId, '_self');

        });

        //VIEW Progress CLICK
        $('#incidentsTable tbody').on('click', '#bViewProgress', function () {
            var tr = $(this).closest('tr');
            var data = incidentsTable.row(tr).data();
            var rowID = data.incident_number;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatHistory());
            historyTable(rowID);
            $('#progressTitle').html("Audit Trail IR" + rowID);
            $('#mProgressModel').modal('show');
            populateLevels(data.display_status);



        });

        //VIEW Progress CLICK
        $('#incidentsTable tbody').on('click', '#bChangeApprover', function () {
            var tr = $(this).closest('tr');
            var data = incidentsTable.row(tr).data();
            var rowID = data.incident_number;
            var status = incidentsTable.row($(this).parents('tr')).data().status;
            if (status === 'L1_CREATED' || status === 'L1_SAVED' || status === 'L2_APPROVED' || status === 'L2_REJECTED' || status === 'L3_SAVED' || status === 'L3_REJECTED') {
                alert("This Incident Report has not been submitted for approval. Please click on the Edit button to change the approver.");
                return;
            } else if (status === 'L4_APPROVED' || status === '_CLOSED') {
                alert("This Incident Report has been closed.");
                return;
            }
            document.getElementById('approverReportId').value = rowID;
            document.getElementById('approverReportType').value = "Incident";
            document.getElementById('currAprroverSite').value = data.site_id;
            document.getElementById('currLevel').value = data.display_status;
            document.getElementById('currApprover').value = data.approver_name;
            document.getElementById('currApproverId').value = data.curr_approver_id;
            //document.getElementById('approverReportType').value = '';
            document.getElementById('approverModalTitle').innerHTML = 'Change Approver for IR' + rowID;
            populateLevels(status);
            document.getElementById('newApprover').options.length = 0;
            $('#mApproverModal').modal('show');
        });

       

        function historyTable(rowID) {
            var bodyTable = $('#incidentHistory').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/incidentHistoryData.php", "data": {incidentId: rowID}, "dataSrc": ""},
                  dom: 'Bfrltip',
                 buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                columns: [
                    {data: "comments", width: "20%"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "updated_by"},
                    {data: "updated_at"}
                ],
                order: []
            });

        }
        var initHazards = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
                if (index === 5 || index === 4 || index === 2 || index === 1) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('#hazardsTable thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var res = d;
                        if (d !== '') {

                            select.append('<option value="' + res + '">' + res + '</option>');

                        }

                    });
                }
            });
        };
        
        var content = "<a class='btn btn-blue' href='#' id ='bViewHazards'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-green' href='#' id='bViewHistHazards'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><span>  </span><a class='btn btn-orange' href='#' id='bChangeHazardApprover'><i class='fa fa-group'></i> Change Approver</a>";
        if(maxLevel === 'L3' || maxLevel==='L4'){
            content = "<a class='btn btn-blue' href='#' id ='bViewHazards'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-green' href='#' id='bViewHistHazards'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><span>  </span><a class='btn btn-orange' href='#' id='bChangeHazardApprover'><i class='fa fa-group'></i> Change Approver</a><span>  </span><a class='btn btn-red' href='#' id='bDeleteHazard'><i class='fa fa-minus'></i> Delete Hazard</a>";
        }
        hazardsTable = $('#hazardsTable').DataTable({
            ajax: {"url": "../masterData/hazardsData.php?filter=SITE", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent:content 
                }
            ],
            initComplete: initHazards,
            orderCellsTop: true,
            autoWidth: false,
            dom: 'Bfrltip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {data: "hazard_number"},
                {data: "code"},
                {data: "location"},
                {data: "hazard_date"},
                {data: "description"},
                {data: "approver"},
                {data: ""}

            ],
            order: []
        });
        $('#hazardsTable tbody').on('click', '#bViewHazards', function () {
            var hid = hazardsTable.row($(this).parents('tr')).data().hazard_number;
            window.open("reportHazard.php?action=VIEW&hid=" + hid, '_self');

        });


        $('#hazardsTable tbody').on('click', '#bChangeHazardApprover', function () {
            var tr = $(this).closest('tr');
            var data = hazardsTable.row(tr).data();
            var rowID = data.hazard_number;
            var status = hazardsTable.row($(this).parents('tr')).data().status;
            if (status === 'L2_APPROVED' || status === '_CLOSED') {
                alert("This Hazard Report has been closed.");
                return;
            }
            document.getElementById('approverReportId').value = rowID;
             document.getElementById('approverReportType').value = "Hazard";
            document.getElementById('currAprroverSite').value = data.site_id;
            document.getElementById('currLevel').value =data.display_status;
            document.getElementById('currApprover').value = data.approver;
            document.getElementById('currApproverId').value = data.curr_approver_id;
            document.getElementById('approverModalTitle').innerHTML = 'Change Approver for HZ' + rowID;
            populateLevels(status);
            document.getElementById('newApprover').options.length = 0;
            $('#mApproverModal').modal('show');
        });

        $('#hazardsTable tbody').on('click', '#bDeleteHazard', function () {
            var tr = $(this).closest('tr');
            var data = hazardsTable.row(tr).data();
            
            if(data.status === 'L2_APPROVED' || data.status === '_CLOSED'){
                
                alert("You cannot delete the Hazard as it is already Closed.");
                return;
            }
            var comments = prompt("Are you sure you want to delete this Hazard Report? This action cannot be undone. Please add the reason for deletion.");
            var hid =data.hazard_number;
            if (comments !== null && comments !== '') {
                var filter = "?hazardNumber=" + hid + "&comments=" + encodeURIComponent(comments) + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "deleteHazard", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response === 'OK') {
                            sendHazardEmails("DELETED", hid);
                            alert("Hazard report has been deleted.");
                           window.open('home.php?show=H', '_self');
                        }

                    }
                });
            }
        });
        
        
        //VIEW Progress CLICK
        $('#hazardsTable tbody').on('click', '#bViewHistHazards', function () {
            var tr = $(this).closest('tr');
            var data = hazardsTable.row(tr).data();
            var rowID = data.hazard_number;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatHazardHistory());
            hazardHistoryTable(rowID);
            $('#progressTitle').html("Audit Trail HZ" + rowID);
            $('#mProgressModel').modal('show');

        });
        function hazardHistoryTable(rowID) {
            var bodyTable = $('#hazardHistory').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/hazardHistoryData.php", "data": {hid: rowID}, "dataSrc": ""},
                buttons: [
                    {extend: 'excel', filename: 'hazardHistory', title: 'Hazard History'}
                ],
                columns: [
                    {data: "comments", width: "20%"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "updated_by"},
                    {data: "updated_at"}
                ],
                order: []
            });

        }
        var initActions = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
                if (index === 1 || index === 2 || index === 4 || index === 5 || index === 9 || index === 7) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('#actionsDataTable thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        if (d !== "") {
                            select.append('<option value="' + d + '">' + d + '</option>');
                        }
                    });
                }
            });
        };

        actionsTable = $('#actionsDataTable').DataTable({
            ajax: {"url": "../masterData/actionsData.php?filter=SITEACTIONS", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-blue' href='#' id ='bViewAction'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-green' href='#' id='bViewActionProgress'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><a class='btn btn-orange' href='#' id='bReopen'><i class='fa fa-folder-open'></i> Re-Open</a><span>  </span><a class='btn btn-red' href='#' id='bChangeOwner'><i class='fa fa-group'></i> Change Owner</a>"
                }
            ],
            initComplete: initActions,
            orderCellsTop: true,
            dom: 'Bfrltip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {data: "actionId",
                    render: function (data, type, row) {
                        return ("AC" + data);
                    }},
                {data: "code"},
                {data: "actionSource"},
                {data: "actionDesc"},
                {data: "reportId",
                    render: function (data, type, row) {
                        if (data.startsWith('HZ')) {
                            return "<a href='reportHazard.php?action=VIEW&hid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        } else   if (data.startsWith('IR')){
                              return "<a href='reportIncident.php?action=VIEW&iid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        }else   if (data.startsWith('RA')){
                              return "<a href='genericRiskAssessment.php?action=VIEW&graid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        }else {
                            return data;
                        }
                    }},
                {data: "reportStatus"},
                {data: "deadline"},
                {data: "actionOwnerName"},
                {data: "estimated_completion_date"},
                {data: "actionStatus"},
                {data: "updated_at",
                    render: function (data, type, row) {
                        if (row.actionStatus === 'Closed') {
                            return data;
                        }
                        return "";
                    }
                },
                {data: ""}

            ],
            order: []
        });
        //VIEW Progress CLICK
        $('#actionsDataTable tbody').on('click', '#bViewActionProgress', function () {
            var tr = $(this).closest('tr');
            var data = actionsTable.row(tr).data();
            var rowID = data.actionId;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatActionHistory());
            actionHistoryTable(rowID);
            $('#progressTitle').html("Audit Trail AC" + rowID);
            $('#mProgressModel').modal('show');


        });
        $('#actionsDataTable tbody').on('click', '#bViewAction', function () {
            var tr = $(this).closest('tr');
            var data = actionsTable.row(tr).data();
            var rowID = data.actionId;
            window.open("action.php?filter=VIEW&id=" + rowID + '&source='+data.actionSource, '_self');
           
        });
        $('#actionsDataTable tbody').on('click', '#bChangeOwner', function () {
            var tr = $(this).closest('tr');
            var data = actionsTable.row(tr).data();
            var rowID = data.actionId;
            if (data.actionStatus === 'Cancelled') {
                alert("You cannot change the owner of this action, as it is has been cancelled. To reinstate this action, click on 'Re-Open' button.");
                return;
            } else if (data.actionStatus === 'Closed') {
                alert("You cannot change the owner of this action, as it is has been closed. To reinstate this action, click on 'Re-Open' button.");
                return;
            }
            $('#prevOwnerId').val(data.action_owner);
            $('#currOwner').val(data.actionOwnerName);
            $('#currSite').val(data.code);
            $('#currManager').val(data.manager_name);
            $('#actionId').val(rowID);
            if (data.reportId.startsWith("IR")) {
                $('#reportId').val(data.reportId.substring(2, data.reportId.length));
                $('#reportSource').val('Incident');
            } else if (data.reportId.startsWith("HZ")) {
                $('#reportId').val(data.reportId.substring(2, data.reportId.length));
                $('#reportSource').val('Hazard');
            } else {
                $('#reportId').val('0');
                $('#reportSource').val('Other');
            }
            $('#newOwner').val('-1');
            $('#newSite').val('-1');
            $('#newManager').val('');
            $('#mComments').val('');
            $('#mOwnerModal').modal('show');
            $('#ownerModalTitle').html("Change the Owner for AC" + rowID);
        });
        $('#actionsDataTable tbody').on('click', '#bReopen', function () {
            var tr = $(this).closest('tr');
            var data = actionsTable.row(tr).data();
            var rowID = data.actionId;
            if (data.actionStatus !== 'Closed' && data.actionStatus !== 'Cancelled') {
                alert("This action is still open.");
                return;
            }
            $('#reopenActionId').val(rowID);
           if (data.reportId.startsWith("IR")) {
                $('#reopenReportId').val(data.reportId.substring(2, data.reportId.length));
                $('#reopenReportType').val('Incident');
            } else if (data.reportId.startsWith("HZ")) {
               $('#reopenReportId').val(data.reportId.substring(2, data.reportId.length));
                $('#reopenReportType').val('Hazard');
            }else{
                 $('#reopenReportId').val('0');
                $('#reopenReportType').val('Other');
            }
            $('#reopenOwner').val('-1');
            $('#reopenSite').val('-1');
            $('#reopenManager').val('');
            $('#reopenComments').val('');
            document.getElementById('reopenModalTitle').innerHTML = 'Re-Open Action AC' + rowID;
            $('#mReOpenModel').modal('show');

        });
        function actionHistoryTable(rowID) {
            var bodyTable = $('#actionHistory').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/actionsData.php?filter=ACTIONHISTORY", "data": {actionId: rowID}, "dataSrc": ""},
                columnDefs: [{
                        targets: -1,
                        data: null
                    }
                ],
                buttons: [
                    {extend: 'excel', filename: 'actionHistory', title: 'Action History'}
                ],
                columns: [
                    {data: "comments"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "updated_by"},
                    {data: "updated_at"}
                ],
                order: []
            });

        }

 var initGRA = function (settings, json) {
            var api = new $.fn.dataTable.Api(settings);
            api.columns().every(function (index) {
                var column = this;
                if (index === 5 || index === 6 || index === 1) {
                    var id = 'col' + index;
                    var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                            .appendTo($('#riskAssessmentTable thead tr:eq(1) td').eq(index))
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                        );
                                column
                                        .search(val ? '^' + val + '$' : '', true, false)
                                        .draw();
                            });
                    column.data().unique().sort().each(function (d, j) {
                        var res = d;
                        if (d !== '') {

                            select.append('<option value="' + res + '">' + res + '</option>');

                        }

                    });
                }
            });
        };
        
        var riskContent = "<a class='btn btn-blue' href='#' id ='bEditGRA'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-green' href='#' id ='bViewGRA'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-orange' href='#' id='bViewGRAProgress'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><span>  </span><a class='btn btn-info' href='#' id='bChangeGRAApprover'><i class='fa fa-group'></i> Change Approver</a><span>  </span><a class='btn btn-warning' href='#' id ='bReopenGRA'><i class='fa fa-folder-open'></i> New Revision</a>";
        if(maxLevel === 'L3' || maxLevel === 'L4'){
            riskContent = "<a class='btn btn-blue' href='#' id ='bEditGRA'><i class='fa fa-edit'></i> Edit</a><span>  </span><a class='btn btn-green' href='#' id ='bViewGRA'><i class='fa fa-file'></i> View</a><span>  </span><a class='btn btn-orange' href='#' id='bViewGRAProgress'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><span>  </span><a class='btn btn-info' href='#' id='bChangeGRAApprover'><i class='fa fa-group'></i> Change Approver</a><span>  </span><a class='btn btn-warning' href='#' id ='bReopenGRA'><i class='fa fa-folder-open'></i> New Revision</a><span>  </span><a class='btn btn-red' href='#' id ='bDeleteGRA'><i class='fa fa-minus'></i> Delete</a>";
        }
        riskAssessmentTable = $('#riskAssessmentTable').DataTable({
            ajax: {"url": "../masterData/graData.php?filter=SITE", "dataSrc": ""},
            columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent:riskContent 
                }
            ],
            initComplete: initGRA,
            orderCellsTop: true,
            autoWidth: false,
            dom: 'Bfrltip',
            buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
            columns: [
                {data: "gra_id",
                    render: function (data, type, row) {
                        return ("RA" + data);
                    }},
                {data: "code"},
                {data: "area_assessed"},
                {data: "graDate"},
                {data: "reviewDate", render: function (data, type, row) {
                        
                            return (row.is_archived === "1") ? "N/A":data;
                        
                    }},
                {data: "description"},
                {data: "approver"},
                {data: "is_archived",
                    render: function (data, type, row) {
                        return (data === '1') ? '<span style="color:red" data-order="0" class="fa fa-remove"></span>' :  '<span data-order="1" style="color:green" class="fa fa-check"></span>';
                    }},
                {data: ""}

            ],
            order: []
        });
        $('#riskAssessmentTable tbody').on('click', '#bViewGRA', function () {
            var hid = riskAssessmentTable.row($(this).parents('tr')).data().gra_id;
            window.open("genericRiskAssessment.php?action=VIEW&graid=" + hid, '_self');
            

        });
        $('#riskAssessmentTable tbody').on('click', '#bDeleteGRA', function () {
            var graid = riskAssessmentTable.row($(this).parents('tr')).data().gra_id;
           if(confirm("Are you sure want to delete this Risk Assessment? All the Risk Hazards/ Actions and all other documentation will be lost if you go ahead.")){
               var filter = "?graId=" + graid + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "deleteRiskAssessment", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response.startsWith('OK')) {
                            alert("Risk Assessment is successfully deleted");
                            location.reload();
                        }
                    }
                });
           }
            

        });
        $('#riskAssessmentTable tbody').on('click', '#bReopenGRA', function () {
              var graid = riskAssessmentTable.row($(this).parents('tr')).data().gra_id;
            var status = riskAssessmentTable.row($(this).parents('tr')).data().status;
			var isArchived = riskAssessmentTable.row($(this).parents('tr')).data().is_archived;
            if(status === '_CLOSED' && isArchived === '1'){
                 alert("This Risk Assessment is already archived and revised.");
            }else if(status === '_CLOSED' && isArchived=== '0'){
                
            if(confirm("Are you sure you want to create a new vesrion of RA"+graid+"?"))
             var filter = "?graId=" + graid + "&requestorId=" + '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                $.ajax({
                    url: "../action/callGetService.php",
                    data: {functionName: "newVersionRiskAssessment", filter: filter},
                    type: 'GET',
                    success: function (response) {
                        if (response.startsWith('OK')) {
                            var res = response.split("-");
                             window.open("genericRiskAssessment.php?action=L1SUBMIT&graid=" + res[1], '_self');
                        }
                    }
                });
            }else{
                alert("This Risk Assessment is not complete yet.");
            }

        });
        $('#riskAssessmentTable tbody').on('click', 'td', function () {
            var tr = $(this).closest('tr');
            if ($(this).index() === 7) {
            var data = riskAssessmentTable.row(tr).data();
            if(data.is_archived=== '1'){
                   alert(data.cnt);
            }
            }
        });
         $('#riskAssessmentTable tbody').on('click', '#bEditGRA', function () {
         
            var graId = riskAssessmentTable.row($(this).parents('tr')).data().gra_id;
            var site = riskAssessmentTable.row($(this).parents('tr')).data().site_id;
            var status = riskAssessmentTable.row($(this).parents('tr')).data().status;
            var approverName = riskAssessmentTable.row($(this).parents('tr')).data().approver;
            var user = '<?php echo $_SESSION['vsmsUserData']['first_name'] . ' ' . $_SESSION['vsmsUserData']['last_name'] ?>';
            if (status === 'L1_SAVED' || status === 'L2_REJECTED') {
                if (isUserAtLevel('L1', site)) {
                    window.open("genericRiskAssessment.php?action=L1SUBMIT&graid=" + graId, '_self');
                } else {
                    alert("You are not authorised to make changes to this Risk Assessment Report. You can choose to view it instead.");
                }
            } else if (status === 'L2_SUBMITTED' || status === 'L2_EDITED') {
                if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to approve or reject this Risk Assessment Report.");
                } else if (isUserAtLevel('L2', site)) {
                    //Other users are L2 stage
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    //L1 or other normal users
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L3_REJECTED') {
                if (isUserAtLevel('L2', site)) {
                    window.open("genericRiskAssessment.php?action=L2APPROVE&graid=" + graId, '_self');
                } else {
                    alert("You are no longer authorised to make changes to this Risk Assessment Report. You can choose to view it instead.");
                }
            } else if (status === 'L2_APPROVED' || status === 'L3_EDITED') {
               if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to sign off this Risk Assessment Report.");
                } else if (isUserAtLevel('L3', site)) {
                    //Other users are L4 stage
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L3_APPROVED' || status === 'L4_EDITED') {
                if (approverName === user) {
                    //The current approver
                    alert("Please go to Pending Approvals to sign off this Risk Assessment Report.");
                } else if (isUserAtLevel('L4', site)) {
                    //Other users are L4 stage
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it or change the approver instead.");
                } else {
                    alert("Only current approver can edit this Risk Assessment Report at this stage. You can choose to view it instead.");
                }
            } else if (status === 'L4_APPROVED' || status === '_CLOSED') {
                alert("This Risk Assessment Report has been closed and can no longer be edited. You can choose to view it instead.");

            }  else if (status === 'L4_REJECTED') {
                if (isUserAtLevel('L3', site)) {
                    window.open("genericRiskAssessment.php?action=L3APPROVE&graid=" + graId, '_self');
                } else {
                    alert("You are no longer authorised to make changes to this Risk Assessment Report. You can choose to view it instead.");
                }
            }else {
                alert("You cannot edit an Risk Assessment Report when it is Pending approval.");
            }


        });


        $('#riskAssessmentTable tbody').on('click', '#bChangeGRAApprover', function () {
            var tr = $(this).closest('tr');
            var data = riskAssessmentTable.row(tr).data();
            var rowID = data.gra_id;
            var status = riskAssessmentTable.row($(this).parents('tr')).data().status;
             if (status === 'L1_CREATED' || status === 'L1_SAVED' || status === 'L2_REJECTED' || status === 'L4_REJECTED' || status === 'L3_REJECTED') {
                alert("This Risk Assessment Report has not been submitted for approval. Please click on the Edit button to change the approver.");
                return;
            } else if (status === 'L4_APPROVED') {
                alert("This Risk Assessment Report has been closed.");
                return;
            }
            document.getElementById('approverReportId').value = rowID;
             document.getElementById('approverReportType').value = "RiskAssessment";
            document.getElementById('currAprroverSite').value = data.site_id;
            document.getElementById('currLevel').value =data.display_status;
            document.getElementById('currApprover').value = data.approver;
            document.getElementById('currApproverId').value = data.curr_approver_id;
            document.getElementById('approverModalTitle').innerHTML = 'Change Approver for RA' + rowID;
            populateLevels(status);
            document.getElementById('newApprover').options.length = 0;
            $('#mApproverModal').modal('show');
        });

      
          function formatGRAHistory() {

            return '<table id="graHistory" class="compact stripe hover row-border" style="width:100%">' +
                    '<thead>' +
                    '<th>Comments</th>' +
                    '<th>Status</th>' +
                    '<th>Updated By</th>' +
                    '<th>Updated On</th>' +
                    '</thead>' +
                    '</table>';
        }
        //VIEW Progress CLICK
        $('#riskAssessmentTable tbody').on('click', '#bViewGRAProgress', function () {
            var tr = $(this).closest('tr');
            var data = riskAssessmentTable.row(tr).data();
            var rowID = data.gra_id;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatGRAHistory());
            graHistoryTable(rowID);
            $('#progressTitle').html("Audit Trail RA" + rowID);
            $('#mProgressModel').modal('show');

        });
        function graHistoryTable(rowID) {
            var bodyTable = $('#graHistory').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/graHistoryData.php", "data": {id: rowID}, "dataSrc": ""},
                buttons: [
                    {extend: 'excel', filename: 'GRAHistory', title: 'RA History'}
                ],
                columns: [
                    {data: "comments", width: "20%"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "updated_by"},
                    {data: "updated_at"}
                ],
                order: []
            });

        }
        

    });
     function isUserAtLevel(level, site) {
            var userSiteLevels = <?php echo json_encode($userSiteLevels) ?>;
            for (var i = 0; i < userSiteLevels.length; i++) {
                if (userSiteLevels[i].site_id === site && userSiteLevels[i].level >= level) {
                    return true;
                }
            }
            return false;
        }
    
   
        
        
    function populateLevels(status) {
        var select = document.getElementById('newLevel');
        select.options.length = 0;
        var option = document.createElement('option');
        option.setAttribute("value", "-1");
        select.add(option);
        levelArr = [];
        if (status === 'L2_SUBMITTED' || status === 'L2_EDITED') {
            levelArr = ['L2', 'L3', 'L4'];
        } else if (status === 'L3_SUBMITTED' || status === 'L3_EDITED') {
            levelArr = ['L3', 'L4'];
        } else if (status === 'L3_APPROVED' || status === 'L4_SAVED') {
            levelArr = ['L4'];
        }

        //This is hazard sttaus 
        if (status === 'L2_SAVED' || status === 'L1_SUBMITTED') {
            levelArr = ['L2'];
        } 
        for (var i = 0; i < levelArr.length; i++) {
            option = document.createElement('option');
            option.setAttribute("value", levelArr[i]);
            option.innerHTML = levelArr[i];
            select.add(option);
        }
    }

    function populateApprovers(elem) {
        var level = $(elem).val();
        var site = document.getElementById('currAprroverSite').value;
        var currApproverName = document.getElementById('currApprover').value;
        var select = document.getElementById('newApprover');
        select.options.length = 0;
        var option = document.createElement('option');
        option.setAttribute("value", "-1");
        select.add(option);
        $.ajax({
            url: "../masterData/incidentsData.php?filter=APPROVERBYLEVEL&level=" + level + "&site=" + site,
            type: 'GET',
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            success: function (response, textstatus) {
                console.log(response);
                var approvers = JSON.parse(response);
                for (var i = 0; i < approvers.length; i++) {
                    if (currApproverName !== (approvers[i].first_name + ' ' + approvers[i].last_name)) {
                        var option = document.createElement('option');
                        option.setAttribute("value", approvers[i].user_id);
                        option.innerHTML = approvers[i].first_name + ' ' + approvers[i].last_name;
                        select.add(option);
                    }
                }
            }
        });


    }
    function populateOwnersForCounterMeasure(elem, flag) {

        var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
        var containmentSite = elem.val();
        var owners = '';
        $(flag).attr('disabled', false);
        var ownerList = $(flag);
        ownerList[0].options.length = 0;
        var option = document.createElement('option');
        option.setAttribute("value", "-1");
        ownerList[0].add(option);
        for (var i = 0; i < ownersMasterList.length; i++) {
            var site = ownersMasterList[i];
            if (site.id === containmentSite) {
                owners = site.owners;
                break;
            }
        }
        for (var i = 0; i < owners.length; i++) {
            var newOption = document.createElement('option');
            newOption.setAttribute("value", owners[i].id);
            newOption.setAttribute("data-divid", owners[i].manager_name);
            newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
            ownerList[0].add(newOption);
        }
    }
    function populateCounterMeasureManager(elem, flag, flag1) {
        document.getElementById(flag).value = $(flag1).find('option:selected').attr('data-divid') === undefined ? '' : $(flag1).find('option:selected').attr('data-divid');
    }
    function changeOwner() {
        var prevOwnerId = document.getElementById("prevOwnerId").value;
        var owner = document.getElementById("newOwner");
        var nextOwner = owner.value;
        var comments = document.getElementById('mComments').value;
        if (nextOwner === '-1' || nextOwner === '' || comments === '') {
            alert("Please add all fields marked mandatory.");
            return;
        }
        var actionId = document.getElementById("actionId").value;
        var actionSite = document.getElementById("newSite").value;
        var reportId = parseInt(document.getElementById("reportId").value);
        var actionSource = document.getElementById("reportSource").value;
        var userId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
        var actionModel = {"comments": comments, "filter": "REASSIGNED", "id": actionId, "createdByUserId": userId, "actionOwner": nextOwner,
            "reportId": reportId, "actionSite": actionSite, "actionSource": actionSource};
        console.log(actionModel);
        var form_data = new FormData();
        form_data.append('filter', JSON.stringify(actionModel));
        form_data.append('function', 'saveAction');
        form_data.append('connection', vsmsservice);


        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function (response, textstatus) {
                console.log(response);
                if (response.startsWith("OK")) {
                    alert("Action Owner successfully changed.");
                    sendActionEmails(actionId, reportId, 'REASSIGNED', prevOwnerId, actionSource);
                    $('#mOwnerModal').modal('hide');
                    window.location.reload(true);
                } else {
                    alert("Something went wrong.Please submit again");
                }
            }});

    }
    function changeApprover() {
        var approver = document.getElementById("newApprover").value;
        var prevApproverId = document.getElementById("currApproverId").value;
        var reportId = document.getElementById("approverReportId").value;
        var reportType = document.getElementById("approverReportType").value;
        var comments = document.getElementById('mChangeApproverComments').value;
        if (approver === '-1' || comments === '') {
            alert("Please add all fields marked mandatory.");
            return;
        }
        var userId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
        var incidentModel = {"reportId": reportId, "approverComments": comments, "currApproverId": approver, "requestorId": userId, "reportType":reportType};
        console.log(incidentModel);
        var form_data = new FormData();
        form_data.append('filter', JSON.stringify(incidentModel));
        form_data.append('function', 'changeApprover');
        form_data.append('connection', vsmsservice);


        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function (response, textstatus) {
                console.log(response);
                if (response.startsWith("OK")) {
                    alert("Approver successfully changed.")
                    $('#mApproverModal').modal('hide');
                    sendEmails(reportId, "ApproverChanged", prevApproverId,reportType);
                    if(reportType ==='Incident'){
                        incidentsTable.ajax.reload();
                    }else if(reportType ==='RiskAssessment'){
                        riskAssessmentTable.ajax.reload();
                    }else{
                        hazardsTable.ajax.reload();
                    }
                   // window.location.reload(true);
                } else {
                    alert("Something went wrong.Please submit again");
                }
            }});

    }
    function sendEmails(iid, action, prevApproverId, reportType) {
        var type, func, url = '';
        if (action === 'ApproverChanged') {
            type = 'Both';
            func = 'ApproverChanged';
        }
        if(reportType === 'Incident'){
            url = "sendEmails.php?incidentId=" + iid + "&function=" + reportType+func + "&type=" + type + "&prevApprover=" + prevApproverId;
        }else if(reportType === 'Hazard'){
             url = "sendEmails.php?hazardId=" + iid + "&function=" + reportType+func + "&type=" + type + "&prevApprover=" + prevApproverId;
        }
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                console.log(data);
            }
        });
    }
    function sendHazardEmails(action, hid) {
            var func, type = "";

            if (action === 'DELETED') {
                func = "HazardDeleted";
                type = "Notification";
            } 
            $.ajax({
                type: "GET",
                url: "sendEmails.php?hazardId=" + hid + "&function=" + func + "&type=" + type,
                success: function (data) {
                    console.log(data);
                }
            });

        }
    function sendActionEmails(actionId, reportId, filter, prevOwnerId, actionSource) {
        var func = '';
        var type = '';
        var reportIdString = '';
        if (actionSource === 'Incident') {
            reportIdString = "&incidentId=" + reportId;
        } else if (actionSource === 'Hazard') {
            reportIdString = "&hazardId=" + reportId;
        }
        var url = "sendEmails.php?actionId=" + actionId + "&function=" + func + "&type=" + type + "&source=" + actionSource + reportIdString;
        if (filter === 'REOPENED') {
            func = 'ActionReopened';
            type = 'Both';
            url = "sendEmails.php?actionId=" + actionId + "&function=" + func + "&type=" + type + "&source=" + actionSource + reportIdString;
        } else if (filter === 'REASSIGNED') {
            func = 'ActionReassigned';
            type = 'Both';
            url = "sendEmails.php?actionId=" + actionId + "&function=" + func + "&type=" + type + '&prevOwnerId=' + prevOwnerId + "&source=" + actionSource + reportIdString;
        }
        $.ajax({
            type: "GET",
            url: url,
            success: function (data) {
                console.log(data);
            }
        });
    }
    function reOpenAction() {
        var owner = document.getElementById("reopenOwner");
        var nextOwner = owner.value;
        var comments = document.getElementById('reopenComments').value;
        var deadline = document.getElementById('reopenDeadline').value;
        var site = document.getElementById('reopenSite').value;
        if (nextOwner === '-1' || nextOwner === '' || comments === '' || deadline === '') {
            alert("Please add all fields marked mandatory.");
            return;
        }
        if (new Date(deadline) < new Date().setHours(0, 0, 0, 0)) {
            alert("Invalid deadline for the action. Please enter a future date.");
            return;
        }
        var actionId = document.getElementById("reopenActionId").value;
        var reportId = parseInt(document.getElementById("reopenReportId").value);
        var actionSource = document.getElementById("reopenReportType").value;
        var userId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
        var actionModel = {"comments": comments, "filter": "REOPEN", "id": actionId, "createdByUserId": userId, "actionOwner": nextOwner, 
            "reportId": reportId, "deadline": deadline, "actionSite": site, "actionSource":actionSource};
        console.log(actionModel);
        var form_data = new FormData();
        form_data.append('filter', JSON.stringify(actionModel));
        form_data.append('function', 'saveAction');
        form_data.append('connection', vsmsservice);


        $.ajax({
            url: "../action/callPostService.php",
            type: 'POST',
            processData: false,
            cache: false,
            contentType: false,
            data: form_data,
            success: function (response, textstatus) {
                console.log(response);
                if (response.startsWith("OK")) {
                    alert("Action successfully re-opened.")
                    sendActionEmails(actionId, reportId, 'REOPENED', 0,actionSource);
                    $('#mReOpenModel').modal('hide');
                   // window.location.reload(true);
                   actionsTable.ajax.reload();
                } else {
                    alert("Something went wrong.Please submit again");
                }
            }});

    }
</script>
