<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    include '../masterData/generalMasterData.php';
    $actionOwners = getActionOwners();
    ?>
    <head>
        <title>VSMS - Pending Approvals</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.16/af-2.2.2/b-1.5.1/b-colvis-1.5.1/b-flash-1.5.1/b-html5-1.5.1/b-print-1.5.1/cr-1.4.1/fc-3.2.4/fh-3.1.3/kt-2.3.2/r-2.2.1/rg-1.0.2/rr-1.2.3/sc-1.4.4/sl-1.2.5/datatables.min.js"></script>-->

        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <div class="pull-right">
            <?php
            include './commonHeader.php';
            ?>
        </div>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Your Outstanding Actions</h1>      
            </div>
        </div>
        <br/>
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#incidentsHome">Outstanding Actions</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="actionsTable" class="container-fluid tab-pane active">
                <br>
                <table id="actionsDataTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Action ID</th>
                            <th>Incident Site</th>
                            <th>Action Source</th>
                            <th>Report ID</th>
                            <th>Report Status</th>
                            <th>Deadline</th>
                            <th>(Re) Assigned by</th>
                            <th>Estimated Comp. Date</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <tr>
                            <td></td>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>


            </div>

        </div>
        <!--Modal Dialogs --->


        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="progressTitle">Audit Trail Action</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="tableDiv">
                    </div>
                </div>

            </div>
        </div>




        <div id="mOwnerModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ownerModalTitle">Change the Owner</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Current Site</label>
                                    <input class="form-control"  type="text" id="currSite" disabled="true"/>
                                    <input type="hidden" id = "actionId"/>
                                    <input type="hidden" id = "reportId"/>
                                    <input type="hidden" id = "reportSource"/>
                                </div>

                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Current Owner</label>
                                    <input class="form-control"  type="text" id="currOwner" disabled="true"/>
                                </div>

                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Current Manager</label>
                                    <input class="form-control"  type="text" id="currManager" disabled="true"/>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Select Site<span style="color: red">*</span></label>
                                    <select class="form-control" id="newSite" onchange="populateOwnersForCounterMeasure($(this))" >
                                        <?php
                                        include ('../config/phpConfig.php');
                                        if (mysqli_connect_errno()) {
                                            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                        }
                                        $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site');
                                        echo "<option value='-1'></option>";
                                        while ($row = mysqli_fetch_array($result)) {
                                            echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                        }
                                        echo '';
                                        mysqli_close($con);
                                        ?>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Select Owner<span style="color: red">*</span></label>
                                    <select class="custom-select" id="newOwner" onchange="populateCounterMeasureManager($(this))">

                                    </select>
                                </div>

                            </div>
                            <div class="col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Manager</label>
                                    <input class="form-control"  type="text" id="newManager" disabled="true"/>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">Comments / Reason for re-assigning<span style="color: red">*</span></label>
                                    <textarea type="text" name="mComments" id="mComments" class="form-control" required maxlength="500"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-green" id="mChangeButton" onclick="changeOwner()">CHANGE</button>
                        <button type="button" class="btn btn-gray" id="mCancelButton" data-dismiss="modal" >CANCEL</button>
                    </div>
                </div>

            </div>
        </div>
        <script>
            $(document).ready(function () {
                function format(d) {
                    // `d` is the original data object for the row
                    return '<table id="actionDetails" class="compact" border="0" style="padding-left:50px; width:100%;">' +
                            '<thead>' +
                            '<th>Action Type</th>' +
                            '<th>Action Description</th>' +
                            '</thead>' +
                            '</table>';
                }
                function formatHistory() {

                    return '<table id="actionHistory" class="compact stripe hover row-border" style="width:100%">' +
                            '<thead>' +
                            '<th>Comments</th>' +
                            '<th>Status</th>' +
                            '<th>Updated By</th>' +
                            '<th>Updated On</th>' +
                            '</thead>' +
                            '</table>';
                }
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var initActions = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 2 || index === 4 || index === 3 ) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#actionsDataTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var actionsTable = $('#actionsDataTable').DataTable({
                    ajax: {"url": "../masterData/actionsData.php?filter=OUTSTANDINGACTIONS", "dataSrc": ""},
                    columnDefs: [{
                            targets: -1,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bOpenAction'><i class='fa fa-edit'></i> Open</a><span>  </span><a class='btn btn-green' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a><span>  </span><a class='btn btn-red' href='#' id='bChangeOwner'><i class='fa fa-group'></i> Change Owner</a>"
                        }
                    ],
                    initComplete: initActions,
                    orderCellsTop: true,
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {
                            "className": 'details-control',
                            "orderable": false,
                            "data": null,
                            "defaultContent": ''
                        },
                        {data: "actionId",
                            render: function (data, type, row) {
                                return ("AC" + data);
                            }},
                        {data: "code"},
                          {data: "action_source"},
                        {data: "reportId",
                            render: function (data, type, row) {
                        if (data.startsWith('HZ')) {
                            return "<a href='reportHazard.php?action=VIEW&hid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        } else   if (data.startsWith('IR')){
                              return "<a href='reportIncident.php?action=VIEW&iid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        } else   if (data.startsWith('RA')){
                              return "<a href='genericRiskAssessment.php?action=VIEW&graid=" + data.substring(2, data.length) + "'>" + data + "</a>";
                        }else {
                            return data;
                        }
                            }},
                        {data: "reportStatus"},
                        {data: "deadline"},
                        {data: "assigned_by"},
                        {data: "estimated_completion_date"},
                        {data: "actionStatus"},
                        {data: ""}

                    ],
                    order: []
                });

                $('#actionsDataTable tbody').on('click', 'td.details-control', function () {
                    var tr = $(this).closest('tr');
                    var row = actionsTable.row(tr);
                    var data = actionsTable.row(tr).data();
                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        var rowID = data.actionId;
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                        descTable(rowID);
                    }
                });

                // table data for the order
                function descTable(rowID) {
                    var bodyTable = $('#actionDetails').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/actionsData.php?filter=ACTIONDETAILS&rowID=" + rowID, "dataSrc": ""},
                        searching: false,
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)'

                        },
                        paging: false,
                        info: false,
                        columns: [
                            {data: "action_type"},
                            {data: "action"}
                        ],
                        order: [[0, 'asc']]
                    });

                }




                $('#actionsDataTable tbody').on('click', '#bOpenAction', function () {
                    var actionId = actionsTable.row($(this).parents('tr')).data().actionId;
                    var reportId = actionsTable.row($(this).parents('tr')).data().reportId;
                    if (reportId === 'N/A') {
                        window.open("action.php?filter=OPEN&id=" + actionId + '&source=Other', '_self');
                    } else if(reportId.startsWith('IR')){
                        window.open("action.php?filter=OPEN&id=" + actionId+ '&source=Incident', '_self');
                    }else if(reportId.startsWith('HZ')){
                        window.open("action.php?filter=OPEN&id=" + actionId+ '&source=Hazard', '_self');
                    }else if(reportId.startsWith('RA')){
                        window.open("action.php?filter=OPEN&id=" + actionId+ '&source=GRA', '_self');
                    }

                });

                //VIEW Progress CLICK
                $('#actionsDataTable tbody').on('click', '#bViewProgress', function () {
                    var tr = $(this).closest('tr');
                    var data = actionsTable.row(tr).data();
                    var rowID = data.actionId;
                    var div = document.getElementById('tableDiv');
                    $('#tableDiv').html('');
                    $('#tableDiv').append(formatHistory());
                    historyTable(rowID);
                    $('#progressTitle').html("Audit Trail AC" + rowID);
                    $('#mProgressModel').modal('show');
                });


                //

                $('#actionsDataTable tbody').on('click', '#bChangeOwner', function () {
                    var tr = $(this).closest('tr');
                    var data = actionsTable.row(tr).data();
                    var rowID = data.actionId;
                    var username = '<?php echo $_SESSION['vsmsUserData']['first_name'] . ' ' . $_SESSION['vsmsUserData']['last_name'] ?>';
                    var manager = '<?php echo $_SESSION['vsmsUserData']['manager_name'] ?>';
                    $('#currOwner').val(username);
                    $('#currSite').val(data.actionsitecode);
                    $('#currManager').val(manager);
                    $('#actionId').val(rowID);
                    if (data.reportId.startsWith("IR")) {
                        $('#reportId').val(data.reportId.substring(2, data.reportId.length));
                         $('#reportSource').val('Incident');
                    }  else if (data.reportId.startsWith("HZ")) {
                         $('#reportId').val(data.reportId.substring(2, data.reportId.length));
                          $('#reportSource').val('Hazard');
                    } else {
                        $('#reportId').val('0');
                         $('#reportSource').val('Other');
                    }
                    $('#newOwner').val('-1');
                    $('#newSite').val('-1');
                    $('#newManager').val('');
                    $('#mComments').val('');
                    $('#mOwnerModal').modal('show');
                    $('#ownerModalTitle').html("Change the Owner for AC" + rowID);
                });
                function historyTable(rowID) {
                    var bodyTable = $('#actionHistory').DataTable({
                        retrieve: true,
                        ajax: {"url": "../masterData/actionsData.php?filter=ACTIONHISTORY", "data": {actionId: rowID}, "dataSrc": ""},
                        columnDefs: [{
                                targets: -1,
                                data: null
                            }
                        ],
                        buttons: [
                            {extend: 'excel', filename: 'actionHistory', title: 'Action History'}
                        ],
                        columns: [
                            {data: "comments"},
                            {data: "status",
                                render: function (data, type, row) {
                                    return (data + " by");
                                }
                            },
                            {data: "updated_by"},
                            {data: "updated_at"}
                        ],
                        order: []
                    });

                }

            });
            function populateOwnersForCounterMeasure(elem) {

                var ownersMasterList = <?php echo json_encode($actionOwners) ?>;
                var containmentSite = elem.val();
                var owners = '';
                $('#newOwner').attr('disabled', false);
                var ownerList = $('#newOwner');
                ownerList[0].options.length = 0;
                var option = document.createElement('option');
                option.setAttribute("value", "-1");
                ownerList[0].add(option);
                for (var i = 0; i < ownersMasterList.length; i++) {
                    var site = ownersMasterList[i];
                    if (site.id === containmentSite) {
                        owners = site.owners;
                        break;
                    }
                }
                for (var i = 0; i < owners.length; i++) {
                    var newOption = document.createElement('option');
                    newOption.setAttribute("value", owners[i].id);
                    newOption.setAttribute("data-divid", owners[i].manager_name);
                    newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                    ownerList[0].add(newOption);
                }
            }
            function populateCounterMeasureManager(elem) {
                document.getElementById('newManager').value = $('#newOwner').find('option:selected').attr('data-divid') === undefined ? '' : $('#newOwner').find('option:selected').attr('data-divid');
            }
            function sendActionEmails(actionId, reportId, filter, prevOwnerId, actionSource) {
                var func = '';
                var type = '';
                var reportIdString ='';
                if(actionSource === 'Incident'){
                    reportIdString = "&incidentId=" + reportId;
                }else if(actionSource ==='Hazard'){
                    reportIdString = "&hazardId=" + reportId;
                }                
                var url = "sendEmails.php?actionId=" + actionId + "&function=" + func + "&type=" + type+"&source="+actionSource +  reportIdString;
                if (filter === 'REASSIGNED') {
                    func = 'ActionReassigned';
                    type = 'Both';
                    url = "sendEmails.php?actionId=" + actionId + "&function=" + func + "&type=" + type + '&prevOwnerId=' + prevOwnerId+"&source="+actionSource +  reportIdString ;
                }
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            function changeOwner() {
                var prevOwnerId = <?php echo $_SESSION['vsmsUserData']['id'] ?>;
                var nextOwner = document.getElementById("newOwner").value;
                var comments = document.getElementById('mComments').value;
                if (nextOwner === '-1' || nextOwner === '' || comments === '') {
                    alert("Please add all fields marked mandatory.");
                    return;
                }
                var actionId = document.getElementById("actionId").value;
                var reportId = parseInt(document.getElementById("reportId").value);
                var actionSource = document.getElementById("reportSource").value;
                var actionSite = document.getElementById('newSite').value;
                var userId = '<?php echo $_SESSION['vsmsUserData']['id'] ?>';
                var actionModel = {"comments": comments, "filter": "REASSIGNED", "id": actionId, "createdByUserId": userId, "actionOwner": nextOwner,
                    "reportId": reportId, "actionSite":actionSite, "actionSource":actionSource};
                console.log(actionModel);
                var form_data = new FormData();
                form_data.append('filter', JSON.stringify(actionModel));
                form_data.append('function', 'saveAction');
                form_data.append('connection', vsmsservice);


                $.ajax({
                    url: "../action/callPostService.php",
                    type: 'POST',
                    processData: false,
                    cache: false,
                    contentType: false,
                    data: form_data,
                    success: function (response, textstatus) {
                        console.log(response);
                        if (response.startsWith("OK")) {
                            alert("Action Owner successfully changed.");
                            sendActionEmails(actionId, reportId, 'REASSIGNED', prevOwnerId, actionSource);
                            $('#mOwnerModal').modal('hide');
                            window.location.reload(true);
                        } else {
                            alert("Something went wrong.Please submit again");
                        }
                    }});

            }



        </script>
    </body>

</html>