<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    $prevToDate = '';
    if (isset($_GET['prevToDate'])) {
        $prevToDate = $_GET['prevToDate'];
    }
    $prevFromDate = '';
    if (isset($_GET['prevFromDate'])) {
        $prevFromDate = $_GET['prevFromDate'];
    }
    ?>

    <head>
        <title>VSMS - Hazard Act or Condition Summary</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }
            .table-cell-total{
                background-color: lightgrey;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Hazards: Act Or Condition</h1>  
                                  <h4 class="text-center" style="font-weight:bolder">Current Period From: <?php echo date("d-m-Y", strtotime($fromDate)). ' To: ' . date("d-m-Y", strtotime($toDate))?></h4>
                <?php if($prevFromDate!=='None'){ ?>
                <h4  class="text-center" style="font-weight: bolder">Previous Period From: <?php echo date("d-m-Y", strtotime($prevFromDate)). ' To: ' . date("d-m-Y", strtotime($prevToDate))?></h4>
                <?php }?>

            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="hazardsTables" class="compact stripe hover cell-border" style="width:100%">
                    <thead>
                        <tr>
                            <th>	Site	</th>
                            <th>	Period	</th>
                            <th>Act</th>
                            <th>Condition</th>
                            <th>Act or Condition</th>
                            <th>	Total	</th>

                        </tr>
                        <tr>
                          
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';
                var prevFromDate = '<?php echo $prevFromDate; ?>';
                var prevToDate = '<?php echo $prevToDate; ?>';

                var initHazards = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 6 || index === 7) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#hazardsTables thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var hazardsTables = $('#hazardsTables').DataTable({
                    ajax: {"url": "../masterData/hazardActConditionSummaryData.php?siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate + "&prevToDate=" + prevToDate + "&prevFromDate=" + prevFromDate, "dataSrc": ""},
                    orderCellsTop: true,
                    ordering: false,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    createdRow: function (row, data, dataIndex) {
                        if (data['hazardSite'] === 'Total') {
                            $(row).css('background-color', 'lightgrey');
                        }

                    },
                    drawCallback: function (settings) {
                        var api = this.api();
                        var rows = api.rows({page: 'current'}).nodes();
                        var last = null;

                        api.column(0, {page: 'current'}).data().each(function (group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                        '<tr class="group" ><td colspan="45" style="background-color:#f7dfc6">' + group + '</td></tr>'
                                        );

                                last = group;
                            }
                        });
                    },
                    
                    columns: [
                        {data: "hazardSite"},
                        {data: "type"},
                        {data: "act"},
                        {data: "conditions"},
                        {data: "actCondition"},
                        {data: "total", "className": "table-cell-total"}


                    ],
                    order: []
                });

            });



        </script>
    </body>

</html>
