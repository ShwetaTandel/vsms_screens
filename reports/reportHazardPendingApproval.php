<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
$baseHref = "http://$_SERVER[HTTP_HOST]" . "/VSMS/screens/";
?>

    <head>
        <title>VSMS - Hazard Reports Pending Approvals</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
               <h1 class="text-center">Hazards Reports Pending Approval</h1>      
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="hazardsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            
                            <th>Hazard ID</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Hazards Date</th>
                           <th>Status</th>
                            <th>Current Approver</th>
                            <th>Overdue(> 72 hrs)</th>
                            <th>Last Saved</th>
                            <th>Last Saved By</th>
                            
                        </tr>
                        <tr>
                            
                          
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>
       
        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var initHazards = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 ||index === 4 ||index === 5 ) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#hazardsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var hazardsTable = $('#hazardsTable').DataTable({
                    ajax: {"url": "../masterData/reports.php?filter=HAZARDSPENDINGAPPROVAL", "dataSrc": ""},
                    initComplete: initHazards,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "hazard_number",
                            render: function (data, type, row) {
                                return ("HZ" + data);
                            }},
                        {data: "code"},
                        {data: "location"},
                        {data: "hazard_date"},

                        {data: "statusDesc"},
                        {data: "approver_name"},
                        {data: "isOverdue",
                            render: function (data, type, row) {
                                var status = row['status'];
                                if((status === 'L1_SUBMITTED' || status === 'L2_SAVED')&&  (new Date(row['created_at']) < new Date())){
                                     return 'Yes';
                                 }else {
                                     return 'No';
                                 }

                                
                            }},
                        {data: "updated_at"},
                        {data: "updated_by"}

                    ],
                    order: []
                });

});



        </script>
    </body>

</html>
