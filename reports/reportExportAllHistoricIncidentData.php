<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $siteNames = '';
    if (isset($_GET['siteNames'])) {
        $siteNames = $_GET['siteNames'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    ?>

    <head>
        <title>VSMS - Export Historic Incident Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }


        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Export Historic Incident Data</h1>    
                <h4 class="text-center" style="font-weight:bolder">Period From: <?php echo date("d-m-Y", strtotime($fromDate)) . ' To: ' . date("d-m-Y", strtotime($toDate)) ?></h4>
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th> Incident ID</th>
                            <th>Status</th>
                            <th>Site</th>
                            <th>H&S Investigator</th>
                            <th>Location</th>
                            <th>Exact location</th>
                            <th>Incident Date</th>
                            <th>Day of Week</th>
                            <th>Incident Time</th>
                            <th>Incident Type</th>
                            <th>Action Type</th>
                            <th>Other Action Type</th>
                            <th>Description</th>
                            <th>Is person Involved?</th>
                            <th>Person Name</th>
                            <th>Age (Years)</th>
                            <th>Age (Months)</th>
                            <th>Age (Days)</th>
                            <th>Employment Status</th>
                            <th>Length of Service (days)</th>
                            <th>Time on Job (days)</th>
                            <th>Previous Incidents</th>
                            <th>Date or Reference of previous incidents</th>
                            <th>Vehicle Involved?</th>
                            <th>Vehicle Type</th>
                            <th>Truck Reg/No</th>
                            <th>Employee removed from truck?</th>
                            <th>Type of Stillage</th>
                            <th>Location of Injury</th>
                            <th>Treatment Administered</th>
                            <th>PPE Worn</th>
                            <th>Was (PPE worn) of required level?</th>
                            <th>Estimated Costs</th>
                            <th>Was the activity standard process?</th>
                            <th>Licences in Date</th>
                            <th>D&A Test required?</th>
                            <th>D&A Pass?</th>
                            <th>SSoW Suitable & Sufficient</th>
                            <th>Comments on SSoW</th>
                            <th>RA Suitable & Sufficient</th>
                            <th>Comments on RA</th>
                            <th>Refresher MHE training required?</th>
                            <th>Category</th>
                            <th>Category Level</th>
                            <th>Type</th>
                            <th>Act or Condition</th>
                            <th>Dangerous Occurrence?</th>
                            <th>Days Lost</th>
                            <th>RIDDOR</th>
                            <th>RIDDOR Reference</th>
                            <th>HSE Report Date</th>

                        </tr>
                        <tr>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>


                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var siteNames = '<?php echo $siteNames; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';

                var initIncidents = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 2) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var incidentTable = $('#incidentsTable').DataTable({
                    ajax: {"url": "../masterData/reports.php?filter=EXPORTHISTORICDATA&siteNames=" + siteNames + "&toDate=" + toDate + "&fromDate=" + fromDate, "dataSrc": ""},
                    initComplete: initIncidents,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columnDefs: [{
                            targets: 12,
                            data: null,
                            defaultContent: "<a class='btn btn-blue' href='#' id ='bShowDesc'><i class='fa fa-edit'></i> Show</a>"
                        }
                    ],
                    columns: [
                        {data: "incident_number",
                            render: function (data, type, row) {
                                return ("IR" + data);
                            }},
                        {data: "status"},
                        {data: "site"},
                        {data: "hs_invenstigator"},
                        {data: "location"},
                        {data: "location_other"},
                        {data: "incident_date"},
                        {data: "day_of_week"},
                        {data: "incident_time"},
                        {data: "incident_type"},
                        {data: "action_type"},
                        {data: "action_type_other"},
                        {data: ""},
                        {data: "is_person_involved"},
                        {data: "person_name"},
                        {data: "age_years"},
                        {data: "age_months"},
                        {data: "age_days"},
                        {data: "employment_status"},
                        {data: "los"},
                        {data: "toj"},
                        {data: "any_previous_incidents"},
                        {data: "prev_incident_date_reference"},
                        {data: "is_vehicle_involved"},
                        {data: "vehicle_type"},
                        {data: "vehicle_reg_no"},
                        {data: "was_person_removed_from_truck"},
                        {data: "stillage_type"},
                        {data: "location_of_injury"},
                        {data: "treatment_administered"},
                        {data: "ppe_worn"},
                        {data: "was_ppe_req_level"},
                        {data: "estimated_cost"},
                        {data: "is_activity_standard_process"},
                        {data: "licenseValid"},
                        {data: "da_test_required"},
                        {data: "da_passed"},
                        {data: "is_ssow_suitable"},
                        {data: "ssow_comments"},
                        {data: "is_ra_suitable"},
                        {data: "ra_comments"},
                        {data: "is_mhe_training_required"},
                        {data: "category"},
                        {data: "category_level"},
                        {data: "type"},
                        {data: "act_or_condition"},
                        {data: "is_dangerous_occurence"},
                        {data: "days_lost"},
                        {data: "isRIDDOR"},
                        {data: "RIDDORRef"},
                        {data: "hse_report_date"}
                    ],
                    order: []
                });

                $('#incidentsTable tbody').on('click', '#bShowDesc', function () {
                    var tr = $(this).closest('tr');
                    var data = incidentTable.row(tr).data();

                    alert(data.description);
                });


            });




        </script>
    </body>

</html>
