<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
        $siteIds= '';
    if(isset($_GET['siteIds'])){
        $siteIds=$_GET['siteIds'];
    }
    $toDate= '';
    if(isset($_GET['toDate'])){
        $toDate=$_GET['toDate'];
    }
    $fromDate= '';
    if(isset($_GET['fromDate'])){
        $fromDate=$_GET['fromDate'];
    }


?>

    <head>
        <title>VSMS - Export All Hazards Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
               <h1 class="text-center">All Hazard Data</h1>      
               
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="hazardsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>
                            
                            <th>Hazard ID</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Exact Location</th>
                            <th>Hazard Date</th>
                            <th>Day Of Week</th>
                            <th>Have you done anything to make it safe?</th>
                            <th>Were there any witnesses?</th>
                             <th>Submitted By By</th>
                            <th>Act Or Condition</th>
                            <th>Risk Category</th>
                            <th>Status</th>
                            <th>Current Approver</th>
                            <th>Date Closed</th>
                            <th>Closed By</th>
                            <th>Total Actions</th>
                            <th>Outstanding Actions</th>
                         
                        </tr>
                        <tr>
                            
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            <td></td>
                           
                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>
       
        <script>
            $(document).ready(function () {
                 var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';
                var filter = '<?php echo $filter; ?>';
                var initHazards = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 ) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#hazardsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var hazardsTable = $('#hazardsTable').DataTable({
                    ajax: {"url": "../masterData/reports.php?siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate+"&filter=ALLHAZARDS", "dataSrc": ""},
                    initComplete: initHazards,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "hazard_number",
                            render: function (data, type, row) {
                                return ("HZ" + data);
                            }},
                        {data: "code"},
                        {data: "location"},
                         {data: "location_other"},
                        {data: "hazard_date"},
                        {data: "dayOfWeek"},
                       
                        {data: "have_done_anything_to_make_it_safe",
                    render: function (data, type, row) {

                        if (data === 'Yes') {
                            return data + ":"+row.what_is_done;
                        } else if (data === 'No' ) {
                             return data +":" +row.why_not_done;
                        }
                    }},
                        
                        {data: "any_witness",
                    render: function (data, type, row) {

                        if (data === 'Yes' ) {
                            return data + ":"+row.witness_name;
                        } else {
                            return data;
                        }
                    }},
                       {data: "created_by"},
                       {data: "act_or_condition"},
                        {data: "risk_category"},
                        {data: "statusDesc"},
                        {data: "approver_name",
                    render: function (data, type, row) {

                        if (row.status === 'L2_APPROVED' || row.status === '_CLOSED' ) {
                            return data;
                        } else {
                            return '';
                        }
                    }},
                         {data: "updated_at", render: function (data, type, row) {

                        if (row.status === 'L2_APPROVED' || row.status === '_CLOSED' ) {
                            return data;
                        } else {
                            return '';
                        }
                    }},
                       {data: "updated_by",
                    render: function (data, type, row) {

                        if (row.status === 'L2_APPROVED' || row.status === '_CLOSED' ) {
                            return data;
                        } else {
                            return '';
                        }
                    }},
                         {data: "openactions",
                    render: function (data, type, row) {

                        if (data==='') {
                            return '0';
                        } else {
                            return data;
                        }
                    }},
                         {data: "totalactions",render: function (data, type, row) {

                        if (data==='') {
                            return '0';
                        } else {
                            return data;
                        }
                    }}
                        
                       
                    ],
                    order: []
                });

});



        </script>
    </body>

</html>
