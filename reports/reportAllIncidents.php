<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    ?>

    <head>
        <title>VSMS - All Incidents Reports</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">All Incidents Reports</h1>      
                <h4 class="text-center" style="font-weight:bolder">Period From: <?php echo date("d-m-Y", strtotime($fromDate)). ' To: ' . date("d-m-Y", strtotime($toDate))?></h4>
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th>Incident ID</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Incident Date</th>
                            <th>Incident Type</th>
                            <th>Status</th>
                            <th>Current Level</th>
                            <th>Current Approver</th>
                            <th>Overdue</th>
                            <th>Total Actions</th>
                            <th>Outstanding Actions</th>
                            <th>Last Saved</th>
                            <th>Last Saved By</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>
            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';

                var initIncidents = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 2 || index === 4 || index === 5 || index === 6  || index === 7  || index === 12) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var incidentTable = $('#incidentsTable').DataTable({
                    ajax: {"url": "../masterData/reports.php?filter=ALLINCIDENTENTSREPORT&siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate, "dataSrc": ""},
                    initComplete: initIncidents,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "incident_number",
                            render: function (data, type, row) {
                                return ("IR" + data);
                            }},
                        {data: "code"},
                        {data: "location"},
                        {data: "incident_date"},
                        {data: "incident_type"},
                        {data: "statusDesc"},
                        {data: "display_status",
                            render: function (data, type, row) {
                                if(row.status==='L4_APPROVED' || row.status ==='_CLOSED'){
                                    return '-';
                                }else{
                                    return data;
                                }
                            }},
                        {data: "approver_name",
                    render: function (data, type, row) {

                        if (row.status === 'L2_SUBMITTED' || row.status === 'L2_EDITED' || row.status === 'L3_SUBMITTED' || row.status === 'L3_EDITED' || row.status === 'L3_APPROVED' || row.status === 'L4_SAVED') {
                            return data;
                        } else {
                            return '';
                        }
                    }},
                        {data: "isOverdue",
                            render: function (data, type, row) {
                              var status = row['status'];
                                if((status === 'L1_CREATED' || status === 'L1_SAVED' || status === 'L2_SUBMITTED' || status === 'L2_EDITED' || status === 'L2_REJECTED')&&  (new Date(row['submit_by']) < new Date())){
                                     return 'Yes';
                                 }else if((  status=== 'L2_APPROVED'|| status === 'L3_SAVED' ||  status=== 'L3_REJECTED') && (new Date(row['fi_submit_by']) < new Date())){
                                     return 'Yes';
                                 }else if(status === 'L3_SUBMITTED' || status=== 'L3_EDITED' || status=== 'L3_APPROVED' ||status === 'L4_SAVED' || status=== 'L4_APPROVED' || status === '_CLOSED'){
                                      return '-';
                                 }else {
                                     return 'No';
                                 }


                            }},
                        {data: "totalCnt"},
                        {data: "outstandingactions"},
                        {data: "updated_at"},
                        {data: "updated_by"}

                    ],
                    order: []
                });

            });



        </script>
    </body>

</html>
