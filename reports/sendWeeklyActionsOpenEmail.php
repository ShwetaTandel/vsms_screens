<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$report = 'true';
if (isset($_GET['report'])) {
    $report = $_GET['report'];
}
  generateReport();
  $mailTo = getEmailForWeeklyOpenActions();
  $email = new EmailHelper();
  $sent = $email->sendOpenActionsWeeklyEmail($mailTo);


function getEmailForWeeklyOpenActions() {
    global $mDbName, $connection, $serverName;
    //if ($serverName === "vantecapps") {
    $sql = "SELECT emails as emails FROM " . $mDbName . ".email_lists where name = 'OPEN_ACTIONS_WEEKLY' and active=true";
    //} 

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = '';
    while ($row = mysqli_fetch_assoc($result)) {
        $record = $row['emails'];
    }
    return $record;
}

function generateReport() {
    global $mDbName, $connection, $serverName;
    $sql = "select actionId as ActionId, actionDesc as Description, action_type as ActionType, actionsiteCode as Site, actionStatus as ActionStatus, estimated_completion_date as Deadline, "
            ." actionOwnerName as actionOwner, action_source as ActionSource,  'No' as Overdue from outstandingactions "
            ." where actionStatus in ('Assigned', 'Re-assigned', 'Re-opened', 'Re-scheduled') and estimated_completion_date between now() and  (NOW() + INTERVAL 7 DAY)".
            "union ".
            "select actionId as ActionId, actionDesc as Description, action_type as ActionType, actionsiteCode as Site, actionStatus as ActionStatus, estimated_completion_date as Deadline,"
            ." actionOwnerName as actionOwner, action_source as ActionSource,  'Yes' as Overdue from outstandingactions "
            ."where actionStatus in ('Assigned', 'Re-assigned', 'Re-opened', 'Re-scheduled') and estimated_completion_date < (now() - interval 1 day) order by Deadline asc";

    $result = mysqli_query($connection, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $filename = 'VSMS Open Actions Report.xls';
  //  header("Content-Type: application/xls");
    //header("Content-Disposition: attachment; filename=".$filename);
    //header("Pragma: no-cache");
    //header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    $column = "";
    foreach ($names as $name) {
       $column.= $name->name . $sep;
    }
    $column.= "\n";
	    $fileData = "";
   while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        //print(trim($schema_insert));
        //print "\n";
        $fileData.=$schema_insert."\n";
    }
    file_put_contents($filename, $column.$fileData);
}

?>
