<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    ?>

    <head>
        <title>VSMS - All Incidents Reports (Time Taken)</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">All Incidents Reports (Time Taken)</h1>      
                <h4 class="text-center" style="font-weight:bolder">Period From: <?php echo date("d-m-Y", strtotime($fromDate)). ' To: ' . date("d-m-Y", strtotime($toDate))?></h4>
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th>Incident ID</th>
                            
                            <th>Incident Date</th>
                            <th>Incident Reported</th>
                            <th>By</th>
                            <th>Initial Report Submitted</th>
                                  <th>By</th>
                            <th>Initial Report Approved</th>
                                  <th>By</th>
                            <th>Full Investigation Submitted</th>
                                  <th>By</th>
                            <th>Full Investigation Approved </th>
                                  <th>By</th>
                            <th>Incident Report Signed Off </th>
                                  <th>By</th>
                            <th>Options </th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                             <td></td>
                            
                            
                        </tr>
                    </thead>
                </table>
            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>
        <div id="mProgressModel" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="progressTitle">Audit Trail IR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="tableDiv">
            </div>
        </div>

    </div>
</div>

        <script>
            $(document).ready(function () {
                  function formatHistory() {

            return '<table id="incidentHistory" class="compact stripe hover row-border" style="width:100%">' +
                    '<thead>' +
                    '<th>Comments</th>' +
                    '<th>Status</th>' +
                    '<th>Updated By</th>' +
                    '<th>Updated On</th>' +
                    '</thead>' +
                    '</table>';
        }
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';

                var initIncidents = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 3 || index === 5 || index === 7 || index === 9 || index === 11 || index === 13 ) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var incidentsTable = $('#incidentsTable').DataTable({
                    ajax: {"url": "../masterData/incidentsTimeTakenData.php?siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate, "dataSrc": ""},
                     initComplete: initIncidents,
                 columnDefs: [{
                    targets: -1,
                    data: null,
                    defaultContent: "<a class='btn btn-orange' href='#' id='bViewProgress'><i class='fa fa-tasks'></i> View Progress</a>"
                }
            ],
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "incident_number",
                            render: function (data, type, row) {
                                return ("IR" + data);
                            }},
                        {data: "incident_date"},
                        {data: "incidentReportedTime"},
                        {data: "incidentReportedBy"},
                        {data: "irReportedTime"},
                        {data: "irReportedBy"},
                        {data: "irApprovedTime"},
                        {data: "irApprovedBy"},
                        {data: "fiSubmittedTime"},
                        {data: "fiSubmittedBy"},
                        {data: "fiApprovedTime"},
                        {data: "fiApprovedBy"},
                        {data: "incidentClosedTime"},
                        {data: "incidentClosedBy"},
                         {data: ""}

                    ],
                    order: []
                });
           function historyTable(rowID) {
            var bodyTable = $('#incidentHistory').DataTable({
                retrieve: true,
                ajax: {"url": "../masterData/incidentHistoryData.php", "data": {incidentId: rowID}, "dataSrc": ""},
                  dom: 'Bfrltip',
                 buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                columns: [
                    {data: "comments", width: "20%"},
                    {data: "status",
                        render: function (data, type, row) {
                            return (data + " by");
                        }
                    },
                    {data: "updated_by"},
                    {data: "updated_at"}
                ],
                order: []
            });

        }
        $('#incidentsTable tbody').on('click', '#bViewProgress', function () {
            var tr = $(this).closest('tr');
            var data = incidentsTable.row(tr).data();
            var rowID = data.incident_number;
            var div = document.getElementById('tableDiv');
            $('#tableDiv').html('');
            $('#tableDiv').append(formatHistory());
            historyTable(rowID);
            $('#progressTitle').html("Audit Trail IR" + rowID);
            $('#mProgressModel').modal('show');
          //  populateLevels(data.display_status);



        });
  

            });



        </script>
    </body>

</html>
