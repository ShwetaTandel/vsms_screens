<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    $filter = '';
    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
    }
    ?>

    <head>
        
        <title>VSMS - <?php echo $filter?> Actions</title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center"><?php echo $filter?> Actions</h1>   
                      <h4 class="text-center" style="font-weight:bolder"><?php echo $filter?> From: <?php echo date("d-m-Y", strtotime($fromDate)). ' To: ' . date("d-m-Y", strtotime($toDate))?></h4>

            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="actionsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th>Action ID</th>
                            <th>Action Category</th>
                            <th>Action Description</th>
                            <th>Incident Site</th>
                            <th>Report ID</th>
                            <th>Report Status</th>
                            <th>Deadline</th>
                            <th>Owner</th>
                            <th>Estimated Comp. Date</th>
                            <th>Status</th>
                            <th>Date Closed</th>
                            <th>Closed Late?</th>
                            <th>Date Cancelled</th>
                            <th>Date Re-opened</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';
                var filter = '<?php echo $filter; ?>';

                var initActions = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 3 || index === 5 || index === 7 || index === 9) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#actionsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };
                var flag = (filter === 'Re-scheduled') ? true: false;
                var actionTables = $('#actionsTable').DataTable({
                    ajax: {"url": "../masterData/closedOrCancelledActions.php?siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate+"&filter="+filter, "dataSrc": ""},
                    initComplete: initActions,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "actionId",
                            render: function (data, type, row) {
                                return ("AC" + data);
                            }},
                        {data: "action_type"},
                        {data: "actionDesc"},
                        {data: "code"},
                        {data: "reportId"},
                        {data: "reportStatus"},
                        {data: "deadline"},
                        {data: "actionOwnerName"},
                        {data: "estimated_completion_date"},
                        {data: "actionStatus"},
                        {data: "updated_at",
                            render: function (data, type, row) {
                                if(row['actionStatus'] === 'Closed'){
                                    return data;
                                }else{
                                     return '';
                                }
                            }},
                        {data: "isLate",
                            render: function (data, type, row) {
                                if(row['actionStatus'] === 'Closed'){
                                    return data;
                                }else{
                                     return '';
                                }
                            }},
                        {data: "updated_at",
                            render: function (data, type, row) {
                                if(row['actionStatus'] === 'Cancelled'){
                                    return data;
                                }else{
                                     return '';
                                }
                            }},
                        {data: "updated_at",
                            render: function (data, type, row) {
                                if(row['actionStatus'] === 'Re-opened'){
                                    return data;
                                }else{
                                    return '';
                                }
                            }}
                    ],
                    order: []
                });

            });



        </script>
    </body>

</html>
