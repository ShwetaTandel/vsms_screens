<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteIds = '';
    if (isset($_GET['siteIds'])) {
        $siteIds = $_GET['siteIds'];
    }
    $toDate = '';
    if (isset($_GET['toDate'])) {
        $toDate = $_GET['toDate'];
    }
    $fromDate = '';
    if (isset($_GET['fromDate'])) {
        $fromDate = $_GET['fromDate'];
    }
    ?>

    <head>
        <title>VSMS - Export All Incident Data</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Export All Incident Data</h1>    
                <h4 class="text-center" style="font-weight:bolder">Period From: <?php echo date("d-m-Y", strtotime($fromDate)). ' To: ' . date("d-m-Y", strtotime($toDate))?></h4>
            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="incidentsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th> Incident ID</th>
                            <th>Status</th>
                            <th>Site</th>
                            <th>Location</th>
                            <th>Exact location</th>
                            <th>Incident Date</th>
                            <th>Day of Week</th>
                            <th>Incident Time</th>
                            <th>Incident Type</th>
                            <th>Incident Details</th>
                            <th>Action Type</th>
                            <th>Other Action Type</th>
                            <th>Area</th>
                            <th>Lighting</th>
                            <th>Temperature</th>
                            <th>Noise</th>
                            <th>Surface Type</th>
                            <th>Surface Condition</th>
                            <th>Is person Involved?</th>
                            <th>Person Name</th>
                            <th>Age (Years)</th>
                            <th>Age (Months)</th>
                            <th>Age (Days)</th>
                            <th>Employment Status</th>
                            <th>Length of Service (days)</th>
                            <th>Time on Job (days)</th>
                            <th>Previous Incidents</th>
                            <th>Date or Reference of previous incidents</th>
                            <th>Vehicle Involved?</th>
                            <th>Vehicle Type</th>
                            <th>Truck Reg/No</th>
                            <th>Employee removed from truck?</th>
                            <th>Type of Stillage</th>
                            <th>Location of Injury</th>
                            <th>Treatment Administered</th>
                            <th>PPE Worn</th>
                            <th>Was (PPE worn) of required level?</th>
                            <th>Estimated Costs</th>
                            <th>Was the activity standard process?</th>
                            <th>Licences in Date</th>
                            <th>D&A Test required?</th>
                            <th>D&A Pass?</th>
                            <th>SSoW Suitable & Sufficient</th>
                            <th>Comments on SSoW</th>
                            <th>RA Suitable & Sufficient</th>
                            <th>Comments on RA</th>
                            <th>Refresher MHE training required?</th>
                            <th>Concentration (Person)</th>
                            <th>Experience (Person)</th>
                            <th>Training (Person)</th>
                            <th>Condition (Machine)</th>
                            <th>Maintenance (Machine)</th>
                            <th>Facility (Machine)</th>
                            <th>SSoW (Method)</th>
                            <th>Ruling (Method)</th>
                            <th>Risk Assessment (Method)</th>
                            <th>Floor (Environment)</th>
                            <th>Lighting (Environment)</th>
                            <th>Condition (Environment)	</th>
                            <th>Category</th>
                            <th>Category Level</th>
                            <th>Type</th>
                            <th>Act or Condition</th>
                            <th>Dangerous Occurrence?</th>
                            <th>Days Lost</th>
                            <th>RIDDOR</th>
                            <th>Current Level</th>
                            <th>Current Approver</th>
                            <th>Overdue?</th>
                            <th>Outstanding Actions</th>
                            <th>Last Saved</th>
                            <th>Last Saved by</th>                            
                        </tr>
                        <tr>

                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteIds = '<?php echo $siteIds; ?>';
                var fromDate = '<?php echo $fromDate; ?>';
                var toDate = '<?php echo $toDate; ?>';

                var initIncidents = function (settings, json) {
                    var api = new $.fn.dataTable.Api(settings);
                    api.columns().every(function (index) {
                        var column = this;
                        if (index === 1 || index === 2) {
                            var id = 'col' + index;
                            var select = $('<select id ="' + id + '"><option id="ssearch" value=""></option></select>')
                                    .appendTo($('#incidentsTable thead tr:eq(1) td').eq(index))
                                    .on('change', function () {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                                $(this).val()
                                                );
                                        column
                                                .search(val ? '^' + val + '$' : '', true, false)
                                                .draw();
                                    });
                            column.data().unique().sort().each(function (d, j) {
                                if (d !== "") {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                }
                            });
                        }
                    });
                };

                var incidentTable = $('#incidentsTable').DataTable({
                    ajax: {"url": "../masterData/reports.php?filter=EXPORTINCIDENTENTSREPORT&siteIds=" + siteIds + "&toDate=" + toDate + "&fromDate=" + fromDate, "dataSrc": ""},
                    initComplete: initIncidents,
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                      {data: "incident_number",
                            render: function (data, type, row) {
                                return ("IR" + data);
                            }},
                        {data: "description"},
                        {data: "code"},
                        {data: "location"},
                        {data: "location_other"},
                        {data: "incidentDate"},
                        {data: "dayOfWeek"},
                        {data: "incidentTime"},
                        {data: "incident_type"},
                        {data: "incident_detail"},
                        {data: "action_type"},
                        {data: "action_type_other"},
                        {data: "area"},
                        {data: "lighting"},
                        {data: "temperature"},
                        {data: "noise"},
                        {data: "surface_type"},
                        {data: "surface_condition"},
                        {data: "is_person_involved",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "person_name"},
                        {data: "ageYears"},
                        {data: "ageMonths"},
                        {data: "ageDays"},
                        {data: "employment_status"},
                        {data: "los"},
                        {data: "toj"},
                        {data: "any_previous_incidents",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }
                        },
                        {data: "prev_incident_date_reference"},
                        {data: "is_vehicle_involved",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                     return 'No';
                                 }
                             }},
                        {data: "vehicle_type",
                            render: function (data, type, row) {
                                if(row.is_vehicle_involved !== '1'){
                                    return '';
                                }else{
                                    return data;
                                }
                                
                            }
                        },
                        {data: "vehicle_reg_no"},
                        {data: "was_person_removed_from_truck",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "stillage_type"},
                        {data: "location_of_injury"},
                        {data: "treatment_administered"},
                        {data: "ppe_worn"},
                        {data: "was_ppe_req_level",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "estimated_cost"},
                        {data: "is_activity_standard_process",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "licenseValid",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1' || row.is_vehicle_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "da_test_required",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "da_passed",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "is_ssow_suitable",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.display_status<'L3'){
                                     return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "ssow_comments"},
                        {data: "is_ra_suitable",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.display_status<'L3'){
                                     return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "ra_comments"},
                        {data: "is_mhe_training_required",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.is_person_involved !== '1'){
                                      return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "person_concentraion"},
                        {data: "person_experience"},
                        {data: "person_training"},
                        {data: "machine_condition"},
                        {data: "machine_maintenance"},
                        {data: "machine_facility"},
                        {data: "method_ssow"},
                        {data: "method_ruling"},
                        {data: "method_risk_assessment"},
                        {data: "environment_floor"},
                        {data: "environment_lighting"},
                        {data: "environment_condition"},
                        {data: "category"},
                        {data: "category_level"},
                        {data: "type"},
                        {data: "act_or_condition"},
                        {data: "is_dangerous_occurence",
                             render: function (data, type, row) {
                                 if(data === '1'){
                                     return 'Yes';
                                 }else if(row.display_status<'L4'){
                                     return '';
                                 }else{
                                      return 'No';
                                 }
                             }},
                        {data: "days_lost"},
                        {data: "isRIDDOR"},
                        {data: "display_status",
                            render: function (data, type, row) {
                                if(row.status==='L4_APPROVED' || row.status ==='_CLOSED'){
                                    return '-';
                                }else{
                                    return data;
                                }
                            }},
                        {data: "currApproverName"},
                        {data: "isOverdue",
                            render: function (data, type, row) {
                                var status = row['status'];
                               if((status === 'L1_CREATED' || status === 'L1_SAVED' || status === 'L2_SUBMITTED' || status === 'L2_EDITED' || status === 'L2_REJECTED')&&  (new Date(row['submit_by']) < new Date())){
                                     return 'Yes';
                                 }else if((  status=== 'L2_APPROVED'|| status === 'L3_SAVED' ||  status=== 'L3_REJECTED') && (new Date(row['fi_submit_by']) < new Date())){
                                     return 'Yes';
                                 }else if(status === 'L3_SUBMITTED' || status=== 'L3_EDITED' || status=== 'L3_APPROVED' ||status === 'L4_SAVED' || status=== 'L4_APPROVED' || status === '_CLOSED'){
                                      return '-';
                                 }else {
                                     return 'No';
                                 }

                                
                            }},
                        {data: "outstandingactions"},
                        {data: "updated_by"},
                        {data: "updated_at"}
                    ],
                    order: []
                });

            });



        </script>
    </body>

</html>
