<!-- This class renders all orders pending for approvals for Stage 1/2 Approvers-->
<html>
    <?php
    session_start();
    include '../config/phpConfig.php';
    include '../config/ChromePhp.php';
    if (!isset($_SESSION['vsmsUserData'])) {
        echo '<h1>Please login. Go back to <a href="../login.php">login</a> page.</h1>';
        die();
    }
    $siteId = '';
    if (isset($_GET['siteId'])) {
        $siteId = $_GET['siteId'];
    }
    $siteName = '';
    if (isset($_GET['siteName'])) {
        $siteName = $_GET['siteName'];
    }
  
    $filter = '';
    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
    }
    ?>

    <head>
        
        <title>VSMS - <?php echo $filter?></title>
        
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <link href="../css/datatables.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 

        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/popper.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/datatables.min.js"></script>
        <script src="../js/IEFixes.js"></script>
        <style>
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }

        </style>
    </head>
    <body>
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center"><?php echo $filter?> </h1>   
                      <h4 class="text-center" style="font-weight:bolder"><?php echo $filter?> Site: <?php echo $siteName?></h4>

            </div>
        </div>
        <br/>
        <div class="container-fluid">
            <div class="container-fluid">
                <br>
                <table id="actionsTable" class="compact stripe hover row-border" style="width:100%">
                    <thead>
                        <tr>

                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Level</th>
                            <th>Email Id</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </thead>
                </table>


            </div>
            <input href="#"  class="btn btn-orange" id="btnBack" type="button" value="BACK" onclick="window.open('../screens/reportAndForms.php', '_self')"></input>
        </div>

        <script>
            $(document).ready(function () {
                var userId = '<?php echo ($_SESSION['vsmsUserData']['id']) ?>';
                var siteId = '<?php echo $siteId; ?>';
                var filter = '<?php echo $filter; ?>';

                var flag = (filter === 'Approvers') ? true: false;
                var actionTables = $('#actionsTable').DataTable({
                    ajax: {"url": "../masterData/userReportsData.php?siteId=" + siteId + "&filter="+filter, "dataSrc": ""},
                    orderCellsTop: true,
                    pageLength: '50',
                    dom: 'Bfrltip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                    columns: [
                        {data: "first_name"},
                        {data: "last_name"},
                        {data: "level", visible: flag},
                        {data: "email_id"},
                        
                        
                        
                    ],
                    order: []
                });

            });



        </script>
    </body>

</html>
