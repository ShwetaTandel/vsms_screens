<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$report = 'true';
if (isset($_GET['report'])) {
    $report = $_GET['report'];
}
  generateReport();
  $mailTo = getEmailsForDAJob();
  $email = new EmailHelper();
  $sent = $email->sendDAJobEmail($mailTo);


function getEmailsForDAJob() {
    global $mDbName, $connection, $serverName;
    //if ($serverName === "vantecapps") {
    $sql = "SELECT emails as emails FROM " . $mDbName . ".email_lists where name = 'DA_REPORT_HR_WEEKLY' and active=true";
    //} 

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = '';
    while ($row = mysqli_fetch_assoc($result)) {
        $record = $row['emails'];
    }
    return $record;
}

function generateReport() {
    global $mDbName, $connection, $serverName;
    $toDate = date("Y-m-d");
    $fromDate = date('Y-m-d', strtotime("-7 days"));
    $sql = "SELECT incident.incident_number as IncidentID, code as Site, incident_date as IncidentDate, incident_type as IncidentType , incident_detail as IncidentDetail,  person_name as PersonName, date(person_dob) as DOB, employment_status as EmploymentStatus, if((is_vehicle_involved=true), 'Yes','No') as IsVehicleInvoled, date(employment_start_date) as EmploymentStartDate, date(time_on_job_start_date) as TimeOnJobStartDate, if((location_of_injury='None' OR location_of_injury=''), 'No','Yes') as IsInjured, location_of_injury as InjuryLocation, prev_incident_date_reference as PrevIncidentDateReference, if((da_test_required=true), 'Yes','No') as IsDATestRequired, if((da_test_agreed=true), 'Yes','No') as IsDATestAgreed, if((da_passed=true), 'Yes','No') as IsDATestPassed, da_tested_by as DATestedBy FROM incident_person_details, incident, site, incident_history where incident_history.incident_number = incident.incident_number and incident_person_details.incident_number = incident.incident_number and incident.site_id= site.id and incident_history.status = 'SUBMITTED' and level = 'L2'  and site_id in (select id from site) and incident.status != '_DELETED' and da_test_required is true and incident_date between '" . $fromDate . "' and '" . $toDate . " 23:59:59' group by IncidentID, PersonName;";

    $result = mysqli_query($connection, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $filename = 'VSMS DA Report.xls';
  //  header("Content-Type: application/xls");
    //header("Content-Disposition: attachment; filename=".$filename);
    //header("Pragma: no-cache");
    //header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    $column = "";
    foreach ($names as $name) {
       $column.= $name->name . $sep;
    }
    $column.= "\n";
	    $fileData = "";
   while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        //print(trim($schema_insert));
        //print "\n";
        $fileData.=$schema_insert."\n";
    }
    file_put_contents($filename, $column.$fileData);
}

?>
