<?php

require('../action/EmailHelper.php');
include ('../config/phpConfig.php');
include '../config/ChromePhp.php';

$report = 'true';
if (isset($_GET['report'])) {
    $report = $_GET['report'];
}
  generateReport();
  $mailTo = getEmailsForActionClosedReport();
  $email = new EmailHelper();
  $sent = $email->sendClosedActionsJobEmail($mailTo);


function getEmailsForActionClosedReport() {
    global $mDbName, $connection, $serverName;
    //if ($serverName === "vantecapps") {
    $sql = "SELECT emails as emails FROM " . $mDbName . ".email_lists where name = 'CLOSED_ACTIONS_DAILY' and active=true";
    //} 

    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));
    $record = '';
    while ($row = mysqli_fetch_assoc($result)) {
        $record = $row['emails'];
    }
    return $record;
}

function generateReport() {
    global $mDbName, $connection, $serverName;
    $sql = "SELECT action_number as Action_Number, actions.action_source as Action_Source, actions.report_id as Report_number, actions.comments as User_Comments , "
            ."actions.updated_by as Closed_by FROM vsms.action_history join vsms.actions on actions.id = action_history.action_number and  "
            ."action_history.status = 'CLOSED' and action_history.updated_at > now()-  interval 24 hour ;";

    $result = mysqli_query($connection, $sql) or die("Couldn't execute query:<br>" . mysqli_error() . "<br>" . mysqli_errno());
    $filename = 'VSMS Actions Closed Report.xls';
  //  header("Content-Type: application/xls");
    //header("Content-Disposition: attachment; filename=".$filename);
    //header("Pragma: no-cache");
    //header("Expires: 0");
    $sep = "\t";
    $names = mysqli_fetch_fields($result);
    $column = "";
    foreach ($names as $name) {
       $column.= $name->name . $sep;
    }
    $column.= "\n";
	    $fileData = "";
   while ($row = mysqli_fetch_row($result)) {
        $schema_insert = "";
        for ($j = 0; $j < mysqli_num_fields($result); $j++) {
            if (!isset($row[$j]))
                $schema_insert .= "NULL" . $sep;
            elseif ($row[$j] != "")
                $schema_insert .= "$row[$j]" . $sep;
            else
                $schema_insert .= "" . $sep;
        }
        $schema_insert = str_replace($sep . "$", "", $schema_insert);
        $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
        $schema_insert .= "\t";
        //print(trim($schema_insert));
        //print "\n";
        $fileData.=$schema_insert."\n";
    }
    file_put_contents($filename, $column.$fileData);
}

?>
