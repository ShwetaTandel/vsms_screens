<!DOCTYPE html>
<?php
session_start();
include 'config/ChromePhp.php';
?>
<head>
    <title>Vantec Safety Management System</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/libs/jquery/jquery.js" type="text/javascript"></script>
    <script src="js/libs/twitter-bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <link href="js/libs/twitter-bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
    <script src="config/screenConfig.js?random=<?php echo filemtime('config/screenConfig.js'); ?>" ></script>
    <style>
          .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
    background-color: #469bc0 !important;
    color: white !important;
}
    </style>
</head>
<body>
    
       <div class="row">
           <div class="col-6">
               <img STYLE="height: 150px; width: 600px" src="images/logo 3.2.png" alt="" class="pull-left"/>        
           </div>
           <div class="col-6" >
               <img STYLE="height: 150px; width: 400px;" src="images/vantec1.png" alt="" class="pull-right"/>        
           </div>
       </div>
         <hr style="height: 10px;
            border: 0;
            box-shadow: 0 10px 10px -10px #8c8b8b inset;"/>
       
        
       
   
  <div class="container">
      <h1 class="text-center">LOGIN</h1>
       <form role="form" onsubmit="return login()">
                    <div class="form-group">
                        <label for="username"><span class="glyphicon glyphicon-user"></span> Username</label>
                         <span style="font-size: smaller; font-weight:bold; color: red">[Use your PC credentials for logging in]</span>
                        <input type="text" class="form-control" id="username" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter password"/>
                    </div>
                    <input type="submit" class="btn btn-blue btn-block" id="EnterKey" value="Log In" onclick="login()"/>
                    <span style="color: red" >This website in not compatible with Internet Explorer and Microsoft Edge. Please use Google Chrome.</span>
                    <a href="#" onclick="sendResetEmail()">Send reset password link</a>
                </form>
            </div>
    <div></div>
    
    <script>
        //ENTER KEY handling
         $(document).keypress(function(e) {
             var keycode = (e.keyCode ? e.keyCode : e.which);
             if (keycode === '13') {
                    document.getElementById("EnterKey").click();
             }
            });
           
           
        function sendResetEmail(){
             var emailId = document.getElementById('username').value;
             if(emailId === ''){
                 alert("Please enter your Vantec Email address and then click the link.");
             }else{
                  $.ajax({
                    url: 'screens/resetPassword.php',
                    type: 'GET',
                    data: { function : 'sendResetLink',
                            email :emailId},
                   success: function (response, textstatus) {
                       
                       if(response.trim() === "SENT"){
                           alert("Please check your email for the password reset link.");
                        }else if(response.trim() ==="FAILED"){
                            alert('Email sending failed. Please try again.');
                        }else if(response.trim() === 'NOTFOUND'){
                            alert('The email address is not registered. Please contact VSMS admin team.');
                        }
                    }
                 });
             }
         }
         //Login - call user.php  
         function login() {
            var userID = document.getElementById('username').value;
            var pass = document.getElementById('password').value;
            var check = 'no';
              var url = 'action/ldapLogin.php';
           if (location.hostname === "localhost" || location.hostname === "127.0.0.1" ||  location.hostname === "172.18.25.33" ){
              url = 'action/userlogin.php';
            }
                 $.ajax({
                    url: url,
                    type: 'POST',
                    data: { userID : userID,
                            pass : pass,
                            check : check},
                   success: function (response, textstatus) {
                      if(response.substring(0, 4) === 'OKUS'){
                            window.open("screens/home.php", "_self");
                        }else if (response.includes('data 532')){
			    alert('Your LDAP password has expired. Please contact IT helpdesk to reset your account.');
			} else if (response.includes('Invalid credentials')){
                            alert('Wrong Username or Password');
                        }else if (response.includes('data 52e')){
                            alert('You need to reset the password first on a PC Login.');
                        }
                    }
                 });
            return false;
        }
    </script>
</body>

