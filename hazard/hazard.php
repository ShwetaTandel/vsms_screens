<?php
include '../config/ChromePhp.php';
include '../config/phpConfig.php';
include '../masterData/generalMasterData.php';
ChromePhp::log("sdcvdcvds".$_SERVER['REMOTE_ADDR']);
$token = authenticateHazardIps($_SERVER['REMOTE_ADDR']);

if(sizeof($token) ==0){
      echo '<h1>Access Denied'.$_SERVER['REMOTE_ADDR'];
      die();
}else{
print_r($token);
$siteId = $token[0]['site_id'];

$data = array();
$approvers = getSiteApproversForHazard();
$actionOwners =array();
    
}
$actionOwners =array();

?>
<html>
    <head>
        <title>VSMS - Report Hazard</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="../config/screenConfig.js?random=<?php echo filemtime('../config/screenConfig.js'); ?>" ></script>
        <meta http-equiv="expires" content="timestamp">
        <meta http-equiv="cache-control" content="no-cache" />
        <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/popper.min.js"></script>
        <link href="../js/libs/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap.min.js"></script>
        <script src="../js/moment.js"></script>
        <script src="../js/tempusdominus-bootstrap-4.min.js"></script>
        <link href="../css/tempusdominus-bootstrap-4.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap-toggle.min.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap-toggle.min.js"></script>
        <link href="../css/bootstrap-select.css" rel="stylesheet" type="text/css"/>
        <script src="../js/bootstrap.bundle.min.js"></script>
        <script src="../js/bootstrap-select.min.js"></script>
        <link href="../css/mainCss.css" rel="stylesheet" type="text/css"/>
        <style>
            .card-default {
                color: #333;
                background: linear-gradient(#fff,#566e69) repeat scroll 0 0 transparent;
                font-weight: 600;
                border-radius: 6px;
            }
            * {
                box-sizing: border-box;
            }
            .card-header .fa {
                transition: .3s transform ease-in-out;
            }
            .card-header .collapsed .fa {
                transform: rotate(-90deg);

            } 
            #overlay >img{

                position:absolute;
                top:38%;
                left: 38%;
                z-index:10;


            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            #modal {
                padding: 20px;
                border: 2px solid #000;
                background-color: #ffffff;
                position:absolute;
                top:20px; right:20px;
                bottom:20px; left:20px;
            }
            #container{
                position:relative;
                display:none;
            }
            #overlay{

                /* ensures it’s invisible until it’s called */
                display: none;
                position: fixed; /* makes the div go into a position that’s absolute to the browser viewing area */
                background-color:rgba(0,0,0,0.7);
                top:0; left:0;
                bottom:0; right:0;
                z-index: 100;

            }
            .btn-blue, .btn-blue:hover, .btn-blue:active, .btn-blue:visited {
                background-color: #469bc0 !important;
                color: white !important;
            }
            .btn-green, .btn-green:hover, .btn-green:active, .btn-green:visited {
                background-color: #4db117 !important;
                color: white !important;
            }
            .btn-orange, .btn-orange:hover, .btn-orange:active, .btn-orange:visited {
                background-color: #ef8929 !important;
                color: white !important;
            }
            .btn-red, .btn-red:hover, .btn-red:active, .btn-red:visited {
                background-color: #ef292d !important;
                color: white !important;
            }
            .btn-gray, .btn-gray:hover, .btn-gray:active, .btn-gray:visited {
                background-color: #696969 !important;
                color: white !important;
            }


        </style>
    </head>
    <body>
      
        <br/><br/>
        <div  class="container">
            <div class="page-header">
                <h1 class="text-center">Report Hazard</h1>      
            </div>
        </div>
        <hr/>
        <div class="alert alert-warning container">
          
                <strong>Please fill out the form below to submit your Hazard Report. All fields marked * are required. You can find all Hazard Reports for your site(s) on
                    Your Site Hazards tab on Home screen.</strong>
          

            <br/>

        </div>
        <div class="alert alert-info container">
            <span >Data Protection: Please ensure all personal data is handled with care and not shared with any unauthorised person.  </span>
        </div>
        <form action="" method="post">
           <ul class="nav nav-tabs container" role="tablist" id="myTabs">
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" id="hazardTab" href="#hazardReport"><span style="font-size: large;font-weight: bold">Hazard Details</span></a>
        </li>
    </ul>

            <div class="container tab-content">
                <div id="hazardReport" class="container tab-pane" style="background-color: whitesmoke ">
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Site <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="hazardSite" required onchange="populateApprovers(this)">
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.site where id='.$siteId);
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['code'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-1 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red">*</span></label>
                                <br/>
                                <select class="custom-select" id="hazardLocation" required>
                                    <?php
                                    include ('../config/phpConfig.php');
                                    if (mysqli_connect_errno()) {
                                        echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
                                    }
                                    $result = mysqli_query($con, 'SELECT * FROM ' . $mDbName . '.LOCATION;');
                                    echo "<option value=-1></option>";
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                    }
                                    echo '';
                                    mysqli_close($con);
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Date & Time (DD/MM/YYYY)<span style="color: red">*</span></label>
                                <div class="input-group date" id="hazardDateTime" data-target-input="nearest">
                                    <input class="form-control datetimepicker-input" type="text" id="hazardDateTime"  data-target="#hazardDateTime" required autocomplete="off"/>
                                    <div class="input-group-append" data-target="#hazardDateTime" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Day of the week</label>
                                <input type="text" class="form-control" id="hazardDay" maxlength="255" readonly/>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-2 col-lg-6">
                            <div class="form-group">
                                <label class="control-label">Please specify exact location</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" id="exactLocation" maxlength="255"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <hr/>
                    <div class="row">
                        <div class="col-md-2 col-lg-12">
                            <div class="form-group">
                                <label class="control-label">Hazard Description<span style="color: red">*</span></label>
                                <div class="input-group">

                                    <textarea rows="3" class="form-control" id="hazardDescription" maxlength="1000" ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Attach Photographs/Sketches 
                                <span style="font-size: small;font-style: italic">(You can attach multiple files at the same time. Total of the files size should not exceed 10MB)</span>
                                <span style="font-size: medium;font-style:normal;font-weight: bold">(ONLY image files to be uploaded. (.jpg, .png, .jpeg, .gif, .bmp, .tif)</span>
                            </label>
                            <br/>
                            <input class="form-control" type="file" accept=".jpg, .png, .jpeg, .gif, .bmp, .tif" multiple="true" id="attachPhotos">

                        </div>

                        

                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-md-3 col-lg-5">

                            <label class="control-label">Have you done anything to make it safe? (Challenged an unsafe act or safely rectified an unsafe condition)<span style="color: red">*</span></label>
                            <select class="custom-select" id="makeItSafe" onchange="changeWhatWhyLabel()">
                                <option value="-1"></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-7">
                            <label class="control-label" id="lblWhatWhy">What?<span style="color: red">*</span></label>
                            <textarea rows="2" id='makeItSafeWhyWhat' class="form-control" maxlength="500"></textarea>
                        </div>

                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-3 col-lg-5">

                            <label class="control-label">Were there any witnesses?<span style="color: red">*</span></label>
                            <select class="custom-select" id="anyWitness">
                                <option value="-1"></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-lg-7">
                            <label class="control-label">Who?</label>
                            <textarea rows="2" id='anyWitnessWho' class="form-control" maxlength="255"></textarea>
                        </div>

                    </div>
                    <br/>
                    <div class="row">

                        <div class="col-md-3 col-lg-6">
                            <label class="control-label">Do you have improvement suggestion?</label>
                            <textarea rows="5" id='improvementSuggestion' class="form-control" maxlength="1000"></textarea>
                        </div>
                        <div class="col-md-3 col-lg-6">
                            <label class="control-label">Your Details</label>
                            <br/>
                            <label class="control-label">Your Name (Optional)</label>
                            <input class="form-control" type="text" id="reporterName" maxlength="255"/>
                            <label class="control-label">Your Department (Optional)</label>
                            <input class="form-control" type="text" id="reporterDepartment" maxlength="255"/>


                        </div>

                    </div>
                </div>

              

            </div>
            <br/>
            <div class="container" style="background-color: whitesmoke">
                <div class = "row">
                    <div class = "col-md-4">
                        <label class = "control-label">Submit to... <span style = "color: red">*</span></label>

                        <select class = "custom-select " id = "approver">
                            <?php
                            echo "<option value='-1'></option>";
                            foreach ($approvers as $value) {
                                echo '<option value=' . $value['user_id'] . '>' . $value['first_name'] . ' ' . $value['last_name'] . '</option>';
                            }
                            echo '';
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div id ="approverValidationError" class="showError alert alert-danger approverValidationError" style="display: none"><strong>Please select an approver.</Strong></div>
                </div>

              
                    <div class="row" >
                        <div class="col-lg-12">
                            <div style="margin-top: 2rem">
                                <div class="pull-right">
                                    <input href="#"  class="btn btn-orange" id="btnDiscard" type="button" value="DISCARD" onclick="discardChanges()"></input>
                                    <input href="#"  class="btn btn-green" id="btnSubmit" type="button" value="SUBMIT" onclick="validate('NEW')"></input>
                                </div>
                            </div>
                        </div>
                    </div>

             
            </div>
            <br />
            <div class="modal fade" id="promptModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
                        </div>
                        <br>
                        <div class="modal-body">
                            <div class="col-md-3 col-lg-12">
                                <div class="form-group">
                                    <label id="promptMessage">Please add comments:</label>
                                    </br>
                                    <textarea class="form-control" rows="2" maxlength="500" id="promptComments"></textarea>
                                    <input type="hidden" id="promptAction"/>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" >
                            <button type="button" class="btn btn-green" id="bYes"  data-dismiss="modal">OK</button>
                            <button type="button" class="btn btn-gray" id="bNo"  data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
      
    </body>
    <script>
        $(document).ready(function () {
            var action = 'NEW';
            var actionOwners = '';
            if (action === 'NEW') {
                $('#hazardTab').trigger('click');

                //Initialize datetime picker
                
                $("#hazardDateTime").datetimepicker({
                    format: 'DD-MM-YYYY HH:mm:ss',
                    locale: moment.locale('en', {week: {dow: 1}}),
                    maxDate: moment()
                });
                
                $("#hazardDateTime").on("change.datetimepicker", function (e) {
                    if (e.date !== undefined) {
                        document.getElementById('hazardDay').value = getDay(e.date);
                    } else {
                        document.getElementById('hazardDay').value = '';
                    }
                });
                document.getElementById('hazardDay').value = getDay(new Date());
            } 
        });

        function populateApprovers(elem) {

            var approverMasterList = <?php echo json_encode($approvers) ?>;
            var containmentSite = elem.value;
            var approvers = '';
            var approverSelect = document.getElementById('approver');
            approverSelect.options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            approverSelect.add(option);
            for (var i = 0; i < approverMasterList.length; i++) {
                var site = approverMasterList[i];
                if (site.id === containmentSite) {
                    approvers = site.approvers;
                    break;
                }
            }
            for (var i = 0; i < approvers.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", approvers[i].user_id);
                //  newOption.setAttribute("data-divid", approvers[i].manager_name);
                newOption.innerHTML = approvers[i].first_name + ' ' + approvers[i].last_name;
                approverSelect.add(newOption);
            }
        }
function populateOwnersForCounterMeasure(elem) {

            var ownersMasterList = '<?php echo json_encode($actionOwners)?>';
            var containmentSite = elem.val();
            var owners = '';
            var ownerList = $(elem).closest("div").next().find("select");
            ownerList[0].options.length = 0;
            var option = document.createElement('option');
            option.setAttribute("value", "-1");
            ownerList[0].add(option);
            for (var i = 0; i < ownersMasterList.length; i++) {
                var site = ownersMasterList[i];
                if (site.id === containmentSite) {
                    owners = site.owners;
                    break;
                }
            }
            for (var i = 0; i < owners.length; i++) {
                var newOption = document.createElement('option');
                newOption.setAttribute("value", owners[i].id);
                newOption.setAttribute("data-divid", owners[i].manager_name);
                newOption.innerHTML = owners[i].first_name + ' ' + owners[i].last_name;
                ownerList[0].add(newOption);
            }
        }
        function populateCounterMeasureManager(elem) {

            $(elem).closest("div").next().find("input")[0].value = $(elem).find('option:selected').attr('data-divid') === undefined ? '' : $(elem).find('option:selected').attr('data-divid');
        }
        
        function validate(action) {

                var valid = true;
                var dateFormats = [moment.ISO_8601, "DD/MM/YYYY"];
                var currentDate = new Date();
                currentDate.setHours(0, 0, 0, 0);
                var hazardDate = moment($('#hazardDateTime').data('date'), 'DD-MM-YYYY').format(moment.HTML5_FMT.DATE);
                $('#counterMeasureRequiredError').css('display', 'none');

                if (document.getElementById('hazardSite').value === '-1' || document.getElementById('hazardLocation').value === '-1'
                        || $('#hazardDateTime').data('date') === undefined || document.getElementById('hazardDescription').value === ''
                        || document.getElementById('makeItSafe').value === '-1' || document.getElementById('anyWitness').value === '-1') {
                    alert("Please enter all the fields marked required.");

                    valid = false;
                } else if (document.getElementById('makeItSafe').value === 'Yes' && document.getElementById('makeItSafeWhyWhat').value === '') {
                    alert("Please enter What you did to make it safe.");
                    valid = false;
                } else if (document.getElementById('makeItSafe').value === 'No' && document.getElementById('makeItSafeWhyWhat').value === '') {
                    alert("Please enter Why haven't you done anything to make it safe.");
                    valid = false;
                } else if (document.getElementById('anyWitness').value === 'Yes' && document.getElementById('anyWitnessWho').value === '') {
                    alert("Please enter the details of the witnesses");
                    valid = false;
                } else if (action === 'NEW' && document.getElementById('approver').value === '-1') {
                    alert("Please select who you want to submit the hazard report to.");
                    valid = false;
                }
                if (valid === false) {
                    $('#hazardTab').trigger('click');
                    return valid;
                }
                if (valid) {
                    if (confirm("Are you sure you want to submit this Hazard Report?")) {
                        submitHazard(action);
                    } else {
                        valid = false;
                    }
                }
                return valid;
            }
            
            function submitHazard(submitAction) {

            var siteId = document.getElementById("hazardSite").value;
            var filter = submitAction;

            var locationElement = document.getElementById("hazardLocation");
            var location = locationElement.options [locationElement.selectedIndex].text;

            var hazardDateTime = moment($('#hazardDateTime').data('date'), 'DD-MM-YYYY HH:mm:ss').toJSON();
            var locationOther = document.getElementById("exactLocation").value;
            var hazardDescription = document.getElementById("hazardDescription").value;


            var makeItSafe = document.getElementById("makeItSafe").value;
            var makeItSafeWhyWhat = document.getElementById("makeItSafeWhyWhat").value;
            var anyWitness = document.getElementById("anyWitness").value;
            var anyWitnessWho = document.getElementById("anyWitnessWho").value;
            var improvementSuggestion = document.getElementById("improvementSuggestion").value;
            var reporterName = document.getElementById("reporterName").value;
            var reporterDepartment = document.getElementById("reporterDepartment").value;
            var ipAddress = '<?php echo $_SERVER['REMOTE_ADDR']?>';
                

            var createdBy, updatedBy = '';
            var requestorId = <?php echo $token[0]['user_id']?>


            var action = 'NEW';
            var hazardRequest = '';
            if (submitAction === 'NEW') {
                var approver = document.getElementById("approver").value;
                //ONLY hazard report fields
                hazardRequest = {"siteId": siteId, "location": location, "hazardDateTime": hazardDateTime, "locationOther": locationOther,
                    "hazardDescription": hazardDescription, "makeItSafe": makeItSafe, "makeItSafeWhyWhat": makeItSafeWhyWhat,
                    "anyWitness": anyWitness, "witnessName": anyWitnessWho, "improvementSuggestions": improvementSuggestion,
                    "reporterName": reporterName, "reporterDepartment": reporterDepartment, "currApproverId": approver, "userId": requestorId, "filter": filter, "ipAddress":ipAddress};
            }

            //var jsonReq = encodeURIComponent(hazardRequest);
            console.log(hazardRequest);
            var form_data = new FormData();
            form_data.append('filter', JSON.stringify(hazardRequest));
            form_data.append('function', 'saveHazardSecure');
            form_data.append('connection', vsmsservice);


            $.ajax({
                url: "../action/callPostService.php",
                type: 'POST',
                processData: false,
                cache: false,
                contentType: false,
                data: form_data,
                success: function (response, textstatus) {
                    console.log(response);
                    if (response.startsWith("OK")) {
                        var res = response.split("-");
                        
                        sendHazardEmails(submitAction, res[1]);
                        uploadHazardFiles(res[1]);
                        $("#overlay").fadeIn("slow");
                        var delay = 5000;
                        setTimeout(function ()
                        {
                            $("#overlay").fadeOut("fast");
                            $('#container').fadeIn();
                            alert('Hazard Data Saved');
                            window.location.reload();
                            
                        }, delay);


                    } else {
                        alert("Something went wrong.Please submit again");
                    }
                }});
        }
        
        function sendHazardEmails(action, hid) {
            var func, type = "";

            if (action === 'NEW') {
                func = "HazardSubmitted";
                type = "Both";
            } 
            $.ajax({
                type: "GET",
                url: "../screens/sendEmails.php?hazardId=" + hid + "&function=" + func + "&type=" + type,
                success: function (data) {
                    console.log(data);
                }
            });

        }
        function uploadHazardFiles(hid) {
         var photoFiles = $('#attachPhotos')[0].files;
         callAjaxForUpload(hid,'Photos', photoFiles );

       }
       function callAjaxForUpload(hid, fileType, fileList){
    
            for (var x = 0; x < fileList.length; x++) {
                var file_data = fileList[x];
                var form_data = new FormData();
                form_data.append('file', file_data);
                form_data.append('hid', hid);
                form_data.append('type', fileType);
                $.ajax({
                    url: '../action/uploadFile.php',
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    async: false,
                    success: function (response) {
                        console.log(x + " number File uploaded for " + fileType);
                    }
                });
            }
    
        }
      
function getDay(date) {
    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var n = weekday[new Date(date).getDay()];
    return n;
}
        
   

    </script>
</html>
